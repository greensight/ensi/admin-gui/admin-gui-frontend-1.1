# ADMIN GUI FRONTEND 1.1

**ADMIN GUI FRONTEND 1.1** Интерфейс управления ENSI. Работает на RWBP + GDS

## Usage

Удостоверьтесь, что версии `node` и `yarn` на вашей машине соответствуют версиям из поля `engines` `package.json`.
Управление версиями `node` можно проиводить через [nvm](https://github.com/nvm-sh/nvm) / [nvm-windows](https://github.com/coreybutler/nvm-windows), `yarn` обновляется через скачивание установщика с [их сайта](https://yarnpkg.com/en/docs/install#windows-stable).

❗️️ Для установки зависимостей на проекте используется пакетный менеджер `yarn` - все зависимости ставятся через него, и сохраняются в `package.json`. `npm` параллельно с `yarn` не использовать. Файл `yarn.lock` лежит в репозитории и хранит информацию по загружаемым пакетам.

Установите зависимости через `yarn`:

```
yarn
```

Чтобы у вас работали ESLint/Prettier/Typescript на уровне IDE, необходимо запустить команду:

```bash
yarn pnpify --sdk vscode
```

❗️️ В зависимости от проекта может потребоваться также задать переменные окружения. Для этого необходимо создать файл `.env`. Доступные переменные описаны в `.env.example`, дефолтные значения в `.env.defaults`.

❗️️ В случае наличия интеграции токенов из Figma (обязательно при использовании иных шрифтов), необходимо добавить импорт токенов в тему `theme.ts` - https://prnt.sc/wope77, и добавить локально шрифты с соответствующими им переменными `fontStyles.ts`

После этого можно использовать команды из списка:

| `yarn <command>`    | Description                                            |
| ------------------- | ------------------------------------------------------ |
| `dev`               | Build app in dev mode and start development server     |
| `dev:measure`       | Start a dev mode with SMP for build time debugging     |
| `eslint`            | Lint all `.js`, `.jsx`, `.ts` and `.tsx` files         |
| `lint-staged`       | `lint-staged` added to scripts to use with `run-p`     |
| `prod`              | Build app in prod mode and start production server     |
| `prod:build`        | Build app in prod mode                                 |
| `prod:build:client` | Build client bundle in prod mode. Used in `prod:build` |
| `prod:build:server` | Build server bundle in prod mode. Used in `prod:build` |
| `prod:measure`      | Start a prod mode with SMP for build time debugging    |
| `prod:tsc`          | Start a prod mode with SMP for build time debugging    |
| `prod:serve`        | Start production server                                |
| `storybook`         | Start a Storybook in dev mode                          |
| `storybook:build`   | Build static Storybook version in `docs` folder        |
| `storybook:serve`   | Run static server to view `docs` folder                |
| `tokens`            | Update tokens from Figma \*                            |
| `tokens:dev`        | Обновляет токены Figma из development проекта          |
| `tsc`               | Run Typescript compiler (noEmit)                       |
| `test`              | Run Jest                                               |
| `test:coverage`     | Run Jest in coverage mode                              |
| `test:watch`        | Run Jest in watch mode                                 |

### Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
