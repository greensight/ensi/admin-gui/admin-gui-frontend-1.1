export interface Products {
    items: {
        id: number;
        name: string;
        code: string;
        price: {
            value: string;
            currency: 'RUB';
        };
        image: string;
    }[];
}

export async function getProducts(subcategoryCode: string) {
    const response = await mockProducts(subcategoryCode);
    return response;
}

const mockProducts = async (subcategoryCode: string) => {
    let mockData: Products | null = null;
    if (subcategoryCode === 'subcategory-1') {
        mockData = {
            items: [
                {
                    id: 1,
                    name: 'Товар 1',
                    code: 'product-1',
                    price: { value: '111.99', currency: 'RUB' },
                    image: 'https://via.placeholder.com/150',
                },
                {
                    id: 2,
                    name: 'Товар 2',
                    code: 'product-2',
                    price: { value: '222.99', currency: 'RUB' },
                    image: 'https://via.placeholder.com/150',
                },
                {
                    id: 3,
                    name: 'Товар 3',
                    code: 'product-3',
                    price: { value: '333.99', currency: 'RUB' },
                    image: 'https://via.placeholder.com/150',
                },
            ],
        };
    } else if (subcategoryCode === 'subcategory-2') {
        mockData = {
            items: [
                {
                    id: 4,
                    name: 'Товар 4',
                    code: 'product-4',
                    price: { value: '444.99', currency: 'RUB' },
                    image: 'https://via.placeholder.com/150',
                },
                {
                    id: 5,
                    name: 'Товар 5',
                    code: 'product-5',
                    price: { value: '555.99', currency: 'RUB' },
                    image: 'https://via.placeholder.com/150',
                },
                {
                    id: 6,
                    name: 'Товар 6',
                    code: 'product-6',
                    price: { value: '666.99', currency: 'RUB' },
                    image: 'https://via.placeholder.com/150',
                },
            ],
        };
    } else if (subcategoryCode === 'subcategory-3') {
        mockData = {
            items: [
                {
                    id: 7,
                    name: 'Товар 7',
                    code: 'product-7',
                    price: { value: '777.99', currency: 'RUB' },
                    image: 'https://via.placeholder.com/150',
                },
                {
                    id: 8,
                    name: 'Товар 8',
                    code: 'product-8',
                    price: { value: '888.99', currency: 'RUB' },
                    image: 'https://via.placeholder.com/150',
                },
                {
                    id: 9,
                    name: 'Товар 9',
                    code: 'product-9',
                    price: { value: '999.99', currency: 'RUB' },
                    image: 'https://via.placeholder.com/150',
                },
            ],
        };
    }
    const response = await Promise.resolve({ data: mockData });
    return response;
};
