export interface Seo {
    title: string;
    description?: string;
    keywords?: string;
    canonical?: string;
}

export async function getSeo(page: string) {
    const response = await mockSeo(page);
    return response;
}

const mockSeo = async (page: string) => {
    let mockData: Seo | null = null;
    const pathname = new URL(page).pathname;
    if (pathname.includes('example')) {
        mockData = {
            title: '{название раздела}',
            description: '{название раздела} в проекте RWBP - возьми себе и захвати другу!',
            keywords: '{название раздела}',
        };
    } else {
        mockData = {
            title: 'RWBP',
            description:
                'Проект RWBP доставляет качественные React проекты по всей стране - возьми себе и захвати другу!',
            keywords: 'React, webpack',
        };
    }
    const response = await Promise.resolve({ data: mockData });
    return response;
};
