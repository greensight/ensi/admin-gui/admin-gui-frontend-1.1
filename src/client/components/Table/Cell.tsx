import React from 'react';
import { format } from 'date-fns';
import { Link, useLocation } from 'react-router-dom';
import Badge from '@components/Badge';
import Picture from '@standart/Picture';
import { convertPrice } from '@scripts/helpers';
import noImage from '@images/simple/noimage.png';
import { CELL_TYPES } from '@scripts/enums';
import { useTheme, scale, Button } from '@greensight/gds';
import getStatusType from '@scripts/getStatusType';
import DragIcon from '@svg/tokens/small/dragAndDrop.svg';

interface CellProps {
    text?: string;
    type?: string;
    items?: any[];
}

const Cell = ({ text, type, items }: CellProps) => {
    const { colors } = useTheme();
    const { pathname } = useLocation();

    if (type === CELL_TYPES.PHOTO) {
        return <Picture css={{ width: scale(6), height: scale(6), maxWidth: 'none' }} image={text || noImage} alt="" />;
    }

    if (type === CELL_TYPES.DOT)
        return (
            <div css={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: scale(5, true) }}>
                <div
                    css={{
                        width: 5,
                        height: 5,
                        borderRadius: '50%',
                        backgroundColor: text ? colors?.primary : 'transparent',
                    }}
                />
            </div>
        );

    if (typeof text !== 'number' && !text) return <p>-</p>;

    if (!type) return <p>{text}</p>;

    if (type === CELL_TYPES.LINKED_ID) {
        return (
            <Link to={`${pathname}/${text}`} css={{ color: colors?.primary }}>
                {text}
            </Link>
        );
    }

    if (type === CELL_TYPES.LINK) {
        const [name, to] = text;
        return (
            <Link to={to} css={{ color: colors?.primary }}>
                {name}
            </Link>
        );
    }

    if (type === CELL_TYPES.DOUBLE) {
        const [title, descr] = text;

        return (
            <div>
                <p>{title}</p> <p css={{ color: colors?.grey800, whiteSpace: 'pre' }}>{descr}</p>
            </div>
        );
    }

    if (type === CELL_TYPES.LINK_WITH_TEXT) {
        const [linkText, to, descr] = text;

        return (
            <div>
                <Link to={to} css={{ color: colors?.primary }}>
                    {linkText}
                </Link>
                <p css={{ color: colors?.grey800, whiteSpace: 'pre' }}>{descr}</p>
            </div>
        );
    }

    if (type === CELL_TYPES.STATUS) return <Badge text={text} type={getStatusType(text)} />;

    if (type === CELL_TYPES.PRICE) {
        const [rub, penny] = text.toString().split('.');
        return <span css={{ whiteSpace: 'nowrap' }}>{convertPrice(rub, penny)}</span>;
    }

    if (type === CELL_TYPES.DATE) return <p>{format(new Date(text), 'dd.MM.yyyy')}</p>;

    if (type === CELL_TYPES.DATE_TIME) return <p>{format(new Date(text), 'dd.MM.yyyy H:m')}</p>;

    if (type === CELL_TYPES.DATE_RANGE) {
        const [from, to] = text;
        return (
            <p>
                c {format(new Date(from), 'dd.MM.yyyy')} по {format(new Date(to), 'dd.MM.yyyy')}
            </p>
        );
    }
    if (type === CELL_TYPES.DRAG) {
        return (
            <Button size="sm" theme="ghost" Icon={DragIcon} hidden css={{ '&:hover': { cursor: 'grab' } }}>
                Драг
            </Button>
        );
    }

    if (type === CELL_TYPES.ARRAY)
        return (
            <>
                {Array.isArray(text) &&
                    text.map((item, index) => (
                        <React.Fragment key={index}>
                            {item ? <p css={{ marginTop: index > 0 ? scale(1) : 0 }}>{item}</p> : null}
                        </React.Fragment>
                    ))}
            </>
        );

    return <p>-</p>;
};

export default Cell;
