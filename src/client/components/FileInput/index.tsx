import React, { useState, useRef } from 'react';
import { scale, useTheme, Button, VisuallyHidden } from '@greensight/gds';
import TrashIcon from '@svg/tokens/small/trash.svg';
import FileAttach from '@svg/tokens/small/fileAttach.svg';

interface ValidationProp {
    type?: {
        value: string[];
        error: string;
    };
    size?: {
        value: number;
        error: string;
    };
}

export interface FileInput extends React.HTMLProps<HTMLInputElement> {
    /** on change handler */
    onChangeHandler?: (file: File | null) => void;
    /** on error handler */
    onSetError?: (error: string) => void;
    /** validation object */
    validation?: ValidationProp;
    /** default uploaded file */
    defaultFile?: File | null;
    /** class name */
    className?: string;
}

const FileInput = ({
    id,
    onChangeHandler,
    children,
    name,
    className,
    validation,
    onSetError,
    defaultFile = null,
    ...props
}: FileInput) => {
    const { colors } = useTheme();
    const fieldId = id || `${name}`;
    const [file, setFile] = useState<File | null>(defaultFile);
    const fileRef = useRef<HTMLInputElement | null>(null);

    const onChange = (file: File | null) => {
        if (file) {
            if (validation) {
                const isTypeValidation = validation.type ? validation.type.value.some(t => t === file.type) : true;
                const isSizeValidation = validation.size ? validation.size.value > file.size : true;
                if (!isTypeValidation || !isSizeValidation) {
                    if (validation.type?.error && onSetError) {
                        onSetError(validation.type?.error);
                    }
                    return;
                }
            }
            setFile(file);
            if (onChangeHandler) onChangeHandler(file);
        }
    };

    return (
        <div css={{ display: 'flex', alignItems: 'center' }} className={className}>
            {!file ? (
                <>
                    <VisuallyHidden>
                        <input
                            type="file"
                            id={fieldId}
                            name={name}
                            onChange={e => {
                                if (e.currentTarget.files?.length) onChange(e.currentTarget.files[0]);
                            }}
                            ref={fileRef}
                            {...props}
                        />
                    </VisuallyHidden>
                    <label
                        htmlFor={fieldId}
                        css={{
                            cursor: 'pointer',
                            'input[data-focus-visible-added] + &': {
                                outline: `2px solid ${colors?.brandSecond} !important`,
                                outlineOffset: '2px',
                            },
                        }}
                    >
                        <Button
                            type="button"
                            Icon={FileAttach}
                            theme="ghost"
                            size="sm"
                            hidden
                            css={{ pointerEvents: 'none' }}
                        >
                            Прикрепить
                        </Button>
                        Прикрепить файл
                    </label>
                    {children}
                </>
            ) : (
                <>
                    <span css={{ marginRight: scale(1, true) }}>{file?.name}</span>
                    <Button
                        type="button"
                        onClick={() => onChange(null)}
                        Icon={TrashIcon}
                        theme="ghost"
                        size="sm"
                        hidden
                    >
                        Удалить
                    </Button>
                </>
            )}
        </div>
    );
};

export default FileInput;
