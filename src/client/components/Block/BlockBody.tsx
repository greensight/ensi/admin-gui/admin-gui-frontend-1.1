import React from 'react';
import { scale } from '@greensight/gds';

export interface BlockBodyProps {
    className?: string;
    children?: React.ReactNode;
}

const BlockBody = ({ className, children }: BlockBodyProps) => {
    return (
        <div
            className={className}
            css={{
                padding: `${scale(2)}px ${scale(3)}px`,
            }}
        >
            {children}
        </div>
    );
};

export default BlockBody;
