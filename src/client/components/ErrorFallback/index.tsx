import React from 'react';
import { scale, useTheme } from '@greensight/gds';
import typography from '@scripts/typography';

function ErrorFallback({ error, resetErrorBoundary }: any) {
    const { colors } = useTheme();

    return (
        <div role="alert" css={{ padding: scale(3), display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <p css={{ ...typography('h1'), marginBottom: scale(3) }}>Что-то пошло не так</p>
            <pre css={{ color: colors?.danger, marginBottom: scale(3) }}>{error.message}</pre>
        </div>
    );
}

export default ErrorFallback;
