import React from 'react';
import { Global } from '@emotion/core';
import { useTheme, scale } from '@greensight/gds';
import typography from '@scripts/typography';
import 'filepond/dist/filepond.min.css';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';

const FilePondStyles = () => {
    const { colors } = useTheme();
    return (
        <Global
            styles={{
                '.file-input': {
                    fontFamily: 'inherit',

                    '.filepond--drop-label': {
                        marginBottom: scale(3),
                        color: colors?.grey400,
                        border: `1px dashed ${colors?.grey700}`,
                        borderRadius: scale(1),
                    },

                    '.filepond--label-action': {
                        textDecoration: 'none',
                        color: colors?.grey900,

                        ' &:hover': {
                            color: colors?.primary,
                        },
                    },

                    '.filepond--panel-root': {
                        background: 'none',
                    },

                    '.filepond--item-panel': {
                        borderRadius: scale(1),
                        background: 'none',
                        boxShadow: 'none',
                        padding: 0,
                        paddingBottom: scale(1),
                    },

                    '[data-filepond-item-state="processing-complete"] .filepond--item-panel ': {
                        backgroundColor: colors?.grey400,
                    },

                    '.filepond--drip-blob': {
                        backgroundColor: colors?.grey400,
                    },

                    '.filepond--file': {
                        color: colors?.white,
                    },

                    '.filepond--list': {
                        top: scale(4),
                        left: 0,
                        right: 0,
                    },

                    '.filepond--item': {
                        marginLeft: 0,
                        marginRight: 0,

                        '.image-preview&': {
                            width: `calc(33% - ${scale(3, true)}px)`,
                            marginBottom: scale(2),
                            marginRight: scale(2),

                            '&:nth-of-type(3n)': {
                                marginRight: 0,
                            },
                        },
                    },
                    '.filepond--file-info': {
                        paddingLeft: scale(1),
                        flexDirection: 'row',
                        alignItems: 'center',
                        alignSelf: 'center',
                        ...typography('bodySm'),
                        '.image-preview&': { background: colors?.white, alignSelf: 'flex-start' },
                    },
                    '.filepond--file-info-main': {
                        fontSize: '16px',
                        color: colors?.grey900,
                    },
                    '.filepond--file-info-sub': {
                        fontSize: '14px',
                        color: colors?.grey900,
                    },
                    '.filepond--item > .filepond--panel .filepond--panel-bottom ': {
                        boxShadow: 'none',
                    },
                    '.filepond--file-info .filepond--file-info-main': {
                        width: 'auto',
                        marginRight: scale(1),
                    },

                    '.filepond--file-action-button': {
                        color: colors?.grey900,
                        backgroundColor: colors?.white,
                        cursor: 'pointer',

                        '&:hover': {
                            boxShadow: `0 0 0 0.125em ${colors?.danger}`,
                        },
                    },

                    '[data-filepond-item-state*="error"] .filepond--item-panel,[data-filepond-item-state*="invalid"] .filepond--item-panel': {
                        background: 'none',
                    },
                    '.filepond--file-status-main': {
                        fontSize: '12px',
                        color: colors?.grey900,
                    },
                    '[data-filepond-item-state="processing-error"] .filepond--file-status-main': {
                        color: colors?.danger,
                    },
                    '[data-filepond-item-state="load-invalid"] .filepond--file-status-main': {
                        color: colors?.danger,
                    },
                    '.filepond--file-status-sub': {
                        fontSize: '10px',
                        color: colors?.grey900,
                        opacity: 1,
                    },
                    '.filepond--progress-indicator': {
                        color: colors?.grey900,
                        marginTop: 0,

                        '&::before, &::after': {
                            position: 'absolute',
                            top: '50%',
                            left: '50%',
                            height: '2px',
                            width: '9px',
                            content: "''",
                            background: colors?.grey900,
                        },

                        '&::before': {
                            transform: 'translate(-50%, -50%) rotate(45deg)',
                        },

                        '&::after': {
                            transform: ' translate(-50%, -50%) rotate(-45deg)',
                        },
                    },
                    '.filepond--file-status': {
                        padding: `0 ${scale(1)}px`,

                        '.image-preview&': {
                            paddingTop: scale(1),
                            paddingBottom: scale(1),
                            background: colors?.white,
                        },
                    },
                },
            }}
        />
    );
};

export default FilePondStyles;
