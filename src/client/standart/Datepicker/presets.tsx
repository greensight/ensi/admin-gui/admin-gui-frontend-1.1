import React from 'react';
import { Global } from '@emotion/core';
import { useTheme, scale } from '@greensight/gds';
import typography from '@scripts/typography';
import { registerLocale } from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import ru from 'date-fns/locale/ru';

registerLocale('ru', ru);

const DatepickerStyles = () => {
    const { colors } = useTheme();
    return (
        <Global
            styles={{
                '.react-datepicker': {
                    '&-wrapper': { display: 'block !important' },
                    '&__input-container': {
                        display: 'block !important',
                        input: {
                            width: '100%',
                            height: scale(7, true),
                            padding: `0 ${scale(1)}px`,
                            ...typography('bodySm'),
                            ':disabled': { backgroundColor: colors?.grey200, color: colors?.grey800 },
                        },
                    },
                    '&__current-month': { textTransform: 'capitalize' },
                    '&__month': {
                        '&-option': {
                            textTransform: 'capitalize',
                            textAlign: 'left',
                            padding: `0 ${scale(1)}px`,
                            '&--selected': { left: 'unset', right: scale(2) },
                        },
                        '&-read-view': {
                            '&--selected-month': { textTransform: 'capitalize' },
                        },
                    },
                    '&__day': {
                        borderRadius: 0,
                        margin: '0 0 2px 0',
                        '&:hover': { backgroundColor: colors?.grey400 },

                        '&-name': { margin: 0 },

                        '&--selected': {
                            backgroundColor: colors?.primary,
                            borderRadius: 5,
                            color: colors?.white,
                            '&:hover': { color: colors?.grey900 },
                        },
                        '&--in-selecting-range': {
                            backgroundColor: colors?.grey200,
                            color: colors?.grey900,
                            '&:hover': { backgroundColor: colors?.grey400 },
                        },
                        '&--in-range': { backgroundColor: colors?.grey200, color: colors?.grey900 },
                        '&--range-end': { backgroundColor: colors?.grey400 },
                    },
                    '&-popper': {
                        zIndex: 3,
                    },
                },
            }}
        />
    );
};

export default DatepickerStyles;
