import React from 'react';
import { FastField, FastFieldProps } from 'formik';
import Legend from '@standart/Legend';
import { FormikProps } from '@standart/Form';
import BasicField from './BasicField';
import { FormFieldProps } from './Field';

export const FormFastField = ({
    name,
    children,
    label,
    success,
    isLegend = false,
    hint,
    Icon,
    iconCss,
    className,
    ...props
}: FormFieldProps) => {
    const inputProps = {
        type: 'text',
        ...props,
    };

    return (
        <div css={{ width: '100%' }} className={className}>
            <FastField name={name}>
                {({ field, meta, form }: FastFieldProps) => {
                    return children ? (
                        <>
                            {React.Children.map(children, child => {
                                if (React.isValidElement(child)) {
                                    const formikProps: FormikProps<any> = {
                                        field,
                                        meta,
                                        helpers: form.getFieldHelpers(name),
                                        name,
                                        id: (child?.type as React.FC)?.displayName !== 'Legend' ? name : '',
                                        ...inputProps,
                                        ...child.props,
                                    };
                                    return (
                                        <>
                                            {((isLegend && meta) || label) && (
                                                <Legend
                                                    name={name}
                                                    meta={meta}
                                                    label={label}
                                                    success={success}
                                                    hint={hint}
                                                />
                                            )}
                                            {React.cloneElement(child, { ...formikProps })}
                                        </>
                                    );
                                }
                            })}
                        </>
                    ) : (
                        <BasicField
                            label={label}
                            success={success}
                            isLegend={isLegend}
                            hint={hint}
                            Icon={Icon}
                            iconCss={iconCss}
                            field={field}
                            meta={meta}
                            name={name}
                            {...inputProps}
                            {...props}
                        />
                    );
                }}
            </FastField>
        </div>
    );
};

export default FormFastField;
