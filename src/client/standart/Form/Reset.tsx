import React from 'react';
import { Button } from '@greensight/gds';
import { useFormikContext, FormikValues } from 'formik';

type ButtonType = typeof Button;

interface ButtonProps extends ButtonType {
    children: React.ReactNode;
    /** simple onClick handler */
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    /** initial values to reset formik statu to */
    initialValues?: FormikValues;
}

export const FormReset = ({ children, onClick, initialValues, ...props }: ButtonProps) => {
    const { resetForm } = useFormikContext();

    return (
        <Button
            onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
                if (onClick) onClick(e);
                resetForm({ values: initialValues });
            }}
            {...props}
        >
            {children}
        </Button>
    );
};

export default FormReset;
