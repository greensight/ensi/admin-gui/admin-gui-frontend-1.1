import React from 'react';
import { useField } from 'formik';
import Legend from '@standart/Legend';
import { FormikProps } from '@standart/Form';
import { CSSObject } from '@emotion/core';
import BasicField from './BasicField';
export interface FormFieldProps extends Omit<React.HTMLProps<HTMLInputElement>, 'label'> {
    /** Input name */
    name: string;
    /** Label for Legend */
    label?: string | React.ReactNode;
    /** success */
    success?: string;
    /** legend visible flag */
    isLegend?: boolean;
    /** hint */
    hint?: string;
    /** Icon */
    Icon?: SVGRIcon;
    /** icon styles */
    iconCss?: CSSObject;
    /** class name */
    className?: string;
    /** Label under field */
    bottomTooltip?: React.ReactNode;
}

export const FormField = ({
    name,
    children,
    label,
    success,
    isLegend = false,
    hint,
    Icon,
    iconCss,
    className,
    bottomTooltip,
    ...props
}: FormFieldProps) => {
    const [field, meta, helpers] = useField(name);

    const inputProps = {
        type: 'text',
        name,
        ...props,
    };

    return (
        <div css={{ width: '100%' }} className={className}>
            {children ? (
                <>
                    {React.Children.map(children, child => {
                        if (React.isValidElement(child)) {
                            const formikProps: FormikProps<any> = {
                                field,
                                meta,
                                helpers,
                                id: (child?.type as React.FC)?.displayName !== 'Legend' ? name : '',
                                ...inputProps,
                                ...child.props,
                            };
                            return (
                                <>
                                    {((isLegend && meta) || label) && (
                                        <Legend name={name} meta={meta} label={label} success={success} hint={hint} />
                                    )}
                                    {React.cloneElement(child, { ...formikProps })}
                                </>
                            );
                        }
                    })}
                </>
            ) : (
                <BasicField
                    label={label}
                    success={success}
                    isLegend={isLegend}
                    hint={hint}
                    Icon={Icon}
                    iconCss={iconCss}
                    field={field}
                    meta={meta}
                    bottomTooltip={bottomTooltip}
                    {...inputProps}
                    {...props}
                />
            )}
        </div>
    );
};

export default FormField;
