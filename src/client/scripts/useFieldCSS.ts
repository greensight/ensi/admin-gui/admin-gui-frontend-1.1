import { useMemo } from 'react';
import { CSSObject } from '@emotion/core';
import { useTheme, scale } from '@greensight/gds';
import typography from '@scripts/typography';
import { FieldMetaProps } from 'formik';

const useFieldCSS = (meta?: FieldMetaProps<any>) => {
    const { colors } = useTheme();

    const basicFieldCSS: CSSObject = useMemo<CSSObject>(
        () => ({
            width: '100%',
            padding: `${scale(1, true)}px ${scale(1)}px`,
            border: `1px solid ${meta?.touched && meta?.error ? colors?.danger : colors?.grey400}`,
            borderRadius: 2,
            ...typography('bodySm'),
            ':focus': { outline: 'none', borderColor: colors?.primary },
            ':disabled': {
                background: colors?.grey200,
                border: `1px solid ${colors?.grey400}`,
                color: colors?.grey600,
            },
            placeholder: { color: colors?.grey800 },
        }),
        [
            meta?.touched,
            meta?.error,
            colors?.danger,
            colors?.grey400,
            colors?.primary,
            colors?.grey200,
            colors?.grey600,
            colors?.grey800,
        ]
    );

    return { basicFieldCSS };
};

export default useFieldCSS;
