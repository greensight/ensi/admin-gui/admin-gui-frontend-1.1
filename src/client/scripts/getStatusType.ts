import { STATUSES } from './enums';

export default (status: string) => {
    let type: STATUSES;
    switch (status.toLowerCase().trim()) {
        case 'создан': {
            type = STATUSES.CREATED;
            break;
        }
        case 'в продаже':
        case 'действует':
        case 'активен':
        case 'Оформлен':
        case 'да':
        case 'доступен к продаже': {
            type = STATUSES.SUCCESS;
            break;
        }
        case 'нет':
        case 'отменён':
        case 'недоступен к продаже':
        case 'отключен': {
            type = STATUSES.ERROR;
            break;
        }
        case 'принят логистическим оператором':
        case 'приостановлен':
        case 'снято с продажи': {
            type = STATUSES.WARNING;
            break;
        }
        default: {
            type = STATUSES.REGULAR;
        }
    }

    return type;
};
