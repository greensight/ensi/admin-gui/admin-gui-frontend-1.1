import menuIcons from '@scripts/menuIcons';
import { nanoid } from 'nanoid';
import { MenuItemProps } from '@components/Sidebar';

const menu: MenuItemProps[] = [
    {
        text: 'Товары',
        Icon: menuIcons.package,
        subMenu: [
            {
                text: 'Каталог товаров',
                link: '/products/catalog',
            },
            {
                text: 'Предложения продавцов',
                link: '/products/offers',
            },
            {
                text: 'Товарные группы',
                link: '/products/variant-groups',
            },
            {
                text: 'Справочники',
                subMenu: [
                    { text: 'Бренды', link: '/products/directories/brands' },
                    { text: 'Категории', link: '/products/directories/categories' },
                    { text: 'Товарные атрибуты', link: '/products/directories/properties' },
                ],
            },
        ],
    },
    {
        text: 'Заказы',
        Icon: menuIcons.cart,
        subMenu: [
            { text: 'Список  заказов', link: '/orders/list' },
            { text: 'Грузы', link: '/orders/cargos' },
            { text: 'Статусы заказов', link: '/orders/statuses' },
        ],
    },
    {
        text: 'Заявки',
        Icon: menuIcons.trello,
        subMenu: [
            { text: 'Проверка товаров', link: '/requests/check' },
            { text: 'Производство  контента', link: '/requests/content' },
            { text: 'Изменение  цен', link: '/requests/prices' },
        ],
    },
    {
        text: 'Контент',
        Icon: menuIcons.image,
        subMenu: [
            { text: 'Меню сайта', link: '/content/menu' },
            { text: 'Управление страницами', link: '/content/landing' },
            { text: 'Управление контактами и соц. сетями', link: '/content/contacts' },
            { text: 'Управление категориями', link: '/content/categories' },
            { text: 'Подборки товаров', link: '/content/product-groups' },
            { text: 'Баннеры', link: '/content/banners' },
            { text: 'Товарные шильдики', link: '/content/badges' },
            { text: 'Поисковые запросы', link: '/content/search-requests' },
            { text: 'Поисковые синонимы', link: '/content/search-synonyms' },
            { text: 'Популярные бренды', link: '/content/popular-brands' },
            { text: 'Популярные товары', link: '/content/popular-products' },
        ],
    },
    {
        text: 'Логистика',
        Icon: menuIcons.truck,
        subMenu: [
            { text: 'Логистические операторы', link: '/logistic/delivery-services' },
            { text: 'Стоимость доставки по регионам', link: '/logistic/delivery-prices' },
            { text: 'Планировщик времени статусов', link: '/logistic/kpi' },
        ],
    },
    { text: 'Склады', link: '/stores/seller-stores', Icon: menuIcons.package },
    { text: 'Клиенты', link: '/customers', Icon: menuIcons.users },
    {
        text: 'Продавцы',
        Icon: menuIcons.award,
        subMenu: [
            { text: 'Заявка на регистрацию', link: '/seller/list/registration' },
            { text: 'Список продавцов', link: '/seller/list/active' },
        ],
    },
    {
        text: 'Маркетинг',
        Icon: menuIcons.birka,
        subMenu: [
            { text: 'Промокоды', link: '/marketing/promocodes' },
            { text: 'Скидки', link: '/marketing/discounts' },
            { text: 'Бандлы', link: '/marketing/bundles' },
        ],
    },
    { text: 'Отчеты', link: '/reports', Icon: menuIcons.chart },
    {
        text: 'Коммуникации',
        Icon: menuIcons.message,
        subMenu: [
            { text: 'Непрочитанные сообщения', link: '/communications/messages' },
            { text: 'Сервисные уведомления', link: '/communications/notifications' },
            { text: 'Массовая рассылка', link: '/communications/broadcast' },
            { text: 'Статусы', link: '/communications/statuses' },
            { text: 'Темы', link: '/communications/subjects' },
            { text: 'Типы', link: '/communications/types' },
        ],
    },
    {
        text: 'Настройки',
        Icon: menuIcons.settings,
        subMenu: [
            { text: 'Пользователи и права', link: '/settings/users' },
            { text: 'Карточка организации', link: '/settings/organizations' },
        ],
    },
];

const enrichMenu = (menu: MenuItemProps[]) => {
    const enrichedMenu = menu.slice();
    menu.forEach((item: any) => {
        item.id = nanoid(5);
        if (item.subMenu) enrichMenu(item.subMenu);
    });
    return enrichedMenu;
};

export default enrichMenu(menu);
