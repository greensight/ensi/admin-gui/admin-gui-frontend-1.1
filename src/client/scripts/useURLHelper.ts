import { useCallback, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import { FormikValues } from 'formik';
import { mergeURLSearchParams } from '@scripts/helpers';

/** берет search-параметры из урла и дополняет ими initialValues */
const useURLHelper = (emptyInitValues: FormikValues) => {
    const {
        push,
        location: { search },
    } = useHistory();

    const searchParams = useMemo(() => new URLSearchParams(search), [search]);

    /** вставляет значения полей формы в историю */
    const URLHelper = useCallback(
        (vals: FormikValues) => {
            const newURLSearchParams = new URLSearchParams();
            Object.keys(vals).forEach(k => {
                if (vals[k]) newURLSearchParams.append(k, JSON.stringify(vals[k]));
            });
            push(mergeURLSearchParams(searchParams, newURLSearchParams));
        },
        [push, searchParams]
    );

    const valuesFromUrl: FormikValues = {};
    for (const [key, value] of searchParams.entries()) {
        if (key !== 'page') {
            /** определяем дату по ключу. Обязательно наличие 'date' в ключе для даты*/
            const isDateField = /date/gim.test(key);
            const parsedValue = JSON.parse(value);
            let preparedValue = parsedValue;
            if (isDateField) {
                if (Array.isArray(parsedValue)) {
                    preparedValue = parsedValue.map(i => new Date(i));
                } else {
                    preparedValue = new Date(parsedValue);
                }
            }

            valuesFromUrl[key] = preparedValue;
        }
    }

    const initialValues: FormikValues = {
        ...emptyInitValues,
        ...valuesFromUrl,
    };
    return { initialValues, URLHelper };
};

export default useURLHelper;
