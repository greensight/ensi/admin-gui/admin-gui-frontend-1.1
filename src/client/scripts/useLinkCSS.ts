import { CSSObject } from '@emotion/core';
import { useTheme } from '@greensight/gds';

export type Link = 'primaryBlue' | 'primaryBlue' | 'secondaryBlue' | 'secondaryBlack';

const useLinkCSS = (type: Link = 'primaryBlue') => {
    const { colors } = useTheme();

    const styles: CSSObject = {
        color: type.toLocaleLowerCase().includes('blue') ? colors?.link : colors?.grey900,
        fill: 'currentColor',
        ':hover': { color: type.toLocaleLowerCase().includes('blue') ? colors?.primaryHover : colors?.link },
    };
    return styles;
};

export default useLinkCSS;
