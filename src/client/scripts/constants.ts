export const MAX_AGE_NEVER = Math.pow(2, 31) - 1;

export const MAX_STRING_SIZE = 40;

export const DAYS = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
