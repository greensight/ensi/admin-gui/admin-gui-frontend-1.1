export enum CELL_TYPES {
    PRICE = 'price',
    STATUS = 'status',
    DOUBLE = 'double',
    DATE = 'date',
    DATE_TIME = 'datetime',
    DATE_RANGE = 'dateRange',
    PHOTO = 'photo',
    QUANTITY = 'quantity',
    TIME = 'time',
    INTERVAL = 'interval',
    ARRAY = 'array',
    LINKED_ID = 'linkedID',
    LINK = 'link',
    LINK_WITH_TEXT = 'linkWithText',
    DOT = 'dot',
    DRAG = 'drag',
}

export enum STATUSES {
    SUCCESS = 'success',
    REGULAR = 'regular',
    CREATED = 'created',
    WARNING = 'warning',
    ERROR = 'error',
}

export enum ActionType {
    Edit = 'edit',
    Add = 'add',
    Close = 'close',
}
