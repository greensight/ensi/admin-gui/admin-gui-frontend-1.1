import { useHistory } from 'react-router-dom';
import { useState, useCallback, useMemo, useEffect } from 'react';
import { mergeURLSearchParams } from '@scripts/helpers';

/** useTabs - хук, который возвращает пропгеттер для компонента <Tabs />
 * @param {string} name - используй в случае нескольких отдельных компонентов табов
 */
export default (tabName = 'tab') => {
    const {
        push,
        location: { search },
    } = useHistory();
    const searchParams = useMemo(() => new URLSearchParams(search), [search]);
    const tabIndex = Number(searchParams.get(tabName)) || 0;
    const [index, setIndex] = useState(tabIndex);

    /** обновляем значение в useEffect, чтобы можно было пользоваться стрелками вперед/назад в браузере */
    useEffect(() => {
        if (tabIndex >= 0) setIndex(tabIndex);
    }, [tabIndex]);

    const getTabsProps = useCallback(
        () => ({
            onSelect: (index: number) => {
                const newURLSearchParams = new URLSearchParams([[tabName, index.toString()]]);
                /** для того, чтобы ?tab=0 не показывалось, удалим из обоих объектов */
                if (index === 0) {
                    searchParams.delete(tabName);
                    newURLSearchParams.delete(tabName);
                }
                push(mergeURLSearchParams(searchParams, newURLSearchParams));
            },
            selectedIndex: index,
        }),
        [index, push, searchParams, tabName]
    );

    return {
        getTabsProps,
    };
};
