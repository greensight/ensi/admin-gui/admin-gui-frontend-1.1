import React from 'react';
import { Redirect } from 'react-router-dom';

const Home = () => <Redirect to={'/products/catalog'} />;

export default Home;
