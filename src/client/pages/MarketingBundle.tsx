import React from 'react';
import { useParams } from 'react-router-dom';
import { scale, Layout, useTheme } from '@greensight/gds';
import { CSSObject } from '@emotion/core';

import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import typography from '@scripts/typography';

const data = [
    { name: 'id', value: '219' },
    { name: 'Дата создания	', value: '2021-05-25 12:02:55' },
    { name: 'Название', value: 'Заявка на бандл' },
    { name: 'Тип и размер скидки', value: 'В рублях, 23 руб.' },
    { name: 'Скидка на', value: 'Бандл из товаров' },
    { name: 'Период действия скидки', value: '2021-05-07 — 2021-05-13' },
    { name: 'Инициатор скидки', value: 'Stylers' },
    { name: 'Автор', value: 'Оболенский Игорь Яковлевич' },
    { name: 'Статус', value: 'Отправлена на согласование' },
];

const kpi = [
    { name: 'Сумма заказов с использованием скидки', value: '0 руб' },
    { name: 'Сумма, которое сэкономили покупатели', value: '0 руб' },
    { name: 'Количество пользователей, которые воспользовались скидкой', value: '0' },
    { name: 'Количество заказов со скидкой', value: '0' },
];

const MarketingBundle = () => {
    const { id } = useParams<{ id: string }>();
    const { colors } = useTheme();

    const dlStyles: CSSObject = {
        display: 'grid',
        gridTemplate: 'auto/1fr 1fr',
    };
    const dtStyles: CSSObject = {
        ...typography('captionBold'),
        borderBottom: `1px solid ${colors?.grey400}`,
        padding: `${scale(1)}px 0`,
    };

    const ddStyles: CSSObject = {
        borderBottom: `1px solid ${colors?.grey400}`,
        padding: `${scale(1)}px 0`,
    };

    return (
        <PageWrapper h1={`Информация о скидке #${id}`}>
            <Layout cols={2}>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Body>
                            <dl css={dlStyles}>
                                {data.map(i => (
                                    <React.Fragment key={i.name}>
                                        <dt css={dtStyles}>{i.name}</dt>
                                        <dd css={ddStyles}>{i.value}</dd>
                                    </React.Fragment>
                                ))}
                            </dl>
                        </Block.Body>
                    </Block>
                </Layout.Item>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Header>KPIs</Block.Header>
                        <Block.Body>
                            <dl css={dlStyles}>
                                {kpi.map(i => (
                                    <React.Fragment key={i.name}>
                                        <dt css={dtStyles}>{i.name}</dt>
                                        <dd css={ddStyles}>{i.value}</dd>
                                    </React.Fragment>
                                ))}
                            </dl>
                        </Block.Body>
                    </Block>
                </Layout.Item>
            </Layout>
        </PageWrapper>
    );
};

export default MarketingBundle;
