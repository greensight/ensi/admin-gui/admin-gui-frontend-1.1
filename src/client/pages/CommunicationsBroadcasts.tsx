import React from 'react';
import { useFormikContext } from 'formik';
import { scale, Button, Layout } from '@greensight/gds';
import Block from '@components/Block';
import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';
import Form from '@standart/Form';
import Select from '@standart/Select';
import MultiSelect from '@standart/MultiSelect';
import Textarea from '@standart/Textarea';
import typography from '@scripts/typography';
import Legend from '@standart/Legend';
import Switcher from '@standart/Switcher';
import PageWrapper from '@components/PageWrapper';
import { prepareForSelect } from '@scripts/helpers';
import { CHANNELS_FOR_SELECT } from '@scripts/data/different';

const roles = prepareForSelect([
    'Оператор продавца',
    'Администратор продавца',
    'Профессионал',
    'Реферальный партнер',
    'Неавторизованный пользователь',
]);

const sellers = prepareForSelect(['ООО "Рога и копыта"', 'Ашан', 'Леруа Мерлен', 'Б.Ю. Александров', 'М.П. Почтомат']);

const users = prepareForSelect(['Дмитрий', 'Василий', 'Роман', 'Владилен', 'Иосиф', 'Илья', 'Мартин', 'Иван']);

const statuses = prepareForSelect(['Открыта', 'Закрыта', 'Отправлено', 'Доставлено', 'В работе', 'Выполнено']);
const types = prepareForSelect(['Тип 1', 'Тип 2', 'Тип 3', 'Тип 4', 'Тип 5', 'Тип 6']);

const UsersSelect = () => {
    const {
        values: { addAllUsers },
    } = useFormikContext();
    return addAllUsers ? null : (
        <Form.Field name="users" label="Пользователи" disabled={addAllUsers} css={{ marginTop: scale(2) }}>
            <MultiSelect options={users} />
        </Form.Field>
    );
};

const CommunicationsBroadcasts = () => {
    return (
        <PageWrapper h1={'Массовая рассылка'}>
            <>
                <FilePondStyles />
                <Block>
                    <Block.Body>
                        <Form
                            initialValues={{
                                role: '',
                                seller: [],
                                channel: '',
                                users: [],
                                addAllUsers: false,
                                theme: '',
                                status: '',
                                types: '',
                                message: '',
                            }}
                            onSubmit={val => console.log(val)}
                        >
                            <Layout cols={1} css={{ maxWidth: scale(128) }}>
                                <Layout.Item col={1}>
                                    <Form.FastField name="role" label="Роли пользователей">
                                        <MultiSelect options={roles} />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="seller" label="Продавец">
                                        <MultiSelect options={sellers} isMulti={false} />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="channel" label="Канал">
                                        <Select items={CHANNELS_FOR_SELECT} />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="addAllUsers">
                                        <Switcher>Добавить всех пользователей</Switcher>
                                    </Form.FastField>
                                    <UsersSelect />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="theme" label="Тема" />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="status" label="Статус">
                                        <Select items={statuses} />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="type" label="Тип">
                                        <Select items={types} />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.FastField name="message">
                                        <Legend label={<p css={typography('h3')}>Сообщение</p>} />
                                        <Textarea />
                                    </Form.FastField>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <hr />
                                </Layout.Item>
                                <Layout.Item col={1} css={{ marginBottom: scale(3) }}>
                                    <p css={{ ...typography('h3'), marginBottom: scale(2) }}>Файлы</p>
                                    <FilePond maxFileSize="10MB" maxTotalFileSize="100MB" />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.Reset theme="outline" size="sm">
                                        Очистить
                                    </Form.Reset>
                                    <Button size="sm" css={{ marginLeft: scale(2) }}>
                                        Создать чат
                                    </Button>
                                </Layout.Item>
                            </Layout>
                        </Form>
                    </Block.Body>
                </Block>
            </>
        </PageWrapper>
    );
};

export default CommunicationsBroadcasts;
