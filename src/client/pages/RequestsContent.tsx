import React, { useState, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { Button, scale, useTheme, Layout } from '@greensight/gds';

import Form from '@standart/Form';
import Pagination from '@standart/Pagination';
import Popup from '@standart/Popup';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';
import MultiSelect from '@standart/MultiSelect';
import Table from '@components/Table';
import Block from '@components/Block';

import { STATUSES } from '@scripts/data/different';
import { CELL_TYPES } from '@scripts/enums';
import typography from '@scripts/typography';
import { sellers, getRandomItem } from '@scripts/mock';

const popupColumns = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Продавец',
        accessor: 'seller',
        getProps: () => ({ type: CELL_TYPES.LINK }),
    },
];

const statuses = STATUSES.map(i => ({ label: i, value: i }));
const SELLERS = sellers.map(i => ({ label: i, value: i }));

const types = [
    { value: 'text', label: 'Текстовое описаниe' },
    { value: 'photo_video', label: 'Фото/видео съёмка' },
    { value: 'text_photo', label: 'Съемка и текст' },
];

const shootingTypes = [
    { value: 'autopsy', label: 'Со вскрытием' },
    { value: 'no_autopsy', label: 'Без вскрытия' },
];

const newRequestsContent = (id: number) => {
    return {
        id: `100${id}`,
        type: getRandomItem(types.map(type => type.label)),
        seller: [getRandomItem(sellers), `/seller/100${id}`],
        status: getRandomItem(STATUSES),
        productsCount: getRandomItem([15, 32, 64, 71, 86, 43, 25, 13]),
        shootingType: getRandomItem([...shootingTypes.map(type => type.label), null]),
        author: getRandomItem([
            ['Овчинников С. Б.', 'test@test.com'],
            ['Резинкин П. П.', 'rezinkin@ya.ru'],
        ]),
        created: new Date(),
    };
};
const makeRequestsContent = (len: number) => [...Array(len).keys()].map(el => newRequestsContent(el));

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: CELL_TYPES.LINKED_ID }),
    },
    {
        Header: 'Тип',
        accessor: 'type',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: CELL_TYPES.STATUS }),
    },
    {
        Header: 'Продавец',
        accessor: 'seller',
        getProps: () => ({ type: CELL_TYPES.LINK }),
    },
    {
        Header: 'Кол-во товаров',
        accessor: 'productsCount',
    },
    {
        Header: 'Тип съемки',
        accessor: 'shootingType',
    },
    {
        Header: 'Автор',
        accessor: 'author',
        getProps: () => ({ type: CELL_TYPES.DOUBLE }),
    },
    {
        Header: 'Создана',
        accessor: 'created',
        getProps: () => ({ type: CELL_TYPES.DATE_TIME }),
    },
];

const RequestsContent = () => {
    const { colors } = useTheme();
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeRequestsContent(10), []);

    const [moreFilters, setMoreFilters] = useState(true);
    const [isAddRequestOpen, setIsAddRequestOpen] = useState(false);
    const [isChangeRequestOpen, setIsChangeRequestOpen] = useState(false);
    const [isDeleteRequestOpen, setIsDeleteRequestOpen] = useState(false);
    const [ids, setIds] = useState<number[]>([]);

    const onRowSelect = (ids: number[]) => setIds(ids);

    const popupTableData = useMemo(() => {
        const rowsData: any = [];
        if (ids.length > 0) {
            ids.forEach(id => {
                rowsData.push(data[id]);
            });
        }
        return rowsData;
    }, [data, ids]);

    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>Заявки на производство контента</h1>

            <DatepickerStyles />
            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        offerId: '',
                        status: [],
                        type: [],
                        seller: [],
                        shootingType: null,
                        author: '',
                        createDate: [],
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Body>
                        <Layout cols={8}>
                            <Layout.Item col={2}>
                                <Form.Field name="offerId" label="ID заявки" />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="type" label="Тип заявки">
                                    <MultiSelect options={types} isMulti={false} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="status" label="Статус заявки">
                                    <MultiSelect options={statuses} isMulti={false} />
                                </Form.Field>
                            </Layout.Item>
                            {moreFilters ? (
                                <>
                                    <Layout.Item col={2}>
                                        <Form.Field name="seller" label="Продавец">
                                            <MultiSelect options={types} isMulti={false} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="shootingType" label="Тип съёмки">
                                            <MultiSelect options={shootingTypes} isMulti={false} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="author" label="Автор" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="createDate" label="Дата создания">
                                            <DatepickerRange />
                                        </Form.Field>
                                    </Layout.Item>
                                </>
                            ) : null}
                        </Layout>
                    </Block.Body>
                    <Block.Footer>
                        <div css={typography('bodySm')}>
                            Найдено 135 предложений{' '}
                            <button
                                type="button"
                                css={{ color: colors?.primary, marginLeft: scale(2) }}
                                onClick={() => setMoreFilters(!moreFilters)}
                            >
                                {moreFilters ? 'Меньше' : 'Больше'} фильтров
                            </button>{' '}
                        </div>
                        <div>
                            <Form.Reset
                                size="sm"
                                theme="secondary"
                                type="button"
                                onClick={() => {
                                    console.log('hoh');
                                }}
                            >
                                Сбросить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Footer>
                </Form>
            </Block>

            <Block>
                <Block.Header>
                    <div>
                        <Button
                            theme="primary"
                            size="sm"
                            css={{ marginRight: scale(2) }}
                            onClick={() => setIsAddRequestOpen(true)}
                        >
                            Создать заявку
                        </Button>
                        {ids.length !== 0 ? (
                            <>
                                <Button
                                    theme="primary"
                                    size="sm"
                                    onClick={() => setIsDeleteRequestOpen(true)}
                                    css={{
                                        backgroundColor: colors?.danger,
                                        '&:hover': {
                                            backgroundColor: colors?.danger,
                                        },
                                        marginRight: scale(2),
                                    }}
                                >
                                    Удалить {ids.length > 1 ? 'заявки' : 'заявку'}
                                </Button>
                                <Button
                                    theme="outline"
                                    size="sm"
                                    css={{ marginRight: scale(2) }}
                                    onClick={() => setIsChangeRequestOpen(true)}
                                >
                                    Изменить статус {ids.length > 1 ? 'заявок' : 'заявки'}
                                </Button>
                            </>
                        ) : null}
                        {ids.length === 1 ? (
                            <Button theme="secondary" size="sm" css={{ marginRight: scale(2) }}>
                                Скачать документы
                            </Button>
                        ) : null}
                    </div>
                </Block.Header>
                <Block.Body>
                    <Table
                        columns={COLUMNS}
                        data={data}
                        handleRowInfo={() => console.log('click')}
                        onRowSelect={onRowSelect}
                    />
                    <Pagination url={pathname} activePage={activePage} pages={7} />
                </Block.Body>
            </Block>

            <Popup
                isOpen={isAddRequestOpen}
                onRequestClose={() => {
                    setIsAddRequestOpen(false);
                }}
                title="Создание заявки на производство контента"
                popupCss={{ minWidth: scale(50) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        addRequestSeller: '',
                        addRequestIDs: '',
                        addRequestType: '',
                    }}
                >
                    <Form.Field label="Продавец" name="addRequestSeller" css={{ marginBottom: scale(2) }}>
                        <MultiSelect options={SELLERS} isMulti={false} />
                    </Form.Field>
                    <Form.Field
                        label="ID товаров"
                        placeholder="ID товаров через запятую"
                        name="addRequestIDs"
                        type="number"
                        css={{ marginBottom: scale(2) }}
                    />
                    <Form.Field
                        name="addRequestType"
                        label="Тип производства контента"
                        css={{ marginBottom: scale(2) }}
                    >
                        <MultiSelect options={types} isMulti={false} />
                    </Form.Field>

                    <Button type="submit" size="sm" theme="primary">
                        Сохранить
                    </Button>
                </Form>
            </Popup>
            <Popup
                isOpen={isChangeRequestOpen}
                onRequestClose={() => {
                    setIsChangeRequestOpen(false);
                }}
                title={`Редактировать статус заяв${ids.length === 1 ? 'ки' : 'ок'}`}
                popupCss={{ minWidth: scale(60) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        changedStatus: null,
                    }}
                >
                    <Table
                        columns={popupColumns}
                        data={popupTableData}
                        needCheckboxesCol={false}
                        needSettingsColumn={false}
                        css={{ marginBottom: scale(2) }}
                    />
                    <Form.Field name="changedStatus" label="Статус" css={{ marginBottom: scale(2) }}>
                        <MultiSelect options={statuses} isMulti={false} />
                    </Form.Field>

                    <Button type="submit" size="sm" theme="primary">
                        Сохранить
                    </Button>
                </Form>
            </Popup>
            <Popup
                isOpen={isDeleteRequestOpen}
                onRequestClose={() => {
                    setIsDeleteRequestOpen(false);
                }}
                title={`Удалить заяв${ids.length === 1 ? 'ки' : 'ку'}`}
                popupCss={{ minWidth: scale(60) }}
            >
                <Form
                    initialValues={{}}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Table
                        columns={popupColumns}
                        data={popupTableData}
                        needCheckboxesCol={false}
                        needSettingsColumn={false}
                        css={{ marginBottom: scale(2) }}
                    />
                    <Button
                        type="submit"
                        size="sm"
                        theme="primary"
                        css={{
                            backgroundColor: colors?.danger,
                            '&:hover': {
                                backgroundColor: colors?.danger,
                            },
                            marginRight: scale(2),
                        }}
                    >
                        Удалить
                    </Button>
                </Form>
            </Popup>
        </main>
    );
};

export default RequestsContent;
