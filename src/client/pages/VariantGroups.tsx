import React, { useState, useMemo } from 'react';
import { Global, CSSObject } from '@emotion/core';
import { useLocation } from 'react-router-dom';
import PageWrapper from '@components/PageWrapper';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import { sellers } from '@scripts/mock';

import MultiSelect from '@standart/MultiSelect';
import Table from '@components/Table';
import Block from '@components/Block';

import Pagination from '@standart/Pagination';
import Form from '@standart/Form';
import Popup, { PopupProps } from '@standart/Popup';

import Datepicker from '@standart/Datepicker';

const variantGroups = [
    'Резинка для волос женская',
    'Стайлер',
    'Декоративная игрушка',
    'Подушка',
    'Карандаш',
    'Бутылка',
    'Мячик',
    'Шарик',
    'Ноутбук',
];

const characteristics = ['Механизмы запирания/отпирания канала ствола', 'Размер стайлера'];

const categories = [
    'Резинки для волос',
    'Стайлеры',
    'Ножницы',
    'Шампуни',
    'Мебель',
    'Профессиональные шампуни',
    'Восстанавливающие шампуни',
    'Тест',
    'Шампуни без парабенов',
    'Тестовая категория',
    'Товары для дома',
    'Текстиль для дома',
];

const brands = ['Vikki&Lilli', 'Ikoo', 'Rowenta', "L'oreal"];

const SELLERS = sellers.map(i => ({ label: i, value: i }));
const CATEGORIES = categories.map(i => ({ label: i, value: i }));
const BRANDS = brands.map(i => ({ label: i, value: i }));

const getRandomItem = (arr: any[]) => arr[Math.floor(Math.random() * arr.length)];

const variantGroupTableItem = (id: number) => {
    return {
        id: `100${id}`,
        title: getRandomItem(variantGroups),
        seller: getRandomItem(sellers),
        characteristics: characteristics,
        products: 'Нет товаров',
        created: new Date(),
        edited: new Date().setDate(1),
    };
};

const makeOffers = (len: number) => [...Array(len).keys()].map(el => variantGroupTableItem(el));

const VariantGroupFilter = () => {
    const { colors } = useTheme();
    const [moreFilters, setMoreFilters] = useState(true);

    return (
        <Block css={{ marginBottom: scale(2) }}>
            <Form
                initialValues={{
                    id: '',
                    seller: [],
                    code: '',
                    article: '',
                    brand: [],
                    category: [],
                    created: '',
                }}
                onSubmit={values => {
                    console.log(values);
                }}
            >
                <Block.Body>
                    <Layout cols={4}>
                        <Layout.Item col={1}>
                            <Form.Field name="id" label="ID" />
                        </Layout.Item>

                        <Layout.Item col={1}>
                            <Form.Field name="seller" label="Продавец">
                                <MultiSelect options={SELLERS} />
                            </Form.Field>
                        </Layout.Item>

                        <Layout.Item col={1}>
                            <Form.Field name="code" label="Код товара из ERP" />
                        </Layout.Item>

                        <Layout.Item col={1}>
                            <Form.Field name="article" label="Артикул" />
                        </Layout.Item>

                        {moreFilters && (
                            <Layout.Item col={4}>
                                <Layout cols={3}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="brand" label="Бренд">
                                            <MultiSelect options={BRANDS} />
                                        </Form.Field>
                                    </Layout.Item>

                                    <Layout.Item col={1}>
                                        <Form.Field name="category" label="Категория">
                                            <MultiSelect options={CATEGORIES} />
                                        </Form.Field>
                                    </Layout.Item>

                                    <Layout.Item col={1}>
                                        <Form.Field name="date" label="Дата создания" />
                                    </Layout.Item>
                                </Layout>
                            </Layout.Item>
                        )}
                    </Layout>
                </Block.Body>

                <Block.Footer>
                    <div css={typography('bodySm')}>
                        Всего товарных групп: 5.{' '}
                        <button
                            type="button"
                            css={{ color: colors?.primary, marginLeft: scale(2) }}
                            onClick={() => setMoreFilters(!moreFilters)}
                        >
                            {moreFilters ? 'Меньше' : 'Больше'} фильтров
                        </button>{' '}
                    </div>

                    <div>
                        <Form.Reset
                            size="sm"
                            theme="secondary"
                            type="button"
                            onClick={() => {
                                console.log('hoh');
                            }}
                        >
                            Сбросить
                        </Form.Reset>

                        <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                            Применить
                        </Button>
                    </div>
                </Block.Footer>
            </Form>
        </Block>
    );
};

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: 'Продавец',
        accessor: 'seller',
    },
    {
        Header: 'Характеристики',
        accessor: 'characteristics',
        getProps: () => ({ type: 'array' }),
    },
    {
        Header: 'Товары',
        accessor: 'products',
    },
    {
        Header: 'Дата создания',
        accessor: 'created',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Дата изменения',
        accessor: 'edited',
        getProps: () => ({ type: 'date' }),
    },
];

const VariantGroupTable = () => {
    const { colors } = useTheme();
    const [moreFilters, setMoreFilters] = useState(true);

    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeOffers(10), []);

    return (
        <Block>
            <Block.Body>
                <Table columns={COLUMNS} data={data} handleRowInfo={() => console.log('click')} />
                <Pagination url={pathname} activePage={activePage} pages={7} />
            </Block.Body>
        </Block>
    );
};

const VariantGroupsPopup = ({ children, ...props }: PopupProps) => {
    const popupCss: CSSObject = {
        padding: 0,
    };

    const modalCss: CSSObject = {
        '.variable-group-popup.popup-content': {
            overflowY: 'visible',
        },
    };

    return (
        <>
            <Global styles={modalCss} />
            <Popup {...props} className="variable-group-popup" popupCss={popupCss}>
                {children}
            </Popup>
        </>
    );
};

const VariantGroups = () => {
    const [isPopupOpen, setIsPopupOpen] = useState(false);

    return (
        <PageWrapper h1="Список товарных групп">
            <>
                <VariantGroupFilter />

                <Button css={{ marginBottom: scale(2) }} onClick={() => setIsPopupOpen(true)}>
                    Создать товарную группу
                </Button>

                <VariantGroupTable />

                <VariantGroupsPopup
                    isOpen={isPopupOpen}
                    onRequestClose={() => setIsPopupOpen(false)}
                    isCloseButton={true}
                    isFullscreen={false}
                >
                    <Block>
                        <Form
                            initialValues={{
                                id: '',
                                seller: [],
                            }}
                            onSubmit={values => {
                                console.log(values);
                            }}
                        >
                            <Block.Header>
                                <h3 css={{ ...typography('h3'), paddingRight: 20 }}>
                                    Добавление новой товарной группы
                                </h3>
                            </Block.Header>

                            <Block.Body>
                                <Layout cols={1}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="name" label="Название" />
                                    </Layout.Item>

                                    <Layout.Item col={1}>
                                        <Form.Field name="seller" label="Продавец">
                                            <MultiSelect options={SELLERS} />
                                        </Form.Field>
                                    </Layout.Item>
                                </Layout>
                            </Block.Body>

                            <Block.Footer>
                                <div
                                    css={{
                                        width: '100%',
                                        display: 'flex',
                                        justifyContent: 'flex-end',
                                    }}
                                >
                                    <Button onClick={() => setIsPopupOpen(false)}>Отмена</Button>
                                    <Button css={{ marginLeft: scale(2) }} onClick={() => setIsPopupOpen(false)}>
                                        Создать
                                    </Button>
                                </div>
                            </Block.Footer>
                        </Form>
                    </Block>
                </VariantGroupsPopup>
            </>
        </PageWrapper>
    );
};

export default VariantGroups;
