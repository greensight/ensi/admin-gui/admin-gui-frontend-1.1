import React from 'react';
import { Link } from 'react-router-dom';
import { Button, scale, useTheme } from '@greensight/gds';
import { CSSObject } from '@emotion/core';

import Form from '@standart/Form';
import Datepicker from '@standart/Datepicker';
import DatepickerStyles from '@standart/Datepicker/presets';
import Textarea from '@standart/Textarea';
import MultiSelect from '@standart/MultiSelect';

import Block from '@components/Block';

import { sellers as sellersMock } from '@scripts/mock';
import typography from '@scripts/typography';
import { prepareForSelect } from '@scripts/helpers';

const Information = () => {
    const { colors } = useTheme();

    // После добавления useListCSS нужно будет заменить на готовые стили
    const dlBaseStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'" };
    const dtBaseStyles: CSSObject = {
        padding: `${scale(1)}px ${scale(1)}px ${scale(1)}px 0 `,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };
    const ddBaseStyles: CSSObject = {
        padding: `${scale(1)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
    };

    return (
        <Form
            initialValues={{
                profession: '',
                gender: '',
                birthday: null,
                city: '',
                certificates: '',
                manager: '',
                comment: '',
            }}
            onSubmit={values => {
                console.log(values);
            }}
        >
            <Block>
                <Block.Header>
                    <h2>Основная информация</h2>
                    <div css={{ button: { marginLeft: scale(2) } }}>
                        <Form.Reset
                            size="sm"
                            theme="secondary"
                            type="button"
                            onClick={() => {
                                console.log('reset');
                            }}
                        >
                            Сбросить
                        </Form.Reset>
                        <Button size="sm" type="submit">
                            Сохранить
                        </Button>
                    </div>
                </Block.Header>
                <Block.Body>
                    <dl css={{ ...dlBaseStyles, gridTemplateColumns: '200px 1fr' }}>
                        <dt css={dtBaseStyles}>Профессиональная деятельность</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="profession">
                                <MultiSelect
                                    isMulti={false}
                                    options={prepareForSelect(['Физрук', 'Штурман', 'Горный проводник'])}
                                />
                            </Form.Field>
                        </dd>
                        <dt css={dtBaseStyles}>Пол</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="gender">
                                <MultiSelect
                                    isMulti={false}
                                    options={prepareForSelect(['Все', 'Мужской', 'Женский'])}
                                />
                            </Form.Field>
                        </dd>
                        <dt css={dtBaseStyles}>Дата рождения</dt>
                        <dd css={ddBaseStyles}>
                            <DatepickerStyles />
                            <Form.Field name="birthday">
                                <Datepicker />
                            </Form.Field>
                        </dd>
                        <dt css={dtBaseStyles}>Возрас</dt>
                        <dd css={ddBaseStyles}>22</dd>
                        <dt css={dtBaseStyles}>Город</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="city" />
                        </dd>
                        <dt css={dtBaseStyles}>Сумма покупок накопительным итогом</dt>
                        <dd css={ddBaseStyles}>
                            <Link to="/">1 545.00</Link>
                        </dd>
                        <dt css={dtBaseStyles}>Доступных бонусов</dt>
                        <dd css={ddBaseStyles}>0</dd>
                        <dt css={dtBaseStyles}>Сертификаты</dt>
                        <dd css={ddBaseStyles}>
                            -
                            <input name="certificates" type="file" css={{ marginLeft: scale(2) }} />
                        </dd>
                        <dt css={dtBaseStyles}>Персональный Менеджер</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="manager">
                                <MultiSelect isMulti={false} options={prepareForSelect(['-'])} />
                            </Form.Field>
                        </dd>
                        <dt css={dtBaseStyles}>Служебный комментарий</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="comment">
                                <Textarea rows={3} />
                            </Form.Field>
                        </dd>
                    </dl>
                </Block.Body>
            </Block>
        </Form>
    );
};

export default Information;
