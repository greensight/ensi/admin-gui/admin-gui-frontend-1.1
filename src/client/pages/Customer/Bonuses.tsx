import React from 'react';
import { CSSObject } from '@emotion/core';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { useLocation } from 'react-router-dom';

import Form from '@standart/Form';
import Pagination from '@standart/Pagination';
import MultiSelect from '@standart/MultiSelect';
import Textarea from '@standart/Textarea';

import Table from '@components/Table';
import Block from '@components/Block';
import DatepickerStyles from '@standart/Datepicker/presets';
import Datepicker from '@standart/Datepicker';

import typography from '@scripts/typography';
import { prepareForSelect } from '@scripts/helpers';
import { CELL_TYPES } from '@scripts/enums';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Дата создания',
        accessor: 'date',
        getProps: () => ({ type: CELL_TYPES.DATE }),
    },
    {
        Header: 'Название',
        accessor: 'name',
        getProps: () => ({ type: CELL_TYPES.LINK_WITH_TEXT }),
    },
    {
        Header: 'Инициатор',
        accessor: 'initiator',
    },
    {
        Header: 'Сумма',
        accessor: 'sum',
    },

    {
        Header: 'Статус',
        accessor: 'status',
    },
    {
        Header: 'Срок действия',
        accessor: 'validity',
    },
];

const Bonuses = () => {
    const { colors } = useTheme();

    const { pathname, search } = useLocation();

    const activePage = +(new URLSearchParams(search).get('page') || 1);

    // После добавления useListCSS нужно будет заменить на готовые стили
    const dlBaseStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'" };
    const dtBaseStyles: CSSObject = {
        padding: `${scale(1)}px ${scale(1)}px ${scale(1)}px 0 `,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };
    const ddBaseStyles: CSSObject = {
        padding: `${scale(1)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
    };

    return (
        <Block>
            <DatepickerStyles />
            <Form
                initialValues={{
                    name: '',
                    message: '',
                    status: '',
                    sum: '',
                    validity: '',
                }}
                onSubmit={values => {
                    console.log(values);
                }}
            >
                <Block.Header>
                    <h2>Фильтр</h2>
                    <div css={{ button: { marginLeft: scale(2) } }}>
                        <Form.Reset
                            size="sm"
                            theme="secondary"
                            type="button"
                            onClick={() => {
                                console.log('reset');
                            }}
                        >
                            Сбросить
                        </Form.Reset>
                        <Button size="sm" type="submit">
                            Создать бонус
                        </Button>
                    </div>
                </Block.Header>
                <Block.Body>
                    <Layout cols={4}>
                        <Layout.Item>
                            <Form.Field name="name" label="Название" />
                        </Layout.Item>
                        <Layout.Item>
                            <Form.Field name="sum" label="Сумма" />
                        </Layout.Item>
                        <Form.Field name="status" label="Статус">
                            <MultiSelect
                                isMulti={false}
                                options={prepareForSelect(['Активный', 'На удержании', 'Списание'])}
                            />
                        </Form.Field>

                        <Form.Field name="validity" label="Срок действия">
                            <Datepicker />
                        </Form.Field>
                        <Layout.Item col={4}>
                            <Form.Field name="message" label="Сообщение для клиента">
                                <Textarea />
                            </Form.Field>
                        </Layout.Item>
                    </Layout>
                </Block.Body>
            </Form>
            <Block.Header>
                <h2>Общая информация</h2>
            </Block.Header>
            <Block.Body>
                <dl css={{ ...dlBaseStyles, gridTemplateColumns: '200px 1fr' }}>
                    <dt css={dtBaseStyles}>Доступных бонусов</dt>
                    <dd css={ddBaseStyles}>1</dd>
                    <dt css={dtBaseStyles}>На удержании</dt>
                    <dd css={ddBaseStyles}>1</dd>
                    <dt css={dtBaseStyles}>Всего</dt>
                    <dd css={ddBaseStyles}>1</dd>
                </dl>
            </Block.Body>
            <Block.Body>
                <Table columns={COLUMNS} data={[]} />
                <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
            </Block.Body>
        </Block>
    );
};

export default Bonuses;
