import React from 'react';

import Block from '@components/Block';

const Subscriptions = () => {
    return (
        <Block>
            <Block.Header>
                <h2>Подписки пользователя</h2>
            </Block.Header>
            <Block.Body></Block.Body>
            <Block.Header>
                <h2>Периодичность уведомлений</h2>
            </Block.Header>
            <Block.Body></Block.Body>
            <Block.Header>
                <h2>Предпочитаемый способ связи</h2>
            </Block.Header>
            <Block.Body></Block.Body>
        </Block>
    );
};

export default Subscriptions;
