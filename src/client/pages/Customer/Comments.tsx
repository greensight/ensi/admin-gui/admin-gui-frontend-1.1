import React from 'react';
import { useLocation } from 'react-router-dom';
import { scale } from '@greensight/gds';

import Pagination from '@standart/Pagination';

import Table from '@components/Table';
import Block from '@components/Block';

const COLUMNS = [
    {
        Header: 'Отзыв на',
        accessor: 'CommentSubject',
    },
    {
        Header: 'Объект отзыва',
        accessor: 'cause',
    },
    {
        Header: 'Рейтинг',
        accessor: 'rating',
    },
    {
        Header: 'Соообщение',
        accessor: 'message',
    },
    {
        Header: 'Достоинства',
        accessor: 'dignity',
    },
    {
        Header: 'Недостатки',
        accessor: 'limitations',
    },
    {
        Header: 'Файлы',
        accessor: 'files',
    },
    {
        Header: 'Лайки/дизлайки',
        accessor: 'evaluation',
    },
    {
        Header: 'Создан',
        accessor: 'creationDate',
    },
];

const Comments = () => {
    const { pathname, search } = useLocation();

    const activePage = +(new URLSearchParams(search).get('page') || 1);

    return (
        <Block>
            <Block.Body>
                <Table columns={COLUMNS} data={[]} />
                <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
            </Block.Body>
        </Block>
    );
};

export default Comments;
