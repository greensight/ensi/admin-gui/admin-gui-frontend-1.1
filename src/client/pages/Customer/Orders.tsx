import React, { useMemo } from 'react';
import { scale, useTheme } from '@greensight/gds';

import Table from '@components/Table';
import Block from '@components/Block';

import { makeCustomerOrders } from '@scripts/mock';
import { CELL_TYPES } from '@scripts/enums';

import CloseIcon from '@svg/tokens/small/closedCircle.svg';
import CheckIcon from '@svg/tokens/small/checkCircle.svg';

const COLUMNS = [
    {
        Header: '№ заказа',
        accessor: 'id',
    },
    {
        Header: 'Дата оформления',
        accessor: 'registrationDate',
        getProps: () => ({ type: CELL_TYPES.DATE }),
    },
    {
        Header: 'Сумма',
        accessor: 'sum',
    },
    {
        Header: 'Оплата',
        accessor: 'paid',
    },
    {
        Header: 'Способ оплаты',
        accessor: 'paymentMethod',
    },
    {
        Header: 'Способ доставки',
        accessor: 'deliveryMethod',
    },
    {
        Header: 'Стоимость доставки',
        accessor: 'deliveryCost',
    },
    {
        Header: 'Тип доставки',
        accessor: 'deliveryType',
    },
    {
        Header: 'Количество доставок',
        accessor: 'deliveryQty',
    },
    {
        Header: 'Служба доставки',
        accessor: 'deliveryService',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Дата доставки',
        accessor: 'deliveryDate',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Комментарий',
        accessor: 'comment',
    },
    {
        Header: 'Дата последнего изменения',
        accessor: 'changeDate',
        getProps: () => ({ type: 'date' }),
    },
];

const Orders = () => {
    const { colors } = useTheme();
    const data = useMemo(
        () =>
            makeCustomerOrders(10).map(({ paid, ...props }) =>
                paid
                    ? { paid: <CheckIcon fill={colors?.success} />, ...props }
                    : { paid: <CloseIcon fill={colors?.danger} />, ...props }
            ),
        [colors?.danger, colors?.success]
    );

    return (
        <>
            <Block>
                <Block.Header>
                    <h2>Заказы</h2>
                </Block.Header>
                <Block.Body>
                    <Table
                        columns={COLUMNS}
                        data={data}
                        needCheckboxesCol={false}
                        needSettingsColumn
                        css={{ marginTop: scale(2) }}
                    />
                </Block.Body>
            </Block>
        </>
    );
};

export default Orders;
