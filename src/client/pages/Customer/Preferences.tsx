import React, { useState } from 'react';
import { CSSObject } from '@emotion/core';
import { Button, scale, useTheme } from '@greensight/gds';

import Form from '@standart/Form';
import CheckboxGroup from '@standart/CheckboxGroup';
import Checkbox from '@standart/Checkbox';
import Popup from '@standart/Popup';

import Block from '@components/Block';

import typography from '@scripts/typography';
import PlusIcon from '@svg/tokens/small/plus.svg';

interface DataItem {
    title: string;
    items: string[];
    selectedItems: string[];
}

const DATA = [
    {
        titleList: 'Личные предпочтения',
        list: [
            {
                title: 'Бренды',
                items: ['Vikki&Lilli', 'Rowenta', "L'oreal"],
                selectedItems: ['Vikki&Lilli', 'Rowenta', "L'oreal"],
            },
            { title: 'Категории', items: ['Vikki&Lilli', 'Rowenta', "L'oreal"], selectedItems: ['Rowenta'] },
        ],
    },
    {
        titleList: 'Профессиональные предпочтения',
        list: [
            { title: 'Бренды', items: ['Vikki&Lilli', 'Rowenta', "L'oreal"], selectedItems: ['Vikki&Lilli'] },
            { title: 'Категории', items: ['Vikki&Lilli', 'Rowenta', "L'oreal"], selectedItems: [] },
        ],
    },
];

const Preferences = () => {
    const { colors } = useTheme();

    const [isEditPreferencesPopup, setIsEditPreferencesPopup] = useState(false);
    const [dataItem, setDataItem] = useState<DataItem | null>(null);

    // После добавления useListCSS нужно будет заменить на готовые стили
    const dlBaseStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'" };
    const dtBaseStyles: CSSObject = {
        padding: `${scale(1)}px ${scale(1)}px ${scale(1)}px 0 `,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };
    const ddBaseStyles: CSSObject = {
        padding: `${scale(1)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
    };

    return (
        <>
            <Block>
                {DATA.map((group, index) => (
                    <div key={`group-${index}`}>
                        <Block.Header>
                            <h2>{group.titleList}</h2>
                        </Block.Header>
                        <Block.Body>
                            {group.list.map(({ title, items, selectedItems }, index) => (
                                <div
                                    key={`group-item-${index}`}
                                    css={{ ...dlBaseStyles, gridTemplateColumns: '200px 1fr' }}
                                >
                                    <dt css={dtBaseStyles}>{title}</dt>
                                    <dd css={ddBaseStyles}>
                                        {selectedItems.length && (
                                            <ul css={{ marginBottom: scale(1) }}>
                                                {selectedItems.map((item, index) => (
                                                    <li key={`selected-item-${index}`}>{item}</li>
                                                ))}
                                            </ul>
                                        )}
                                        <button
                                            onClick={() => {
                                                setDataItem({ title, items, selectedItems });
                                                setIsEditPreferencesPopup(true);
                                            }}
                                            css={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                color: colors?.primary,
                                                fill: colors?.primary,
                                                ':hover': {
                                                    color: colors?.primaryHover,
                                                    fill: colors?.primaryHover,
                                                },
                                            }}
                                        >
                                            {!selectedItems?.length ? <PlusIcon /> : null}
                                            {selectedItems?.length ? 'Редактировать' : 'Добавить'}
                                        </button>
                                    </dd>
                                </div>
                            ))}
                        </Block.Body>
                    </div>
                ))}
            </Block>
            <Popup
                isOpen={isEditPreferencesPopup}
                onRequestClose={() => setIsEditPreferencesPopup(false)}
                title={`Редактирование "${dataItem?.title}"`}
                popupCss={{ minWidth: scale(50) }}
            >
                <Form
                    initialValues={{
                        checkboxGroup: dataItem?.selectedItems,
                    }}
                    onSubmit={vals => {
                        console.log(vals);
                        setIsEditPreferencesPopup(false);
                        setDataItem(null);
                    }}
                >
                    <Form.Field name="checkboxGroup">
                        <CheckboxGroup>
                            {dataItem?.items.map((item, index) => (
                                <Checkbox key={`checkbox-${index}`} value={item}>
                                    {item}
                                </Checkbox>
                            ))}
                        </CheckboxGroup>
                    </Form.Field>

                    <div css={{ display: 'flex', marginTop: scale(1), justifyContent: 'flex-end' }}>
                        <Button
                            type="button"
                            size="sm"
                            theme="outline"
                            css={{ marginRight: scale(1) }}
                            onClick={() => {
                                setIsEditPreferencesPopup(false);
                                setDataItem(null);
                            }}
                        >
                            Отмена
                        </Button>
                        <Button type="submit" size="sm">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </>
    );
};

export default Preferences;
