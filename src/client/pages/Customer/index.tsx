import React from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { CSSObject } from '@emotion/core';

import Tabs from '@standart/Tabs';
import Popup from '@standart/Popup';
import Form from '@standart/Form';
import Select from '@standart/Select';
import Mask from '@standart/Mask';

import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';

import Information from './Information';
import Transactions from './Transactions';
import EducationalEvents from './EducationalEvents';
import Promotions from './Promotions';
import Presents from './Presents';
import Refunds from './Refunds';
import Certificates from './Certificates';
import Logs from './Logs';
import Segment from './Segment';
import Preferences from './Preferences';
import Orders from './Orders';
import Subscriptions from './Subscriptions';
import Bonuses from './Bonuses';
import Communication from './Communication';
import Comments from './Comments';

import { maskPhone } from '@scripts/mask';

import EditIcon from '@svg/tokens/small/edit.svg';

const Customer = () => {
    const { colors } = useTheme();
    const [isChangeStatusOpen, setIsChangeStatusOpen] = React.useState(false);

    const trStyles: CSSObject = {
        ':not(:last-of-type)': { borderBottom: `1px solid ${colors?.grey200}` },
    };

    const thStyles: CSSObject = {
        textAlign: 'left',
        padding: scale(1),
    };

    const tdStyles: CSSObject = {
        padding: scale(1),
    };

    return (
        <>
            <PageWrapper h1="Тестовый Ensi Клиент">
                <>
                    <Layout cols={2} gap={scale(2)} css={{ marginBottom: scale(2) }}>
                        <Layout.Item>
                            <Block>
                                <Block.Body css={{ display: 'flex' }}>
                                    <Form
                                        initialValues={{
                                            name: 'Тестовый',
                                            surname: 'Ensi',
                                            lastName: 'Клиент',
                                            email: '',
                                            phone: '',
                                        }}
                                        css={{ width: '100%' }}
                                        onSubmit={values => console.log(values)}
                                    >
                                        <table css={{ width: '100%', borderCollapse: 'collapse' }}>
                                            <thead>
                                                <tr css={{ borderBottom: `1px solid ${colors?.grey200}` }}>
                                                    <th css={thStyles}>Инфопанель</th>
                                                    <td
                                                        colSpan={3}
                                                        css={{
                                                            textAlign: 'right',
                                                            button: { marginRight: scale(1) },
                                                            ...tdStyles,
                                                        }}
                                                    >
                                                        <Button size="sm">Сохранить</Button>
                                                        <Form.Reset size="sm" theme="outline">
                                                            Отменить
                                                        </Form.Reset>
                                                        <Button size="sm">Изменить статус</Button>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr css={trStyles}>
                                                    <th css={thStyles}>Роль</th>
                                                    <td css={tdStyles} colSpan={3}>
                                                        Профессионал
                                                    </td>
                                                </tr>
                                                <tr css={trStyles}>
                                                    <th css={thStyles}>Статус</th>
                                                    <td css={tdStyles}>Активный</td>
                                                    <th css={thStyles}>Сегмент</th>
                                                    <td css={tdStyles}>-</td>
                                                </tr>
                                                <tr css={trStyles}>
                                                    <th css={thStyles}>ФИО</th>
                                                    <td css={tdStyles}>
                                                        <Form.Field name="name" />
                                                    </td>
                                                    <td css={tdStyles}>
                                                        <Form.Field name="surname" />
                                                    </td>
                                                    <td css={tdStyles}>
                                                        <Form.Field name="lastName" />
                                                    </td>
                                                </tr>
                                                <tr css={trStyles}>
                                                    <th css={thStyles}>ID</th>
                                                    <td css={tdStyles}>16</td>
                                                    <th css={thStyles}>Фото</th>
                                                    <td css={tdStyles}>
                                                        <input type="file" name="file" required />
                                                    </td>
                                                </tr>
                                                <tr css={trStyles}>
                                                    <th css={thStyles}>E-mail</th>
                                                    <td css={tdStyles}>
                                                        <Form.Field name="email" />
                                                    </td>
                                                    <th css={thStyles}>Телефон</th>
                                                    <td css={tdStyles}>
                                                        <Form.Field name="phone">
                                                            <Mask mask={maskPhone} />
                                                        </Form.Field>
                                                    </td>
                                                </tr>
                                                <tr css={trStyles}>
                                                    <th css={thStyles}>Соцсети</th>
                                                    <td css={tdStyles}>-</td>
                                                    <th css={thStyles}>Ссылка на портфолио</th>
                                                    <td css={tdStyles}>
                                                        -
                                                        <Button
                                                            Icon={EditIcon}
                                                            type="button"
                                                            css={{ marginLeft: scale(2) }}
                                                            size="sm"
                                                        >
                                                            Редактировать
                                                        </Button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </Form>
                                </Block.Body>
                            </Block>
                        </Layout.Item>
                        <Layout.Item>
                            <Block>
                                <Block.Body css={{ display: 'flex' }}>
                                    <table css={{ width: '100%', borderCollapse: 'collapse' }}>
                                        <tbody>
                                            <tr css={trStyles}>
                                                <th css={thStyles}>KPIs</th>
                                                <td css={tdStyles}>-</td>
                                            </tr>
                                            <tr css={trStyles}>
                                                <th css={thStyles}>Количество заказов</th>
                                                <td css={tdStyles}>1</td>
                                            </tr>
                                            <tr css={trStyles}>
                                                <th css={thStyles}>Сумма заказов накопительным итогом</th>
                                                <td css={tdStyles}>1 545.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </Block.Body>
                            </Block>
                        </Layout.Item>
                    </Layout>
                    <Tabs>
                        <Tabs.List>
                            <Tabs.Tab>Информация</Tabs.Tab>
                            <Tabs.Tab>Предпочтения</Tabs.Tab>
                            <Tabs.Tab>Подписки</Tabs.Tab>
                            <Tabs.Tab>Транзакции</Tabs.Tab>
                            <Tabs.Tab>Заказы</Tabs.Tab>
                            <Tabs.Tab>Образовательные события</Tabs.Tab>
                            <Tabs.Tab>Возвраты</Tabs.Tab>
                            <Tabs.Tab>Коммуникация</Tabs.Tab>
                            <Tabs.Tab>Отзывы</Tabs.Tab>
                            <Tabs.Tab>Промокоды и Скидки</Tabs.Tab>
                            <Tabs.Tab>Бонусы</Tabs.Tab>
                            <Tabs.Tab>Подарки</Tabs.Tab>
                            <Tabs.Tab>Сертификаты</Tabs.Tab>
                            <Tabs.Tab>Сегмент</Tabs.Tab>
                            <Tabs.Tab>Логи</Tabs.Tab>
                        </Tabs.List>

                        <Tabs.Panel>
                            <Information />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Preferences />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Subscriptions />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Transactions text={'Заглушка'} />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Orders />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <EducationalEvents text={'Заглушка'} />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Refunds text={'Заглушка'} />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Communication />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Comments />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Promotions text={'Заглушка'} />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Bonuses />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Presents text={'Заглушка'} />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Certificates text={'Заглушка'} />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Segment text={'Заглушка'} />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Logs text={'Заглушка'} />
                        </Tabs.Panel>
                    </Tabs>
                </>
            </PageWrapper>
            <Popup
                isOpen={isChangeStatusOpen}
                onRequestClose={() => setIsChangeStatusOpen(false)}
                title="Изменить статус"
                popupCss={{
                    maxWidth: 'initial',
                    width: scale(70),
                }}
            >
                <Form
                    onSubmit={values => console.log(values)}
                    initialValues={{
                        status: null,
                    }}
                >
                    <Form.Field name="status" label="Статус проверки" css={{ marginBottom: scale(2) }}>
                        <Select
                            items={[
                                { label: 'Согласовано', value: 'agreed' },
                                { label: 'Не согласовано', value: 'notAgreed' },
                                { label: 'Отправлено', value: 'sent' },
                                { label: 'На рассмотрении', value: 'inProcess' },
                                { label: 'Отклонено', value: 'rejected' },
                            ]}
                        ></Select>
                    </Form.Field>
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsChangeStatusOpen(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </>
    );
};

export default Customer;
