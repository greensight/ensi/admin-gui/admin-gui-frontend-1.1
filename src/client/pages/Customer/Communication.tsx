import React, { useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { Button, scale, Layout } from '@greensight/gds';

import Form from '@standart/Form';
import Pagination from '@standart/Pagination';
import MultiSelect from '@standart/MultiSelect';

import Table from '@components/Table';
import Block from '@components/Block';

import { prepareForSelect } from '@scripts/helpers';
import { makeCommunicationTheme } from '@scripts/mock';
import { CELL_TYPES } from '@scripts/enums';

const COLUMNS = [
    {
        Header: 'Тема',
        accessor: 'theme',
    },
    {
        Header: 'Пользователь',
        accessor: 'user',
        getProps: () => ({ type: CELL_TYPES.LINK_WITH_TEXT }),
    },
    {
        Header: 'Канал',
        accessor: 'channel',
    },
    {
        Header: 'ID коммуникации',
        accessor: 'communicationId',
    },
    {
        Header: 'Последнее сообщение',
        accessor: 'dateMessage',
        getProps: () => ({ type: CELL_TYPES.DATE }),
    },
    {
        Header: 'Статус',
        accessor: 'status',
    },
    {
        Header: 'Тип',
        accessor: 'type',
    },
];

const Communication = () => {
    const { pathname, search } = useLocation();

    const activePage = +(new URLSearchParams(search).get('page') || 1);

    const data = useMemo(() => makeCommunicationTheme(10), []);

    return (
        <Block>
            <Form
                initialValues={{
                    theme: '',
                    channel: '',
                    status: '',
                    type: '',
                }}
                onSubmit={values => {
                    console.log(values);
                }}
            >
                <Block.Header>
                    <h2>Фильтр</h2>
                    <div css={{ button: { marginLeft: scale(2) } }}>
                        <Form.Reset
                            size="sm"
                            theme="secondary"
                            type="button"
                            onClick={() => {
                                console.log('reset');
                            }}
                        >
                            Сбросить
                        </Form.Reset>
                        <Button size="sm" type="submit">
                            Применить
                        </Button>
                    </div>
                </Block.Header>
                <Block.Body>
                    <Layout cols={4}>
                        <Layout.Item>
                            <Form.Field name="theme" label="Тема" />
                        </Layout.Item>
                        <Form.Field name="channel" label="Канал">
                            <MultiSelect isMulti={false} options={prepareForSelect(['infinity', 'SMS-центр'])} />
                        </Form.Field>
                        <Form.Field name="status" label="Статус">
                            <MultiSelect
                                isMulti={false}
                                options={prepareForSelect(['Открыта', 'Закрыта', 'Отправлено', 'Доставлено'])}
                            />
                        </Form.Field>
                        <Form.Field name="type" label="Тип">
                            <MultiSelect
                                isMulti={false}
                                options={prepareForSelect(['Вопрос', 'Действия', 'Претензия', 'Маркетинг'])}
                            />
                        </Form.Field>
                    </Layout>
                </Block.Body>
            </Form>
            <Block.Body>
                {/* Вызывает попап, он создан в другой ветке */}
                <Button>Создать чат</Button>
            </Block.Body>
            <Block.Body>
                <Table columns={COLUMNS} data={data} />
                <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
            </Block.Body>
        </Block>
    );
};

export default Communication;
