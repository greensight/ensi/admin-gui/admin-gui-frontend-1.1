import React, { useMemo, useState } from 'react';
import * as Yup from 'yup';
import { useLocation } from 'react-router-dom';
import { scale, Button } from '@greensight/gds';

import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Table from '@components/Table';

import { getRandomItem, makeRandomData } from '@scripts/mock';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import { Flatten } from '@scripts/helpers';

import RemoveIcon from '@svg/tokens/small/trash.svg';

const columns = [
    {
        Header: 'Название категории',
        accessor: 'title',
    },
    {
        Header: 'Иконка категории',
        accessor: 'logo',
        customRender: true,
        Cell: ({ value, row }: any) => {
            return (
                <div css={{ display: 'flex', alignItems: 'center' }}>
                    {value ? (
                        <>
                            <img width="30" height="30" src={value} alt="" />
                            <Button size="sm" theme="ghost" Icon={RemoveIcon} css={{ marginLeft: scale(1) }} hidden>
                                удалить
                            </Button>
                        </>
                    ) : (
                        <>
                            <form onSubmit={e => e.preventDefault()}>
                                <input name={`${row.id}-file`} accept="image/svg+xml" type="file" required />
                            </form>
                        </>
                    )}
                </div>
            );
        },
    },
];

const categoriesName = [
    'Резинки для волос',
    'Стайлеры',
    'Ножницы',
    'Шампуни',
    'Профессиональные шампуни',
    'Восстанавливающие шампуни',
    'Мебель',
    'Тест',
    'Товары для дома',
    'Товары для кухни',
];

const getTableItem = (id: number) => {
    return {
        id,
        title: categoriesName[id],
        logo: getRandomItem([null, '/logo.svg']),
        drag: 1,
    };
};

const MAX_SELECTED_CATEGORIES = 2;

const ContentCategories = () => {
    const [data, setData] = useState(makeRandomData(5, getTableItem));
    const [ids, setIds, selectedRows] = useSelectedRowsData<Flatten<typeof data>>(data);

    return (
        <PageWrapper h1="Управление категориями">
            <p css={{ marginBottom: scale(2) }}>
                Выбранные категории будут отображаться на главной странице (не более {MAX_SELECTED_CATEGORIES}{' '}
                категорий)
            </p>
            <p css={{ marginBottom: scale(2) }}>Выбрано категорий: {ids.length}</p>
            <Block>
                <Block.Body>
                    <Table
                        data={data}
                        columns={columns}
                        onRowSelect={setIds}
                        setData={setData}
                        needSettingsColumn={false}
                        isDragDisabled={false}
                        css={{ marginBottom: scale(2) }}
                        maxSelectedRows={MAX_SELECTED_CATEGORIES}
                    >
                        <colgroup>
                            <col width="5%" />
                            <col width="5%" />
                            <col width="45%" />
                            <col width="45%" />
                        </colgroup>
                    </Table>
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default ContentCategories;
