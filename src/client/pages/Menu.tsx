import React from 'react';
import { scale } from '@greensight/gds';
import { CSSObject } from '@emotion/core';

import typography from '@scripts/typography';
import { menuTable } from '@scripts/mock';

import Table from '@components/Table';
import Block from '@components/Block';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Код',
        accessor: 'code',
    },
    {
        Header: 'Название',
        accessor: 'title',
        getProps: () => ({ type: 'link' }),
    },
];

const Menu = () => {
    const titleCss: CSSObject = {
        ...typography('h1'),
        marginBottom: scale(2),
        marginTop: 0,
    };

    return (
        <>
            <main css={{ padding: `${scale(2)}px ${scale(3)}px` }}>
                <h1 css={titleCss}>Меню</h1>

                <div css={typography('bodySm')}>Всего меню: {menuTable.length}.</div>

                <Block css={{ marginTop: scale(2) }}>
                    <Block.Body>
                        <Table
                            columns={COLUMNS}
                            data={menuTable}
                            handleRowInfo={row => console.log('rowdata', row)}
                            needCheckboxesCol={false}
                            needSettingsColumn={false}
                        />
                    </Block.Body>
                </Block>
            </main>
        </>
    );
};

export default Menu;
