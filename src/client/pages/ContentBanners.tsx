import React, { useState, useMemo } from 'react';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import { scale, Button, Layout } from '@greensight/gds';
import { FormikValues } from 'formik';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Table from '@components/Table';
import Form from '@standart/Form';
import Popup from '@standart/Popup';
import Select from '@standart/Select';
import Switcher from '@standart/Switcher';
import { Flatten, prepareForSelect } from '@scripts/helpers';
import { getRandomItem, makeRandomData } from '@scripts/mock';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import usePopupState from '@scripts/usePopupState';
import usePrevious from '@scripts/usePrevious';
import { ActionType } from '@scripts/enums';
import useURLHelper from '@scripts/useURLHelper';

const titles = ['Баннер 1', 'Баннер 2', 'Баннер 3', 'Баннер 4', 'Баннер 5'];
const types = [
    'Не выбрано',
    'В каталоге сверху',
    'В каталоге снизу',
    'На продуктовой странице',
    'В виджете',
    'В шапке',
];
const preparedTitles = prepareForSelect(titles);
const preparedTypes = prepareForSelect(types);
const columns = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Видимость',
        accessor: 'active',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Изображение',
        accessor: 'photo',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: 'Тип',
        accessor: 'type',
    },
];

const getTableItem = (id: number) => {
    return {
        id,
        active: getRandomItem(['Активен', 'Неактивен']),
        photo: '',
        title: titles[id],
        type: types[id],
    };
};

const initialState = { type: ActionType.Close, open: false, title: '', id: '' };

type State = {
    open?: boolean;
    id?: string;
    title?: string;
};

const Filters = ({
    className,
    initialValues,
    emptyInitialValues,
    onSubmit,
    onReset,
}: {
    className?: string;
    onSubmit: (vals: FormikValues) => void;
    onReset?: (vals: FormikValues) => void;
    emptyInitialValues: FormikValues;
    initialValues: FormikValues;
}) => {
    return (
        <Block className={className}>
            <Form initialValues={initialValues} onSubmit={onSubmit} onReset={onReset}>
                <Block.Body>
                    <Layout cols={12}>
                        <Layout.Item col={3}>
                            <Form.FastField name="ID" label="ID" />
                        </Layout.Item>
                        <Layout.Item col={3}>
                            <Form.FastField name="types" label="Типы">
                                <Select items={preparedTypes} />
                            </Form.FastField>
                        </Layout.Item>
                    </Layout>
                </Block.Body>
                <Block.Footer>
                    <div>
                        <Form.Reset size="sm" theme="secondary" type="button" initialValues={emptyInitialValues}>
                            Сбросить
                        </Form.Reset>
                        <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                            Применить
                        </Button>
                    </div>
                </Block.Footer>
            </Form>
        </Block>
    );
};

const ContentBanner = () => {
    const [popupState, popupDispatch] = usePopupState<State>(initialState);
    const data = useMemo(() => makeRandomData(5, getTableItem), []);

    const emptyInitialValues = {
        ID: '',
        type: '',
    };

    const { initialValues, URLHelper } = useURLHelper(emptyInitialValues);
    return (
        <PageWrapper h1="Баннеры">
            <>
                <Filters
                    initialValues={initialValues}
                    emptyInitialValues={emptyInitialValues}
                    onSubmit={URLHelper}
                    css={{ marginBottom: scale(3) }}
                />
                <div css={{ marginBottom: scale(2), display: 'flex', alignItems: 'center' }}>
                    <Button
                        size="sm"
                        Icon={PlusIcon}
                        to="/content/banners/create"
                        as={Link}
                        onClick={() => popupDispatch({ type: ActionType.Add })}
                    >
                        Создать баннер
                    </Button>
                </div>
                <Block>
                    <Block.Body>
                        <Table
                            data={data}
                            columns={columns}
                            needCheckboxesCol={false}
                            needDeletingColumn
                            handleRowInfo={row => {
                                popupDispatch({
                                    type: ActionType.Edit,
                                    payload: {
                                        id: row?.id,
                                        title: row?.title,
                                    },
                                });
                            }}
                            css={{ marginBottom: scale(2) }}
                        />
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={Boolean(popupState.open)}
                    onRequestClose={() => popupDispatch({ type: ActionType.Close })}
                    title={`Вы уверены, что хотите удалить баннер "${popupState.title}"?`}
                    popupCss={{ minWidth: scale(50) }}
                >
                    <Form
                        initialValues={{ title: popupState.title, id: popupState.id }}
                        onSubmit={vals => console.log(vals)}
                    >
                        <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <Form.Reset
                                size="sm"
                                theme="fill"
                                css={{ marginRight: scale(2) }}
                                onClick={() =>
                                    popupDispatch({
                                        type: ActionType.Close,
                                    })
                                }
                            >
                                Отменить
                            </Form.Reset>
                            <Button type="submit" size="sm">
                                Удалить
                            </Button>
                        </div>
                    </Form>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default ContentBanner;
