import React, { useMemo, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Button, scale, Layout } from '@greensight/gds';
import { FormikValues } from 'formik';

import Table from '@components/Table';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';

import Form from '@standart/Form';
import MultiSelect from '@standart/MultiSelect';
import { sellers, makeStores } from '@scripts/mock';
import Pagination from '@standart/Pagination';
import Popup from '@standart/Popup';

import typography from '@scripts/typography';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import useURLHelper from '@scripts/useURLHelper';

import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Название',
        accessor: 'title',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Продавец',
        accessor: 'seller',
    },
    {
        Header: 'Населенный пункт',
        accessor: 'city',
    },
];

const SELLERS = sellers.map(i => ({ label: i, value: i }));

const Filters = ({
    className,
    initialValues,
    emptyInitialValues,
    onSubmit,
    onReset,
}: {
    className?: string;
    onSubmit: (vals: FormikValues) => void;
    onReset?: (vals: FormikValues) => void;
    emptyInitialValues: FormikValues;
    initialValues: FormikValues;
}) => {
    return (
        <>
            <Block className={className}>
                <Form initialValues={initialValues} onSubmit={onSubmit} onReset={onReset}>
                    <Block.Body>
                        <Layout cols={8}>
                            <Layout.Item col={2}>
                                <Form.Field name="sellerID" label="ID продавца" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.Field name="seller" label="Продавец">
                                    <MultiSelect name="seller" options={SELLERS} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.Field name="title" label="Название" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.Field name="sity" label="Населенный пункт" />
                            </Layout.Item>
                        </Layout>
                    </Block.Body>
                    <Block.Footer>
                        <div css={typography('bodySm')}>Найдено 135 предложений </div>
                        <div>
                            <Form.Reset size="sm" theme="secondary" type="button" initialValues={emptyInitialValues}>
                                Сбросить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Footer>
                </Form>
            </Block>
        </>
    );
};

const SellerStores = () => {
    const { pathname, search } = useLocation();
    const [idDeleteOpen, setIsDeleteOpen] = useState(false);
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeStores(10), []);

    const [ids, setIds, selectedRows] = useSelectedRowsData<typeof data[0]>(data);

    const emptyInitialValues = {
        sellerID: '',
        title: '',
        sity: '',
        seller: '',
    };

    const { initialValues, URLHelper } = useURLHelper(emptyInitialValues);

    return (
        <PageWrapper h1="Склады продавцов">
            <>
                <Filters
                    initialValues={initialValues}
                    emptyInitialValues={emptyInitialValues}
                    onSubmit={vals => {
                        URLHelper(vals);
                    }}
                    css={{ marginBottom: scale(3) }}
                />

                <Button
                    size="sm"
                    Icon={PlusIcon}
                    as={Link}
                    to="/stores/seller-stores/create/"
                    css={{ marginRight: scale(2), marginBottom: scale(2) }}
                >
                    Добавить склад
                </Button>
                {ids.length > 0 ? (
                    <Button
                        size="sm"
                        Icon={TrashIcon}
                        css={{ marginRight: scale(2) }}
                        onClick={() => setIsDeleteOpen(true)}
                    >
                        Удалить склад{ids.length > 1 && 'ы'}
                    </Button>
                ) : null}
                <Block>
                    <Block.Body>
                        <Table columns={COLUMNS} data={data} onRowSelect={setIds} needSettingsColumn={false} />
                        <Pagination url={pathname} activePage={activePage} pages={7} />
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={idDeleteOpen}
                    onRequestClose={() => setIsDeleteOpen(false)}
                    title="Вы уверены, что хотите удалить следующие склады?"
                    popupCss={{ minWidth: scale(50) }}
                >
                    <ul css={{ marginBottom: scale(2) }}>
                        {selectedRows.map(r => (
                            <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                                #{r.id} – {r.title[0]}
                            </li>
                        ))}
                    </ul>
                    <Button size="sm">Удалить</Button>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default SellerStores;
