import React from 'react';
import { useLocation, Link } from 'react-router-dom';
import { Button, scale } from '@greensight/gds';
import PageWrapper from '@components/PageWrapper';

const NotFound = () => {
    const { pathname } = useLocation();

    return (
        <PageWrapper title="Страница не найдена" h1="Ошибка 404">
            <>
                <p css={{ marginBottom: scale(2) }}>
                    Страница <strong>{pathname}</strong> не найдена
                </p>
                <Button as={Link} to="/" size="sm">
                    На главную
                </Button>
            </>
        </PageWrapper>
    );
};
export default NotFound;
