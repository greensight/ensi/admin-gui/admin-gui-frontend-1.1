import React from 'react';
import { scale, Layout, Button } from '@greensight/gds';
import { useLocation, Link } from 'react-router-dom';
import { FormikValues } from 'formik';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Form from '@standart/Form';
import Select from '@standart/Select';
import Table from '@components/Table';
import { prepareForSelect } from '@scripts/helpers';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import { getRandomItem, makeRandomData } from '@scripts/mock';
import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import Pagination from '@standart/Pagination';
import Popup from '@standart/Popup';

const types = ['Не выбрано', 'Акции', 'Подборки', 'Бренда'];
const preparedTypes = prepareForSelect(types);

const columns = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Видимость',
        accessor: 'visibility',
    },
    {
        Header: 'Изображение',
        accessor: 'photo',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: 'Тип',
        accessor: 'type',
    },
];

const getTableItem = (id: number) => ({
    id: `100${id}`,
    visibility: getRandomItem(['Видим', 'Не видим']),
    photo: getRandomItem(['https://placehold.it/100x100', '']),
    title: getRandomItem(['Название', 'Тестовое название']),
    type: getRandomItem(types),
});

const Filter = ({ onSubmit }: { onSubmit: (vals: FormikValues) => void }) => {
    return (
        <Form onSubmit={onSubmit} initialValues={{ id: '', type: '' }}>
            <Layout cols={4} css={{ marginBottom: scale(2) }}>
                <Layout.Item col={1}>
                    <Form.FastField name="id" label="ID" />
                </Layout.Item>
                <Layout.Item col={1}>
                    <Form.FastField name="types" label="Тип">
                        <Select items={preparedTypes} />
                    </Form.FastField>
                </Layout.Item>
            </Layout>
            <Form.Reset theme="fill" size="sm" css={{ marginRight: scale(2) }}>
                Очистить
            </Form.Reset>
            <Button size="sm" type="submit">
                Применить
            </Button>
        </Form>
    );
};

const ContentProductGroups = () => {
    const data = React.useMemo(() => makeRandomData(5, getTableItem), []);
    const [, setIds, selectedRows] = useSelectedRowsData(data);
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const [isDeleteOpen, setIsDeleteOpen] = React.useState(false);
    return (
        <PageWrapper h1="Подборки">
            <>
                <Block css={{ marginBottom: scale(2) }}>
                    <Block.Body>
                        <Filter onSubmit={val => console.log(val)} />
                    </Block.Body>
                </Block>
                {selectedRows.length === 0 ? (
                    <Button
                        size="sm"
                        css={{ marginBottom: scale(2) }}
                        Icon={PlusIcon}
                        as={Link}
                        to="/content/product-groups/create"
                    >
                        Создать
                    </Button>
                ) : (
                    <Button
                        size="sm"
                        css={{ marginBottom: scale(2) }}
                        Icon={TrashIcon}
                        onClick={() => setIsDeleteOpen(true)}
                    >
                        Удалить тем{selectedRows.length === 1 ? 'y' : 'ы'}
                    </Button>
                )}
                <Block>
                    <Block.Body>
                        <Table columns={columns} data={data} onRowSelect={setIds} css={{ marginBottom: scale(2) }} />
                        <Pagination url={pathname} activePage={activePage} pages={7} />
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={isDeleteOpen}
                    onRequestClose={() => setIsDeleteOpen(false)}
                    title="Вы уверены, что хотите удалить следующие подборки?"
                    popupCss={{ minWidth: scale(50) }}
                >
                    <ul css={{ marginBottom: scale(2) }}>
                        {selectedRows.map(r => (
                            <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                                #{r.id} – {r.title}
                            </li>
                        ))}
                    </ul>
                    <Button size="sm">Удалить</Button>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default ContentProductGroups;
