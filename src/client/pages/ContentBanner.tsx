import React from 'react';
import { useFormikContext } from 'formik';
import { useParams } from 'react-router-dom';
import * as Yup from 'yup';
import { scale, Button } from '@greensight/gds';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';
import Form from '@standart/Form';
import Select from '@standart/Select';
import Switcher from '@standart/Switcher';
import { prepareForSelect } from '@scripts/helpers';

const types = [
    'Не выбрано',
    'В каталоге среди товаров',
    'В каталоге сверху',
    'На продуктовой странице',
    'В виджете',
    'В шапке',
];
const preparedTypes = prepareForSelect(types);

const BannerPopupChildren = () => {
    const {
        values: { hasBtn },
    } = useFormikContext<{ hasBtn: boolean }>();

    return (
        <>
            <Form.FastField name="title" label="Наимeнование" css={{ marginBottom: scale(2) }} />
            <Form.FastField name="isActive" css={{ marginBottom: scale(2) }}>
                <Switcher>Активность</Switcher>
            </Form.FastField>
            <p css={{ marginBottom: scale(1) }}>Изображения: десктопное, планшетное, мобильное</p>
            <FilePond imageLayout maxFiles={3} />
            <Form.FastField label="Тип" name="type" css={{ marginBottom: scale(2) }}>
                <Select items={preparedTypes} />
            </Form.FastField>
            <Form.FastField name="link" label="Ссылка" type="link" css={{ marginBottom: scale(2) }} />
            <Form.FastField name="hasBtn" css={{ marginBottom: scale(2) }}>
                <Switcher>С кнопкой</Switcher>
            </Form.FastField>
            {hasBtn ? (
                <>
                    <Form.FastField name="btnType" label="Текст кнопки" css={{ marginBottom: scale(2) }}>
                        <Select items={preparedTypes} />
                    </Form.FastField>
                    <Form.FastField name="btnText" label="Тип кнопки" css={{ marginBottom: scale(2) }}>
                        <Select items={preparedTypes} />
                    </Form.FastField>
                    <Form.FastField name="btnPosition" label="Местоположение кнопки" css={{ marginBottom: scale(2) }}>
                        <Select items={preparedTypes} />
                    </Form.FastField>
                </>
            ) : null}
            <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Form.Reset size="sm" theme="fill" css={{ marginRight: scale(2) }}>
                    Очистить
                </Form.Reset>
                <Button size="sm" type="submit">
                    Сохранить
                </Button>
            </div>
        </>
    );
};

const ContentBanners = () => {
    const { id } = useParams<{ id: string }>();

    return (
        <PageWrapper h1={id === 'create' ? 'Создать баннер' : `Редактировать баннер ${id}`}>
            <Block css={{ marginBottom: scale(2), maxWidth: '75%' }}>
                <Block.Body>
                    <FilePondStyles />
                    <Form
                        onSubmit={val => console.log(val)}
                        initialValues={{
                            title: '',
                            isActive: true,
                            type: '',
                            link: '',
                            hasBtn: false,
                            btnText: '',
                            btnType: '',
                            btnPosition: '',
                        }}
                        validationSchema={Yup.object().shape({
                            title: Yup.string().required('Обязательное поле'),
                            type: Yup.string().required('Обязательное поле'),
                            link: Yup.string().required('Обязательное поле'),
                            btnText: Yup.string().when('hasBtn', {
                                is: true,
                                then: Yup.string().required('Обязательное поле'),
                                otherwise: Yup.string(),
                            }),
                            btnType: Yup.string().when('hasBtn', {
                                is: true,
                                then: Yup.string().required('Обязательное поле'),
                                otherwise: Yup.string(),
                            }),
                            btnPosition: Yup.string().when('hasBtn', {
                                is: true,
                                then: Yup.string().required('Обязательное поле'),
                                otherwise: Yup.string(),
                            }),
                        })}
                    >
                        <BannerPopupChildren />
                    </Form>
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default ContentBanners;
