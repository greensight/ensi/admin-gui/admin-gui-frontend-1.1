import React from 'react';
import { scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import Table, { TableColumnProps, TableRowProps, makeStringRow } from '@components/Table';
import Block from '@components/Block';
import Form from '@standart/Form';
import { Link } from 'react-router-dom';
import Tooltip from '@standart/Tooltip';
import Mask from '@standart/Mask';
import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import Tabs from '@standart/Tabs';
import Badge, { BadgeProps } from '@components/Badge';
import { STATUSES as BADGE_STATATUSES } from '@scripts/enums';
import { maskPhone } from '@scripts/mask';
import Textarea from '@standart/Textarea';
import HeaderDouble from '@components/Table/HeaderDouble';
import headerWithTooltip from '@components/Table/HeaderWithTooltip';
import PageWrapper from '@components/PageWrapper';
import { default as DButton } from '@components/DropdownButton';
import CrossIcon from '@svg/cross.svg';

const Digest = () => {
    const { colors } = useTheme();

    return (
        <>
            <Block css={{ marginBottom: scale(3) }}>
                <Block.Body>
                    <p css={{ marginBottom: scale(2) }}>
                        <strong>Общая информация</strong>
                    </p>
                    <Layout cols={3} gap={[scale(1), scale(3)]} css={{ marginBottom: scale(2) }}>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Кол-во доставок:</strong> 1 шт.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Кол-во отправлений:</strong> 2 шт.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}></Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Кол-во товаров:</strong> 9 шт.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Вес заказа:</strong> 786 г.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}></Layout.Item>
                        <Layout.Item col={1} css={{ marginBottom: scale(1) }}>
                            <p>
                                <strong>Продавцы:</strong>{' '}
                                <Link to={'/'} css={{ color: colors?.primary }}>
                                    Резинки для волос
                                </Link>
                                ,{' '}
                                <Link to={'/'} css={{ color: colors?.primary }}>
                                    Stylers
                                </Link>
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1} css={{ marginBottom: scale(1) }}>
                            <p>
                                <strong>Тип подтверждения:</strong> SMS
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}></Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Стоимость заказа:</strong> 298 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Стоимость заказа без скидки:</strong> 1 151 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Скидка заказа:</strong> 853 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Стоимость товаров:</strong> 288 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Стоимость товаров без скидки:</strong> 1 103 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Скидка на товары:</strong> 815 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Стоимость доставки:</strong> 10 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Стоимость доставки без скидки:</strong> 48 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Скидка на доставку:</strong> 38 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Потрачено бонусов:</strong> 0
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Начислено бонусов:</strong> 0 руб
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}></Layout.Item>
                    </Layout>

                    <table css={{ width: '100%', borderCollapse: 'collapse', marginBottom: scale(2) }}>
                        <thead css={{ textAlign: 'left' }}>
                            <tr css={{ height: scale(5) }}>
                                <th
                                    css={{
                                        padding: scale(1),
                                        borderTop: `1px solid ${colors?.grey400}`,
                                        borderBottom: `2px solid ${colors?.grey400}`,
                                    }}
                                >
                                    Реферальный партнер
                                </th>
                                <th
                                    css={{
                                        padding: scale(1),
                                        borderTop: `1px solid ${colors?.grey400}`,
                                        borderBottom: `2px solid ${colors?.grey400}`,
                                    }}
                                >
                                    Элемент корзины
                                </th>
                                <th
                                    css={{
                                        padding: scale(1),
                                        borderTop: `1px solid ${colors?.grey400}`,
                                        borderBottom: `2px solid ${colors?.grey400}`,
                                    }}
                                >
                                    Промокод
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr
                                css={{
                                    height: scale(5),
                                    borderTop: `1px solid ${colors?.grey400}`,
                                    '&:hover': {
                                        backgroundColor: colors?.grey200,
                                    },
                                }}
                            >
                                <td colSpan={3} css={{ padding: scale(1) }}>
                                    Реферальных партнеров нет
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <p>
                        <strong>Промокоды:</strong> Нет
                    </p>
                </Block.Body>
            </Block>

            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        name: 'Беспалова Анастасия Романовна',
                        phoneNumber: '+7(812) 345-73-03',
                        email: 'xkulakova@inbox.ru',
                        address: '170034, Тверская обл, г Тверь, ул Дарвина, д 1, кв 1',
                        entrance: '',
                        floor: '1',
                        intercom: '',
                        clientComment: '',
                        managerComment:
                            'Одна подвинулась нечаянно, я ее — отодвину, изволь. — А у нас просто, по — двугривенному ревизскую душу? — Но позвольте спросить вас, — сказал Чичиков хладнокровно и, — вообрази, кто? Вот ни за.',
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Body>
                        <p css={{ marginBottom: scale(2) }}>
                            <strong>Получатель</strong>
                        </p>
                        <Layout cols={3} css={{ marginBottom: scale(2) }}>
                            <Layout.Item col={1}>
                                <Form.Field name="name" label="ФИО*" disabled />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="phoneNumber" label="Телефон*" disabled>
                                    <Mask mask={maskPhone} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="email" label="E-mail" disabled />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="address" label="Адрес до квартиры/офиса включительно*" disabled />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Layout cols={3}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="entrance" label="Подъезд" disabled />
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="floor" label="Этаж" disabled />
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="intercom" label="Домофон" disabled />
                                    </Layout.Item>
                                    <Layout.Item col={3}>
                                        <Form.Field
                                            name="clientComment"
                                            label="Комментарий курьеру от клиента"
                                            disabled
                                        >
                                            <Textarea
                                                rows={2}
                                                isResize={true}
                                                css={{ width: '100%', padding: `${scale(1)}px ${scale(2)}px` }}
                                            />
                                        </Form.Field>
                                    </Layout.Item>
                                </Layout>
                            </Layout.Item>
                        </Layout>

                        <p css={{ marginBottom: scale(2) }}>
                            <strong>Дополнительная информация</strong>
                        </p>
                        <Form.Field name="managerComment" label="Комментарий менеджера" disabled>
                            <Textarea
                                rows={2}
                                isResize={true}
                                css={{ width: '100%', padding: `${scale(1)}px ${scale(2)}px` }}
                            />
                        </Form.Field>
                    </Block.Body>
                </Form>
            </Block>
        </>
    );
};

const COLUMNS_DEPARTURE = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Фото',
        accessor: 'photo',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: () =>
            HeaderDouble({
                headerText: 'Название',
                smallText: 'Артикул',
            }),
        accessor: 'name',
        getProps: () => ({ type: 'linkWithText' }),
    },
    {
        Header: () =>
            HeaderDouble({
                headerText: 'Категория',
                smallText: 'Бренд',
            }),
        accessor: 'category',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: () =>
            HeaderDouble({
                headerText: 'Количество',
                smallText: 'Вес 1 шт\nДxШxВ 1 шт',
            }),
        accessor: 'amount',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Сумма за единицу без скидки',
                tooltipText: 'Цена товара без скидки за единицу товара',
            }),
        accessor: 'unitPriceWithoutDiscount',
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Скидка за единицу',
                tooltipText: 'Величина скидки за единицу товара',
            }),
        accessor: 'unitDiscount',
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Цена за единицу со скидкой',
                tooltipText: 'Цена товара со всеми скидками за единицу товара',
            }),
        accessor: 'unitPriceWithDiscount',
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Сумма без скидки',
                tooltipText: 'Цена товара без скидки с учётом количества',
            }),
        accessor: 'sumWithoutDiscount',
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Скидка',
                tooltipText: 'Величина скидки с учётом количества',
            }),
        accessor: 'discountSum',
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Цена со скидкой',
                tooltipText: 'Цена товара со всеми скидками с учётом количества',
            }),
        accessor: 'sumWithDiscount',
    },
];

const orderRowConstructor = (
    id: number,
    photo: string | null,
    name: Array<string>,
    category: Array<string>,
    amount: Array<string>,
    unitPriceWithoutDiscount: string,
    unitDiscount: string,
    unitPriceWithDiscount: string,
    sumWithoutDiscount: string,
    discountSum: string,
    sumWithDiscount: string
) => {
    return {
        id,
        photo,
        name,
        category,
        amount,
        unitPriceWithoutDiscount,
        unitDiscount,
        unitPriceWithDiscount,
        sumWithoutDiscount,
        discountSum,
        sumWithDiscount,
    };
};

const firstDeparture = [
    makeStringRow(153, 'Коробка #1 (ID: 153, Коробка 10x20x30, вес брутто 94 г, вес пустой коробки 10 г)'),
    orderRowConstructor(
        522,
        null,
        [
            'Резинка для волос женская и детская, прочная и долговечная (браслет пружинка), набор из 3 шт.',
            '/',
            '899008',
        ],
        ['', 'Vikki&Lilli'],
        ['7 шт', '12 г\n125 x 60 x 30 мм'],
        '105.29 руб',
        '0 руб',
        '17.86 руб',
        '737 руб',
        '0 руб',
        '125 руб'
    ),
];

const secondDeparture = [
    orderRowConstructor(
        527,
        null,
        ['Стайлер для волос IKOO E-styler Pro White Platina', '/', '292447'],
        ['', 'Ikoo'],
        ['1 шт', '387 г\n198 x 56 x 450 мм'],
        '193 руб',
        '0 руб',
        '89 руб',
        '193 руб',
        '0 руб',
        '89 руб'
    ),
    orderRowConstructor(
        528,
        null,
        ['Стайлер для волос IKOO E-styler Beluga Black', '/', '292119'],
        ['', 'Ikoo'],
        ['1 шт', '387 г\n198 x 56 x 450 мм'],
        '173 руб',
        '0 руб',
        '74 руб',
        '173 руб',
        '0 руб',
        '74 руб'
    ),
];

const OrderDetails = () => {
    const { colors } = useTheme();

    return (
        <>
            <Link to={'/'} css={{ color: colors?.primary, padding: `${scale(1)}px ${scale(2)}px` }}>
                Доставка 1000033-1
            </Link>
            <Block css={{ marginBottom: scale(3) }}>
                <Block.Body>
                    <Link to={'/'} css={{ color: colors?.primary, padding: `${scale(1)}px 0` }}>
                        Отправление 1000033-1000033-1-01
                    </Link>
                    <Table
                        columns={COLUMNS_DEPARTURE}
                        data={firstDeparture}
                        handleRowInfo={() => console.log('click')}
                        needCheckboxesCol={false}
                        needSettingsColumn={false}
                    />
                </Block.Body>
            </Block>
            <Block>
                <Block.Body>
                    <Link to={'/'} css={{ color: colors?.primary, padding: `${scale(1)}px 0` }}>
                        Отправление 1000033-1000033-1-02
                    </Link>
                    <Table
                        columns={COLUMNS_DEPARTURE}
                        data={secondDeparture}
                        handleRowInfo={() => console.log('click')}
                        needCheckboxesCol={false}
                        needSettingsColumn={false}
                    />
                </Block.Body>
            </Block>
        </>
    );
};

const Deliveries = () => {
    const { colors } = useTheme();

    return (
        <>
            <Block>
                <Block.Body>
                    <h2 css={{ ...typography('h2'), marginBottom: scale(2) }}>Доставка 1000033-1</h2>
                    <Layout
                        cols={2}
                        gap={[scale(1), scale(0)]}
                        css={{
                            '>div:nth-of-type(2n)': {
                                paddingLeft: scale(1),
                            },
                        }}
                    >
                        <Layout.Item col={1}>
                            <p>
                                <strong>ID:</strong> 135
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Статус доставки:</strong>{' '}
                                <Badge text={'Доставлена получателю'} type={BADGE_STATATUSES.SUCCESS} /> 15.10.2020
                                13:50:19
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Статус оплаты:</strong>{' '}
                                <Badge text={'Не оплачено'} type={BADGE_STATATUSES.WARNING} />
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Статус доставки у ЛО:</strong> Доставлено (частично){' '}
                                <Tooltip content="Заказ доставлен частично" placement="right" arrow={true}>
                                    <button css={{ verticalAlign: 'middle' }}>
                                        <TipIcon />
                                    </button>
                                </Tooltip>{' '}
                                15.10.2020 13:50:19
                            </p>
                        </Layout.Item>
                        <Layout.Item
                            col={1}
                            css={{
                                borderTop: `1px solid ${colors?.grey400}`,
                                borderBottom: `1px solid ${colors?.grey400}`,
                                paddingTop: scale(1),
                                paddingBottom: scale(2),
                            }}
                        >
                            <p>
                                <strong>Продавцы:</strong>{' '}
                                <Link to={'/'} css={{ color: colors?.primary }}>
                                    Резинки для волос
                                </Link>
                                ,{' '}
                                <Link to={'/'} css={{ color: colors?.primary }}>
                                    Stylers
                                </Link>
                            </p>
                        </Layout.Item>
                        <Layout.Item
                            col={1}
                            css={{
                                borderTop: `1px solid ${colors?.grey400}`,
                                borderBottom: `1px solid ${colors?.grey400}`,
                                paddingTop: scale(1),
                                paddingBottom: scale(2),
                            }}
                        >
                            <p>
                                <strong>№ отправлений:</strong>{' '}
                                <Link to={'/'} css={{ color: colors?.primary }}>
                                    1000033-1000033-1-01
                                </Link>
                                ,{' '}
                                <Link to={'/'} css={{ color: colors?.primary }}>
                                    1000033-1000033-1-02
                                </Link>
                            </p>
                        </Layout.Item>

                        <Layout.Item col={1}>
                            <p>
                                <strong>Способ доставки:</strong> Курьерская доставка
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Логистический оператор:</strong>{' '}
                                <Link to={'/'} css={{ color: colors?.primary }}>
                                    B2Cpl
                                </Link>
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>DT:</strong> 2 дня
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>PDD:</strong> 13.09.2020
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Желаемая покупателем дата доставки:</strong> 13.09.2020
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Адрес доставки:</strong> 170034, Тверская обл, г Тверь, ул Дарвина, д 1, эт 1,
                                кв кв 1
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Желаемое покупателем время доставки:</strong> не указано
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}></Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Трекинг-номер у ЛО:</strong> 222
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Последняя ошибка при создании заказа на доставку у ЛО:</strong>
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Тариф ЛО:</strong> Почта России - посылка онлайн (ID=пр12)
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}></Layout.Item>
                        <Layout.Item
                            col={1}
                            css={{
                                borderTop: `1px solid ${colors?.grey400}`,
                                borderBottom: `1px solid ${colors?.grey400}`,
                                paddingTop: scale(1),
                                paddingBottom: scale(2),
                            }}
                        >
                            <p>
                                <strong>Стоимость товаров:</strong> 288 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item
                            col={1}
                            css={{
                                borderTop: `1px solid ${colors?.grey400}`,
                                borderBottom: `1px solid ${colors?.grey400}`,
                                paddingTop: scale(1),
                                paddingBottom: scale(2),
                            }}
                        >
                            <p>
                                <strong>Стоимость доставки от ЛО:</strong> 332.61 руб.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Габариты (ДxШxВ):</strong> 148x450x148 мм
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1} row={2}>
                            <p>
                                <strong>Вес:</strong> 868 г
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <p>
                                <strong>Кол-во товаров:</strong> 2 шт.
                            </p>
                        </Layout.Item>
                        <Layout.Item col={1}></Layout.Item>
                    </Layout>
                </Block.Body>
            </Block>
        </>
    );
};

const Departures = () => {
    const { colors } = useTheme();

    interface DepartureData {
        departureNumber: string;
        id: number;
        departureStatus: {
            status: BadgeProps;
            date: string;
        };
        deliveryId: number;
        paymentStatus: {
            status: BadgeProps;
        };
        deliveryStatusLO: {
            status: {
                text: string;
                toolTipText: string;
            };
            date: string;
        };
        seller: {
            name: string;
            link: string;
        };
        stock: {
            name: string;
            link: string;
        };
        cargo: string;
        logisticsOperatorZeroMile: {
            name: string;
            link: string;
        };
        logisticsOperatorLastMile: {
            name: string;
            link: string;
        };
        psd: string;
        pdd: string;
        itemsCost: string;
        dimensions: string;
        weight: string;
        boxesAmount: string;
        itemsAmount: string;
    }

    const Departure = ({
        data,
        tableColumns,
        tableData,
    }: {
        data: DepartureData;
        tableColumns: TableColumnProps[];
        tableData: TableRowProps[];
    }) => {
        const {
            departureNumber,
            id,
            departureStatus,
            deliveryId,
            paymentStatus,
            deliveryStatusLO,
            seller,
            stock,
            cargo,
            logisticsOperatorZeroMile,
            logisticsOperatorLastMile,
            psd,
            pdd,
            itemsCost,
            dimensions,
            weight,
            boxesAmount,
            itemsAmount,
        } = data;

        return (
            <Block
                css={{
                    '&:not(:last-child)': {
                        marginBottom: scale(3),
                    },
                }}
            >
                <Block.Body>
                    <Layout type={'flex'} justify={'space-between'} align={'start'}>
                        <Layout.Item>
                            <h2
                                css={{ ...typography('h2'), marginBottom: scale(2) }}
                            >{`Отправление ${departureNumber}`}</h2>
                        </Layout.Item>
                        <Layout.Item
                            css={{
                                display: 'flex',
                                marginBottom: scale(1),
                                '>*:not(:last-child)': {
                                    marginRight: scale(1),
                                },
                            }}
                        >
                            <DButton buttonContent={'Документы'}>
                                <DButton.Option onClick={() => console.log('Карточка сборки')}>
                                    Карточка сборки
                                </DButton.Option>
                                <DButton.Option onClick={() => console.log('Опись')}>Опись</DButton.Option>
                                <DButton.Option onClick={() => console.log('Акт приёма-передачи')}>
                                    Акт приёма-передачи
                                </DButton.Option>
                            </DButton>
                            <DButton buttonContent={'Шаблоны документов'}>
                                <DButton.Option onClick={() => console.log('Акт-претензия')}>
                                    Акт-претензия
                                </DButton.Option>
                                <DButton.Option onClick={() => console.log('Акт приёма-передачи')}>
                                    Акт приёма-передачи
                                </DButton.Option>
                                <DButton.Option onClick={() => console.log('Карточка сборки')}>
                                    Карточка сборки
                                </DButton.Option>
                                <DButton.Option onClick={() => console.log('Опись')}>Опись</DButton.Option>
                            </DButton>
                            <DButton buttonContent={'Действия'} isDropRight>
                                <DButton.Option onClick={() => console.log('Отменить отправление')}>
                                    <CrossIcon css={{ verticalAlign: 'middle' }} /> Отменить отправление
                                </DButton.Option>
                            </DButton>
                        </Layout.Item>
                    </Layout>
                    <Layout
                        cols={2}
                        gap={[scale(1), scale(0)]}
                        css={{
                            '>div:nth-of-type(2n)': {
                                paddingLeft: scale(1),
                            },
                            marginBottom: scale(3),
                        }}
                    >
                        <Layout.Item>
                            <p>
                                <strong>ID:</strong> {id}
                            </p>
                        </Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>Статус отправления:</strong>{' '}
                                <Badge text={departureStatus.status.text} type={departureStatus.status.type} />{' '}
                                {departureStatus.date}
                            </p>
                        </Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>ID доставки:</strong> {deliveryId}
                            </p>
                        </Layout.Item>
                        <Layout.Item></Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>Статус оплаты:</strong>{' '}
                                <Badge text={paymentStatus.status.text} type={paymentStatus.status.type} />
                            </p>
                        </Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>Статус доставки у ЛО:</strong> {deliveryStatusLO.status.text}{' '}
                                <Tooltip content={deliveryStatusLO.status.toolTipText} placement="right" arrow={true}>
                                    <button css={{ verticalAlign: 'middle' }}>
                                        <TipIcon />
                                    </button>
                                </Tooltip>{' '}
                                {deliveryStatusLO.date}
                            </p>
                        </Layout.Item>
                        <Layout.Item
                            css={{
                                borderTop: `1px solid ${colors?.grey400}`,
                                borderBottom: `1px solid ${colors?.grey400}`,
                                paddingBottom: scale(1),
                                paddingTop: scale(1, true),
                            }}
                        >
                            <p>
                                <strong>Продавец:</strong>{' '}
                                <Link to={seller.link} css={{ color: colors?.primary }}>
                                    {seller.name}
                                </Link>
                            </p>
                        </Layout.Item>
                        <Layout.Item
                            css={{
                                borderTop: `1px solid ${colors?.grey400}`,
                                borderBottom: `1px solid ${colors?.grey400}`,
                                paddingBottom: scale(1),
                                paddingTop: scale(1, true),
                            }}
                        >
                            <p>
                                <strong>Склад:</strong>{' '}
                                <Link to={stock.link} css={{ color: colors?.primary }}>
                                    {stock.name}
                                </Link>
                            </p>
                        </Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>Груз:</strong> {cargo}
                            </p>
                        </Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>Логистический оператор для нулевой мили:</strong>{' '}
                                <Link to={logisticsOperatorZeroMile.link} css={{ color: colors?.primary }}>
                                    {logisticsOperatorZeroMile.name}
                                </Link>
                            </p>
                        </Layout.Item>
                        <Layout.Item></Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>Логистический оператор для последней мили:</strong>{' '}
                                <Link to={logisticsOperatorLastMile.link} css={{ color: colors?.primary }}>
                                    {logisticsOperatorLastMile.name}
                                </Link>
                            </p>
                        </Layout.Item>
                        <Layout.Item
                            css={{
                                marginBottom: scale(2),
                            }}
                        >
                            <p>
                                <strong>PSD:</strong> {psd}
                            </p>
                        </Layout.Item>
                        <Layout.Item
                            css={{
                                marginBottom: scale(2),
                            }}
                        >
                            <p>
                                <strong>PDD:</strong> {pdd}
                            </p>
                        </Layout.Item>
                        <Layout.Item
                            css={{
                                borderTop: `1px solid ${colors?.grey400}`,
                                borderBottom: `1px solid ${colors?.grey400}`,
                                paddingBottom: scale(1),
                                paddingTop: scale(1, true),
                            }}
                        >
                            <p>
                                <strong>Стоимость товаров:</strong> {itemsCost}
                            </p>
                        </Layout.Item>
                        <Layout.Item
                            css={{
                                borderTop: `1px solid ${colors?.grey400}`,
                                borderBottom: `1px solid ${colors?.grey400}`,
                                paddingBottom: scale(1),
                                paddingTop: scale(1, true),
                            }}
                        ></Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>Габариты (ДxШxВ):</strong> {dimensions}
                            </p>
                        </Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>Вес:</strong> {weight}
                            </p>
                        </Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>Кол-во коробок:</strong> {boxesAmount}
                            </p>
                        </Layout.Item>
                        <Layout.Item>
                            <p>
                                <strong>Кол-во товаров:</strong> {itemsAmount}
                            </p>
                        </Layout.Item>
                    </Layout>
                    <Table
                        columns={tableColumns}
                        data={tableData}
                        handleRowInfo={() => console.log('click')}
                        needCheckboxesCol={false}
                        needSettingsColumn={false}
                        css={{ borderTop: `1px solid ${colors?.grey400}` }}
                    />
                </Block.Body>
            </Block>
        );
    };

    return (
        <>
            <Departure
                data={{
                    departureNumber: '1000033-1000033-1-01',
                    id: 195,
                    departureStatus: {
                        status: {
                            text: 'Принято в пункте назначения',
                            type: BADGE_STATATUSES.REGULAR,
                        },
                        date: '2020-10-15 13:50:19',
                    },
                    deliveryId: 135,
                    paymentStatus: {
                        status: {
                            text: 'Не оплачено',
                            type: BADGE_STATATUSES.WARNING,
                        },
                    },
                    deliveryStatusLO: {
                        status: {
                            text: 'Доставлено (частично)',
                            toolTipText: 'Заказ доставлен частично',
                        },
                        date: '15.10.2020 13:50:19',
                    },
                    seller: {
                        name: 'Резинки для волос',
                        link: '/',
                    },
                    stock: {
                        name: '124460, г Москва, г Зеленоград, р-н Силино, к 1206А',
                        link: '/',
                    },
                    cargo: 'Нет',
                    logisticsOperatorZeroMile: {
                        name: 'B2Cpl',
                        link: '/',
                    },
                    logisticsOperatorLastMile: {
                        name: 'B2Cpl',
                        link: '/',
                    },
                    psd: '11.09.2020 14:24:23',
                    pdd: '13.09.2020',
                    itemsCost: '125 руб.',
                    dimensions: '10x10x10 мм',
                    weight: '94 г',
                    boxesAmount: '1 шт',
                    itemsAmount: '0 шт.',
                }}
                tableColumns={COLUMNS_DEPARTURE}
                tableData={firstDeparture}
            />

            <Departure
                data={{
                    departureNumber: '1000033-1000033-1-02',
                    id: 196,
                    departureStatus: {
                        status: {
                            text: 'Проверка АОЗ',
                            type: BADGE_STATATUSES.WARNING,
                        },
                        date: '2020-10-15 13:50:21',
                    },
                    deliveryId: 135,
                    paymentStatus: {
                        status: {
                            text: 'Не оплачено',
                            type: BADGE_STATATUSES.WARNING,
                        },
                    },
                    deliveryStatusLO: {
                        status: {
                            text: 'Доставлено (частично)',
                            toolTipText: 'Заказ доставлен частично',
                        },
                        date: '15.10.2020 13:50:19',
                    },
                    seller: {
                        name: 'Stylers',
                        link: '/',
                    },
                    stock: {
                        name: '124460, г Москва, г Зеленоград, р-н Силино, к 1206А',
                        link: '/',
                    },
                    cargo: 'Нет',
                    logisticsOperatorZeroMile: {
                        name: 'B2Cpl',
                        link: '/',
                    },
                    logisticsOperatorLastMile: {
                        name: 'B2Cpl',
                        link: '/',
                    },
                    psd: '11.09.2020 15:30:23',
                    pdd: '13.09.2020',
                    itemsCost: '163 руб.',
                    dimensions: '148x450x148 мм',
                    weight: '774 г',
                    boxesAmount: '0 шт',
                    itemsAmount: '2 шт.',
                }}
                tableColumns={COLUMNS_DEPARTURE}
                tableData={secondDeparture}
            />
        </>
    );
};

const Order = () => {
    const { colors } = useTheme();

    return (
        <PageWrapper h1={`Заказ 1000033 от 11.09.2020 11:26:23`}>
            <Layout cols={3}>
                <Layout.Item col={2}>
                    <Block css={{ marginBottom: scale(3) }}>
                        <Block.Body>
                            <p css={{ marginBottom: scale(2) }}>
                                <strong>Инфопанель</strong>
                            </p>
                            <Layout cols={2} gap={[scale(1), scale(3)]}>
                                <Layout.Item col={1} css={{ marginBottom: scale(2) }}>
                                    <p css={{ marginBottom: scale(1) }}>
                                        <strong>Заказ 1000033 от 11.09.2020 11:26:23</strong>
                                    </p>
                                    <Badge text={'Товары'} />
                                </Layout.Item>
                                <Layout.Item col={1} css={{ marginBottom: scale(2) }}>
                                    <p css={{ marginBottom: scale(1) }}>
                                        <strong>Статус заказа:</strong>{' '}
                                        <Badge text={'Ожидает подтверждения Продавцом'} />
                                    </p>
                                    <p css={{ marginBottom: scale(1) }}>15.10.2020 13:46:04</p>
                                    <p>
                                        <Badge text={'Отменён'} type={BADGE_STATATUSES.ERROR} /> 15.10.2020 13:46:04
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <p>
                                        <strong>Покупатель:</strong> N/A
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <p>
                                        <strong>Сегмент:</strong> N/A
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ marginBottom: scale(2) }}>
                                    <p>
                                        <strong>Телефон:</strong> N/A
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ marginBottom: scale(2) }}></Layout.Item>
                                <Layout.Item col={1}>
                                    <p>
                                        <strong>Тип доставки:</strong> Одной доставкой
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <p>
                                        <strong>Город доставки:</strong> г Тверь
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ marginBottom: scale(2) }}>
                                    <p>
                                        <strong>Способ доставки:</strong> Курьерская доставка
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ marginBottom: scale(2) }}>
                                    <p>
                                        <strong>Логистический оператор:</strong>{' '}
                                        <Link to={'/'} css={{ color: colors?.primary }}>
                                            B2Cpl
                                        </Link>
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <p>
                                        <strong>Сумма заказа:</strong> 298 руб.
                                        <Tooltip
                                            content="С учётом всех скидок и доставки"
                                            placement="right"
                                            arrow={true}
                                        >
                                            <button css={{ verticalAlign: 'middle' }}>
                                                <TipIcon />
                                            </button>
                                        </Tooltip>
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <p>
                                        <strong>Статус оплаты:</strong> <Badge text={'Холдирование'} /> 15.10.2020
                                        13:46:04
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ marginBottom: scale(2) }}>
                                    <p>
                                        <strong>Способ оплаты:</strong> Онлайн
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ marginBottom: scale(2) }}>
                                    <p>
                                        <strong>К оплате:</strong> 0 руб.
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ marginBottom: scale(2) }}>
                                    <p>
                                        <strong>Тип подтверждения:</strong> SMS
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ marginBottom: scale(2) }}></Layout.Item>
                                <Layout.Item col={1}>
                                    <p>
                                        <strong>Кол-во ед. товара:</strong> 9 шт.
                                    </p>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <p>
                                        <strong>Вес заказа:</strong> 786 г.
                                    </p>
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                    </Block>
                </Layout.Item>
                <Layout.Item col={1}>
                    <Block css={{ marginBottom: scale(3) }}>
                        <Block.Body>
                            <p css={{ marginBottom: scale(2) }}>
                                <strong>KPIs</strong>
                            </p>

                            <p css={{ marginBottom: scale(1) }}>
                                <strong>Оформлен:</strong> 15.10.2020 10:46:02
                            </p>
                            <p css={{ marginBottom: scale(1) }}>
                                <strong>Ожидает подтверждения Продавцом:</strong> 15.10.2020&nbsp;10:46:04
                            </p>
                        </Block.Body>
                    </Block>
                </Layout.Item>
            </Layout>

            <Tabs>
                <Tabs.List>
                    <Tabs.Tab>Дайджест</Tabs.Tab>
                    <Tabs.Tab>Состав заказа</Tabs.Tab>
                    <Tabs.Tab>Доставки</Tabs.Tab>
                    <Tabs.Tab>Отправления</Tabs.Tab>
                </Tabs.List>
                <Block>
                    <Block.Body>
                        <Tabs.Panel>
                            <Digest />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <OrderDetails />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Deliveries />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <Departures />
                        </Tabs.Panel>
                    </Block.Body>
                </Block>
            </Tabs>
        </PageWrapper>
    );
};

export default Order;
