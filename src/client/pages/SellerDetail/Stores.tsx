import React, { useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import Form from '@standart/Form';
import Block from '@components/Block';
import Table from '@components/Table';
import Pagination from '@standart/Pagination';
import { Button, scale, Layout } from '@greensight/gds';
import { makeRandomData, getRandomItem, mockAddresses, mockNames, mockPhones } from '@scripts/mock';

const COLUMNS = [
    {
        Header: '#',
        accessor: 'storeNumber',
    },
    {
        Header: 'Название склада',
        accessor: 'storeName',
    },
    {
        Header: 'Адрес склада',
        accessor: 'storeAddress',
    },
    {
        Header: 'ФИО контактного лица',
        accessor: 'fullName',
    },
    {
        Header: 'Телефон',
        accessor: 'phone',
    },
];

const storeTableItem = (id: number) => {
    return {
        storeNumber: `100${id}`,
        storeName: getRandomItem(['Главный склад', 'Второй склад', 'Третий склад', 'Четвертый склад']),
        storeAddress: getRandomItem(mockAddresses),
        fullName: getRandomItem(mockNames),
        phone: getRandomItem(mockPhones),
    };
};

const Stores = () => {
    const data = useMemo(() => makeRandomData(10, storeTableItem), []);
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);

    return (
        <>
            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        id: '',
                        title: '',
                        storeAddress: '',
                        name: '',
                        phone: '',
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Header>
                        <h2>Фильтр</h2>
                        <div css={{ button: { marginLeft: scale(2) } }}>
                            <Form.Reset
                                size="sm"
                                theme="secondary"
                                type="button"
                                onClick={() => {
                                    console.log('reset');
                                }}
                            >
                                Очистить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Layout cols={5}>
                            <Layout.Item col={1}>
                                <Form.Field name="id" label="ID" type="number" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.Field name="title" label="Название" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.Field name="storeAddress" label="Адрес склада" />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="name" label="ФИО контактного лица" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.Field name="phone" label="Телефон контактного лица" />
                            </Layout.Item>
                        </Layout>
                    </Block.Body>
                </Form>

                <Block.Body css={{ display: 'flex', button: { marginRight: scale(2) } }}>
                    <Button size="sm">Создать склад</Button>
                    <Button size="sm">Редактировать склад</Button>
                    <Button theme="outline" size="sm">
                        Удалить склады
                    </Button>
                </Block.Body>

                <Block.Body>
                    <Table columns={COLUMNS} data={data} handleRowInfo={() => console.log('click')} />
                    <Pagination url={pathname} activePage={activePage} pages={7} />
                </Block.Body>
            </Block>
        </>
    );
};

export default Stores;
