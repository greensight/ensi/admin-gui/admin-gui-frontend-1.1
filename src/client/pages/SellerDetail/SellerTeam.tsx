import React, { useMemo, useState } from 'react';
import { useLocation } from 'react-router-dom';
import Form from '@standart/Form';
import Block from '@components/Block';
import Table from '@components/Table';
import MultiSelect from '@standart/MultiSelect';
import SendMessagePopup from '../SellerList/SendMessagePopup';
import CheckboxGroup from '@standart/CheckboxGroup';
import Checkbox from '@standart/Checkbox';
import Popup from '@standart/Popup';
import Pagination from '@standart/Pagination';
import { Button, scale, Layout, useTheme } from '@greensight/gds';
import { declOfNum } from '@scripts/helpers';
import { makeTeam, mockPosts } from '@scripts/mock';
import typography from '@scripts/typography';
import { PLACES_OF_COMMUNICATION, SELLER_STATUSES } from '@scripts/data/different';

const communications = PLACES_OF_COMMUNICATION.map(i => ({ label: i, value: i }));
const roles = mockPosts.map(i => ({ label: i, value: i }));
const statuses = SELLER_STATUSES.map(i => ({ label: i, value: i }));

const COLUMNS = [
    {
        Header: '#',
        accessor: 'number',
    },
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'ФИО',
        accessor: 'name',
    },
    {
        Header: 'Email',
        accessor: 'email',
    },
    {
        Header: 'Телефон',
        accessor: 'phone',
    },
    {
        Header: 'Способ связи',
        accessor: 'communication',
    },
    {
        Header: 'Роли',
        accessor: 'role',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Логин',
        accessor: 'login',
    },
];

const SellerTeam = () => {
    const data = useMemo(() => makeTeam(10), []);
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);

    const { colors } = useTheme();

    const [moreFilters, setMoreFilters] = useState(true);

    const [isCreationChatPopup, setIsCreationChatPopup] = useState(false);
    const [isRemovePopup, setIsRemovePopup] = useState(false);
    const [isSwitchRolePopup, setIsSwitchRolePopup] = useState(false);

    const persons = data.slice(0, 2);

    return (
        <>
            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        id: '',
                        name: '',
                        email: '',
                        login: '',
                        phone: '',
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Header>
                        <h2>Фильтр</h2>
                        <div css={{ button: { marginLeft: scale(2) } }}>
                            <button
                                type="button"
                                css={{ color: colors?.primary, ...typography('bodySm') }}
                                onClick={() => setMoreFilters(!moreFilters)}
                            >
                                {moreFilters ? 'Меньше' : 'Больше'} фильтров
                            </button>
                            <Form.Reset
                                size="sm"
                                theme="secondary"
                                type="button"
                                onClick={() => {
                                    console.log('reset');
                                }}
                            >
                                Очистить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Layout cols={6}>
                            <Layout.Item col={1}>
                                <Form.Field name="id" label="ID пользователя" type="number" />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="name" label="ФИО" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.Field name="email" label="Email" />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="phone" label="Телефон" />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="login" label="Логин" />
                            </Layout.Item>
                            {moreFilters && (
                                <>
                                    <Layout.Item col={2}>
                                        <Form.Field name="communication" label="Способ связи">
                                            <MultiSelect isMulti={false} options={communications} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="role" label="Роли">
                                            <MultiSelect isMulti={false} options={roles} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="status" label="Статус">
                                            <MultiSelect isMulti={false} options={statuses} />
                                        </Form.Field>
                                    </Layout.Item>
                                </>
                            )}
                        </Layout>
                    </Block.Body>
                </Form>
                <Block.Body css={{ display: 'flex', button: { marginRight: scale(2) } }}>
                    <Button size="sm">Создать оператора</Button>
                    <Button size="sm">Редактировать оператора</Button>
                    <Button theme="outline" size="sm" onClick={() => setIsRemovePopup(true)}>
                        Удалить операторов
                    </Button>
                    <Button size="sm" onClick={() => setIsCreationChatPopup(true)}>
                        Написать операторам
                    </Button>
                    <Button size="sm" onClick={() => setIsSwitchRolePopup(true)}>
                        Сменить роль операторов
                    </Button>
                </Block.Body>
                <Block.Body>
                    <Table columns={COLUMNS} data={data} handleRowInfo={() => console.log('click')} />
                    <Pagination url={pathname} activePage={activePage} pages={7} />
                </Block.Body>
            </Block>
            {isCreationChatPopup && (
                <SendMessagePopup
                    isOpen={isCreationChatPopup}
                    close={() => setIsCreationChatPopup(false)}
                    onSubmit={() => {
                        console.log('submit');
                    }}
                    selectedRows={[]}
                    statuses={[]}
                />
            )}
            {isRemovePopup && (
                <Popup
                    isOpen={isRemovePopup}
                    onRequestClose={() => {
                        setIsRemovePopup(false);
                    }}
                    title={`Вы уверены, что хотите удалить ${declOfNum(persons.length, [
                        'следующего оператора',
                        'следующих операторов',
                        'следующих операторов',
                    ])}?`}
                >
                    <ul css={{ marginBottom: scale(2) }}>
                        {persons.map(({ number, name }) => (
                            <li key={number} css={{ ':not(:last-child)': { marginBottom: scale(1, true) } }}>
                                #{number}&nbsp;{name}
                            </li>
                        ))}
                    </ul>
                    <Button>Удалить</Button>
                </Popup>
            )}
            {isSwitchRolePopup && (
                <Popup
                    isOpen={isSwitchRolePopup}
                    popupCss={{ minWidth: scale(50) }}
                    onRequestClose={() => {
                        setIsSwitchRolePopup(false);
                    }}
                    title="Изменить роль Имя"
                >
                    <Form initialValues={{ roles: [] }} onSubmit={() => setIsSwitchRolePopup(false)}>
                        <Layout cols={1} gap={scale(2)}>
                            <Layout.Item>
                                <Form.Field name="roles">
                                    {mockPosts.map((item, i) => (
                                        <CheckboxGroup key={i}>
                                            <Checkbox value={item}>{item}</Checkbox>
                                        </CheckboxGroup>
                                    ))}
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item>
                                <Button
                                    size="sm"
                                    theme="secondary"
                                    onClick={() => setIsSwitchRolePopup(false)}
                                    css={{ marginRight: scale(2) }}
                                >
                                    Отменить
                                </Button>
                                <Button type="submit" size="sm">
                                    Сохранить{' '}
                                </Button>
                            </Layout.Item>
                        </Layout>
                    </Form>
                </Popup>
            )}
        </>
    );
};

export default SellerTeam;
