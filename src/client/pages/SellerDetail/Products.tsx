import React, { useMemo, useState } from 'react';
import { useLocation } from 'react-router-dom';
import Form from '@standart/Form';
import Block from '@components/Block';
import Table from '@components/Table';
import MultiSelect from '@standart/MultiSelect';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';
import Pagination from '@standart/Pagination';
import { Button, scale, Layout, useTheme } from '@greensight/gds';
import { makeSellerProducts } from '@scripts/mock';
import typography from '@scripts/typography';
import { STATUSES } from '@scripts/data/different';

const statuses = STATUSES.map(i => ({ label: i, value: i }));

const COLUMNS = [
    {
        Header: 'ID оффера',
        accessor: 'id',
    },
    {
        Header: 'Название оффера',
        accessor: 'title',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Текущая цена, руб',
        accessor: 'price',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: 'Текущий остаток товаров оффера, шт',
        accessor: 'quantity',
    },
    {
        Header: 'Дата создания оффера',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
];

const Products = () => {
    const data = useMemo(() => makeSellerProducts(10), []);
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);

    const { colors } = useTheme();

    const [moreFilters, setMoreFilters] = useState(true);

    return (
        <>
            <DatepickerStyles />
            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        id: '',
                        name: '',
                        email: '',
                        login: '',
                        phone: '',
                        date: [null, null],
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Header>
                        <h2>Фильтр</h2>
                        <div css={{ button: { marginLeft: scale(2) } }}>
                            <button
                                type="button"
                                css={{ color: colors?.primary, ...typography('bodySm') }}
                                onClick={() => setMoreFilters(!moreFilters)}
                            >
                                {moreFilters ? 'Меньше' : 'Больше'} фильтров
                            </button>
                            <Form.Reset
                                size="sm"
                                theme="secondary"
                                type="button"
                                onClick={() => {
                                    console.log('reset');
                                }}
                            >
                                Очистить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Layout cols={8}>
                            <Layout.Item col={4}>
                                <Form.Field name="id" label="ID оффера" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="name" label="Название оффера" />
                            </Layout.Item>
                            {moreFilters && (
                                <>
                                    <Layout.Item col={4}>
                                        <Form.Field name="status" label="Статус оффера">
                                            <MultiSelect isMulti={false} options={statuses} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field
                                            name="priceFrom"
                                            label="Текщая цена оффера, руб"
                                            placeholder="от"
                                            type="number"
                                        />
                                    </Layout.Item>
                                    <Layout.Item col={2} align="end">
                                        <Form.Field name="priceTo" placeholder="до" type="number" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field
                                            name="qtyFrom"
                                            label="Текщий остаток оффера, шт"
                                            placeholder="от"
                                            type="number"
                                        />
                                    </Layout.Item>
                                    <Layout.Item col={2} align="end">
                                        <Form.Field name="qtyTo" placeholder="до" type="number" />
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="date" label="Дата создания">
                                            <DatepickerRange />
                                        </Form.Field>
                                    </Layout.Item>
                                </>
                            )}
                        </Layout>
                    </Block.Body>
                </Form>
                <Block.Body css={{ display: 'flex', button: { marginRight: scale(2) } }}>
                    <Button size="sm">Сменить статус офферов</Button>
                    <Button size="sm">Редактировать оффер</Button>
                    <Button theme="outline" size="sm">
                        Удалить офферы
                    </Button>
                </Block.Body>

                <Block.Body>
                    <Table columns={COLUMNS} data={data} handleRowInfo={() => console.log('click')} />
                    <Pagination url={pathname} activePage={activePage} pages={7} />
                </Block.Body>
            </Block>
        </>
    );
};

export default Products;
