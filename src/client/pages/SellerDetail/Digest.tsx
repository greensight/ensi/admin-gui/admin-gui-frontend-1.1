import React, { useState, useRef } from 'react';
import Form from '@standart/Form';
import Block from '@components/Block';
import { Button, scale } from '@greensight/gds';
import Textarea from '@standart/Textarea';
import useListCSS from '@scripts/useListCSS';
import useFieldCSS from '@scripts/useFieldCSS';
import CloseIcon from '@svg/tokens/small/closedCircle.svg';

const Digest = () => {
    const { dlBaseStyles, dtBaseStyles, ddBaseStyles } = useListCSS();
    const { basicFieldCSS } = useFieldCSS();
    const [search, setSearch] = useState('');
    const inputRef = useRef<HTMLInputElement>(null);

    return (
        <Block>
            <Form
                initialValues={{
                    note: '',
                }}
                onSubmit={values => {
                    console.log(values);
                }}
            >
                <Block.Header>
                    <h2>Текущий отчетный период Февраяль 2021</h2>
                    <div css={{ display: 'flex', button: { marginRight: scale(2) } }}>
                        <Button size="sm" type="submit">
                            Сохранить
                        </Button>
                        <div css={{ position: 'relative' }}>
                            <input
                                ref={inputRef}
                                value={search}
                                onChange={e => setSearch(e.target.value)}
                                type="text"
                                id="searchAttr"
                                css={{ height: '100%', ...basicFieldCSS }}
                                placeholder="Поиск"
                            />
                            {search.length > 0 ? (
                                <button
                                    type="reset"
                                    title="Очистить"
                                    css={{
                                        position: 'absolute',
                                        bottom: 0,
                                        right: 0,
                                        height: scale(7, true),
                                        width: scale(7, true),
                                    }}
                                    onClick={() => {
                                        setSearch('');
                                        inputRef?.current?.focus();
                                    }}
                                >
                                    <CloseIcon />
                                </button>
                            ) : null}
                        </div>
                    </div>
                </Block.Header>
                {[...new Array(5).keys()].map(item => (
                    <Block.Body key={item}>
                        <dl css={{ ...dlBaseStyles, gridTemplateColumns: '200px 1fr' }}>
                            <dt css={dtBaseStyles}>Товаров на витрине</dt>
                            <dd css={ddBaseStyles}>0</dd>
                            <dt css={dtBaseStyles}>Приянто заказов</dt>
                            <dd css={ddBaseStyles}>0</dd>
                            <dt css={dtBaseStyles}>Доставлено заказов</dt>
                            <dd css={ddBaseStyles}>0</dd>
                            <dt css={dtBaseStyles}>Продано заказов</dt>
                            <dd css={ddBaseStyles}>0</dd>
                            <dt css={dtBaseStyles}>Начислено комиссии</dt>
                            <dd css={ddBaseStyles}>0 руб.</dd>
                            <dt css={dtBaseStyles}>Примечание к продавцу</dt>
                            <dd css={ddBaseStyles}>
                                <Form.Field name="note">
                                    <Textarea rows={3} />
                                </Form.Field>
                            </dd>
                        </dl>
                    </Block.Body>
                ))}
            </Form>
        </Block>
    );
};

export default Digest;
