import React, { useState } from 'react';
import { Button, scale, Layout, useTheme } from '@greensight/gds';
import { FieldArray, useFormikContext } from 'formik';
import Form from '@standart/Form';
import Block from '@components/Block';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';
import MultiSelect from '@standart/MultiSelect';
import useListCSS from '@scripts/useListCSS';
import typography from '@scripts/typography';
import { SELLER_COMMISSION_TYPES } from '@scripts/data/different';
import PlusIcon from '@svg/plus.svg';
import RemoveIcon from '@svg/tokens/small/trash.svg';
import { mockCategories } from '@scripts/mock';

const types = [
    ...[{ label: 'Выберите тип', value: 'Выберите тип' }],
    ...SELLER_COMMISSION_TYPES.map(i => ({ label: i, value: i })),
];
const categories = [{ label: 'Выберите категорию', value: '' }, ...mockCategories.map(i => ({ label: i, value: i }))];

const FieldArrayBlock = () => {
    const {
        values: { commissionArray },
    } = useFormikContext<{
        commissionArray: {
            type: { label: string; value: string };
            commission: number;
            product: { label: string; value: string };
            date: Date[];
        }[];
    }>();

    const { colors } = useTheme();
    const titleCSS = {
        ...typography('bodySmBold'),
    };

    const trCSS = {
        ':not(:last-child)': { borderBottom: `1px solid ${colors?.grey400}` },
    };

    const itemCSS = {
        padding: `${scale(3, true)}px`,
    };
    return (
        <>
            <Layout cols={[2, 1, 3, 3, `minmax(180px, 2fr)`]} gap={0} css={trCSS}>
                <Layout.Item css={{ ...titleCSS, ...itemCSS }}>Тип</Layout.Item>
                <Layout.Item css={{ ...titleCSS, ...itemCSS }}>Комиссия</Layout.Item>
                <Layout.Item css={{ ...titleCSS, ...itemCSS }}>Бренд/Категория/Товар</Layout.Item>
                <Layout.Item css={{ ...titleCSS, ...itemCSS }}>Даты активности</Layout.Item>
            </Layout>
            <FieldArray
                name="commissionArray"
                render={({ push, remove }) =>
                    commissionArray.map((c, index) => {
                        const isNewItem = commissionArray.length === index + 1;
                        return (
                            <Layout
                                cols={[2, 1, 3, 3, `minmax(180px, 2fr)`]}
                                gap={0}
                                align={isNewItem ? 'end' : 'start'}
                                css={trCSS}
                                key={`commission-item-${index}`}
                            >
                                <Layout.Item css={{ ...itemCSS }}>
                                    {isNewItem ? (
                                        <Form.Field name={`commissionArray[${index}][type]`}>
                                            <MultiSelect isMulti={false} options={types} />
                                        </Form.Field>
                                    ) : (
                                        <p>{c.type.label}</p>
                                    )}
                                </Layout.Item>
                                <Layout.Item css={{ ...itemCSS }}>
                                    <Form.Field name={`commissionArray[${index}][commission]`} />
                                </Layout.Item>
                                <Layout.Item css={{ ...itemCSS }}>
                                    {isNewItem ? (
                                        <Form.Field name={`commissionArray[${index}][product]`}>
                                            <MultiSelect isMulti={false} options={categories} />
                                        </Form.Field>
                                    ) : (
                                        <p>Категория: {c.product.label}</p>
                                    )}
                                </Layout.Item>
                                <Layout.Item css={{ ...itemCSS }}>
                                    <Form.Field name={`commissionArray[${index}][date]`}>
                                        <DatepickerRange />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item css={{ display: 'flex', ...itemCSS }}>
                                    {isNewItem && (
                                        <Button
                                            size="sm"
                                            theme="outline"
                                            hidden
                                            Icon={PlusIcon}
                                            onClick={() =>
                                                push({
                                                    type: { label: '', value: '' },
                                                    commission: '',
                                                    product: { label: '', value: '' },
                                                    date: '',
                                                })
                                            }
                                        >
                                            Добавить
                                        </Button>
                                    )}
                                    {!isNewItem && (
                                        <Button
                                            size="sm"
                                            theme="outline"
                                            onClick={() => console.log('Сохранить')}
                                            type="submit"
                                        >
                                            Сохранить
                                        </Button>
                                    )}
                                    {!isNewItem && (
                                        <Button
                                            size="sm"
                                            theme="outline"
                                            hidden
                                            Icon={RemoveIcon}
                                            css={{ marginLeft: scale(1) }}
                                            onClick={() => remove(index)}
                                        >
                                            Удалить
                                        </Button>
                                    )}
                                </Layout.Item>
                            </Layout>
                        );
                    })
                }
            />
        </>
    );
};

const Commission = () => {
    const { dlBaseStyles, dtBaseStyles, ddBaseStyles } = useListCSS();
    const [startDate, setStartDate] = useState<Date | null>(null);
    const [endDate, setEndDate] = useState<Date | null>(null);

    return (
        <>
            <DatepickerStyles />
            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        sellerCommission: '',
                        commissionArray: [
                            {
                                type: '',
                                commission: '',
                                product: '',
                                date: '',
                            },
                        ],
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Header>
                        <h2>Фильтр</h2>
                        <div css={{ display: 'flex', justifyContent: 'flex-end', button: { marginLeft: scale(2) } }}>
                            <Button theme="outline" size="sm">
                                Удалить
                            </Button>
                            <Button size="sm" type="submit">
                                Сохранить
                            </Button>
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <dl css={{ ...dlBaseStyles, gridTemplateColumns: '200px 1fr' }}>
                            <dt css={dtBaseStyles}>Комиссия продавца, %</dt>
                            <dd css={{ ...ddBaseStyles, display: 'flex' }}>
                                <Form.Field name="sellerCommission" />
                            </dd>
                        </dl>
                    </Block.Body>
                    <Block.Body>
                        <FieldArrayBlock />
                    </Block.Body>
                </Form>
            </Block>
        </>
    );
};

export default Commission;
