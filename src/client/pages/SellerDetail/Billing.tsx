import React, { useState, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import Form from '@standart/Form';
import Block from '@components/Block';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import Table from '@components/Table';
import Popup from '@standart/Popup';
import Textarea from '@standart/Textarea';
import Pagination from '@standart/Pagination';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';
import { makeSellerBilling } from '@scripts/mock';

const COLUMNS = [
    {
        Header: 'ID заказа/транзакции',
        accessor: 'id',
    },
    {
        Header: 'Дата',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Комментарий',
        accessor: 'comment',
    },
    {
        Header: 'Сумма',
        accessor: 'sum',
    },
    {
        Header: 'Операция',
        accessor: 'operation',
    },
];

const Billing = () => {
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);

    const [isCorrectingPopupOpen, setIsCorrectingPopupOpen] = useState(false);

    const data = useMemo(() => makeSellerBilling(10), []);

    const dataTable = [
        {
            id: 'Всего',
            date: '',
            comment: '',
            sum: data.reduce((acc, { sum }) => (acc += sum), 0 as number),
            operation: (
                <Button size="sm" onClick={() => setIsCorrectingPopupOpen(true)}>
                    Корректировка
                </Button>
            ),
        },
        ...data,
    ];

    console.log(dataTable);

    return (
        <>
            <Block css={{ marginBottom: scale(3) }}>
                <DatepickerStyles />
                <Form
                    initialValues={{
                        period: '',
                        date: [null, null],
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Header>
                        <h2>Настройка</h2>
                        <div css={{ button: { marginLeft: scale(2) } }}>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Сохранить
                            </Button>
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Form.Field
                            name="period"
                            label="Биллинговый период, кол-во дней"
                            type="number"
                            css={{ maxWidth: scale(40) }}
                        />
                    </Block.Body>
                </Form>
                <Form
                    initialValues={{
                        period: '',
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Header>
                        <h2>Отчеты</h2>
                        <div css={{ button: { marginLeft: scale(2) } }}>
                            <Button size="sm" theme="primary" type="submit">
                                Сделать внеочередной биллинг
                            </Button>
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Layout cols={2} gap={scale(1)} css={{ maxWidth: scale(60) }}>
                            <Layout.Item col={2}>
                                <Form.Field name="date" label="Дата">
                                    <DatepickerRange />
                                </Form.Field>
                            </Layout.Item>
                        </Layout>
                    </Block.Body>
                </Form>

                <Block.Body>
                    <Table columns={COLUMNS} data={dataTable} handleRowInfo={() => console.log('click')} />
                    {/* <Pagination url={pathname} activePage={activePage} pages={7} /> */}
                </Block.Body>
            </Block>
            <Popup
                isOpen={isCorrectingPopupOpen}
                onRequestClose={() => {
                    setIsCorrectingPopupOpen(false);
                }}
                title="Корректировка биллинга"
                popupCss={{ minWidth: scale(50) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        sum: '',
                        comment: '',
                    }}
                >
                    <Layout cols={1} gap={scale(2)}>
                        <Layout.Item col={1}>
                            <Form.Field type="number" name="sum" label="Сумма" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="message" label="Сообщение">
                                <Textarea rows={3} />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Button type="submit" size="sm" theme="primary">
                                Сохранить
                            </Button>
                        </Layout.Item>
                    </Layout>
                </Form>
            </Popup>
        </>
    );
};

export default Billing;
