import React, { useMemo, useState } from 'react';
import { useLocation } from 'react-router-dom';
import Form from '@standart/Form';
import Block from '@components/Block';
import Table from '@components/Table';
import MultiSelect from '@standart/MultiSelect';
import Legend from '@standart/Legend';
import Pagination from '@standart/Pagination';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';
import { Button, scale, Layout, useTheme } from '@greensight/gds';
import { makeSellerOffers, mockDeliveryCompanies, mockLocations } from '@scripts/mock';
import typography from '@scripts/typography';
import { DELIVERY_STATUSES, DELIVERY_TYPE, DELIVERY_METHOD } from '@scripts/data/different';

const statuses = DELIVERY_STATUSES.map(i => ({ label: i, value: i }));
const deliveryTypes = DELIVERY_TYPE.map(i => ({ label: i, value: i }));
const deliveryMethods = DELIVERY_METHOD.map(i => ({ label: i, value: i }));
const deliveryCompanies = mockDeliveryCompanies.map(i => ({ label: i, value: i }));
const stores = mockLocations.map(i => ({ label: i, value: i }));

const COLUMNS = [
    {
        Header: '№ заказа',
        accessor: 'orderNumber',
    },
    {
        Header: '№ отправления',
        accessor: 'departureNumber',
    },
    {
        Header: 'ФИО + ID клиента',
        accessor: 'name',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'Статус отправления текущий',
        accessor: 'status',
    },
    {
        Header: 'Кол-во коробок',
        accessor: 'quantity',
    },
    {
        Header: 'Вес отправления',
        accessor: 'weight',
    },
    {
        Header: 'Сумма',
        accessor: 'sum',
    },
    {
        Header: 'Тип доставки',
        accessor: 'deliveryType',
    },
    {
        Header: 'Способ доставки',
        accessor: 'deliveryMethod',
    },
    {
        Header: 'ЛО - последняя миля',
        accessor: 'deliveryCompanyLast',
    },
    {
        Header: 'ЛО - нулевая миля',
        accessor: 'deliveryComplanyZero',
    },
    {
        Header: 'Склад',
        accessor: 'store',
    },
    {
        Header: 'Адрес прибытия',
        accessor: 'address',
    },
    {
        Header: 'PSD - Дата отгрузки отправления',
        accessor: 'shippingDate',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'PSD - Дата доставки отправления плановая',
        accessor: 'deliveryDate',
        getProps: () => ({ type: 'date' }),
    },
];

const Offers = () => {
    const data = useMemo(() => makeSellerOffers(10), []);
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);

    const { colors } = useTheme();

    const [moreFilters, setMoreFilters] = useState(true);

    return (
        <>
            <DatepickerStyles />
            <Block>
                <Form
                    initialValues={{
                        orderId: '',
                        departureId: '',
                        status: '',
                        problem: '',
                        clientId: '',
                        name: '',
                        qtyFrom: '',
                        qtyTo: '',
                        weightTo: '',
                        weightFrom: '',
                        priceFrom: '',
                        priceTo: '',
                        deliveryType: '',
                        deliveryMethod: '',
                        deliveryCompanyLast: '',
                        deliveryCompanyZero: '',
                        store: '',
                        index: '',
                        region: '',
                        city: '',
                        street: '',
                        house: '',
                        entrance: '',
                        floor: '',
                        apartment: '',
                        shippingDate: [null, null],
                        deliveryDate: [null, null],
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                    css={{ marginBottom: scale(3) }}
                >
                    <Block.Header>
                        <h2>Фильтр</h2>
                        <div css={{ button: { marginLeft: scale(2) } }}>
                            <button
                                type="button"
                                css={{ color: colors?.primary, ...typography('bodySm') }}
                                onClick={() => setMoreFilters(!moreFilters)}
                            >
                                {moreFilters ? 'Меньше' : 'Больше'} фильтров
                            </button>
                            <Form.Reset size="sm" theme="secondary" type="button">
                                Очистить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Layout cols={16} gap={scale(2)}>
                            <Layout.Item col={4}>
                                <Form.Field name="orderId" label="ID заказа" type="number" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="departureId" label="ID заказа" type="number" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="status" label="Статус отправления текущий">
                                    <MultiSelect isMulti={false} options={statuses} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="problem" label="Проблемное/нет проблем">
                                    <MultiSelect
                                        isMulti={false}
                                        options={[
                                            { label: 'Не выбрано', value: '' },
                                            { label: 'Проблемное', value: 'problem' },
                                            { label: 'Нет проблем', value: 'noProblem' },
                                        ]}
                                    />
                                </Form.Field>
                            </Layout.Item>
                            {moreFilters && (
                                <>
                                    <Layout.Item col={4}>
                                        <Form.Field name="clientId" label="ID клиента" type="number" />
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="name" label="ФИО" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field
                                            name="qtyFrom"
                                            label="Кол-во коробок, шт"
                                            placeholder="от"
                                            type="number"
                                        />
                                    </Layout.Item>
                                    <Layout.Item col={2} align="end">
                                        <Form.Field name="qtyTo" placeholder="до" type="number" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field
                                            name="weightFrom"
                                            label="Вес отправления, кг"
                                            placeholder="от"
                                            type="number"
                                        />
                                    </Layout.Item>
                                    <Layout.Item col={2} align="end">
                                        <Form.Field name="weightTo" placeholder="до" type="number" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field
                                            name="priceFrom"
                                            label="Сумма товаров, руб"
                                            placeholder="от"
                                            type="number"
                                        />
                                    </Layout.Item>
                                    <Layout.Item col={2} align="end">
                                        <Form.Field name="priceTo" placeholder="до" type="number" />
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="deliveryType" label="Тип доставки">
                                            <MultiSelect isMulti={false} options={deliveryTypes} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="deliveryMethod" label="Способ доставки">
                                            <MultiSelect isMulti={false} options={deliveryMethods} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="deliveryCompanyLast" label="ЛО - последняя миля">
                                            <MultiSelect isMulti={false} options={deliveryCompanies} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="deliveryCompanyZero" label="ЛО - нулевая миля">
                                            <MultiSelect isMulti={false} options={deliveryCompanies} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="store" label="Склад отгрузки">
                                            <MultiSelect isMulti={false} options={stores} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={16} css={{ marginTop: scale(1) }}>
                                        <h3>Адрес доставки:</h3>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="index" label="Индекс" />
                                    </Layout.Item>
                                    <Layout.Item col={3}>
                                        <Form.Field name="region" label="Регион" />
                                    </Layout.Item>
                                    <Layout.Item col={3}>
                                        <Form.Field name="city" label="Город" />
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="street" label="Улица" />
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="house" label="Дом" />
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="entrance" label="Подъезд" />
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="floor" label="Этаж" />
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="apartment" label="Кв." />
                                    </Layout.Item>
                                    <Layout.Item col={8}>
                                        <Form.Field name="deliveryDate" label="PSD - Дата отгрузки отправления">
                                            <DatepickerRange />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={8}>
                                        <Form.Field
                                            name="shippingDate"
                                            label="PDD - Дата доставки отправления плановая"
                                        >
                                            <DatepickerRange />
                                        </Form.Field>
                                    </Layout.Item>
                                </>
                            )}
                        </Layout>
                    </Block.Body>
                </Form>

                <Block.Body>
                    <div>
                        <Table
                            columns={COLUMNS}
                            data={data}
                            handleRowInfo={() => console.log('click')}
                            headerCellCSS={{ whiteSpace: 'normal' }}
                        />
                    </div>
                    <Pagination url={pathname} activePage={activePage} pages={7} />
                </Block.Body>
            </Block>
        </>
    );
};

export default Offers;
