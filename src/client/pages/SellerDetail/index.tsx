import React from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import Form from '@standart/Form';
import MultiSelect from '@standart/MultiSelect';
import { RATING, SELLER_STATUSES } from '@scripts/data/different';
import Tabs from '@standart/Tabs';
import Digest from './Digest';
import Information from './Information';
import Stores from './Stores';
import Commission from './Commission';
import SellerTeam from './SellerTeam';
import Products from './Products';
import Offers from './Offers';
import Marketing from './Marketing';
import Billing from './Billing';

const ratings = RATING.map(i => ({ label: i, value: i }));
const statuses = SELLER_STATUSES.map(i => ({ label: i, value: i }));

const SellerDetail = () => {
    const { colors } = useTheme();

    const getLayoutItemStyle = (cols: number | number[] = [1, 3], gap = scale(1, true)) => ({
        display: 'grid',
        gridGap: `${gap}px`,
        gridTemplateColumns: typeof cols === 'number' ? `repeat(${cols}, 1fr)` : cols.map(col => `${col}fr`).join(' '),
        padding: `${scale(3, true)}px 0`,
        ':not(:last-child)': { borderBottom: `1px solid ${colors?.grey200}` },
    });

    return (
        <PageWrapper h1="Название продавца">
            <>
                <Block css={{ marginBottom: scale(3) }}>
                    <Form
                        initialValues={{
                            organizationName: 'Название организации',
                            city: '',
                            manager: '',
                            rating: '',
                            status: '',
                        }}
                        onSubmit={values => {
                            console.log(values);
                        }}
                    >
                        <Block.Body>
                            <Layout cols={1} gap={0} css={{ ...typography('bodyMd') }}>
                                <Layout.Item col={1} css={{ ...getLayoutItemStyle() }}>
                                    <span>
                                        <b>Название организации</b>
                                    </span>
                                    <Form.Field name="organizationName" />
                                </Layout.Item>
                                <Layout.Item col={1} css={{ ...getLayoutItemStyle() }}>
                                    <span>
                                        <b>Город</b>
                                    </span>
                                    <Form.Field name="city" />
                                </Layout.Item>
                                <Layout.Item col={1} css={{ ...getLayoutItemStyle() }}>
                                    <span>
                                        <b>ФИО</b>
                                    </span>
                                    <span>Тестовый Тест Тестович</span>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ ...getLayoutItemStyle(4, scale(2)) }}>
                                    <span>
                                        <b>E-mail</b>
                                    </span>
                                    <span>te@st.test</span>
                                    <span>
                                        <b>Телефон</b>
                                    </span>
                                    <span>+70000000000</span>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ ...getLayoutItemStyle(4, scale(2)) }}>
                                    <span>
                                        <b>Статус</b>
                                    </span>
                                    <Form.Field name="status">
                                        <MultiSelect isMulti={false} options={statuses} />
                                    </Form.Field>
                                    <span>
                                        <b>Дата получения статуса</b>
                                    </span>
                                    <span>2021-02-19 16:42:18</span>
                                </Layout.Item>
                                <Layout.Item col={1} css={{ ...getLayoutItemStyle(4, scale(2)) }}>
                                    <span>
                                        <b>Рейтинг</b>
                                    </span>
                                    <Form.Field name="rating">
                                        <MultiSelect isMulti={false} options={ratings} />
                                    </Form.Field>
                                    <span>
                                        <b>Менеджер</b>
                                    </span>
                                    <Form.Field name="manager">
                                        <MultiSelect isMulti={false} options={[{ label: '-', value: '-' }]} />
                                    </Form.Field>
                                </Layout.Item>
                            </Layout>
                        </Block.Body>
                        <Block.Footer css={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <Form.Reset size="sm" theme="secondary" type="button">
                                Сбросить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </Block.Footer>
                    </Form>
                </Block>
                <Tabs>
                    <Tabs.List>
                        <Tabs.Tab>Дайджест</Tabs.Tab>
                        <Tabs.Tab>Информация</Tabs.Tab>
                        <Tabs.Tab>Склады</Tabs.Tab>
                        <Tabs.Tab>Комиссия</Tabs.Tab>
                        <Tabs.Tab>Команда продавца</Tabs.Tab>
                        <Tabs.Tab>Товары</Tabs.Tab>
                        <Tabs.Tab>Заказы</Tabs.Tab>
                        <Tabs.Tab>Маркетинг</Tabs.Tab>
                        <Tabs.Tab>Биллинг</Tabs.Tab>
                    </Tabs.List>
                    <Tabs.Panel>
                        <Digest />
                    </Tabs.Panel>
                    <Tabs.Panel>
                        <Information />
                    </Tabs.Panel>
                    <Tabs.Panel>
                        <Stores />
                    </Tabs.Panel>
                    <Tabs.Panel>
                        <Commission />
                    </Tabs.Panel>
                    <Tabs.Panel>
                        <SellerTeam />
                    </Tabs.Panel>
                    <Tabs.Panel>
                        <Products />
                    </Tabs.Panel>
                    <Tabs.Panel>
                        <Offers />
                    </Tabs.Panel>
                    <Tabs.Panel>
                        <Marketing />
                    </Tabs.Panel>
                    <Tabs.Panel>
                        <Billing />
                    </Tabs.Panel>
                </Tabs>
            </>
        </PageWrapper>
    );
};

export default SellerDetail;
