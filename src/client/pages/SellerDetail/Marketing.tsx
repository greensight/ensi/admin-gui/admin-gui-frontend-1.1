import React, { useState, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import Form from '@standart/Form';
import Block from '@components/Block';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import Checkbox from '@standart/Checkbox';
import Tabs from '@standart/Tabs';
import Table from '@components/Table';
import Legend from '@standart/Legend';
import Pagination from '@standart/Pagination';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';
import MultiSelect from '@standart/MultiSelect';
import { mockPromoServices, mockPosts, mockNames, makeSellerDiscount, makeSellerPromo } from '@scripts/mock';
import { PROMO_STATUSES, PROMO_TYPES } from '@scripts/data/different';

const statuses = PROMO_STATUSES.map(i => ({ label: i, value: i }));
const services = mockPromoServices.map(i => ({ label: i, value: i }));
const roles = mockPosts.map(i => ({ label: i, value: i }));
const promoTypes = [{ label: '-', value: '' }, ...PROMO_TYPES.map(i => ({ label: i, value: i }))];
const users = mockNames.map(i => ({ label: i, value: i }));

const sponsors = [
    { label: '-', value: '' },
    { label: 'Маркетплейс', value: 'Маркетплейс' },
    { label: 'Продавец', value: 'Продавец' },
];

const COLUMNS_DISCOUNTS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Дата создания',
        accessor: 'dateCreation',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: 'Скидка',
        accessor: 'discount',
    },
    {
        Header: 'Период действия',
        accessor: 'discountPeriod',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Инициатор',
        accessor: 'initiator',
    },
    {
        Header: 'Автор',
        accessor: 'author',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
        /**Статусы не внесены в метод getStatusType */
    },
];

const COLUMNS_PROMO = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Дата создания',
        accessor: 'dateCreation',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: 'Тип',
        accessor: 'type',
    },
    {
        Header: 'Период действия',
        accessor: 'promoPeriod',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Спонсор',
        accessor: 'sponsor',
    },
    {
        Header: 'Пользователь',
        accessor: 'user',
    },
    {
        Header: 'Реферальный партнер',
        accessor: 'partner',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
];

const Marketing = () => {
    const { colors } = useTheme();
    const [moreFilters, setMoreFilters] = useState(true);

    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);

    const discountData = useMemo(() => makeSellerDiscount(10), []);
    const promoData = useMemo(() => makeSellerPromo(10), []);

    return (
        <>
            <Block css={{ marginBottom: scale(3) }}>
                <DatepickerStyles />
                <Tabs>
                    <div css={{ padding: scale(2) }}>
                        <Tabs.List>
                            <Tabs.Tab>Скидки продавца</Tabs.Tab>
                            <Tabs.Tab>Промокоды продавца</Tabs.Tab>
                        </Tabs.List>
                    </div>

                    <Tabs.Panel>
                        <Form
                            initialValues={{
                                id: '',
                                title: '',
                                status: '',
                                service: '',
                                role: '',
                                initiator: '',
                                author: '',
                                dateCreation: [null, null],
                                discountPeriod: [null, null],
                            }}
                            onSubmit={values => {
                                console.log(values);
                            }}
                        >
                            <Block.Header>
                                <h2>Скидки продавца</h2>
                                <div css={{ button: { marginLeft: scale(2) } }}>
                                    <button
                                        type="button"
                                        css={{ color: colors?.primary, marginLeft: scale(2) }}
                                        onClick={() => setMoreFilters(!moreFilters)}
                                    >
                                        {moreFilters ? 'Меньше' : 'Больше'} фильтров
                                    </button>
                                    <Form.Reset size="sm" theme="secondary" type="button">
                                        Очистить
                                    </Form.Reset>
                                    <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                        Применить
                                    </Button>
                                </div>
                            </Block.Header>
                            <Block.Body>
                                <Layout cols={8}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="id" label="ID" type="number" />
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="title" label="Название" />
                                    </Layout.Item>
                                    <Layout.Item col={3}>
                                        <Form.Field name="status" label="Статус">
                                            <MultiSelect isMulti={false} options={statuses} />
                                        </Form.Field>
                                    </Layout.Item>
                                    {moreFilters && (
                                        <>
                                            <Layout.Item col={2}>
                                                <Form.Field name="service" label="Скидки на">
                                                    <MultiSelect isMulti={false} options={services} />
                                                </Form.Field>
                                            </Layout.Item>
                                            <Layout.Item col={2}>
                                                <Form.Field name="role" label="Роль">
                                                    <MultiSelect isMulti={false} options={roles} />
                                                </Form.Field>
                                            </Layout.Item>
                                            <Layout.Item col={2}>
                                                <Form.Field name="initiator" label="Инициатор" />
                                            </Layout.Item>
                                            <Layout.Item col={2}>
                                                <Form.Field name="author" label="Автор" />
                                            </Layout.Item>
                                            <Layout.Item col={4}>
                                                <Form.Field name="dateCreation" label="Дата создания">
                                                    <DatepickerRange />
                                                </Form.Field>
                                            </Layout.Item>
                                            <Layout.Item col={4}>
                                                <Form.Field name="discountPeriod" label="Период действия скидки">
                                                    <DatepickerRange />
                                                </Form.Field>
                                            </Layout.Item>
                                        </>
                                    )}
                                </Layout>
                            </Block.Body>
                        </Form>
                        <Block.Body
                            css={{ display: 'flex', marginBottom: scale(3), button: { marginRight: scale(2) } }}
                        >
                            <Button size="sm">Создать скидку</Button>
                            <Button size="sm">Редактировать скидку</Button>
                            <Button theme="outline" size="sm">
                                Удалить скидку
                            </Button>
                            <Button size="sm">Изменить статус скидки</Button>
                        </Block.Body>
                        <Block.Body>
                            <Table
                                columns={COLUMNS_DISCOUNTS}
                                data={discountData}
                                handleRowInfo={() => console.log('click')}
                            />
                            <Pagination url={pathname} activePage={activePage} pages={7} />
                        </Block.Body>
                    </Tabs.Panel>
                    <Tabs.Panel>
                        <Form
                            initialValues={{
                                indefinite: '',
                                dateCreation: [null, null],
                                discountPeriod: [null, null],
                            }}
                            onSubmit={values => {
                                console.log(values);
                            }}
                        >
                            <Block.Header>
                                <h2>Промокоды продавца</h2>
                                <div css={{ button: { marginLeft: scale(2) } }}>
                                    <Form.Reset size="sm" theme="secondary" type="button">
                                        Очистить
                                    </Form.Reset>
                                    <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                        Применить
                                    </Button>
                                </div>
                            </Block.Header>
                            <Block.Body>
                                <Layout cols={8}>
                                    <Layout.Item col={2}>
                                        <Form.Field name="dateCreation" label="Дата создания">
                                            <DatepickerRange />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="discountPeriod" label="Период действия скидки">
                                            <Legend />
                                            <DatepickerRange />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="indefinite" label="Бессрочный">
                                            <Checkbox>Да</Checkbox>
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="promoType" label="Тип">
                                            <MultiSelect isMulti={false} options={promoTypes} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="sponsor" label="Спонсор">
                                            <MultiSelect isMulti={false} options={sponsors} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="status" label="Статус">
                                            <MultiSelect isMulti={false} options={statuses} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="user" label="Пользователи">
                                            <MultiSelect isMulti={false} options={users} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="id" label="ID" type="number" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="title" label="Название" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="code" label="Код" type="nubmer" />
                                    </Layout.Item>
                                    <Layout.Item col={2}>
                                        <Form.Field name="partner" label="Реферальный партнер">
                                            <MultiSelect isMulti={false} options={[{ label: '-', value: '' }]} />
                                        </Form.Field>
                                    </Layout.Item>
                                </Layout>
                            </Block.Body>
                        </Form>
                        <Block.Body>
                            <Table
                                columns={COLUMNS_PROMO}
                                data={promoData}
                                handleRowInfo={() => console.log('click')}
                                headerCellCSS={{ whiteSpace: 'normal' }}
                            />
                            <Pagination url={pathname} activePage={activePage} pages={7} />
                        </Block.Body>
                    </Tabs.Panel>
                </Tabs>
            </Block>
        </>
    );
};

export default Marketing;
