import React from 'react';
import Form from '@standart/Form';
import Block from '@components/Block';
import { Button, scale } from '@greensight/gds';
import Textarea from '@standart/Textarea';
import useListCSS from '@scripts/useListCSS';
import MultiSelect from '@standart/MultiSelect';
import { sellers as sellersMock, mockCategories } from '@scripts/mock';

const sellers = sellersMock.map(i => ({ label: i, value: i }));
const categories = mockCategories.map(i => ({ label: i, value: i }));

const Information = () => {
    const { dlBaseStyles, dtBaseStyles, ddBaseStyles } = useListCSS();

    return (
        <Form
            initialValues={{
                storeAddresses: '',
                brands: '',
                categories: '',
                nds: '',
                commercialCondition: '',
                legalAddress: '',
                actualAddress: '',
                kpp: '',
                inn: '',
                surname: '',
                name: '',
                middleName: '',
                contractNumber: '',
                contractDate: '',
                bankNumber: '',
                bankName: '',
                bankCorrespondentNumber: '',
                bankLegalAddress: '',
                bikBank: '',
            }}
            onSubmit={values => {
                console.log(values);
            }}
        >
            <Block>
                <Block.Header>
                    <h2>Основная информация</h2>
                    <div css={{ button: { marginLeft: scale(2) } }}>
                        <Form.Reset
                            size="sm"
                            theme="secondary"
                            type="button"
                            onClick={() => {
                                console.log('reset');
                            }}
                        >
                            Сбросить
                        </Form.Reset>
                        <Button size="sm" type="submit">
                            Сохранить
                        </Button>
                    </div>
                </Block.Header>
                <Block.Body>
                    <dl css={{ ...dlBaseStyles, gridTemplateColumns: '200px 1fr' }}>
                        <dt css={dtBaseStyles}>ID</dt>
                        <dd css={ddBaseStyles}>43</dd>
                        <dt css={dtBaseStyles}>Дата регистрации</dt>
                        <dd css={ddBaseStyles}>22 декабря 2020г., 11:27</dd>
                        <dt css={dtBaseStyles}>Адреса складов отгрузки*</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="storeAddresses">
                                <Textarea rows={3} />
                            </Form.Field>
                        </dd>
                        <dt css={dtBaseStyles}>Бренды и товарные категории*</dt>
                        <dd
                            css={{
                                display: 'grid',
                                gridTemplateColumns: '1fr 1fr',
                                gridGap: scale(2),
                                ...ddBaseStyles,
                            }}
                        >
                            <Form.Field name="brands" label="Бренды">
                                <MultiSelect options={sellers} css={{ marginBottom: scale(2) }} />
                            </Form.Field>
                            <Form.Field name="categories" label="Категории">
                                <MultiSelect options={categories} />
                            </Form.Field>
                        </dd>
                        <dt css={dtBaseStyles}>Начислено комиссии</dt>
                        <dd css={ddBaseStyles}>0 руб.</dd>
                        <dt css={dtBaseStyles}>Ставка НДС, тип налогообложения</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="nds" type="number" />
                        </dd>
                        <dt css={dtBaseStyles}>Коммерческое условия</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="commercialCondition">
                                <Textarea rows={3} />
                            </Form.Field>
                        </dd>
                    </dl>
                </Block.Body>
                <Block.Header>
                    <h2>Реквизиты юридического лица</h2>
                </Block.Header>
                <Block.Body>
                    <dl css={{ ...dlBaseStyles, gridTemplateColumns: '200px 1fr' }}>
                        <dt css={dtBaseStyles}>Юридический адрес*</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="legalAddress" />
                        </dd>
                        <dt css={dtBaseStyles}>Фактический адрес*</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="actualAddress" />
                        </dd>
                        <dt css={dtBaseStyles}>ИНН*</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="inn" />
                        </dd>
                        <dt css={dtBaseStyles}>КПП*</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="kpp" />
                        </dd>
                    </dl>
                </Block.Body>
                <Block.Header>
                    <h2>ФИО генерального</h2>
                </Block.Header>
                <Block.Body>
                    <dl css={{ ...dlBaseStyles, gridTemplateColumns: '200px 1fr' }}>
                        <dt css={dtBaseStyles}>Фамилия</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="surname" />
                        </dd>
                        <dt css={dtBaseStyles}>Имя</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="name" />
                        </dd>
                        <dt css={dtBaseStyles}>Отчество</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="middleName" />
                        </dd>
                    </dl>
                </Block.Body>

                <Block.Header>
                    <h2>Банковские реквизиты</h2>
                </Block.Header>
                <Block.Body>
                    <dl css={{ ...dlBaseStyles, gridTemplateColumns: '200px 1fr' }}>
                        <dt css={dtBaseStyles}>Номер банковского счета*</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="bankNumber" />
                        </dd>
                        <dt css={dtBaseStyles}>Банк*</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="bankName" />
                        </dd>
                        <dt css={dtBaseStyles}>Номер корреспондентского счета банка*</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="bankCorrespondentNumber" />
                        </dd>
                        <dt css={dtBaseStyles}>Юридический адрес банка*</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="bankLegalAddress" />
                        </dd>
                        <dt css={dtBaseStyles}>БИК банка*</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="bikBank" />
                        </dd>
                    </dl>
                </Block.Body>
                <Block.Header>
                    <h2>Документы</h2>
                </Block.Header>
                <Block.Body>
                    <dl css={{ ...dlBaseStyles, gridTemplateColumns: '200px 1fr' }}>
                        <dt css={dtBaseStyles}>Номер договора</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="contractNumber" />
                        </dd>
                        <dt css={dtBaseStyles}>Дата договора</dt>
                        <dd css={ddBaseStyles}>
                            <Form.Field name="contractDate" />
                        </dd>
                        <dt css={dtBaseStyles}>Документы</dt>
                        <dd css={ddBaseStyles}>-</dd>
                    </dl>
                </Block.Body>
            </Block>
        </Form>
    );
};

export default Information;
