import React from 'react';
import { Button, scale } from '@greensight/gds';
import { CSSObject } from '@emotion/core';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Table from '@components/Table';
import { makeRandomData, getRandomItem } from '@scripts/mock';
import Popup from '@standart/Popup';
import Form from '@standart/Form';
import Textarea from '@standart/Textarea';
import Mask from '@standart/Mask';
import Select from '@standart/Select';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import { Flatten, prepareForSelect } from '@scripts/helpers';
import { maskPhone } from '@scripts/mask';
import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';
import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import usePopupState from '@scripts/usePopupState';
import { ActionType } from '@scripts/enums';

const columns = [
    {
        Header: 'Название и описание',
        accessor: 'title',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'E-mail / Телефон',
        accessor: 'contacts',
        getProps: () => ({ type: 'array' }),
    },
    {
        Header: 'Иконка',
        accessor: 'icon',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: 'Тип',
        accessor: 'type',
        getProps: () => ({ type: 'status' }),
    },
];

const types = prepareForSelect(['E-mail', 'Социальная сеть', 'Номер телефона', 'Мессенджер', 'Другое']);

const getTableItem = (id: number) => {
    return {
        id: `100${id}`,
        title: getRandomItem([
            ['Ромашка', ''],
            ['Тестовое название', 'Тестовое описание'],
        ]),
        contacts: getRandomItem([['8-495-934-12-32', 'test@test.test'], ['8-495-934-12-32'], ['', 'test@test.test']]),
        icon: getRandomItem(['', 'https://placehold.it/50x50']),
        type: getRandomItem(['Номер телефона', 'Социальная сеть']),
    };
};

const initialState = {
    title: '',
    description: '',
    mail: '',
    phone: '',
    type: '',
    action: ActionType.Close,
};

type State = {
    title?: string;
    description?: string;
    mail?: string;
    phone?: string;
    type?: string;
    action?: ActionType;
    open?: false;
};

const ContentContacts = () => {
    const [popupState, popupDispatch] = usePopupState<State>(initialState);
    const [isDeleteOpen, setIsDeleteOpen] = React.useState(false);
    const data = React.useMemo(() => makeRandomData<ReturnType<typeof getTableItem>>(5, getTableItem), []);
    const [, setIds, selectedRows] = useSelectedRowsData<Flatten<typeof data>>(data);
    const marginBottom: CSSObject = { marginBottom: scale(2) };
    return (
        <>
            <PageWrapper h1="Управление соц. сетями и контактами">
                <Button
                    size="sm"
                    Icon={PlusIcon}
                    css={{ ...marginBottom, marginRight: scale(2) }}
                    onClick={() => popupDispatch({ type: ActionType.Add })}
                >
                    Добавить
                </Button>
                {selectedRows.length > 0 ? (
                    <Button
                        size="sm"
                        Icon={TrashIcon}
                        css={{ ...marginBottom, marginRight: scale(2) }}
                        onClick={() => setIsDeleteOpen(true)}
                    >
                        Удалить контакт{selectedRows.length > 1 ? 'ы' : ''}
                    </Button>
                ) : null}
                <Block>
                    <Block.Body>
                        <Table
                            data={data}
                            columns={columns}
                            onRowSelect={setIds}
                            handleRowInfo={row => {
                                popupDispatch({
                                    type: ActionType.Edit,
                                    payload: {
                                        title: row?.title[0],
                                        description: row?.title[1],
                                        phone: row?.contacts[0],
                                        mail: row?.contacts[1],
                                        type: row?.type,
                                    },
                                });
                            }}
                        />
                    </Block.Body>
                </Block>
            </PageWrapper>
            <Popup
                isOpen={Boolean(popupState.open)}
                onRequestClose={() => popupDispatch({ type: ActionType.Close })}
                title={`${popupState.action === 'edit' ? 'Редактирование' : 'Добавление'} контакта`}
                popupCss={{ minWidth: scale(75) }}
            >
                <Form
                    initialValues={{
                        title: popupState.title,
                        description: popupState.description,
                        mail: popupState.mail,
                        phone: popupState.phone,
                        type: popupState.type,
                    }}
                    onSubmit={vals => console.log(vals)}
                >
                    <Form.FastField name="title" label="Название" css={marginBottom} />
                    <Form.FastField name="description" label="Описание" css={marginBottom}>
                        <Textarea />
                    </Form.FastField>
                    <Form.FastField name="mail" label="E-mail" css={marginBottom} />
                    <Form.FastField name="phone" label="Телефон" type="tel" css={marginBottom}>
                        <Mask mask={maskPhone} />
                    </Form.FastField>
                    <Form.FastField name="type" label="Тип" css={marginBottom}>
                        <Select items={types} defaultValue={popupState.type} />
                    </Form.FastField>
                    <FilePondStyles />
                    <FilePond
                        server="/api"
                        acceptedFileTypes={['application/pdf']}
                        maxFileSize="10MB"
                        maxTotalFileSize="100MB"
                        allowMultiple={false}
                        css={marginBottom}
                    />
                    <Form.Reset size="sm" theme="fill" css={{ marginRight: scale(2) }}>
                        {popupState.action === 'edit' ? 'Сбросить' : 'Очистить'}
                    </Form.Reset>
                    <Button type="submit" size="sm">
                        Сохранить
                    </Button>
                </Form>
            </Popup>
            <Popup
                isOpen={isDeleteOpen}
                onRequestClose={() => setIsDeleteOpen(false)}
                title="Вы уверены, что хотите удалить следующие контакты?"
                popupCss={{ minWidth: scale(50) }}
            >
                <ul css={{ marginBottom: scale(2) }}>
                    {selectedRows.map(r => (
                        <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                            #{r.id} - {r.title[0]}
                        </li>
                    ))}
                </ul>
                <Button size="sm">Удалить</Button>
            </Popup>
        </>
    );
};

export default ContentContacts;
