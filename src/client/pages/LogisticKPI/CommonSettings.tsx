import React from 'react';
import { Button, scale, Layout } from '@greensight/gds';
import * as Yup from 'yup';
import Form from '@standart/Form';

const CommonSettings = () => {
    return (
        <Form
            initialValues={{ RTG: 0, CT: 0, PPT: 0 }}
            onSubmit={val => console.log(val)}
            validationSchema={Yup.object().shape({
                RTG: Yup.number().min(0, 'Веедите число больше или равное 0').integer('Только целые числа'),
                CT: Yup.number().min(0, 'Веедите число больше или равное 0').integer('Только целые числа'),
                PPT: Yup.number().min(0, 'Веедите число больше или равное 0').integer('Только целые числа'),
            })}
        >
            <Layout cols={3}>
                <Layout.Item col={1}>
                    <Form.FastField
                        label="RTG (мин)"
                        hint="Ready-To-Go time - время для проверки заказа АОЗ до его передачи в MAS"
                        name="RTG"
                        type="number"
                        min={0}
                    />
                </Layout.Item>
                <Layout.Item col={1} row="2/3">
                    <Form.FastField
                        label="CT (мин)"
                        hint="Confirmation Time - время перехода Отправления из статуса “Ожидает подтверждения” в статус “На комплектации”"
                        name="CT"
                        type="number"
                        min={0}
                    />
                </Layout.Item>
                <Layout.Item col={1} row="2/3">
                    <Form.FastField
                        label="PPT (мин)"
                        hint="Planned Processing Time - плановое время для прохождения Отправлением статусов от “На комплектации” до “Готов к передаче ЛО”"
                        name="PPT"
                        type="number"
                        min={0}
                    />
                </Layout.Item>
                <Layout.Item col={1} row="3/4">
                    <Form.Reset theme="fill" css={{ marginRight: scale(2) }} size="sm">
                        Сбросить
                    </Form.Reset>
                    <Button size="sm" type="submit">
                        Сохранить
                    </Button>
                </Layout.Item>
            </Layout>
        </Form>
    );
};

export default CommonSettings;
