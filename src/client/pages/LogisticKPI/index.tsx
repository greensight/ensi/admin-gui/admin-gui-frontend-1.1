import React from 'react';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import Tabs from '@standart/Tabs';
import FormMaker from './FormMaker';
import CommonSettings from './CommonSettings';
import useTabs from '@scripts/useTabs';

const sellers = [
    'Резинки для волос',
    'Одеколон',
    'ООО Вектор плюс',
    'Stylers',
    'ООО Мегафон',
    'ООО Рога и копыта',
    'ООО Бивис и батхет',
    'Макарун',
];

const deliveryCompanies = ['СДЭК', 'BoxBerry', 'Почта России'];

const LogisticKPI = () => {
    const { getTabsProps } = useTabs();
    return (
        <PageWrapper h1="Планировщик времени статусов">
            <Tabs {...getTabsProps()}>
                <Tabs.List>
                    <Tabs.Tab>Общие настройки</Tabs.Tab>
                    <Tabs.Tab>CT для продавцов</Tabs.Tab>
                    <Tabs.Tab>PPT для продавцов</Tabs.Tab>
                    <Tabs.Tab>PCT для логистических операторов</Tabs.Tab>
                </Tabs.List>
                <Block>
                    <Block.Body>
                        <Tabs.Panel>
                            <CommonSettings />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <FormMaker
                                name="Продавец"
                                value="CT (мин)"
                                tooltipText="Confirmation Time - время перехода Отправления из статуса “Ожидает подтверждения” в статус “На комплектации”"
                                selectOptions={sellers}
                                onSubmit={val => console.log(val)}
                                initialValues={{
                                    list: [
                                        { name: 'Stylers', value: 40 },
                                        { name: 'ООО Мегафон', value: 10 },
                                    ],
                                }}
                            />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <FormMaker
                                name="Продавец"
                                value="PPT (мин)"
                                tooltipText="Planned Processing Time - плановое время для прохождения Отправлением статусов от “На комплектации” до “Готов к передаче ЛО”"
                                selectOptions={sellers}
                                onSubmit={val => console.log(val)}
                            />
                        </Tabs.Panel>
                        <Tabs.Panel>
                            <FormMaker
                                name="Служба доставки"
                                value="PCT (мин)"
                                tooltipText="Planned Сonsolidation Time - плановое время доставки заказа от склада продавца до логистического хаба ЛО и обработки заказа в сортировочном центре или хабе на стороне ЛО"
                                selectOptions={deliveryCompanies}
                                initialValues={{ list: [{ name: 'СДЭК', value: 60 }] }}
                                onSubmit={val => console.log(val)}
                            />
                        </Tabs.Panel>
                    </Block.Body>
                </Block>
            </Tabs>
        </PageWrapper>
    );
};

export default LogisticKPI;
