import React, { useMemo } from 'react';
import { Button, scale, Layout } from '@greensight/gds';
import { useFormikContext, FieldArray, FormikValues } from 'formik';
import * as Yup from 'yup';
import typography from '@scripts/typography';
import Form from '@standart/Form';
import Select from '@standart/Select';
import Tooltip from '@standart/Tooltip';
import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import { prepareForSelect } from '@scripts/helpers';

interface FormChildrenProps {
    selectOptions: string[];
}

const FormChildren = ({ selectOptions }: FormChildrenProps) => {
    const {
        values: { list, select, field },
        setFieldValue,
    } = useFormikContext<FormMakerFormValues>();

    const options = useMemo(
        () =>
            prepareForSelect(
                selectOptions.filter(s => !list.some(i => i.name.toLowerCase().trim() === s.toLowerCase().trim()))
            ),
        [list, selectOptions]
    );

    return (
        <>
            <FieldArray
                name="list"
                render={({ remove, push }) => (
                    <>
                        {list.map((item, index) => (
                            <React.Fragment key={item.name}>
                                <Layout.Item col={1}>{item.name}</Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.Field
                                        isLegend
                                        type="number"
                                        name={`list[${index}].value`}
                                        min={0}
                                        css={{ marginRight: scale(1) }}
                                    />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Button size="sm" theme="fill" Icon={TrashIcon} onClick={() => remove(index)}>
                                        Удалить
                                    </Button>
                                </Layout.Item>
                            </React.Fragment>
                        ))}
                        <Layout.Item col={1}>
                            <Form.FastField name="select">
                                <Select items={options} />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.FastField name="field" type="number" min={0} css={{ marginRight: scale(2) }} />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Button
                                size="sm"
                                Icon={PlusIcon}
                                onClick={() => {
                                    if (select) {
                                        push({ name: select, value: field });
                                        setFieldValue('select', '');
                                        setFieldValue('field', 0);
                                    }
                                }}
                                disabled={!select}
                            >
                                Добавить
                            </Button>
                        </Layout.Item>
                    </>
                )}
            />
            <Layout.Item col={1}>
                <Button type="submit" size="sm" theme="primary">
                    Сохранить
                </Button>
            </Layout.Item>
        </>
    );
};

interface FormMakerFormValues {
    /** select value */
    select?: string;
    /** field value */
    field?: number;
    /** list array values */
    list: {
        /** list array item name */
        name: string;
        /** list array item value */
        value: number;
    }[];
}

interface FormMakerProps {
    /** left column name */
    name: string;
    /** right column name */
    value: string;
    /** tooltip text */
    tooltipText?: string;
    /** optios for select to add FormArray item */
    selectOptions: string[];
    /** initial values of form */
    initialValues?: FormMakerFormValues;
    /** on submit handler */
    onSubmit: (vals: FormikValues) => void;
}

const FormMaker = ({ name, value, tooltipText, selectOptions, initialValues, onSubmit }: FormMakerProps) => {
    return (
        <Form
            initialValues={{ select: '', field: 0, list: [], ...initialValues }}
            onSubmit={onSubmit}
            validationSchema={Yup.object().shape({
                select: Yup.string(),
                field: Yup.number().min(0, 'Веедите число больше или равное 0').integer('Только целые числа'),
            })}
        >
            <Layout cols={3}>
                <Layout.Item col={1}>
                    <p css={typography('bodyMdBold')}>{name}</p>
                </Layout.Item>
                <Layout.Item col={2}>
                    <p css={typography('bodyMdBold')}>
                        {value}{' '}
                        {tooltipText ? (
                            <Tooltip content={tooltipText} arrow maxWidth={scale(30)}>
                                <button type="button" css={{ verticalAlign: 'middle' }}>
                                    <TipIcon />
                                </button>
                            </Tooltip>
                        ) : null}
                    </p>
                </Layout.Item>
                <FormChildren selectOptions={selectOptions} />
            </Layout>
        </Form>
    );
};

export default FormMaker;
