import React, { useState, useMemo } from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { useLocation } from 'react-router-dom';
import * as Yup from 'yup';
import typography from '@scripts/typography';
import Table from '@components/Table';
import Block from '@components/Block';
import MultiSelect from '@standart/MultiSelect';
import Form from '@standart/Form';
import { makeUsers } from '@scripts/mock';
import Pagination from '@standart/Pagination';
import Password from '@standart/Password';
import Popup from '@standart/Popup';
import PageWrapper from '@components/PageWrapper';

const COLUMNS = [
    {
        Header: '№',
        accessor: 'id',
    },
    {
        Header: 'Login',
        accessor: 'login',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Система',
        accessor: 'system',
    },
    {
        Header: 'E-mail подтверждён',
        accessor: 'emailStatus',
        getProps: () => ({ type: 'status' }),
    },
];

const systems = [
    { label: 'Админка', value: 'Админка' },
    { label: 'MAS', value: 'MAS' },
    { label: 'Витрина', value: 'Витрина' },
];

const Users = () => {
    const { colors } = useTheme();
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const [isAddUserOpen, setIsAddUserOpen] = useState<boolean>(false);
    const [formError, setFormError] = useState<string | null>(null);

    const data = useMemo(() => makeUsers(10), []);

    return (
        <PageWrapper h1="Список пользователей">
            <main>
                <Block>
                    <Block.Header>
                        <div>
                            <Button
                                theme="primary"
                                size="sm"
                                css={{ marginRight: scale(2) }}
                                onClick={() => setIsAddUserOpen(true)}
                            >
                                Добавить пользователя
                            </Button>
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Table columns={COLUMNS} data={data} needSettingsColumn={false} />
                        <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
                    </Block.Body>
                </Block>
            </main>
            <Popup
                isOpen={isAddUserOpen}
                onRequestClose={() => {
                    setIsAddUserOpen(false);
                }}
                title="Добавление пользователя"
                popupCss={{ minWidth: scale(75) }}
            >
                <Form
                    onSubmit={values => {
                        setFormError(null);
                        if (values.userPassword !== values.userPasswordConfirm) {
                            setFormError('Пароли не совпадают');
                            return;
                        }
                        console.log(values);
                        setIsAddUserOpen(false);
                    }}
                    initialValues={{
                        userLogin: '',
                        userSystem: '',
                        userPassword: '',
                        userPasswordConfirm: '',
                    }}
                    validationSchema={Yup.object().shape({
                        userLogin: Yup.string().required('Введите логин'),
                        userSystem: Yup.string().required('Выберите систему'),
                        userPassword: Yup.string().min(8, 'Минимум 8 символов').required('Введите пароль'),
                        userPasswordConfirm: Yup.string()
                            .min(8, 'Минимум 8 символов')
                            .required('Введите подтверждение пароля'),
                    })}
                >
                    <Layout cols={2}>
                        <Layout.Item col={2}>
                            <Form.Field label="Логин" name="userLogin" />
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <Form.Field name="userSystem" label="Система">
                                <MultiSelect options={systems} isMulti={false} />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, sm: 2 }}>
                            <Form.FastField name="userPassword" label="Пароль">
                                <Password />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, sm: 2 }}>
                            <Form.FastField name="userPasswordConfirm" label="Повтор пароля">
                                <Password />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, sm: 2 }}>
                            <Button type="submit" size="sm" theme="primary">
                                Сохранить
                            </Button>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, sm: 2 }} css={{ alignSelf: 'center' }}>
                            {formError && <Form.Error error={formError} />}
                        </Layout.Item>
                    </Layout>
                </Form>
            </Popup>
        </PageWrapper>
    );
};

export default Users;
