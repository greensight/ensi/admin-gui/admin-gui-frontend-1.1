import React, { useMemo, useState } from 'react';
import { useParams, Link } from 'react-router-dom';
import { CSSObject } from '@emotion/core';
import { format } from 'date-fns';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import PageWrapper from '@components/PageWrapper';
import Table from '@components/Table';
import Block from '@components/Block';
import Badge from '@components/Badge';
import Popup from '@standart/Popup';
import Form from '@standart/Form';
import Tabs from '@standart/Tabs';
import Tooltip from '@standart/Tooltip';
import Checkbox from '@standart/Checkbox';
import CheckboxGroup from '@standart/CheckboxGroup';
import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import PlusIcon from '@svg/tokens/small/plus.svg';
import ExportIcon from '@svg/tokens/small/export.svg';
import FileIcon from '@svg/tokens/small/file.svg';
import typography from '@scripts/typography';
import { formatPrice } from '@scripts/helpers';
import { makeCargos, makeCargosHistory, makeCargosComposition } from '@scripts/mock';

export interface Store {
    storeName: string;
    quantity: number;
}

const compositionСolumns = [
    {
        Header: '№ отправления',
        accessor: 'departure',
    },
    {
        Header: 'Дата отправления',
        accessor: 'dateFrom',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Сумма, руб.',
        accessor: 'summary',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: 'Требуемая дата отгрузки',
        accessor: 'dateTo',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Кол-во коробок	',
        accessor: 'boxQuantity',
    },
];

const historyСolumns = [
    {
        Header: 'Дата',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Пользователь',
        accessor: 'user',
    },
    {
        Header: 'Сущность',
        accessor: 'essense',
    },
    {
        Header: 'Действие',
        accessor: 'action',
    },
];

const checkboxes = [
    { value: '1', name: '1000007-1000007-1-02' },
    { value: '2', name: '1000007-1000007-1-03' },
    { value: '3', name: '1000007-1000007-1-04' },
    { value: '4', name: '1000007-1000007-1-05' },
    { value: '5', name: '1000007-1000007-1-06' },
    { value: '6', name: '1000007-1000007-1-07' },
];

const cargo = makeCargos(1)[0];

const Cargo = () => {
    const [isAddingOrderPopupOpen, setAddingOrderPopupOpen] = useState(false);
    const { id } = useParams<{ id: string }>();
    const { weigth, price, discount, created, delivery, warehouse, status, boxes, pickupNumber, quantity } = cargo;
    const { colors } = useTheme();

    const dlStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'" };
    const dtStyles: CSSObject = {
        padding: `${scale(1, true)}px ${scale(1)}px ${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };
    const ddStyles: CSSObject = {
        padding: `${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
        textAlign: 'right',
    };
    const tableCompositionData = useMemo(() => makeCargosComposition(10), []);
    const tableHistoryData = useMemo(() => makeCargosHistory(10), []);
    const tableCompositionСolumns = useMemo(() => compositionСolumns, []);
    const tableHistoryСolumns = useMemo(() => historyСolumns, []);

    return (
        <PageWrapper h1="">
            <main>
                <Layout cols={6} css={{ marginBottom: scale(3) }}>
                    <Layout.Item col={{ xxxl: 2, md: 6 }}>
                        <Block>
                            <Block.Header>
                                <h1 css={{ ...typography('h2'), margin: 0, paddingBottom: scale(1) }}>
                                    Груз&nbsp;#{id}&nbsp;от&nbsp;{format(new Date(created), 'dd.MM.yyyy')}
                                </h1>
                                <Tooltip
                                    content={
                                        <div style={{ padding: scale(1), display: 'grid', gridGap: scale(1, true) }}>
                                            <Button theme="secondary" size="sm" css={{ width: '100%' }}>
                                                Создать задание на забор груза
                                            </Button>
                                            <Button theme="secondary" size="sm" css={{ width: '100%' }}>
                                                Отменить груз
                                            </Button>
                                        </div>
                                    }
                                    trigger="mouseenter focus click "
                                    placement="right"
                                >
                                    <Button size="sm">Действия</Button>
                                </Tooltip>
                            </Block.Header>
                            <Block.Body>
                                <dl css={{ ...dlStyles, gridTemplateColumns: 'auto auto' }}>
                                    <dt css={dtStyles}>
                                        <Badge text={status} />
                                    </dt>
                                    <dd css={ddStyles}>
                                        <Badge text="Груз передан курьеру" />
                                    </dd>
                                    <dt css={dtStyles}>Служба доставки</dt>
                                    <dd css={ddStyles}>{delivery}</dd>
                                    <dt css={dtStyles}>Номер задания на забор груза</dt>
                                    <dd css={ddStyles}>{pickupNumber || 'N/A'}</dd>
                                    <dt css={dtStyles}>Последнее изменение:</dt>
                                    <dd css={ddStyles}>{format(new Date(created), 'dd.MM.yyyy')}</dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 2, md: 6 }}>
                        <Block>
                            <Block.Header>
                                <h1 css={{ ...typography('h2'), margin: 0 }}>
                                    Сумма груза {formatPrice(price - discount)} руб.
                                </h1>
                                <Tooltip content="С учётом скидки" placement="right" maxWidth={scale(30)}>
                                    <button type="button" css={{ verticalAlign: 'middle' }}>
                                        <TipIcon />
                                    </button>
                                </Tooltip>
                            </Block.Header>
                            <Block.Body>
                                <dl css={{ ...dlStyles, gridTemplateColumns: 'auto auto' }}>
                                    <dt css={dtStyles}>Скидка</dt>
                                    <dd css={{ ...ddStyles, color: '#e85446' }}>-&nbsp;{formatPrice(discount)} руб.</dd>
                                    <dt css={dtStyles}>Сумма без скидки</dt>
                                    <dd css={{ ...ddStyles, color: '#e85446' }}>{formatPrice(price)} руб.</dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                    <Layout.Item col={{ xxxl: 2, md: 6 }}>
                        <Block>
                            <Block.Header>
                                <h1 css={{ ...typography('h2'), margin: 0 }}>{quantity} ед. товара</h1>
                            </Block.Header>
                            <Block.Body>
                                <dl css={{ ...dlStyles, gridTemplateColumns: 'auto auto' }}>
                                    <dt css={dtStyles}></dt>
                                    <dd css={ddStyles}>кол-во коробок:&nbsp;{boxes}&nbsp;шт.</dd>
                                    <dt css={dtStyles}>Вес:</dt>
                                    <dd css={ddStyles}>{weigth} г</dd>
                                    <dt css={dtStyles}>Склад отгрузки:</dt>
                                    <dd css={ddStyles}>{warehouse}</dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                </Layout>
                <Tabs>
                    <Tabs.List>
                        <Tabs.Tab>Состав груза</Tabs.Tab>
                        <Tabs.Tab>История</Tabs.Tab>
                    </Tabs.List>
                    <Block>
                        <Block.Body>
                            <Tabs.Panel>
                                <div
                                    css={{
                                        display: 'grid',
                                        gridGap: scale(2),
                                        gridTemplateColumns: '200px 200px auto',
                                    }}
                                >
                                    <Tooltip
                                        content={
                                            <div
                                                css={{
                                                    display: 'grid',
                                                    gridGap: scale(1, true),
                                                    '& > a': {
                                                        width: '100%',
                                                        padding: `${scale(1, true)}px ${scale(1)}px`,
                                                    },
                                                }}
                                            >
                                                <Link to="#">Все</Link>
                                                <Link to="#">Покупателю</Link>
                                                <Link to="#">Курьевру</Link>
                                            </div>
                                        }
                                        trigger="mouseenter focus click "
                                        placement="bottom"
                                    >
                                        <button
                                            css={{
                                                display: 'inline-flex',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}
                                        >
                                            <ExportIcon />
                                            &nbsp;Скачать документы
                                        </button>
                                    </Tooltip>
                                    <Tooltip
                                        content={
                                            <div
                                                css={{
                                                    display: 'grid',
                                                    gridGap: scale(1, true),
                                                    '& > a': {
                                                        width: '100%',
                                                        padding: `${scale(1, true)}px ${scale(1)}px`,
                                                    },
                                                }}
                                            >
                                                <Link to="#">Все</Link>
                                                <Link to="#">Покупателю</Link>
                                                <Link to="#">Курьевру</Link>
                                            </div>
                                        }
                                        trigger="mouseenter focus click "
                                        placement="bottom"
                                    >
                                        <button
                                            css={{
                                                display: 'inline-flex',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}
                                        >
                                            <FileIcon />
                                            &nbsp;Распечатать документы
                                        </button>
                                    </Tooltip>
                                    <Button
                                        css={{ marginLeft: 'auto ' }}
                                        Icon={PlusIcon}
                                        onClick={() => {
                                            setAddingOrderPopupOpen(true);
                                        }}
                                    >
                                        Добавить заказы
                                    </Button>
                                    <Popup
                                        isOpen={isAddingOrderPopupOpen}
                                        onRequestClose={() => {
                                            setAddingOrderPopupOpen(false);
                                        }}
                                        title="Добавление заказов в груз"
                                        popupCss={{ minWidth: scale(60) }}
                                    >
                                        <Form
                                            onSubmit={values => {
                                                console.log(values);
                                            }}
                                            initialValues={{
                                                checkboxes: [],
                                            }}
                                        >
                                            <CheckboxGroup name="checkbox-group">
                                                {checkboxes?.map((item, index) => (
                                                    <Checkbox key={`checkbox-${index}`} value={item.value}>
                                                        {item.name}
                                                    </Checkbox>
                                                ))}
                                            </CheckboxGroup>
                                            <Button
                                                type="submit"
                                                size="sm"
                                                theme="primary"
                                                css={{ marginTop: scale(2) }}
                                            >
                                                Сохранить
                                            </Button>
                                        </Form>
                                    </Popup>
                                </div>
                                <Table
                                    columns={tableCompositionСolumns}
                                    data={tableCompositionData}
                                    needCheckboxesCol={false}
                                    needSettingsColumn
                                    needDeletingColumn
                                    css={{ marginTop: scale(2) }}
                                />
                            </Tabs.Panel>
                            <Tabs.Panel>
                                <Table
                                    columns={tableHistoryСolumns}
                                    data={tableHistoryData}
                                    needCheckboxesCol={false}
                                    needSettingsColumn={false}
                                    css={{ marginTop: scale(2) }}
                                />
                            </Tabs.Panel>
                        </Block.Body>
                    </Block>
                </Tabs>
            </main>
        </PageWrapper>
    );
};

export default Cargo;
