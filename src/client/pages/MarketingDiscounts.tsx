import React, { useState, useMemo } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import Table from '@components/Table';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import Datepicker from '@standart/Datepicker';
import DatepickerStyles from '@standart/Datepicker/presets';
import Legend from '@standart/Legend';
import Popup from '@standart/Popup';
import Form from '@standart/Form';
import Pagination from '@standart/Pagination';
import MultiSelect from '@standart/MultiSelect';
import Checkbox from '@standart/Checkbox';
import Tooltip from '@standart/Tooltip';
import typography from '@scripts/typography';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import {
    makeDiscounts,
    discountsStatuses,
    discountsProducts,
    discountsRoles,
    discountsInitiators,
    discountsCreators,
} from '@scripts/mock';

import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import { prepareForSelect } from '@scripts/helpers';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Дата создания',
        accessor: 'createDate',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Название',
        accessor: 'name',
    },
    {
        Header: 'Скидка',
        accessor: 'discount',
    },
    {
        Header: 'Период действия',
        accessor: 'activePeriod',
    },
    {
        Header: 'Инициатор',
        accessor: 'initiator',
    },
    {
        Header: 'Автор',
        accessor: 'creator',
    },
    {
        Header: 'Статус',
        accessor: 'status',
    },
];

const STATUSES = prepareForSelect(discountsStatuses);
const DISCOUNTSPRODUCTS = prepareForSelect(discountsProducts);
const DISCOUNTSROLES = prepareForSelect(discountsRoles);
const DISCOUNTSINITIATORS = prepareForSelect(discountsInitiators);
const DISCOUNTSCREATORS = prepareForSelect(discountsCreators);
const deleteDiscountsPopupColumns = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Название',
        accessor: 'name',
    },
];

const MarketingDiscounts = () => {
    const { colors } = useTheme();
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeDiscounts(10), []);
    const [moreFilters, setMoreFilters] = useState(false);
    const [creationDateStart, setCreationDateStart] = useState<Date | null>(null);
    const [creationDateEnd, setCreationDateEnd] = useState<Date | null>(null);
    const [activePeriodStart, setActivePeriodStart] = useState<Date | null>(null);
    const [activePeriodEnd, setActivePeriodEnd] = useState<Date | null>(null);
    const [deleteDiscountsOpen, setDeleteDiscountsOpen] = useState(false);
    const [changeStatusesOpen, setChangeStatusesOpenOpen] = useState(false);

    const [ids, setIds, popupTableData] = useSelectedRowsData<typeof data[0]>(data);

    return (
        <PageWrapper h1="Скидки">
            <>
                <DatepickerStyles />
                <Block css={{ marginBottom: scale(3) }}>
                    <Form
                        initialValues={{
                            id: '',
                            name: '',
                            status: [],
                            discountProducts: [],
                            role: [],
                            initiator: [],
                            creator: [],
                            creationDateStart: null,
                            creationDateEnd: null,
                            activePeriodStart: null,
                            activePeriodEnd: null,
                            exactDiscountStartDate: '',
                            exactDiscountEndDate: '',
                            isIndefiniteDiscountEndDate: '',
                        }}
                        onSubmit={values => {
                            console.log(values);
                        }}
                    >
                        <Block.Body>
                            <Layout cols={4}>
                                <Layout.Item col={1}>
                                    <Form.Field name="id" label="ID" />
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="name" label="Название" />
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.Field name="status" label="Статус">
                                        <MultiSelect options={STATUSES} />
                                    </Form.Field>
                                </Layout.Item>

                                {moreFilters ? (
                                    <>
                                        <Layout.Item col={4}>
                                            <hr />
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.Field name="discountProducts" label="Скидка на">
                                                <MultiSelect options={DISCOUNTSPRODUCTS} />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={1}>
                                            <Form.Field name="role" label="Роль">
                                                <MultiSelect isMulti={false} options={DISCOUNTSROLES} />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.Field name="initiator" label="Инициатор">
                                                <MultiSelect options={DISCOUNTSINITIATORS} />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.Field name="creator" label="Автор">
                                                <MultiSelect options={DISCOUNTSCREATORS} />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={1}>
                                            <Form.Field name="creationDateStart">
                                                <Legend label="Дата создания от" />
                                                <Datepicker
                                                    selectsStart
                                                    selected={creationDateStart}
                                                    startDate={creationDateStart}
                                                    endDate={creationDateEnd}
                                                    maxDate={creationDateEnd}
                                                    onChange={setCreationDateStart}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={1}>
                                            <Form.Field name="creationDateEnd">
                                                <Legend label="Дата создания до" />
                                                <Datepicker
                                                    selectsEnd
                                                    selected={creationDateEnd}
                                                    startDate={creationDateStart}
                                                    endDate={creationDateEnd}
                                                    minDate={creationDateStart}
                                                    onChange={setCreationDateEnd}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Layout cols={3}>
                                                <Layout.Item col={1}>
                                                    <Form.Field name="activePeriodStart">
                                                        <Legend label="Период действия скидки от" />
                                                        <Datepicker
                                                            selectsEnd
                                                            selected={activePeriodStart}
                                                            startDate={activePeriodStart}
                                                            endDate={activePeriodEnd}
                                                            minDate={activePeriodEnd}
                                                            onChange={setActivePeriodStart}
                                                        />
                                                    </Form.Field>
                                                </Layout.Item>
                                                <Layout.Item col={2} align="end">
                                                    <Form.Field name="exactDiscountStartDate">
                                                        <Checkbox value="true">
                                                            Точная дата
                                                            <Tooltip
                                                                content="Искать точное совпадание с датой начала и/или окончания скидки"
                                                                arrow
                                                                maxWidth={scale(30)}
                                                            >
                                                                <button
                                                                    type="button"
                                                                    css={{
                                                                        marginLeft: scale(1, true),
                                                                        verticalAlign: 'middle',
                                                                    }}
                                                                >
                                                                    <TipIcon />
                                                                </button>
                                                            </Tooltip>
                                                        </Checkbox>
                                                    </Form.Field>
                                                </Layout.Item>
                                                <Layout.Item col={1}>
                                                    <Form.Field name="activePeriodEnd">
                                                        <Legend label="Период действия скидки до" />
                                                        <Datepicker
                                                            selectsEnd
                                                            selected={activePeriodEnd}
                                                            startDate={activePeriodStart}
                                                            endDate={activePeriodEnd}
                                                            minDate={activePeriodStart}
                                                            onChange={setActivePeriodEnd}
                                                        />
                                                    </Form.Field>
                                                </Layout.Item>
                                                <Layout.Item col={1} align="end">
                                                    <Form.Field name="exactDiscountEndDate">
                                                        <Checkbox value="true">
                                                            Точная дата
                                                            <Tooltip
                                                                content="Искать точное совпадание с датой начала и/или окончания скидки"
                                                                arrow
                                                                maxWidth={scale(30)}
                                                            >
                                                                <button
                                                                    type="button"
                                                                    css={{
                                                                        marginLeft: scale(1, true),
                                                                        verticalAlign: 'middle',
                                                                    }}
                                                                >
                                                                    <TipIcon />
                                                                </button>
                                                            </Tooltip>
                                                        </Checkbox>
                                                    </Form.Field>
                                                </Layout.Item>
                                                <Layout.Item col={1} align="end">
                                                    <Form.Field name="isIndefiniteDiscountEndDate">
                                                        <Checkbox value="true">Бессрочная</Checkbox>
                                                    </Form.Field>
                                                </Layout.Item>
                                            </Layout>
                                        </Layout.Item>
                                    </>
                                ) : null}
                            </Layout>
                        </Block.Body>
                        <Block.Footer>
                            <div css={typography('bodySm')}>
                                Найдено 135 товаров{' '}
                                <button
                                    type="button"
                                    css={{ color: colors?.primary, marginLeft: scale(2) }}
                                    onClick={() => setMoreFilters(!moreFilters)}
                                >
                                    {moreFilters ? 'Меньше' : 'Больше'} фильтров
                                </button>{' '}
                            </div>
                            <div>
                                <Form.Reset
                                    size="sm"
                                    theme="secondary"
                                    type="button"
                                    onClick={() => {
                                        setCreationDateStart(null);
                                        setCreationDateEnd(null);
                                        setActivePeriodStart(null);
                                        setActivePeriodEnd(null);
                                    }}
                                >
                                    Сбросить
                                </Form.Reset>
                                <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                    Применить
                                </Button>
                            </div>
                        </Block.Footer>
                    </Form>
                </Block>

                <Block>
                    <Block.Header>
                        <div css={{ display: 'flex' }}>
                            <Button
                                theme="primary"
                                size="sm"
                                as={Link}
                                to="discounts/create"
                                css={{ marginRight: scale(2) }}
                            >
                                Создать скидку
                            </Button>
                            <Button
                                theme="primary"
                                size="sm"
                                css={{ marginRight: scale(2) }}
                                onClick={() => {
                                    setDeleteDiscountsOpen(true);
                                }}
                                disabled={!(ids.length > 0)}
                            >
                                Удалить скидки
                            </Button>
                            <Button
                                theme="primary"
                                size="sm"
                                css={{ marginRight: scale(2) }}
                                onClick={() => {
                                    setChangeStatusesOpenOpen(true);
                                }}
                                disabled={!(ids.length > 0)}
                            >
                                Изменить статус скидок
                            </Button>
                            <Button
                                theme="primary"
                                size="sm"
                                css={{ marginRight: scale(2) }}
                                onClick={() => {
                                    console.log('create report');
                                }}
                            >
                                Сгенерировать отчет
                            </Button>
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Table
                            columns={COLUMNS}
                            data={data}
                            handleRowInfo={() => console.log('click')}
                            onRowSelect={setIds}
                        />
                        <Pagination url={pathname} activePage={activePage} pages={7} />
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={deleteDiscountsOpen}
                    onRequestClose={() => {
                        setDeleteDiscountsOpen(false);
                    }}
                    title="Вы уверены, что хотите удалить следующие скидки?"
                    popupCss={{ minWidth: scale(100) }}
                >
                    {/* <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        changedStatus: null,
                    }}
                > */}
                    <Table
                        columns={deleteDiscountsPopupColumns}
                        data={popupTableData}
                        needCheckboxesCol={false}
                        needSettingsColumn={false}
                        css={{ marginBottom: scale(2) }}
                    />
                    <Button type="submit" theme="primary">
                        Удалить
                    </Button>
                    {/* </Form> */}
                </Popup>
                <Popup
                    isOpen={changeStatusesOpen}
                    onRequestClose={() => {
                        setChangeStatusesOpenOpen(false);
                    }}
                    title="Обновление статуса"
                    popupCss={{ minWidth: scale(100) }}
                >
                    <Form
                        onSubmit={values => {
                            console.log(values);
                        }}
                        initialValues={{
                            changedStatus: null,
                        }}
                    >
                        <Table
                            columns={deleteDiscountsPopupColumns}
                            data={popupTableData}
                            needCheckboxesCol={false}
                            needSettingsColumn={false}
                            css={{ marginBottom: scale(2) }}
                        />
                        <Form.Field name="changedStatus" label="Статус" css={{ marginBottom: scale(2) }}>
                            <MultiSelect options={STATUSES} isMulti={false} />
                        </Form.Field>

                        <Button type="submit" size="sm" theme="primary">
                            Изменить статус
                        </Button>
                    </Form>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default MarketingDiscounts;
