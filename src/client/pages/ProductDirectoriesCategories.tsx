import React, { useMemo } from 'react';
import { Button, scale, useTheme } from '@greensight/gds';
import * as Yup from 'yup';

import PageWrapper from '@components/PageWrapper';
import Table from '@components/Table';
import Block from '@components/Block';

import Form from '@standart/Form';
import Select from '@standart/Select';
import Popup from '@standart/Popup';
import YesNoSwitcher from '@standart/YesNoSwitcher';

import PlusIcon from '@svg/plus.svg';
import usePopupState from '@scripts/usePopupState';
import { ActionType } from '@scripts/enums';
import { prepareForSelect } from '@scripts/helpers';
import Switcher from '@standart/Switcher';

const COLUMNS = [
    {
        Header: 'Название категории',
        accessor: 'title',
    },
    {
        Header: 'Код',
        accessor: 'code',
    },
    {
        Header: 'Активна',
        accessor: 'active',
        getProps: () => ({ type: 'status' }),
    },
];

const data = [
    {
        id: 1,
        title: 'Резинки для волос',
        code: 'rezinki_dlya_volos',
        active: 'Да',
        parentCategory: 'Нет',
    },
    {
        id: 2,
        title: 'Шампуни',
        code: 'shampuni',
        active: 'Нет',
        parentCategory: 'Нет',
        subRows: [
            {
                id: 3,
                title: 'Профессиональные шампуни',
                code: 'professionalnye_shampuni',
                parentCategory: 'Шампуни',
                active: 'Да',
                subRows: [
                    {
                        id: 4,
                        title: 'Шампуни без парабенов',
                        code: 'shampuni_bez_parabenov',
                        parentCategory: 'Профессиональные шампуни',
                        active: 'Да',
                    },
                ],
            },
            {
                id: 5,
                title: 'Восстанавливающие шампуни',
                code: 'vosstanavlivayushchie_shampuni',
                active: 'Да',
                parentCategory: 'Шампуни',
            },
        ],
    },
    {
        id: 6,
        title: 'Товары для кухни',
        code: 'dlya-kuchni',
        active: 'Нет',
    },
];

const categories = ['Нет', 'Резинки для волос', 'Тест 1', 'Шампуни', 'Профессиональные шампуни'];
const categoriesForSelect = prepareForSelect(categories);

const initialState = {
    id: '',
    title: '',
    code: '',
    parentCategory: '',
    active: true,
    action: ActionType.Close,
    open: false,
};

type State = {
    id?: string;
    title?: string;
    parentCategory?: string;
    code?: string;
    active?: boolean;
    action?: ActionType;
    open?: boolean;
};

const ProductDirectoriesCategories = () => {
    const [popupState, popupDispatch] = usePopupState<State>(initialState);
    const tableData = useMemo(() => data, []);
    return (
        <PageWrapper h1="Категории">
            <Button
                size="sm"
                css={{ marginBottom: scale(2) }}
                Icon={PlusIcon}
                onClick={() => popupDispatch({ type: ActionType.Add })}
            >
                Создать категорию
            </Button>
            <Block>
                <Block.Body>
                    <Table
                        columns={COLUMNS}
                        data={tableData}
                        expandable
                        needCheckboxesCol={false}
                        handleRowInfo={row => {
                            popupDispatch({
                                type: ActionType.Edit,
                                payload: {
                                    id: row?.id,
                                    code: row?.code,
                                    title: row?.title,
                                    active: row?.active?.toLowerCase() === 'да',
                                    parentCategory: row?.parentCategory,
                                },
                            });
                        }}
                    />
                </Block.Body>
            </Block>
            <Popup
                isOpen={Boolean(popupState.open)}
                onRequestClose={() => popupDispatch({ type: ActionType.Close })}
                title={`${popupState.action === 'edit' ? 'Редактированить' : 'Создать новую'} категорию`}
                popupCss={{ minWidth: scale(50) }}
            >
                <Form
                    initialValues={{
                        code: popupState.code,
                        title: popupState.title,
                        active: popupState.active,
                        categories: popupState.parentCategory || 'Нет',
                    }}
                    onSubmit={vals => console.log(vals)}
                    validationSchema={Yup.object().shape({
                        code: Yup.string().required('Обязательное поле'),
                        title: Yup.string(),
                        active: Yup.boolean(),
                        parentCategory: Yup.string().required('Обязательное поле'),
                    })}
                >
                    <Form.FastField name="title" label="Название категории" css={{ marginBottom: scale(2) }} />
                    <Form.FastField
                        name="code"
                        label="Символьный код"
                        hint="Оставьте поле пустым, если хотите, чтобы код был сгенерирован автоматически"
                        css={{ marginBottom: scale(2) }}
                    />
                    <Form.FastField name="categories" label="Родительская категория" css={{ marginBottom: scale(2) }}>
                        <Select items={categoriesForSelect} defaultValue={popupState.parentCategory} />
                    </Form.FastField>
                    <Form.FastField name="active" css={{ marginBottom: scale(2) }}>
                        <Switcher>Активна</Switcher>
                    </Form.FastField>
                    <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <Form.Reset
                            size="sm"
                            theme="fill"
                            css={{ marginRight: scale(2) }}
                            onClick={() => popupDispatch({ type: ActionType.Close })}
                        >
                            Отмена
                        </Form.Reset>
                        <Button type="submit" size="sm">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </PageWrapper>
    );
};

export default ProductDirectoriesCategories;
