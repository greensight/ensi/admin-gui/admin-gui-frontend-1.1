import React from 'react';
import { scale, Layout, Button } from '@greensight/gds';
import * as Yup from 'yup';
import { FieldArray, useFormikContext } from 'formik';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Form from '@standart/Form';
import Select from '@standart/Select';
import Table from '@components/Table';
import Switcher from '@standart/Switcher';
import { prepareForSelect } from '@scripts/helpers';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import { getRandomItem, makeRandomData } from '@scripts/mock';
import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import Pagination from '@standart/Pagination';
import Popup from '@standart/Popup';
import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';

const types = ['Не выбрано', 'Акции', 'Подборки', 'Бренда'];
const preparedTypes = prepareForSelect(types);

const Categories = () => {
    const {
        values: { categories },
    } = useFormikContext<{ categories: string[] }>();

    return (
        <FieldArray
            name="categories"
            render={({ remove, push }) => (
                <>
                    <p css={{ display: 'flex', alignItems: 'center', marginBottom: scale(2) }}>
                        Категория{' '}
                        <Button
                            size="sm"
                            Icon={PlusIcon}
                            hidden
                            onClick={() => push('')}
                            css={{ marginLeft: scale(2) }}
                        >
                            Добавить
                        </Button>
                    </p>
                    {categories.map((c, index) => (
                        <div key={index} css={{ display: 'flex', alignItems: 'center', marginBottom: scale(2) }}>
                            <Form.FastField name={`categories[${index}]`}>
                                <Select items={preparedTypes} />
                            </Form.FastField>
                            <Button
                                Icon={TrashIcon}
                                onClick={() => remove(index)}
                                hidden
                                size="sm"
                                css={{ marginLeft: scale(2) }}
                            >
                                удалить
                            </Button>
                        </div>
                    ))}
                </>
            )}
        />
    );
};

const BannerPopupChildren = () => {
    const {
        values: { hasBtn },
    } = useFormikContext<{ hasBtn: boolean }>();

    return (
        <>
            <Form.FastField name="title" label="Наимeнование" css={{ marginBottom: scale(2) }} />
            <Form.FastField name="isActive" css={{ marginBottom: scale(2) }}>
                <Switcher>Активность</Switcher>
            </Form.FastField>
            <p css={{ marginBottom: scale(1) }}>Изображения: десктопное, планшетное, мобильное</p>
            <FilePond imageLayout maxFiles={3} />
            <Form.FastField label="Тип" name="type" css={{ marginBottom: scale(2) }}>
                <Select items={preparedTypes} />
            </Form.FastField>
            <Form.FastField name="link" label="Ссылка" type="link" css={{ marginBottom: scale(2) }} />
            <Form.FastField name="hasBtn" css={{ marginBottom: scale(2) }}>
                <Switcher>С кнопкой</Switcher>
            </Form.FastField>
            {hasBtn ? (
                <>
                    <Form.FastField name="btnType" label="Текст кнопки" css={{ marginBottom: scale(2) }}>
                        <Select items={preparedTypes} />
                    </Form.FastField>
                    <Form.FastField name="btnText" label="Тип кнопки" css={{ marginBottom: scale(2) }}>
                        <Select items={preparedTypes} />
                    </Form.FastField>
                    <Form.FastField name="btnPlace" label="Местоположение кнопки" css={{ marginBottom: scale(2) }}>
                        <Select items={preparedTypes} />
                    </Form.FastField>
                </>
            ) : null}
            <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                <Form.Reset size="sm" theme="fill" css={{ marginRight: scale(2) }}>
                    Очистить
                </Form.Reset>
                <Button size="sm" type="submit">
                    Сохранить
                </Button>
            </div>
        </>
    );
};

const ContentProductGroupsCreate = () => {
    const [isOpen, setIsOpen] = React.useState(false);

    return (
        <>
            <PageWrapper h1="Создание подборки товара">
                <Block>
                    <Block.Body>
                        <FilePondStyles />
                        <Form
                            onSubmit={val => console.log(val)}
                            initialValues={{
                                title: '',
                                code: '',
                                active: false,
                                inLists: false,
                                type: '',
                                categories: [],
                            }}
                            css={{ maxWidth: '75%' }}
                            validationSchema={Yup.object().shape({
                                title: Yup.string().required('Обязательное поле'),
                                code: Yup.string().required('Обязательное поле'),
                                type: Yup.string().required('Обязательное поле'),
                            })}
                        >
                            <Form.FastField name="title" label="Наименование" css={{ marginBottom: scale(2) }} />
                            <Form.FastField name="code" label="Символьный код" css={{ marginBottom: scale(2) }} />
                            <Form.FastField name="active" css={{ marginBottom: scale(2) }}>
                                <Switcher>Активность</Switcher>
                            </Form.FastField>
                            <Form.FastField name="inLists" css={{ marginBottom: scale(2) }}>
                                <Switcher>Наличие в списках</Switcher>
                            </Form.FastField>
                            <FilePond maxFileSize="10MB" maxTotalFileSize="100MB" css={{ marginBottom: scale(2) }} />
                            <p css={{ marginBottom: scale(1) }}>Баннер</p>
                            <Button size="sm" css={{ marginBottom: scale(2) }} onClick={() => setIsOpen(true)}>
                                Добавить баннер
                            </Button>
                            <Form.FastField name="type" label="Тип" css={{ marginBottom: scale(2) }}>
                                <Select items={preparedTypes} />
                            </Form.FastField>
                            <Categories />
                            <p css={{ marginBottom: scale(1) }}>Фильтры</p>
                            <p css={{ marginBottom: scale(2) }}>Фильтры для выбранных категорий не найдены</p>
                            <Form.Reset css={{ marginRight: scale(2) }} theme="fill" size="sm">
                                Очистить
                            </Form.Reset>
                            <Button size="sm" type="submit">
                                Сохранить
                            </Button>
                        </Form>
                    </Block.Body>
                </Block>
            </PageWrapper>
            <Popup
                isOpen={isOpen}
                onRequestClose={() => setIsOpen(false)}
                title="Создание баннера"
                popupCss={{ minWidth: scale(75) }}
                scrollInside
            >
                <Form
                    onSubmit={val => console.log(val)}
                    initialValues={{
                        title: '',
                        isActive: true,
                        type: '',
                        link: '',
                        hasBtn: false,
                        btnText: '',
                        btnType: '',
                        btnPosition: '',
                    }}
                >
                    <BannerPopupChildren />
                </Form>
            </Popup>
        </>
    );
};

export default ContentProductGroupsCreate;
