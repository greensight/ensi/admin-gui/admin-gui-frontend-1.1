import React, { useState, useEffect } from 'react';
import { useFormikContext } from 'formik';
import { scale, Button, Layout, useTheme } from '@greensight/gds';
import { CSSObject } from '@emotion/core';
import * as Yup from 'yup';

import PlusIcon from '@svg/plus.svg';
import { chatsUnread as data } from '@scripts/mock';
import { prepareForSelect } from '@scripts/helpers';
import useOnceChanged from '@scripts/useOnceChanged';

import Block from '@components/Block';
import Table, { expandableRowInfoProps } from '@components/Table';
import MultiSelect from '@standart/MultiSelect';
import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';
import Form from '@standart/Form';
import PageWrapper from '@components/PageWrapper';
import Popup from '@standart/Popup';
import Select from '@standart/Select';
import Switcher from '@standart/Switcher';
import Textarea from '@standart/Textarea';
import Legend from '@standart/Legend';
import { lastDayOfQuarterWithOptions } from 'date-fns/fp';
import typography from '@scripts/typography';

const columns = [
    {
        Header: ' ',
        accessor: 'unread',
        getProps: () => ({ type: 'dot' }),
    },
    {
        Header: 'Тема',
        accessor: 'theme',
    },
    {
        Header: 'Пользователь',
        accessor: 'user',
    },
    {
        Header: 'Канал',
        accessor: 'channel',
    },
    {
        Header: 'ID коммуникации',
        accessor: 'communictionId',
    },
    {
        Header: 'Последнее сообщение',
        accessor: 'lastMsgData',
    },
    {
        Header: 'Статус',
        accessor: 'status',
    },
    {
        Header: 'Тип',
        accessor: 'type',
    },
];

const statuses = prepareForSelect([
    'Открыта',
    'Закрыта',
    'Отправлено',
    'Доставлено',
    'В работе',
    'Выполнена',
    'Открыта. Внутренний',
    'Закрыта. Внутренний',
    'Промежуточный',
]);

const channels = prepareForSelect([
    'Внутренний коммуникационный модуль',
    'SMS-центр',
    'LiveTex. Viber',
    'LiveTex. Telegram',
    'LiveTex. Messenger FB',
    'LiveTex. VK',
    'E-mail',
]);

const roles = prepareForSelect([
    'Оператор продавца',
    'Администратор продавца',
    'Профессионал',
    'Реферальный партнер',
    'Неавторизованный пользователь',
]);

const sellers = prepareForSelect(['ООО "Рога и копыта"', 'Ашан', 'Леруа Мерлен', 'Б.Ю. Александров', 'М.П. Почтомат']);

const users = prepareForSelect(['Дмитрий', 'Василий', 'Роман', 'Владилен', 'Иосиф', 'Илья', 'Мартин', 'Иван']);

const types = prepareForSelect(['Вопрос', 'Действия', 'Претензия', 'Маркетинг', 'Другое', 'Предупреждение']);

const UsersSelect = () => {
    const {
        values: { addAllUsers },
    } = useFormikContext();
    return addAllUsers ? null : (
        <Form.Field name="users" label="Пользователи" disabled={addAllUsers}>
            <MultiSelect options={users} />
        </Form.Field>
    );
};

const CreatePopup = ({ isOpen, close }: { isOpen: boolean; close: () => void }) => {
    return (
        <Popup
            isOpen={isOpen}
            onRequestClose={close}
            title="Создание чата"
            scrollInside
            css={{ padding: `0 ${scale(3)}px` }}
        >
            <FilePondStyles />
            <Form
                initialValues={{
                    roles: [],
                    seller: '',
                    channel: '',
                    users: [],
                    addAll: false,
                    theme: '',
                    status: '',
                    type: '',
                    message: '',
                }}
                validationSchema={Yup.object().shape({
                    message: Yup.string().required('Обязательное поле'),
                    status: Yup.string().required('Обязательное поле'),
                    theme: Yup.string().required('Обязательное поле'),
                    channel: Yup.string().required('Обязательное поле'),
                })}
                onSubmit={values => {
                    console.log(values);
                    close();
                }}
            >
                <Layout cols={1} gap={scale(1)}>
                    <Form.FastField name="roles" label="Роли пользователей">
                        <MultiSelect options={roles} />
                    </Form.FastField>

                    <Form.FastField name="seller" label="Продавец">
                        <Select items={sellers} />
                    </Form.FastField>

                    <Form.FastField name="channel" label="Канал">
                        <Select items={channels} />
                    </Form.FastField>

                    <Form.FastField name="addAllUsers">
                        <Switcher css={{ margin: `${scale(1)}px 0` }}>Добавить всех пользователей</Switcher>
                    </Form.FastField>

                    <UsersSelect />

                    <Form.FastField name="theme" label="Тема" />

                    <Form.FastField name="status" label="Статус">
                        <Select items={statuses} />
                    </Form.FastField>

                    <Form.FastField name="type" label="Тип">
                        <Select items={types} />
                    </Form.FastField>

                    <Form.FastField name="message">
                        <Legend label="Сообщение" />
                        <Textarea />
                    </Form.FastField>

                    <p>Файлы</p>
                    <FilePond maxFileSize="10MB" maxTotalFileSize="100MB" />

                    <Button size="sm" type="submit">
                        Создать чат
                    </Button>
                </Layout>
            </Form>
        </Popup>
    );
};

const EditPopup = ({ isOpen, close, editData }: { isOpen: boolean; close: () => void; editData: any }) => {
    return (
        <Popup isOpen={isOpen} onRequestClose={close} title="Редактирование чата" css={{ padding: `0 ${scale(3)}px` }}>
            <Form
                initialValues={{
                    theme: editData.theme,
                    status: editData.status,
                    type: editData.type,
                }}
                validationSchema={Yup.object().shape({
                    message: Yup.string().required('Обязательное поле'),
                    status: Yup.string().required('Обязательное поле'),
                })}
                onSubmit={values => {
                    console.log(values);
                    close();
                }}
            >
                <Layout cols={1} gap={scale(1)} justify="start">
                    <Form.FastField name="theme" label="Тема" />

                    <Form.FastField name="status" label="Статус">
                        <Select items={statuses} />
                    </Form.FastField>

                    <Form.FastField name="type" label="Тип">
                        <Select items={types} />
                    </Form.FastField>

                    <Button size="sm" type="submit">
                        Сохранить изменения
                    </Button>
                </Layout>
            </Form>
        </Popup>
    );
};

const AdditionalInfo = ({ rowInfo }: { rowInfo: any }) => {
    const { colors } = useTheme();

    const infoRowCss: CSSObject = {
        borderBottom: `1px solid ${colors?.grey400}`,
        padding: `${scale(2)}px ${scale(7, true)}px`,
        width: '90%',
    };

    const formCss: CSSObject = {
        padding: `${scale(2)}px ${scale(7, true)}px`,
        width: '70%',
    };

    return (
        <div css={{ borderBottom: `1px solid ${colors?.grey400}` }}>
            <FilePondStyles />

            <Layout cols={3} css={infoRowCss}>
                <Layout.Item>{rowInfo.user}</Layout.Item>
                <Layout.Item>Поступило новое сообщение</Layout.Item>
                <Layout.Item>{rowInfo.fullMsgData}</Layout.Item>
            </Layout>
            <Form
                css={formCss}
                initialValues={{
                    message: '',
                }}
                validationSchema={Yup.object().shape({
                    message: Yup.string().required('Обязательное поле'),
                })}
                onSubmit={values => console.log(values)}
            >
                <Form.FastField name="message">
                    <h2 css={typography('bodySmBold')}>Сообщение</h2>
                    <Textarea />
                </Form.FastField>

                <h2 css={{ margin: `${scale(1)}px 0`, ...typography('bodySmBold') }}>Файлы</h2>
                <FilePond maxFileSize="10MB" maxTotalFileSize="100MB" />

                <Button size="sm" type="submit">
                    Отправить сообщение
                </Button>
            </Form>
        </div>
    );
};

const CommunicationMessages = () => {
    /* Попап создания и редактирования сообщения */
    const [open, setOpen] = useState('none');
    const close = () => setOpen('none');
    const wasCreateOpened = useOnceChanged(open === 'create');
    const wasEditOpened = useOnceChanged(open === 'edit');
    const [popupInfo, setPopupInfo] = useState(null);

    /* Раскрытие строки таблицы*/
    const [rowAdditional, setRowAdditional] = useState<expandableRowInfoProps | undefined>(undefined);

    useEffect(() => {
        if (popupInfo !== null) setOpen('edit');
        else close();
    }, [popupInfo]);

    return (
        <PageWrapper h1={'Непрочитанные сообщения'}>
            <>
                <Layout rows={['auto']} cols={1} gap={scale(3)}>
                    <Block>
                        <Block.Header>Фильтр</Block.Header>

                        <Form
                            initialValues={{
                                theme: '',
                                channel: [],
                                status: [],
                                type: [],
                            }}
                            onSubmit={values => {
                                console.log(values);
                            }}
                        >
                            <Block.Body>
                                <Layout cols={4}>
                                    <Form.FastField name="theme" label="Тема" placeholder="Введите тему" />
                                    <Form.Field name="channel" label="Канал">
                                        <MultiSelect options={channels} />
                                    </Form.Field>
                                    <Form.Field name="status" label="Статус">
                                        <MultiSelect options={statuses} />
                                    </Form.Field>
                                    <Form.Field name="type" label="Тип">
                                        <MultiSelect options={types} />
                                    </Form.Field>
                                </Layout>
                            </Block.Body>

                            <Block.Footer>
                                <Layout cols={2} gap={scale(2)}>
                                    <Button size="sm" theme="primary" type="submit">
                                        Применить
                                    </Button>
                                    <Form.Reset size="sm" theme="secondary" type="button">
                                        Очистить
                                    </Form.Reset>
                                </Layout>
                            </Block.Footer>
                        </Form>
                    </Block>

                    <Layout.Item justify="start">
                        <Button theme="primary" onClick={() => setOpen('create')} Icon={PlusIcon}>
                            Создать чат
                        </Button>
                    </Layout.Item>

                    <Block>
                        <Block.Body>
                            <Table
                                data={data}
                                columns={columns}
                                needCheckboxesCol={false}
                                handleRowInfo={(originalRow: any) => setPopupInfo(originalRow)}
                                onRowClick={row =>
                                    setRowAdditional(
                                        rowAdditional && rowAdditional.rowId === row.id
                                            ? undefined
                                            : {
                                                  rowId: row.id,
                                                  content: <AdditionalInfo rowInfo={row} />,
                                              }
                                    )
                                }
                                expandableRowInfo={rowAdditional}
                            />
                        </Block.Body>
                    </Block>
                </Layout>

                {wasCreateOpened ? <CreatePopup isOpen={open === 'create'} close={close} /> : null}

                {wasEditOpened ? <EditPopup isOpen={open === 'edit'} close={close} editData={popupInfo} /> : null}
            </>
        </PageWrapper>
    );
};

export default CommunicationMessages;
