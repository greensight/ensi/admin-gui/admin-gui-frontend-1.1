import React, { useState } from 'react';
import * as Yup from 'yup';
import { scale, Button } from '@greensight/gds';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Table from '@components/Table';
import Form from '@standart/Form';
import Popup from '@standart/Popup';
import Select from '@standart/Select';
import Switcher from '@standart/Switcher';
import { Flatten, prepareForSelect } from '@scripts/helpers';
import { getRandomItem, makeRandomData } from '@scripts/mock';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import usePopupState from '@scripts/usePopupState';
import usePrevious from '@scripts/usePrevious';
import { ActionType } from '@scripts/enums';

const columns = [
    {
        Header: 'Название',
        accessor: 'title',
    },
];

const getTableItem = (id: number) => {
    return {
        id,
        title: [
            'Резинка для  волос женская и детская, прочная и долговечная',
            'Автомобиль typuta yaris',
            'Фен Rowenta 2000w',
            'Утюг Loreal',
            'Ikoo',
        ][id],
    };
};

const initialState = { title: '' };

type State = {
    title?: string;
    open?: boolean;
    action?: ActionType;
};

const ContentPopularProducts = () => {
    const [popupState, popupDispatch] = usePopupState<State>(initialState);
    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const [data, setData] = useState(makeRandomData(5, getTableItem));
    const [ids, setIds, selectedRows] = useSelectedRowsData<Flatten<typeof data>>(data);
    const [enabledSaveOrder, setEnabledSaveOrder] = useState(false);

    const prevData = usePrevious(data.map(d => d.id).join(''));
    React.useEffect(() => {
        if (!enabledSaveOrder && prevData && prevData !== data.map(d => d.id).join('')) setEnabledSaveOrder(true);
    }, [data, enabledSaveOrder, prevData]);

    return (
        <PageWrapper h1="Популярные товары">
            <>
                <div css={{ marginBottom: scale(2), display: 'flex', alignItems: 'center' }}>
                    {ids.length === 0 ? (
                        <Button size="sm" Icon={PlusIcon} onClick={() => popupDispatch({ type: ActionType.Add })}>
                            Добавить популярный товар
                        </Button>
                    ) : (
                        <Button size="sm" Icon={TrashIcon} onClick={() => setIsDeleteOpen(true)}>
                            Удалить популярны{ids.length === 1 ? 'й' : 'е'} товар{ids.length === 1 ? '' : 'ы'}
                        </Button>
                    )}
                    {enabledSaveOrder ? (
                        <Button
                            size="sm"
                            theme="outline"
                            css={{ marginLeft: scale(2) }}
                            onClick={() => setEnabledSaveOrder(false)}
                        >
                            Сохранить порядок
                        </Button>
                    ) : null}
                </div>
                <Block>
                    <Block.Body>
                        <Table
                            data={data}
                            columns={columns}
                            onRowSelect={setIds}
                            handleRowInfo={row => {
                                popupDispatch({ type: ActionType.Edit, payload: { title: row?.title } });
                            }}
                            setData={setData}
                            isDragDisabled={false}
                            css={{ marginBottom: scale(2) }}
                        >
                            <colgroup>
                                <col width="5%" />
                                <col width="90%" />
                                <col width="5%" />
                            </colgroup>
                        </Table>
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={Boolean(popupState.open)}
                    onRequestClose={() => popupDispatch({ type: ActionType.Close })}
                    title={`${popupState.action === 'edit' ? 'Редактирование' : 'Создание'} популярного товара`}
                    popupCss={{ minWidth: scale(50) }}
                >
                    <Form
                        initialValues={{ title: popupState.title }}
                        validationSchema={Yup.object().shape({ title: Yup.string().required('Обязательное поле') })}
                        onSubmit={vals => console.log(vals)}
                    >
                        <Form.FastField name="title" label="Товар" css={{ marginBottom: scale(2) }} />
                        <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <Form.Reset size="sm" theme="fill" css={{ marginRight: scale(2) }}>
                                {popupState.action === 'edit' ? 'Сбросить' : 'Очистить'}
                            </Form.Reset>
                            <Button type="submit" size="sm">
                                Сохранить
                            </Button>
                        </div>
                    </Form>
                </Popup>
                <Popup
                    isOpen={isDeleteOpen}
                    onRequestClose={() => setIsDeleteOpen(false)}
                    title="Вы уверены, что хотите удалить следующие популярные товары?"
                    popupCss={{ minWidth: scale(50) }}
                >
                    <ul css={{ marginBottom: scale(2) }}>
                        {selectedRows.map(r => (
                            <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                                # {r.title}
                            </li>
                        ))}
                    </ul>
                    <Button size="sm">Удалить</Button>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default ContentPopularProducts;
