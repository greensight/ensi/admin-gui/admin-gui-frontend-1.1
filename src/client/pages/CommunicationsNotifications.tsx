import React, { useMemo, useState } from 'react';
import * as Yup from 'yup';
import { scale, Button } from '@greensight/gds';
import { useLocation } from 'react-router-dom';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Table from '@components/Table';
import Pagination from '@standart/Pagination';
import Form from '@standart/Form';
import Select from '@standart/Select';
import { prepareForSelect, Flatten } from '@scripts/helpers';
import { getRandomItem } from '@scripts/mock';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import Popup from '@standart/Popup';
import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import usePopupState from '@scripts/usePopupState';
import { ActionType } from '@scripts/enums';
import { CHANNELS } from '@scripts/data/different';

const columns = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Название',
        accessor: 'title',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Тип',
        accessor: 'type',
    },
    {
        Header: 'Канал',
        accessor: 'channel',
    },
    {
        Header: 'Тема',
        accessor: 'subject',
    },
    {
        Header: 'Отправить от',
        accessor: 'sendFrom',
    },
];

const rawTypes = ['Тип 1', 'Тип 2', 'Тип 3', 'Тип 4', 'Тип 5', 'Тип 6'];

const tableItem = (id: number) => {
    return {
        id,
        title: getRandomItem(
            [
                'Сервисные Подана заявка на регистрацию',
                'Сервисные Обработана заявка на регистрацию. Статус Активный',
                'Сервисные Заявка на регистрацию обработана. Статус Отклонен',
                'Сервисные Приветствие "Добро пожаловать" - первая сессия (однократно)',
            ].map(i => [i, `notifications/${id}`])
        ),
        type: getRandomItem(rawTypes),
        channel: getRandomItem(CHANNELS),
        subject: getRandomItem(['', '', 'Тема 1', 'Тема 2']),
        sendFrom: getRandomItem(['', '', 'Администратор', 'test@mail.test']),
    };
};

const makeData = (len: number) => [...Array(len).keys()].map(el => tableItem(el));

const channels = prepareForSelect(CHANNELS);

const initialState = { title: '', active: false, channel: '', action: ActionType.Close, open: false };

type State = {
    id?: string;
    title?: string;
    type?: string;
    channel?: string;
    subject?: string;
    sendFrom?: string;
    action?: ActionType;
    open?: boolean;
};

const CommunicationsNotifications = () => {
    const [popupState, popupDispatch] = usePopupState<State>(initialState);
    const [idDeleteOpen, setIsDeleteOpen] = useState(false);
    const data = useMemo(() => makeData(5), []);

    const [ids, setIds, selectedRows] = useSelectedRowsData<Flatten<typeof data>>(data);

    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    return (
        <PageWrapper h1="Непрочитанные сообщения">
            <>
                <div css={{ marginBottom: scale(2) }}>
                    <Button size="sm" Icon={PlusIcon} onClick={() => popupDispatch({ type: ActionType.Add })}>
                        Создать сервисное уведомление
                    </Button>
                    {ids.length > 0 ? (
                        <Button
                            size="sm"
                            Icon={TrashIcon}
                            onClick={() => setIsDeleteOpen(true)}
                            css={{ marginLeft: scale(2) }}
                        >
                            Удалить сообщени{ids.length === 1 ? 'е' : 'я'}
                        </Button>
                    ) : null}
                </div>
                <Block>
                    <Block.Body>
                        <Table
                            data={data}
                            columns={columns}
                            onRowSelect={setIds}
                            handleRowInfo={row => {
                                popupDispatch({
                                    type: ActionType.Edit,
                                    payload: {
                                        id: row?.id,
                                        title: row?.title[0],
                                        subject: row?.subject,
                                        channel: row?.channel,
                                        sendFrom: row?.sendFrom,
                                        type: row?.type,
                                    },
                                });
                            }}
                            css={{ marginBottom: scale(2) }}
                        />
                        <Pagination url={pathname} activePage={activePage} pages={7} />
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={Boolean(popupState.open)}
                    onRequestClose={() => popupDispatch({ type: ActionType.Close })}
                    title={`${popupState.action === 'edit' ? 'Редактировать' : 'Создать'} сервисное уведомление`}
                    popupCss={{ minWidth: scale(50) }}
                >
                    <Form
                        initialValues={{
                            title: popupState.title,
                            type: popupState.type,
                            channel: popupState.channel,
                            subject: popupState.subject,
                            sendFrom: popupState.sendFrom,
                        }}
                        onSubmit={vals => console.log(vals)}
                        validationSchema={Yup.object().shape({
                            title: Yup.string().required('Обязательное поле'),
                            type: Yup.string().required('Обязательное поле'),
                            subject: Yup.string().required('Обязательное поле'),
                            sendFrom: Yup.string().required('Обязательное поле'),
                        })}
                    >
                        <Form.FastField name="title" label="Название" css={{ marginBottom: scale(2) }} />
                        <Form.FastField name="type" label="Тип" css={{ marginBottom: scale(2) }} />
                        <Form.FastField name="subject" label="Тема" css={{ marginBottom: scale(2) }} />
                        <Form.FastField name="channel" label="Канал" css={{ marginBottom: scale(2) }}>
                            <Select items={channels} defaultValue={popupState.channel} />
                        </Form.FastField>
                        <Form.FastField
                            name="sendFrom"
                            label="Отправить от лица пользователя"
                            css={{ marginBottom: scale(4) }}
                        />
                        <Form.Reset size="sm" theme="fill" css={{ marginRight: scale(2) }}>
                            {popupState.action === 'edit' ? 'Сбросить' : 'Очистить'}
                        </Form.Reset>
                        <Button type="submit" size="sm">
                            Сохранить
                        </Button>
                        {/* При желании можно добавить кнопку "удалить" в этот попап. Тогда для подтверждения удаления рекомендую использовать тултип */}
                    </Form>
                </Popup>
                <Popup
                    isOpen={idDeleteOpen}
                    onRequestClose={() => setIsDeleteOpen(false)}
                    title="Вы уверены, что хотите удалить следующие уведомления?"
                    popupCss={{ minWidth: scale(50) }}
                >
                    <ul css={{ marginBottom: scale(2) }}>
                        {selectedRows.map(r => (
                            <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                                #{r.id} – {r.title[0]}
                            </li>
                        ))}
                    </ul>
                    <Button size="sm">Удалить</Button>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default CommunicationsNotifications;
