import React from 'react';
import loadable from '@loadable/component';
import Loader from '@standart/Loader';

const AsyncPage = loadable((props: any) => import(`./${props.page}`), {
    fallback: (
        <div css={{ width: '100%', height: '60vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Loader />
        </div>
    ),
});

interface Route {
    /** Page URL */
    path: string;
    /** Page component name */
    page: string;
    /** Exact */
    exact?: boolean;
    /** Status code (200 if not defined) */
    status?: number;
    /** Data loading function for SSR */
    loadData?: ({ store, req }: any) => Promise<any> | Promise<any>[];
}

const routes: Route[] = [
    {
        path: '/',
        page: 'Home',
        exact: true,
    },
    {
        path: '/products/directories/properties/create',
        page: 'ProductPropertyCreate',
    },
    {
        path: '/products/directories/properties',
        page: 'ProductProperties',
    },
    {
        path: '/products/directories/categories',
        page: 'ProductDirectoriesCategories',
    },
    {
        path: '/products/catalog/:id',
        page: 'Product',
    },
    {
        path: '/products/catalog',
        page: 'ProductsCatalog',
    },
    {
        path: '/products/offers/:id',
        page: 'Offer',
    },
    {
        path: '/products/offers',
        page: 'Offers',
    },
    {
        path: '/marketing/promocodes',
        page: 'Promocodes',
    },
    {
        path: '/marketing/promocode/create',
        page: 'PromocodeCreate',
    },
    {
        path: '/seller/list/:type',
        page: 'SellerList',
    },
    {
        path: '/seller/detail/:id/',
        page: 'SellerDetail',
    },
    {
        path: '/stores/seller-stores',
        page: 'SellerStores',
        exact: true,
    },
    {
        path: '/stores/seller-stores/create',
        page: 'SellerStoresCreate',
    },
    {
        path: '/stores/seller-stores/:id',
        page: 'SellerStoresEdit',
    },
    {
        path: '/requests/prices',
        page: 'PriceChanges',
        exact: true,
    },
    {
        path: '/requests/prices/:requestId',
        page: 'PriceChange',
    },
    {
        path: '/requests/content',
        page: 'RequestsContent',
        exact: true,
    },
    {
        path: '/requests/check',
        page: 'RequestsCheck',
        exact: true,
    },
    {
        path: '/requests/check/:requestId',
        page: 'RequestCheck',
    },
    {
        path: '/requests/content/:requestId',
        page: 'RequestContent',
    },
    {
        path: '/logistic/delivery-services',
        exact: true,
        page: 'DeliveryServices',
    },
    {
        path: '/logistic/delivery-services/:id/',
        page: 'DeliveryServiceDetail',
    },
    {
        path: '/logistic/kpi',
        page: 'LogisticKPI',
    },
    {
        path: '/products/variant-groups',
        page: 'VariantGroups',
        exact: true,
    },
    {
        path: '/products/variant-groups/:id',
        page: 'VariantGroup',
    },
    {
        path: '/content/categories',
        page: 'ContentCategories',
    },
    {
        path: '/content/menu',
        page: 'Menu',
        exact: true,
    },
    {
        path: '/content/menu/:id',
        page: 'MenuDetails',
        exact: true,
    },
    {
        path: '/orders/cargos/:id',
        page: 'Cargo',
    },
    {
        path: '/orders/cargos',
        page: 'Cargos',
    },
    {
        path: '/products/directories/brands',
        page: 'Brands',
        exact: true,
    },
    {
        path: '/orders/statuses',
        page: 'Statuses',
    },
    {
        path: '/orders/list/:id',
        page: 'Order',
    },
    {
        path: '/orders/list',
        page: 'OrderList',
    },
    {
        path: '/marketing/discounts/create',
        page: 'MarketingDiscountsCreate',
    },
    {
        path: '/marketing/discounts',
        page: 'MarketingDiscounts',
    },
    {
        path: '/settings/users',
        page: 'Users',
        exact: true,
    },
    {
        path: '/settings/users/:id',
        page: 'User',
    },
    {
        path: '/settings/organizations',
        page: 'OrganizationCard',
    },
    {
        path: '/marketing/bundles/create',
        page: 'MarketingBundlesCreate',
    },
    {
        path: '/marketing/discounts/:id',
        page: 'MarketingBundle',
    },
    {
        path: '/marketing/bundles',
        page: 'MarketingBundles',
    },
    {
        path: '/reports',
        page: 'Reports',
    },
    {
        path: '/communications/messages',
        page: 'CommunicationMessages',
    },
    {
        path: '/communications/subjects',
        page: 'CommunicationsSubjects',
    },
    {
        path: '/communications/statuses',
        page: 'CommunicationsStatuses',
    },
    {
        path: '/communications/types',
        page: 'CommunicationsTypes',
    },
    {
        path: '/communications/broadcast',
        page: 'CommunicationsBroadcasts',
    },
    {
        path: '/communications/notifications/:id',
        page: 'CommunicationsNotification',
    },
    {
        path: '/communications/notifications',
        page: 'CommunicationsNotifications',
    },
    {
        path: '/customers/:id',
        page: 'Customer',
    },
    {
        path: '/content/contacts',
        page: 'ContentContacts',
    },
    {
        path: '/content/search-requests',
        page: 'ContentSearchRequests',
    },
    {
        path: '/content/search-synonyms',
        page: 'ContentSearchSynonyms',
    },
    {
        path: '/content/popular-brands',
        page: 'ContentPopularBrands',
    },
    {
        path: '/content/badges',
        page: 'ContentPopularBadges',
    },
    {
        path: '/content/popular-products',
        page: 'ContentPopularProducts',
    },
    {
        path: '/content/banners/:id',
        page: 'ContentBanner',
    },
    {
        path: '/content/banners',
        page: 'ContentBanners',
    },
    {
        path: '/content/product-groups/create',
        page: 'ContentProductGroupsCreate',
    },
    {
        path: '/content/product-groups',
        page: 'ContentProductGroups',
    },
    {
        path: '/customers',
        page: 'Customers',
    },
    {
        path: '/logistic/delivery-prices',
        page: 'DeliveryPrices',
    },
    {
        path: '*',
        page: 'NotFound',
        status: 404,
    },
];

const finalRoutes = routes.map(route => ({
    ...route,
    exact: !!route.exact,
    page: () => <AsyncPage page={route.page} />,
}));

export default finalRoutes;
