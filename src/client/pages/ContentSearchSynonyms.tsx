import React, { useMemo, useState } from 'react';
import * as Yup from 'yup';
import { useLocation } from 'react-router-dom';
import { scale, Button } from '@greensight/gds';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Table from '@components/Table';
import Pagination from '@standart/Pagination';
import Form from '@standart/Form';
import { Flatten } from '@scripts/helpers';
import { getRandomItem, makeRandomData } from '@scripts/mock';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import Popup from '@standart/Popup';
import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import usePopupState from '@scripts/usePopupState';
import { ActionType } from '@scripts/enums';
import Textarea from '@standart/Textarea';

const columns = [
    {
        Header: 'Cинонимы',
        accessor: 'title',
    },
];

const getTableItem = (id: number) => {
    return {
        id,
        title: getRandomItem([
            'большой, огромный, необъятный',
            'доставка, деливери',
            'маленький, крохотный, миниатюрный, крошечный, мало, немного, чуть-чуть',
        ]),
    };
};

const initialState = { title: '' };

type State = {
    title?: string;
    open?: boolean;
    action?: ActionType;
};

const ContentSearchSynonyms = () => {
    const [popupState, popupDispatch] = usePopupState<State>(initialState);
    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const data = useMemo(() => makeRandomData(5, getTableItem), []);
    const [ids, setIds, selectedRows] = useSelectedRowsData<Flatten<typeof data>>(data);
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    return (
        <PageWrapper h1="Поисковые синонимы">
            <>
                <div css={{ marginBottom: scale(2), display: 'flex', alignItems: 'center' }}>
                    {ids.length === 0 ? (
                        <Button size="sm" Icon={PlusIcon} onClick={() => popupDispatch({ type: ActionType.Add })}>
                            Добавить группу синонимов
                        </Button>
                    ) : (
                        <Button size="sm" Icon={TrashIcon} onClick={() => setIsDeleteOpen(true)}>
                            Удалить групп{ids.length === 1 ? 'у' : 'ы'} синонимов
                        </Button>
                    )}
                </div>
                <Block>
                    <Block.Body>
                        <Table
                            data={data}
                            columns={columns}
                            onRowSelect={setIds}
                            handleRowInfo={row => {
                                popupDispatch({
                                    type: ActionType.Edit,
                                    payload: { title: row?.title },
                                });
                            }}
                            css={{ marginBottom: scale(2) }}
                        >
                            <colgroup>
                                <col width="5%" />
                                <col width="90%" />
                                <col width="5%" />
                            </colgroup>
                        </Table>
                        <Pagination url={pathname} activePage={activePage} pages={7} />
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={Boolean(popupState.open)}
                    onRequestClose={() => popupDispatch({ type: ActionType.Close })}
                    title={`${popupState.action === 'edit' ? 'Редактирование' : 'Создание'} группы синонимов`}
                    popupCss={{ minWidth: scale(50) }}
                >
                    <Form
                        initialValues={{ title: popupState.title }}
                        validationSchema={Yup.object().shape({ title: Yup.string().required('Обязательное поле') })}
                        onSubmit={vals => console.log(vals)}
                    >
                        <Form.FastField
                            name="title"
                            label="Группа синонимов"
                            hint="Вводите значения через запятую"
                            css={{ marginBottom: scale(2) }}
                        >
                            <Textarea />
                        </Form.FastField>
                        <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <Form.Reset size="sm" theme="fill" css={{ marginRight: scale(2) }}>
                                {popupState.action === 'edit' ? 'Сбросить' : 'Очистить'}
                            </Form.Reset>
                            <Button type="submit" size="sm">
                                Сохранить
                            </Button>
                        </div>
                    </Form>
                </Popup>
                <Popup
                    isOpen={isDeleteOpen}
                    onRequestClose={() => setIsDeleteOpen(false)}
                    title="Вы уверены, что хотите удалить следующие типы?"
                    popupCss={{ minWidth: scale(50) }}
                >
                    <ul css={{ marginBottom: scale(2) }}>
                        {selectedRows.map(r => (
                            <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                                #{r.title}
                            </li>
                        ))}
                    </ul>
                    <Button size="sm">Удалить</Button>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default ContentSearchSynonyms;
