import React, { useState, useMemo } from 'react';
import { useLocation, Link } from 'react-router-dom';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import Table from '@components/Table';
import Block from '@components/Block';
import MultiSelect from '@standart/MultiSelect';
import Form from '@standart/Form';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';
import Legend from '@standart/Legend';
import Pagination from '@standart/Pagination';
import Tooltip from '@standart/Tooltip';
import typography from '@scripts/typography';
import { sellers, makeCargos, cargoStatuses, warehouses, deliveryServices } from '@scripts/mock';
import ExportIcon from '@svg/tokens/small/export.svg';
import FileIcon from '@svg/tokens/small/file.svg';
import PageWrapper from '@components/PageWrapper';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Продавец',
        accessor: 'seller',
    },
    {
        Header: 'Создано',
        accessor: 'created',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Служба доставки',
        accessor: 'delivery',
    },
    {
        Header: 'Склад',
        accessor: 'warehouse',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Кол-во коробок',
        accessor: 'boxes',
    },
    {
        Header: 'Комментарий',
        accessor: 'comment',
    },
];

const attrTypes = [
    { label: 'Не выбрано', value: '' },
    { label: 'Да', value: '1' },
    { label: 'Нет', value: '0' },
];

const SELLERS = sellers.map(i => ({ label: i, value: i }));
const DELIVERIES = deliveryServices.map(i => ({ label: i, value: i }));
const WAREHOUSES = warehouses.map(i => ({ label: i, value: i }));
const statuses = cargoStatuses.map(i => ({ label: i, value: i }));

const Cargos = () => {
    const { colors } = useTheme();
    const [moreFilters, setMoreFilters] = useState(false);
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeCargos(9), []);

    return (
        <PageWrapper h1="Грузы">
            <main>
                <Block css={{ marginBottom: scale(3) }}>
                    <Form
                        initialValues={{
                            cargoID: '',
                            seller: [],
                            cargoStatus: [],
                            deliveryService: [],
                            warehouse: [],
                            departureNo: '',
                            canceled: '',
                            createdAt: '',
                            datepickerRange: [null, null],
                        }}
                        onSubmit={values => {
                            console.log(values);
                        }}
                    >
                        <Block.Body>
                            <Layout cols={9}>
                                <Layout.Item col={1}>
                                    <Form.Field name="cargoID" label="ID" />
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="seller" label="Продавец">
                                        <MultiSelect options={SELLERS} />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="cargoStatus" label="Статус груза">
                                        <MultiSelect options={statuses} />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="deliveryService" label="Служба доставки">
                                        <MultiSelect options={DELIVERIES} />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="warehouse" label="Склад отгрузки">
                                        <MultiSelect options={WAREHOUSES} />
                                    </Form.Field>
                                </Layout.Item>
                                {moreFilters ? (
                                    <>
                                        <Layout.Item col={2}>
                                            <Form.Field name="departureNo" label="№ отправления" />
                                        </Layout.Item>
                                        <Layout.Item col={4}>
                                            <Form.Field name="datepickerRange" label="Введите дату">
                                                <DatepickerStyles />
                                                <DatepickerRange />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.Field name="canceled" label="Тип атрибута">
                                                <MultiSelect options={attrTypes} isMulti={false} />
                                            </Form.Field>
                                        </Layout.Item>
                                    </>
                                ) : null}
                            </Layout>
                        </Block.Body>
                        <Block.Footer>
                            <div css={typography('bodySm')}>
                                Найдено 135 позиций{' '}
                                <button
                                    type="button"
                                    css={{ color: colors?.primary, marginLeft: scale(2) }}
                                    onClick={() => setMoreFilters(!moreFilters)}
                                >
                                    {moreFilters ? 'Меньше' : 'Больше'} фильтров
                                </button>
                            </div>
                            <div>
                                <Form.Reset
                                    size="sm"
                                    theme="secondary"
                                    type="button"
                                    onClick={() => {
                                        console.log('hoh');
                                    }}
                                >
                                    Сбросить
                                </Form.Reset>
                                <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                    Применить
                                </Button>
                            </div>
                        </Block.Footer>
                    </Form>
                </Block>

                <Block css={{ marginBottom: scale(3) }}>
                    <Block.Body>
                        <div
                            css={{
                                display: 'grid',
                                gridGap: scale(2),
                                gridTemplateColumns: '200px 200px auto',
                            }}
                        >
                            <Tooltip
                                content={
                                    <div
                                        css={{
                                            display: 'grid',
                                            gridGap: scale(1, true),
                                            '& > a': {
                                                width: '100%',
                                                padding: `${scale(1, true)}px ${scale(1)}px`,
                                            },
                                        }}
                                    >
                                        <Link to="#">Все</Link>
                                        <Link to="#">Покупателю</Link>
                                        <Link to="#">Курьевру</Link>
                                    </div>
                                }
                                trigger="mouseenter focus click "
                                placement="bottom"
                            >
                                <button
                                    css={{ display: 'inline-flex', justifyContent: 'center', alignItems: 'center' }}
                                >
                                    <ExportIcon />
                                    &nbsp;Скачать документы
                                </button>
                            </Tooltip>
                            <Tooltip
                                content={
                                    <div
                                        css={{
                                            display: 'grid',
                                            gridGap: scale(1, true),
                                            '& > a': {
                                                width: '100%',
                                                padding: `${scale(1, true)}px ${scale(1)}px`,
                                            },
                                        }}
                                    >
                                        <Link to="#">Все</Link>
                                        <Link to="#">Покупателю</Link>
                                        <Link to="#">Курьевру</Link>
                                    </div>
                                }
                                trigger="mouseenter focus click "
                                placement="bottom"
                            >
                                <button
                                    css={{ display: 'inline-flex', justifyContent: 'center', alignItems: 'center' }}
                                >
                                    <FileIcon />
                                    &nbsp;Распечатать документы
                                </button>
                            </Tooltip>
                        </div>
                    </Block.Body>
                </Block>

                <Block>
                    <Block.Body>
                        <Table columns={COLUMNS} data={data} handleRowInfo={() => console.log('click')} />
                        <Pagination url={pathname} activePage={activePage} pages={7} />
                    </Block.Body>
                </Block>
            </main>
        </PageWrapper>
    );
};

export default Cargos;
