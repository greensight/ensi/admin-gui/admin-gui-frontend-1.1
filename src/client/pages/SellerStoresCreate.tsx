import React, { useState } from 'react';
import { Button, scale, Layout } from '@greensight/gds';

import PageWrapper from '@components/PageWrapper';
import Select, { SelectItemProps } from '@standart/Select';
import Form from '@standart/Form';
import { sellers } from '@scripts/mock';
import Block from '@components/Block';

const SELLERS = sellers.map(i => ({ label: i, value: i }));

const SellerStoresCreate = () => {
    const [selectedSeller, setSelectedSeller] = useState<SelectItemProps | null | undefined>(null);
    return (
        <PageWrapper h1="Добавление склада продавца">
            <Block>
                <Block.Body>
                    <Form
                        initialValues={{
                            seller: '',
                            title: '',
                            code: '',
                            address: '',
                            entrance: '',
                            floor: '',
                            intercom: '',
                            comment: '',
                        }}
                        onSubmit={values => {
                            console.log(values);
                        }}
                    >
                        <Layout cols={3} css={{ maxWidth: scale(128) }}>
                            <Layout.Item col={3}>
                                <Form.Field name="seller">
                                    <Select
                                        name="seller"
                                        label="Продавец"
                                        selectedItem={selectedSeller}
                                        items={SELLERS}
                                        onChange={value => setSelectedSeller(value.selectedItem)}
                                    />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.Field name="title" label="Название склада" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="code" label="Внешний код" />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="adress" label="Адрес" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="entrance" label="Подъезд" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="floor" label="Этаж" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="intercom" label="Домофон" />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="comment" label="Комментарий к адресу" />
                            </Layout.Item>
                        </Layout>
                        <Button size="sm" theme="primary" css={{ marginTop: scale(2) }} type="submit">
                            Добавить
                        </Button>
                    </Form>
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default SellerStoresCreate;
