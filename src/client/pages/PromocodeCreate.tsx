import React, { useState } from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { nanoid } from 'nanoid';
import Block from '@components/Block';
import MultiSelect from '@standart/MultiSelect';
import Legend from '@standart/Legend';
import Switcher from '@standart/Switcher';
import Datepicker from '@standart/Datepicker';
import DatepickerStyles from '@standart/Datepicker/presets';
import Form from '@standart/Form';
import typography from '@scripts/typography';
import { PROMOCODESTATUSES, PROMOTYPES, sellers } from '@scripts/mock';

const promocodeStatuses = PROMOCODESTATUSES.map(i => ({ label: i, value: i }));
const types = PROMOTYPES.map(i => ({ label: i, value: i }));
const sponsors = sellers.map(i => ({ label: i, value: i }));

const PromocodeCreate = () => {
    const { colors } = useTheme();

    const [promocode, setPromocode] = useState<string>('');
    const [dateStart, setDateStart] = useState<Date | null>(null);
    const [dateEnd, setDateEnd] = useState<Date | null>(null);
    const [showSponsor, setShowSponsor] = useState<boolean>(false);
    const [showReferral, setShowReferral] = useState<boolean>(false);
    const [showMaxUsesCount, setShowMaxUsesCount] = useState<boolean>(false);
    const [showBuyers, setShowBuyers] = useState<boolean>(false);

    //Функция для расчета высоты layout, автоматическии не получилось подогнать при direction: 'column'
    const calculateHeight = (arr: boolean[]) => {
        let count = 0;
        arr.forEach((el: boolean) => {
            if (el) count++;
        });
        return count;
    };

    const extraHeight = calculateHeight([showSponsor, showReferral, showMaxUsesCount, showBuyers]);

    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>Создание промокода</h1>

            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        name: '',
                        code: promocode,
                        type: [],
                        startDate: null,
                        endDate: null,
                        sponsorFlag: [],
                        sponsor: [],
                        referralFlag: [],
                        referralId: [],
                        maxUsesCountFlag: [],
                        maxUsesCount: [],
                        buyersFlag: [],
                        buyers: [],
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                    enableReinitialize
                >
                    <DatepickerStyles />
                    <Block.Body>
                        <Layout direction="column" cols={4} rows={5} gap={scale(2)}>
                            <Layout.Item col={4}>
                                <Form.Field name="name" label="Название" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Layout cols={2}>
                                    <Layout.Item col={1}>
                                        <Form.Field
                                            name="code"
                                            label="Код"
                                            value={promocode}
                                            onChange={e => {
                                                setPromocode(e.currentTarget.value);
                                            }}
                                            bottomTooltip={
                                                <p css={{ color: colors?.grey600, marginTop: scale(1, true) }}>
                                                    Промокод должен быть уникальным
                                                </p>
                                            }
                                        />
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <p css={{ textAlign: 'center', paddingBottom: scale(1, true) }}>
                                            Сгенерировать&nbsp;случаный&nbsp;промокод
                                        </p>
                                        <Button
                                            css={{ width: '100%' }}
                                            onClick={() => {
                                                setPromocode(nanoid(10));
                                            }}
                                        >
                                            {' '}
                                            Сгенерировать{' '}
                                        </Button>
                                    </Layout.Item>
                                </Layout>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="type" label="Тип промокода">
                                    <MultiSelect isMulti={false} options={types} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Layout cols={2}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="startDate">
                                            <Legend label="Дата старта" placeholder="дд.мм.гггг" />
                                            <Datepicker selected={dateStart} onChange={setDateStart} />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="endDate">
                                            <Legend label="Дата окончания" placeholder="дд.мм.гггг" />
                                            <Datepicker selected={dateEnd} onChange={setDateEnd} />
                                        </Form.Field>
                                    </Layout.Item>
                                </Layout>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="status" label="Статус">
                                    <MultiSelect isMulti={false} options={promocodeStatuses} css={{ zIndex: 3 }} />
                                </Form.Field>
                            </Layout.Item>
                        </Layout>
                        <Layout
                            direction="column"
                            cols={4}
                            gap={0}
                            rows={6 + extraHeight}
                            css={{ marginTop: scale(2), marginBottom: scale(4) }}
                        >
                            <Layout.Item col={1}>
                                <Form.Field name="sponsorFlag">
                                    <Switcher
                                        value="sponsorFlag"
                                        onInput={e => setShowSponsor(e.currentTarget.checked)}
                                    >
                                        Спонсор
                                    </Switcher>
                                </Form.Field>
                            </Layout.Item>
                            {showSponsor && (
                                <Layout.Item col={1}>
                                    <Form.Field name="sponsor" label="Выберите продавца">
                                        <MultiSelect isMulti={false} options={sponsors} css={{ zIndex: 3 }} />
                                    </Form.Field>
                                </Layout.Item>
                            )}
                            <Layout.Item col={1}>
                                <Form.Field name="referralFlag">
                                    <Switcher
                                        value="referralFlag"
                                        onInput={e => setShowReferral(e.currentTarget.checked)}
                                    >
                                        Привязать промокод к РП
                                    </Switcher>
                                </Form.Field>
                            </Layout.Item>
                            {showReferral && (
                                <Layout.Item col={1}>
                                    <Form.Field
                                        name="referralId"
                                        bottomTooltip={
                                            <p css={{ color: colors?.grey600, marginTop: scale(1, true) }}>
                                                ID реферального партнера
                                            </p>
                                        }
                                    />
                                </Layout.Item>
                            )}
                            <p css={{ ...typography('bodyMdBold'), paddingTop: scale(2) }}>
                                Ограничение&nbsp;на&nbsp;применение&nbsp;промокода
                            </p>
                            <Layout.Item col={1}>
                                <Form.Field name="maxUsesCountFlag">
                                    <Switcher
                                        value="maxUsesCountFlag"
                                        onInput={e => setShowMaxUsesCount(e.currentTarget.checked)}
                                    >
                                        Максимальное количество применений
                                    </Switcher>
                                </Form.Field>
                            </Layout.Item>
                            {showMaxUsesCount && (
                                <Layout.Item col={1}>
                                    <Form.Field
                                        name="maxUsesCount"
                                        bottomTooltip={
                                            <p css={{ color: colors?.grey600, marginTop: scale(1, true) }}>
                                                Не более N раз
                                            </p>
                                        }
                                    />
                                </Layout.Item>
                            )}
                            <Layout.Item col={1}>
                                <Form.Field name="buyersFlag">
                                    <Switcher value="buyersFlag" onInput={e => setShowBuyers(e.currentTarget.checked)}>
                                        Покупатели
                                    </Switcher>
                                </Form.Field>
                            </Layout.Item>
                            {showBuyers && (
                                <Layout.Item col={2}>
                                    <Form.Field
                                        name="buyers"
                                        bottomTooltip={
                                            <p css={{ color: colors?.grey600, marginTop: scale(1, true) }}>
                                                ID пользователей (через запятую)
                                            </p>
                                        }
                                    />
                                </Layout.Item>
                            )}
                            <Layout.Item col={1} css={{ marginTop: scale(3) }}>
                                <Button type="submit">Сохранить</Button>
                            </Layout.Item>
                        </Layout>
                    </Block.Body>
                </Form>
            </Block>
        </main>
    );
};

export default PromocodeCreate;
