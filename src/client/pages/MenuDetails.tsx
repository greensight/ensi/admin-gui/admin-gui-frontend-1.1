import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { FormikValues } from 'formik';
import { useTheme, scale, Layout, Button } from '@greensight/gds';
import { CSSObject } from '@emotion/core';
import * as Yup from 'yup';
import { DragDropContext, Droppable, Draggable, DropResult } from 'react-beautiful-dnd';

import typography from '@scripts/typography';
import { menuTable, menuItems } from '@scripts/mock';
import TrashIcon from '@svg/tokens/small/trash.svg';
import EditIcon from '@svg/tokens/small/edit.svg';
import ViewIcon from '@svg/view.svg';
import PlusIcon from '@svg/plus.svg';

import Block from '@components/Block';
import Form from '@standart/Form';
import Checkbox from '@standart/Checkbox';
import Popup from '@standart/Popup';
import { CheckboxGroup } from '@standart/CheckboxGroup';

interface IMenuItem {
    name: string;
    link: string;
}

const MenuEditPopup = ({
    isOpen,
    onRequestClose,
    editedItem = null,
}: {
    isOpen: boolean;
    onRequestClose: () => void;
    editedItem?: IMenuItem | null;
}) => {
    return (
        <Popup
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            title={editedItem ? 'Редактирование пункта меню' : 'Создание пункта меню'}
            popupCss={{ minWidth: scale(60) }}
        >
            <Form
                onSubmit={(values: FormikValues) => {
                    console.log(values);
                }}
                initialValues={{
                    name: editedItem ? editedItem.name : '',
                    link: editedItem ? editedItem.link : '',
                }}
                validationSchema={Yup.object().shape({
                    name: Yup.string().required('Обязательное поле'),
                    link: Yup.string().required('Обязательное поле'),
                })}
            >
                <Form.FastField name="name" label="Название*" css={{ marginBottom: scale(2) }} />

                <Form.FastField name="link" label="Ссылка*" css={{ marginBottom: scale(2) }} />

                <Button type="submit" size="sm">
                    Применить
                </Button>
            </Form>
        </Popup>
    );
};

const MenuItem = ({
    item,
    subItems,
    checkboxName,
}: {
    item: IMenuItem;
    subItems: IMenuItem[];
    checkboxName: string;
}) => {
    const { colors } = useTheme();

    const subItemStyle: CSSObject = {
        paddingTop: scale(1),
        paddingBottom: scale(1),
        borderBottom: `1px solid ${colors?.grey400}`,
    };

    const [isPopupOpen, setIsPopupOpen] = useState(false);
    const [editedItem, setEditedItem] = useState<IMenuItem | null>(null);

    const [subMenu, setSubMenu] = useState(subItems);

    const reorderItems = React.useCallback(
        (startIndex: number, endIndex: number) => {
            const newData = [...subMenu];
            const [movedRow] = newData.splice(startIndex, 1);
            newData.splice(endIndex, 0, movedRow);
            if (setSubMenu) setSubMenu(newData);
        },
        [subMenu, setSubMenu]
    );

    const onDragEnd = React.useCallback(
        ({ source, destination }: DropResult) => {
            if (!destination || destination.index === source.index) return;
            reorderItems(source.index, destination.index);
        },
        [reorderItems]
    );

    return (
        <>
            <Block css={{ marginTop: scale(2) }}>
                <Block.Header>
                    <Layout cols={2} align="center" css={{ width: '100%' }}>
                        <Layout.Item>
                            <Form.Field name={checkboxName}>
                                <Checkbox name={checkboxName} value={item.name}>
                                    <b>
                                        {item.name} ({item.link})
                                    </b>
                                </Checkbox>
                            </Form.Field>
                        </Layout.Item>

                        <Layout.Item justify="end">
                            <Layout cols={['auto', 'auto']} gap={scale(2)}>
                                <Button
                                    theme="outline"
                                    size="sm"
                                    Icon={EditIcon}
                                    hidden
                                    onClick={() => {
                                        setIsPopupOpen(true);
                                        setEditedItem(item);
                                    }}
                                >
                                    Изменить
                                </Button>
                                <Button
                                    theme="secondary"
                                    size="sm"
                                    Icon={TrashIcon}
                                    hidden
                                    onClick={() => alert(`Удалить: ${item.name}`)}
                                >
                                    Удалить
                                </Button>
                            </Layout>
                        </Layout.Item>
                    </Layout>
                </Block.Header>

                <Block.Body css={subMenu.length == 0 && { padding: 0, paddingTop: scale(2) }}>
                    <DragDropContext onDragEnd={onDragEnd}>
                        <Droppable droppableId={`droppable-${item.name}`}>
                            {provided => (
                                <div ref={provided.innerRef} {...provided.droppableProps}>
                                    {subMenu.length != 0 &&
                                        subMenu.map((subItem, index) => (
                                            <Draggable
                                                draggableId={`draggable-${item.name}-${index}`}
                                                key={index}
                                                index={index}
                                            >
                                                {(draggableProvided, snapshot) => (
                                                    <div
                                                        ref={draggableProvided.innerRef}
                                                        {...draggableProvided.dragHandleProps}
                                                        {...draggableProvided.draggableProps}
                                                        key={index}
                                                    >
                                                        <Layout cols={2} align="center" css={subItemStyle}>
                                                            <Layout.Item col={1}>
                                                                {index + 1}. {subItem.name} ({subItem.link})
                                                            </Layout.Item>

                                                            <Layout.Item col={1} justify="end">
                                                                <Layout cols={['auto', 'auto']} gap={scale(2)}>
                                                                    <Button
                                                                        theme="outline"
                                                                        size="sm"
                                                                        Icon={EditIcon}
                                                                        hidden
                                                                        onClick={() => {
                                                                            setIsPopupOpen(true);
                                                                            setEditedItem(subItem);
                                                                        }}
                                                                    >
                                                                        Изменить
                                                                    </Button>
                                                                    <Button
                                                                        theme="secondary"
                                                                        size="sm"
                                                                        Icon={TrashIcon}
                                                                        hidden
                                                                        onClick={() =>
                                                                            alert(`Удалить: ${subItem.name}`)
                                                                        }
                                                                    >
                                                                        Удалить
                                                                    </Button>
                                                                </Layout>
                                                            </Layout.Item>
                                                        </Layout>
                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                    </DragDropContext>
                </Block.Body>

                <Button
                    theme="primary"
                    size="sm"
                    css={{ marginLeft: scale(3), marginBottom: scale(2) }}
                    Icon={PlusIcon}
                    onClick={() => setIsPopupOpen(true)}
                >
                    Добавить подпункт
                </Button>
            </Block>

            <MenuEditPopup
                isOpen={isPopupOpen}
                onRequestClose={() => {
                    setIsPopupOpen(false);
                    setEditedItem(null);
                }}
                editedItem={editedItem}
            />
        </>
    );
};

const MenuDetails = () => {
    const { colors } = useTheme();
    const { id } = useParams<{ id: string }>();
    const menuInfo = menuTable.find(item => item.id.toString() == id);
    const items = id == '2' ? menuItems : [];

    const formCss: CSSObject = {
        paddingBottom: scale(3),
        marginBottom: scale(2),
        borderBottom: `1px solid ${colors?.grey600}`,
    };

    const titleCss: CSSObject = {
        ...typography('h1'),
        marginBottom: scale(2),
        marginTop: 0,
    };

    const [isPopupOpen, setIsPopupOpen] = useState(false);

    return (
        <>
            <main css={{ padding: `${scale(2)}px ${scale(3)}px` }}>
                <h1 css={titleCss}>{menuInfo?.title[0]}</h1>

                <Form
                    initialValues={{ hiddenItems: [] }}
                    onSubmit={(values: FormikValues) => alert(values.hiddenItems)}
                    css={formCss}
                >
                    <Layout cols={2} align="center">
                        <Layout.Item col={1}>
                            <h3> Скрытые пункты меню </h3>
                        </Layout.Item>

                        <Layout.Item col={1} justify="end">
                            <Button theme="primary" size="sm" Icon={ViewIcon} type="submit">
                                Активировать выбранные
                            </Button>
                        </Layout.Item>
                    </Layout>

                    {items.map(
                        (item, index) =>
                            item.hidden && (
                                <MenuItem item={item} subItems={item.subItems} checkboxName="hiddenItems" key={index} />
                            )
                    )}
                </Form>
                <Form
                    initialValues={{ activeItems: [] }}
                    onSubmit={(values: FormikValues) => alert(values.activeItems)}
                    css={formCss}
                >
                    <Layout cols={2} align="center" css={{ marginTop: scale(3) }}>
                        <Layout.Item col={1}>
                            <h3> Активные пункты меню </h3>
                        </Layout.Item>

                        <Layout.Item col={1} justify="end">
                            <Button theme="primary" size="sm" Icon={ViewIcon} type="submit">
                                Скрыть выбранные
                            </Button>
                        </Layout.Item>
                    </Layout>

                    <Form.Field name="activeItems">
                        <CheckboxGroup name="activeItems">
                            {items.map(
                                (item, index) =>
                                    !item.hidden && (
                                        <MenuItem
                                            item={item}
                                            subItems={item.subItems}
                                            checkboxName="activeItems"
                                            key={index}
                                        />
                                    )
                            )}
                        </CheckboxGroup>
                    </Form.Field>
                </Form>

                <Layout cols={['auto', 'auto', 1]} gap={scale(2)}>
                    <Button theme="primary" size="sm" Icon={PlusIcon} onClick={() => setIsPopupOpen(true)}>
                        Добавить пункт
                    </Button>
                    <Button theme="outline" size="sm">
                        Сохранить
                    </Button>
                </Layout>

                <MenuEditPopup
                    isOpen={isPopupOpen}
                    onRequestClose={() => {
                        setIsPopupOpen(false);
                    }}
                />
            </main>
        </>
    );
};

export default MenuDetails;
