import React, { useMemo } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { scale } from '@greensight/gds';

import Pagination from '@standart/Pagination';
import Table from '@components/Table';
import Block from '@components/Block';
import Badge from '@components/Badge';

import { makePriceChange } from '@scripts/mock';
import typography from '@scripts/typography';
import { STATUSES as STATUSES_TYPES, CELL_TYPES } from '@scripts/enums';

const COLUMNS = [
    {
        Header: 'ID предложения продавца',
        accessor: 'id',
    },
    {
        Header: 'ID товара',
        accessor: 'productId',
    },
    {
        Header: 'Название товара',
        accessor: 'name',
    },
    {
        Header: 'Артикул товара',
        accessor: 'productArticle',
    },
    {
        Header: 'Старая цена предложения продавца',
        accessor: 'oldPrice',
        getProps: () => ({ type: CELL_TYPES.PRICE }),
    },
    {
        Header: 'Новая цена предложения продавца',
        accessor: 'newPrice',
        getProps: () => ({ type: CELL_TYPES.PRICE }),
    },
    {
        Header: 'Статус изменения цены',
        accessor: 'status',
        getProps: () => ({ type: CELL_TYPES.STATUS }),
    },
    {
        Header: 'Комментарий по изменению цены',
        accessor: 'comment',
    },
];

const PriceChange = () => {
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makePriceChange(2), []);
    const { requestId }: { requestId: string } = useParams();

    const descriptionTitles = [
        {
            id: 1,
            name: 'Продавец',
            value: 'Резинки для волос',
        },
        {
            id: 2,
            name: 'Автор',
            value: 'Резинкин Петр Петрович',
        },
        {
            id: 3,
            name: 'Количество',
            value: '2 ед. товара',
        },
    ];

    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <Block css={{ marginBottom: scale(3) }}>
                <Block.Header>
                    <div css={{ display: 'flex', flexDirection: 'column', minWidth: 400 }}>
                        <h1
                            css={{
                                ...typography('h1'),
                                marginBottom: scale(2),
                                marginTop: 0,
                                display: 'flex',
                                alignItems: 'center',
                            }}
                        >
                            <span css={{ marginRight: scale(1) }}>Заявка #{requestId} от 2020-12-30 15:27:16</span>
                            <Badge text="В работе" type={STATUSES_TYPES.SUCCESS} />
                        </h1>
                        <div>
                            {descriptionTitles.map(description => (
                                <div
                                    css={{ display: 'flex', justifyContent: 'space-between', marginBottom: scale(1) }}
                                    key={description.id}
                                >
                                    <dd css={{ fontWeight: 'bold' }}>{description.name}: </dd>
                                    <dt>{description.value}</dt>
                                </div>
                            ))}
                        </div>
                    </div>
                </Block.Header>
                <Block.Body>
                    <Table columns={COLUMNS} data={data} handleRowInfo={() => console.log('click')} />
                    <Pagination url={pathname} activePage={activePage} pages={1} />
                </Block.Body>
            </Block>
        </main>
    );
};

export default PriceChange;
