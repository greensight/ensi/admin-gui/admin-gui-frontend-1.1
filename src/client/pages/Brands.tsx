import React, { useState, useMemo } from 'react';
import { Global, CSSObject } from '@emotion/core';
import { useLocation } from 'react-router-dom';
import PageWrapper from '@components/PageWrapper';

import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';

import MultiSelect from '@standart/MultiSelect';
import Table from '@components/Table';
import Block from '@components/Block';

import Pagination from '@standart/Pagination';
import Form from '@standart/Form';
import Textarea from '@standart/Textarea';
import Popup, { PopupProps } from '@standart/Popup';

const brands = ['Vikki&Lilli', 'Ikoo', 'Rowenta', "L'oreal"];

const brandGroupTableItem = (id: number, brand: string) => {
    return {
        id: `100${id}`,
        title: brand,
        logo: null,
        code: '001',
    };
};

const makeBrands = () => brands.map((el, index) => brandGroupTableItem(index, el));

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Логотип',
        accessor: 'logo',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: 'Код',
        accessor: 'code',
    },
];

const BrandTable = () => {
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makeBrands(), []);

    return (
        <Block>
            <Block.Body>
                <Table columns={COLUMNS} data={data} handleRowInfo={() => console.log('click')}>
                    <colgroup>
                        <col width="5%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="30%" />
                        <col width="40%" />
                        <col width="5%" />
                    </colgroup>
                </Table>
                <Pagination url={pathname} activePage={activePage} pages={7} />
            </Block.Body>
        </Block>
    );
};

const BrandsPopup = ({ children, ...props }: PopupProps) => {
    const popupCss: CSSObject = {
        padding: 0,
    };

    const modalCss: CSSObject = {
        '.brands-popup.popup-content': {
            minWidth: 424,
            overflowY: 'visible',
        },
    };

    return (
        <>
            <Global styles={modalCss} />
            <Popup {...props} className="brands-popup" popupCss={popupCss}>
                {children}
            </Popup>
        </>
    );
};

const Brands = () => {
    const { colors } = useTheme();
    const [isPopupOpen, setIsPopupOpen] = useState(false);

    const MockDropzone: React.FunctionComponent = ({ children }) => {
        const labelCss: CSSObject = {
            display: 'block',
            padding: `6px 12px`,
            backgroundColor: '#fff',
            border: '1px solid',
            borderColor: colors?.grey400,

            '&:focus-within': {
                borderColor: colors?.primary,
            },
        };

        const inputCss: CSSObject = { position: 'absolute', zIndex: -1, opacity: 0 };

        return (
            <label css={labelCss}>
                <input type="file" css={inputCss} />
                {children}
            </label>
        );
    };

    return (
        <PageWrapper h1="Бренды">
            <>
                <Button css={{ marginBottom: scale(2) }} onClick={() => setIsPopupOpen(true)}>
                    Создать бренд
                </Button>

                <BrandTable />

                <BrandsPopup
                    isOpen={isPopupOpen}
                    onRequestClose={() => setIsPopupOpen(false)}
                    isCloseButton={true}
                    isFullscreen={false}
                >
                    <Block>
                        <Form
                            initialValues={{
                                title: '',
                                code: '',
                                description: '',
                            }}
                            onSubmit={values => {
                                console.log(values);
                            }}
                        >
                            <Block.Header>
                                <h3 css={{ ...typography('h3'), paddingRight: 20 }}>Добавление нового бренда</h3>
                            </Block.Header>

                            <Block.Body>
                                <Layout cols={1}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="name" label="Название" />
                                    </Layout.Item>

                                    <Layout.Item col={1}>
                                        <Form.Field name="code" label="Код" />
                                    </Layout.Item>

                                    <Layout.Item col={1}>
                                        <Form.Field name="description" label="Описание">
                                            <Textarea rows={3} />
                                        </Form.Field>
                                    </Layout.Item>

                                    <Layout.Item col={1}>
                                        <MockDropzone>Выберите файл</MockDropzone>
                                    </Layout.Item>
                                </Layout>
                            </Block.Body>

                            <Block.Footer>
                                <div
                                    css={{
                                        width: '100%',
                                        display: 'flex',
                                        justifyContent: 'flex-end',
                                    }}
                                >
                                    <Button onClick={() => setIsPopupOpen(false)}>Отмена</Button>
                                    <Button css={{ marginLeft: scale(2) }} onClick={() => setIsPopupOpen(false)}>
                                        Сохранить
                                    </Button>
                                </div>
                            </Block.Footer>
                        </Form>
                    </Block>
                </BrandsPopup>
            </>
        </PageWrapper>
    );
};

export default Brands;
