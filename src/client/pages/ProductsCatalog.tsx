import React, { useState, useMemo, useCallback } from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import PageWrapper from '@components/PageWrapper';
import Table from '@components/Table';
import Block from '@components/Block';
import Form from '@standart/Form';
import Select from '@standart/Select';
import { makeProducts } from '@scripts/mock';
import Pagination from '@standart/Pagination';
import { useLocation } from 'react-router-dom';
import Popup from '@standart/Popup';
import Tooltip from '@standart/Tooltip';
import TipIcon from '@svg/tokens/small/tooltip/tip.svg';
import DatepickerStyles from '@standart/Datepicker/presets';
import Datepicker from '@standart/Datepicker';
import Legend from '@standart/Legend';
import CopyIcon from '@svg/tokens/small/copy.svg';
import SettingsIcon from '@svg/tokens/small/settings.svg';
import CheckboxGroup from '@standart/CheckboxGroup';
import Checkbox from '@standart/Checkbox';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import ExportIcon from '@svg/tokens/small/export.svg';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: '',
        accessor: 'photo',
        getProps: () => ({ type: 'photo' }),
    },
    {
        Header: 'Название и артикул',
        accessor: 'title',
        getProps: () => ({ type: 'double' }),
    },
    {
        Header: 'Бренд',
        accessor: 'brand',
    },
    {
        Header: 'Категория',
        accessor: 'category',
    },
    {
        Header: 'Дата создания',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Цена, руб.',
        accessor: 'price',
        getProps: () => ({ type: 'price' }),
    },
    {
        Header: 'Количество, шт',
        accessor: 'quantity',
    },
    {
        Header: 'На витрине',
        accessor: 'active',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'В архиве',
        accessor: 'archive',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Контент',
        accessor: 'content',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Согласование',
        accessor: 'agreed',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Отгружен в Shoppilot',
        accessor: 'shoppilot',
    },
];

const changeStatusPopupColumns = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Название',
        accessor: 'title',
        getProps: () => ({ type: 'double' }),
    },
];

const ProductsCatalog = () => {
    const { colors } = useTheme();
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const [startDate, setStartDate] = React.useState<Date | null>(null);
    const [endDate, setEndDate] = React.useState<Date | null>(null);
    const [moreFilters, setMoreFilters] = useState(false);
    const [isChangeBadgeOpen, setIsChangeBadgeOpen] = useState(false);
    const [isChangeStatusOpen, setIsChangeStatusOpen] = useState(false);
    const data = useMemo(() => makeProducts(10), []);
    const [ids, setIds, popupTableData] = useSelectedRowsData<typeof data[0]>(data);

    const copyToClipBoard = useCallback(() => {
        const selectedIDs = popupTableData.map((d: { id: string }) => d.id);
        navigator.clipboard.writeText(selectedIDs.join(',')).catch(() => alert('Не удалось скопировать ID!'));
    }, [popupTableData]);

    return (
        <PageWrapper h1="Товары">
            <>
                <DatepickerStyles />
                <Block css={{ marginBottom: scale(3) }}>
                    <Form
                        initialValues={{
                            name: '',
                            vendorCode: '',
                            id: '',
                            activeOptions: '',
                            archiveOptions: '',
                            priceFrom: '',
                            priceTo: '',
                            quantityFrom: '',
                            quantityTo: '',
                            dateFrom: null,
                            dateTo: null,
                            brand: '',
                            category: '',
                            agreed: '',
                            content: '',
                        }}
                        onSubmit={values => {
                            console.log(values);
                        }}
                    >
                        <Block.Body>
                            <Layout cols={12}>
                                <Layout.Item col={4}>
                                    <Form.Field name="name" label="Название" />
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="vendorCode" label="Артикул" />
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="id" label="ID" />
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="activeOptions">
                                        <Select
                                            label="На витрине"
                                            defaultIndex={0}
                                            items={[
                                                { value: '', label: 'Не выбрано' },
                                                { value: 'yes', label: 'Да' },
                                                { value: 'no', label: 'Нет' },
                                            ]}
                                        />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="archiveOptions">
                                        <Select
                                            label="В архиве"
                                            defaultIndex={0}
                                            items={[
                                                { value: '', label: 'Не выбрано' },
                                                { value: 'inArchive', label: 'В архиве' },
                                                { value: 'notInArchive', label: 'Не в архиве' },
                                            ]}
                                        />
                                    </Form.Field>
                                </Layout.Item>

                                {moreFilters ? (
                                    <>
                                        <Layout.Item col={2}>
                                            <Form.Field name="priceFrom" label="Цена" type="number" placeholder="От" />
                                        </Layout.Item>
                                        <Layout.Item col={2} css={{ marginTop: scale(3) }} align="end">
                                            <Form.Field name="priceTo" type="number" placeholder="До" />
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.Field
                                                name="quantityFrom"
                                                label="Количество"
                                                type="number"
                                                placeholder="От"
                                            />
                                        </Layout.Item>
                                        <Layout.Item col={2} css={{ marginTop: scale(3) }} align="end">
                                            <Form.Field name="quantityTo" type="number" placeholder="До" />
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.Field name="dateFrom">
                                                <Legend label="Введите дату от" />
                                                <Datepicker
                                                    selectsStart
                                                    selected={startDate}
                                                    startDate={startDate}
                                                    endDate={endDate}
                                                    maxDate={endDate}
                                                    onChange={setStartDate}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={2}>
                                            <Form.Field name="DateTo">
                                                <Legend label="Введите дату до" />
                                                <Datepicker
                                                    selectsEnd
                                                    selected={endDate}
                                                    startDate={startDate}
                                                    endDate={endDate}
                                                    minDate={startDate}
                                                    onChange={setEndDate}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={3}>
                                            <Form.Field name="brand">
                                                <Select
                                                    label="Бренд"
                                                    defaultIndex={0}
                                                    items={[
                                                        { value: '', label: 'Не выбрано' },
                                                        { value: 'loreal', label: 'Loreal' },
                                                        { value: 'BrookBond', label: 'BrookBond' },
                                                    ]}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={3}>
                                            <Form.Field name="category">
                                                <p css={{ marginBottom: scale(1) }}>
                                                    Категория{' '}
                                                    <Tooltip
                                                        content="Будут показаны товары выбранной и всех дочерних категорий"
                                                        arrow
                                                        maxWidth={scale(30)}
                                                    >
                                                        <button type="button" css={{ verticalAlign: 'middle' }}>
                                                            <TipIcon />
                                                        </button>
                                                    </Tooltip>
                                                </p>
                                                <Select
                                                    defaultIndex={0}
                                                    items={[
                                                        { value: '', label: 'Не выбрано' },
                                                        { value: 'loreal', label: 'Loreal' },
                                                        { value: 'BrookBond', label: 'BrookBond' },
                                                    ]}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={3}>
                                            <Form.Field name="agreed">
                                                <Select
                                                    label="Согласовано"
                                                    defaultIndex={0}
                                                    items={[
                                                        { value: '', label: 'Не выбрано' },
                                                        { value: 'notAgreed', label: 'Не согласовано' },
                                                        { value: 'sent', label: 'Отправлено' },
                                                        { value: 'inProcess', label: 'На рассмотрении' },
                                                        { value: 'rejected', label: 'Отклонено' },
                                                        { value: 'agreed', label: 'Согласовано' },
                                                    ]}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={3}>
                                            <Form.Field name="content">
                                                <Select
                                                    label="Контент"
                                                    defaultIndex={0}
                                                    items={[
                                                        { value: '', label: 'Не выбрано' },
                                                        { value: 'needShooting', label: 'Необходима съемка' },
                                                        { value: 'shootingAllowed', label: 'Съемка одобрена' },
                                                        { value: 'accepted', label: 'Принято на студии' },
                                                        { value: 'shooting', label: 'На съемке' },
                                                        { value: 'agreed', label: 'Согласовано' },
                                                        { value: 'uploaded', label: 'Загружено' },
                                                        { value: 'clarify', label: 'Требуется уточнение' },
                                                        { value: 'rejected', label: 'Отклонено' },
                                                    ]}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                    </>
                                ) : null}
                            </Layout>
                        </Block.Body>
                        <Block.Footer>
                            <div css={typography('bodySm')}>
                                Найдено 135 товаров{' '}
                                <button
                                    type="button"
                                    css={{ color: colors?.primary, marginLeft: scale(2) }}
                                    onClick={() => setMoreFilters(!moreFilters)}
                                >
                                    {moreFilters ? 'Меньше' : 'Больше'} фильтров
                                </button>{' '}
                            </div>
                            <div>
                                <Form.Reset
                                    size="sm"
                                    theme="secondary"
                                    type="button"
                                    onClick={() => {
                                        setStartDate(null);
                                        setEndDate(null);
                                    }}
                                >
                                    Сбросить
                                </Form.Reset>
                                <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                    Применить
                                </Button>
                            </div>
                        </Block.Footer>
                    </Form>
                </Block>

                <Block>
                    <Block.Header>
                        <div css={{ display: 'flex' }}>
                            <Button
                                theme="primary"
                                size="sm"
                                css={{ marginRight: scale(2) }}
                                onClick={() => alert('Тут должен быть экспорт')}
                                Icon={ExportIcon}
                            >
                                Экспорт отфильтрованных товаров
                            </Button>
                            {ids.length !== 0 ? (
                                <>
                                    <Button
                                        theme="primary"
                                        size="sm"
                                        css={{ marginRight: scale(2) }}
                                        onClick={() => alert('Тут должен быть экспорт')}
                                        Icon={ExportIcon}
                                    >
                                        Экспорт выбранных
                                    </Button>
                                    <Button
                                        theme="primary"
                                        size="sm"
                                        css={{ marginRight: scale(2) }}
                                        onClick={() => setIsChangeStatusOpen(true)}
                                    >
                                        Изменить статус
                                    </Button>
                                    <Tooltip
                                        content={`ID скопирован${ids.length > 1 ? 'ы' : ''}`}
                                        arrow
                                        maxWidth={scale(30)}
                                        trigger="click"
                                        onShow={instance => {
                                            setTimeout(() => {
                                                instance.hide();
                                            }, 1000);
                                        }}
                                    >
                                        <Button
                                            theme="outline"
                                            size="sm"
                                            css={{ marginRight: scale(2) }}
                                            Icon={CopyIcon}
                                            onClick={copyToClipBoard}
                                        >
                                            Копировать ID
                                        </Button>
                                    </Tooltip>
                                    <Button
                                        theme="outline"
                                        size="sm"
                                        css={{ marginRight: scale(2) }}
                                        Icon={SettingsIcon}
                                        onClick={() => setIsChangeBadgeOpen(true)}
                                    >
                                        Назначить шильдики
                                    </Button>
                                </>
                            ) : null}
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Table
                            columns={COLUMNS}
                            data={data}
                            handleRowInfo={row => console.log('rowdata', row)}
                            onRowSelect={setIds}
                        />
                        <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={isChangeStatusOpen}
                    onRequestClose={() => setIsChangeStatusOpen(false)}
                    title={`Редактировать статус оффер${ids.length === 1 ? 'a' : 'ов'}`}
                    popupCss={{ minWidth: scale(60) }}
                >
                    <Form
                        onSubmit={values => {
                            console.log(values);
                        }}
                        initialValues={{
                            status: null,
                        }}
                    >
                        <div
                            css={{
                                maxHeight: scale(40),
                                overflow: 'auto',
                                WebkitOverflowScrolling: 'touch',
                                marginBottom: scale(4),
                            }}
                        >
                            <Table
                                columns={changeStatusPopupColumns}
                                data={popupTableData}
                                needCheckboxesCol={false}
                                needSettingsColumn={false}
                                css={{ marginBottom: scale(2) }}
                            />
                        </div>
                        <Form.Field name="status" css={{ marginBottom: scale(2) }}>
                            <Select
                                label="Сменить статус"
                                items={[
                                    { value: 'inArchive', label: 'В архив' },
                                    { value: 'fromArchive', label: 'Из архива' },
                                    { value: 'needShooting', label: 'Контент: Необходима съемка' },
                                    { value: 'shootingAllowed', label: 'Контент: Съемка одобрена' },
                                    { value: 'accepted', label: 'Контент: Принято на студии' },
                                    { value: 'shooting', label: 'Контент: На съемке' },
                                    { value: 'agreed', label: 'Контент: Согласовано' },
                                    { value: 'uploaded', label: 'Контент: Загружено' },
                                    { value: 'clarify', label: 'Контент: Требуется уточнение' },
                                    { value: 'rejected', label: 'Контент: Отклонено' },
                                    { value: 'notAgreed', label: 'Согласование: Не согласовано' },
                                    { value: 'sent', label: 'Согласование: Отправлено' },
                                    { value: 'inProcess', label: 'Согласование: На рассмотрении' },
                                    { value: 'rejected', label: 'Согласование: Отклонено' },
                                    { value: 'agreed', label: 'Согласование: Согласовано' },
                                ]}
                            />
                        </Form.Field>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </Form>
                </Popup>
                <Popup
                    isOpen={isChangeBadgeOpen}
                    onRequestClose={() => setIsChangeBadgeOpen(false)}
                    title={`Редактирование шильдиков товар${ids.length === 1 ? 'a' : 'ов'}`}
                    popupCss={{ minWidth: scale(60) }}
                >
                    <Form
                        onSubmit={values => {
                            console.log(values);
                        }}
                        initialValues={{
                            badges: [],
                        }}
                    >
                        <Form.Field name="badges" css={{ marginBottom: scale(2) }}>
                            <CheckboxGroup label="Шильдики">
                                <Checkbox value="new">Новинки</Checkbox>
                                <Checkbox value="discount">Скидки</Checkbox>
                                <Checkbox value="week">Товар недели</Checkbox>
                                <Checkbox value="hit">Хит</Checkbox>
                            </CheckboxGroup>
                        </Form.Field>
                        <div
                            css={{
                                maxHeight: scale(40),
                                overflow: 'auto',
                                WebkitOverflowScrolling: 'touch',
                                marginBottom: scale(2),
                            }}
                        >
                            <Table
                                columns={changeStatusPopupColumns}
                                data={popupTableData}
                                needCheckboxesCol={false}
                                needSettingsColumn={false}
                                css={{ marginBottom: scale(2) }}
                            />
                        </div>

                        <div css={{ display: 'flex' }}>
                            <Form.Reset
                                size="sm"
                                theme="outline"
                                onClick={() => setIsChangeBadgeOpen(false)}
                                css={{ marginRight: scale(2) }}
                            >
                                Отменить
                            </Form.Reset>
                            <Button type="submit" size="sm" theme="primary">
                                Сохранить
                            </Button>
                        </div>
                    </Form>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default ProductsCatalog;
