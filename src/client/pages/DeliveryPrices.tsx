import React, { useState, useMemo, useEffect, useCallback } from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import Table from '@components/Table';
import Block from '@components/Block';
import Form from '@standart/Form';
import Tabs from '@standart/Tabs';
import Popup from '@standart/Popup';
import Switcher from '@standart/Switcher';
import { makeDeliveryPricesData, deliveryServices } from '@scripts/mock';
import usePopupState from '@scripts/usePopupState';
import { ActionType } from '@scripts/enums';

const COLUMNS = [
    {
        Header: 'Оператор',
        accessor: 'operator',
    },
    {
        Header: 'Caмовывоз',
        accessor: 'pickupPrice',
    },
    {
        Header: 'Доставка',
        accessor: 'deliveryPrice',
    },
    {
        Header: 'Активность',
        accessor: 'activity',
    },
];

const initialState = { action: ActionType.Close, open: false };

type State = {
    action?: ActionType;
    open?: boolean;
    operator?: string;
    pickupPrice?: number;
    deliveryPrice?: number;
    activity?: boolean;
};

const DeliveryPrices = () => {
    const [popupState, popupDispatch] = usePopupState<State>(initialState);

    return (
        <>
            <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
                <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>
                    Стоимость доставки по регионам
                </h1>
                <Tabs defaultIndex={0}>
                    <Tabs.List>
                        {deliveryServices.map((item, i) => (
                            <Tabs.Tab key={i}>{item}</Tabs.Tab>
                        ))}
                    </Tabs.List>
                    <Block>
                        <Block.Body css={{ display: 'flex', justifyContent: 'space-between' }}>
                            <Button theme="primary" size="sm" onClick={() => popupDispatch({ type: ActionType.Add })}>
                                Добавить регион
                            </Button>
                        </Block.Body>
                        <Block.Footer css={{ flexDirection: 'column', alignItems: 'stretch' }}>
                            {deliveryServices.map((item, i) => (
                                <Tabs.Panel key={i}>
                                    <Table
                                        needCheckboxesCol={false}
                                        columns={COLUMNS}
                                        data={makeDeliveryPricesData()}
                                        expandable
                                        handleRowInfo={row => {
                                            popupDispatch({
                                                type: ActionType.Edit,
                                                payload: {
                                                    operator: row?.operator,
                                                    pickupPrice: row?.pickupPrice,
                                                    deliveryPrice: row?.deliveryPrice,
                                                    activity: row?.activity.toLowerCase() === 'да',
                                                },
                                            });
                                        }}
                                    >
                                        <colgroup>
                                            <col width="60%" />
                                            <col width="10%" />
                                            <col width="10%" />
                                            <col width="10%" />
                                            <col width="5%" />
                                            <col width="5%" />
                                        </colgroup>
                                    </Table>
                                </Tabs.Panel>
                            ))}
                        </Block.Footer>
                    </Block>
                </Tabs>
            </main>
            <Popup
                isOpen={Boolean(popupState.open)}
                onRequestClose={() => popupDispatch({ type: ActionType.Close })}
                title={`${popupState.action === 'edit' ? 'Редактировать' : 'Добавить новый'} регион`}
                popupCss={{ minWidth: scale(50) }}
            >
                <Form
                    initialValues={{
                        operator: popupState.operator,
                        pickupPrice: popupState.pickupPrice,
                        deliveryPrice: popupState.deliveryPrice,
                        activity: popupState.activity,
                    }}
                    onSubmit={vals => console.log(vals)}
                >
                    <Form.FastField name="operator" label="Оператор" css={{ marginBottom: scale(2) }} />
                    <Form.FastField name="pickupPrice" label="Caмовывоз" css={{ marginBottom: scale(2) }} />
                    <Form.FastField name="deliveryPrice" label="Доставка" css={{ marginBottom: scale(2) }} />
                    <div css={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Form.FastField name="activity">
                            <Switcher>Активность</Switcher>
                        </Form.FastField>
                        <Button type="submit" size="sm">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </>
    );
};

export default DeliveryPrices;
