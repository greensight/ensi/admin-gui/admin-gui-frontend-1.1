import React, { useState, useMemo } from 'react';
import { useParams } from 'react-router-dom';
import { CSSObject } from '@emotion/core';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import { format } from 'date-fns';

import Form from '@standart/Form';
import Tabs from '@standart/Tabs';
import Textarea from '@standart/Textarea';
import Table from '@components/Table';
import Block from '@components/Block';
import Badge from '@components/Badge';

import typography from '@scripts/typography';
import { CELL_TYPES } from '@scripts/enums';

const columnsProducts = [
    {
        Header: 'ID товара',
        accessor: 'id',
    },
    {
        Header: 'Название',
        accessor: 'name',
        getProps: () => ({ type: CELL_TYPES.LINK }),
    },
    {
        Header: 'Артикул',
        accessor: 'article',
    },
    {
        Header: 'Бренд',
        accessor: 'brand',
    },
    {
        Header: 'Категория',
        accessor: 'category',
    },
];

const columnsHistory = [
    {
        Header: 'Дата',
        accessor: 'date',
        getProps: () => ({ type: CELL_TYPES.DATE_TIME }),
    },
    {
        Header: 'Пользователь',
        accessor: 'contact',
        getProps: () => ({ type: CELL_TYPES.DOUBLE }),
    },
    {
        Header: 'Действие',
        accessor: 'action',
    },
];
const dataProducts = [
    {
        id: 1003,
        name: ['Расческа выпрямитель для волос', '/product/2'],
        article: 'ik0011',
        brand: 'Ikoo',
        category: 'Стайлеры',
    },
    {
        id: 1492,
        name: ['Стайлер для волос E-Styler Pro', '/product/6'],
        article: 'ik0012',
        brand: 'Ikoo',
        category: 'Стайлеры',
    },
];

const dataHistory = [
    {
        date: new Date(),
        contact: ['Овчинников С. Б.', 'Супер-администратор'],
        action: 'Оставил комментарий к заявке: "rf"',
    },
    {
        date: new Date(),
        contact: ['Резинкин П. П.', 'Администратор продавца'],
        action: 'Создал заявку',
    },
];

const RequestContent = () => {
    const { colors } = useTheme();
    const { requestId }: { requestId: string } = useParams();

    const dlStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'" };
    const dtStyles: CSSObject = {
        padding: `${scale(1, true)}px ${scale(1)}px ${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };
    const ddStyles: CSSObject = {
        padding: `${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
    };

    const tableDataHistory = useMemo(() => dataHistory, []);
    const tableDataProducts = useMemo(() => dataProducts, []);

    const tableColumnsHistory = useMemo(() => columnsHistory, []);
    const tableColumnsProducts = useMemo(() => columnsProducts, []);
    return (
        <>
            <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
                <Layout cols={5} css={{ marginBottom: scale(3) }}>
                    <Layout.Item col={2}>
                        <Block>
                            <Block.Header>Комментарий продавца</Block.Header>
                            <Block.Body>
                                <Form
                                    onSubmit={values => {
                                        console.log(values);
                                    }}
                                    initialValues={{
                                        addProductID: '',
                                        addProductSellers: '',
                                        addProductPrice: '',
                                    }}
                                >
                                    <Form.Field name="textarea">
                                        <Textarea rows={3} disabled defaultValue="Нет комментариев" />
                                    </Form.Field>
                                </Form>
                            </Block.Body>
                        </Block>
                        <Block css={{ marginTop: scale(2) }}>
                            <Block.Header>Служебный комментарий</Block.Header>
                            <Block.Body>
                                <Form
                                    onSubmit={values => {
                                        console.log(values);
                                    }}
                                    initialValues={{
                                        addProductID: '',
                                        addProductSellers: '',
                                        addProductPrice: '',
                                    }}
                                >
                                    <Form.Field name="textarea">
                                        <Textarea rows={3} />
                                    </Form.Field>
                                    <Button type="submit" css={{ marginTop: scale(2) }}>
                                        Сохранить
                                    </Button>
                                </Form>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                    <Layout.Item col={3}>
                        <Block css={{ marginBottom: scale(3) }}>
                            <Block.Header>
                                <h1 css={{ ...typography('h2'), margin: 0 }}>
                                    Заявка на производство контента #{requestId}
                                </h1>
                            </Block.Header>
                            <Block.Body>
                                <dl css={{ ...dlStyles, gridTemplateColumns: '150px 1fr' }}>
                                    <dt css={dtStyles}>Статус</dt>
                                    <dd css={ddStyles}>
                                        <Badge text="Новая" />
                                    </dd>
                                    <dt css={dtStyles}>ID</dt>
                                    <dd css={ddStyles}>{requestId}</dd>
                                    <dt css={dtStyles}>Продавец </dt>
                                    <dd css={ddStyles}>Резинки для волос</dd>
                                    <dt css={dtStyles}>Тип заявки</dt>
                                    <dd css={ddStyles}>Фото/видео съёмка</dd>
                                    <dt css={dtStyles}>Тип фотосъемки:</dt>
                                    <dd css={ddStyles}>Со вскрытием</dd>
                                    <dt css={dtStyles}>Автор:</dt>
                                    <dd css={ddStyles}>Резинкин П. П.</dd>
                                    <dt css={dtStyles}>Дата создания</dt>
                                    <dd css={ddStyles}>{format(new Date(), 'dd.MM.yyyy h:m')}</dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                </Layout>
                <Tabs>
                    <Tabs.List>
                        <Tabs.Tab>Товары</Tabs.Tab>
                        <Tabs.Tab>История</Tabs.Tab>
                    </Tabs.List>
                    <Block>
                        <Block.Body>
                            <Tabs.Panel>
                                <Table
                                    columns={tableColumnsProducts}
                                    data={tableDataProducts}
                                    needCheckboxesCol={false}
                                    needSettingsColumn={false}
                                />
                            </Tabs.Panel>
                            <Tabs.Panel>
                                <Table
                                    columns={tableColumnsHistory}
                                    data={tableDataHistory}
                                    needCheckboxesCol={false}
                                    needSettingsColumn={false}
                                />
                            </Tabs.Panel>
                        </Block.Body>
                    </Block>
                </Tabs>
            </main>
        </>
    );
};

export default RequestContent;
