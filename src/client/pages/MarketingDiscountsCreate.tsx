import React, { useState } from 'react';
import { Button, scale, Layout, useTheme } from '@greensight/gds';
import * as Yup from 'yup';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import Datepicker from '@standart/Datepicker';
import DatepickerStyles from '@standart/Datepicker/presets';
import Legend from '@standart/Legend';
import Form from '@standart/Form';
import Checkbox from '@standart/Checkbox';
import MultiSelect from '@standart/MultiSelect';
import {
    discountsStatuses,
    brands,
    categories,
    deliveryMethods,
    paymentMethods,
    regionsArray,
    segments,
    discountsRoles,
} from '@scripts/mock';
import { prepareForSelect } from '@scripts/helpers';

const MarketingDiscounts = () => {
    const { colors } = useTheme();

    const [startDate, setStartDate] = useState<Date | null>(null);
    const [endDate, setEndDate] = useState<Date | null>(null);
    const [valueType, setValueType] = useState('');
    const [condition, setCondition] = useState('');
    const [object, setObject] = useState('');

    const STATUSES = prepareForSelect(discountsStatuses);
    const BRANDS = prepareForSelect(brands);
    const CATEGORIES = prepareForSelect(categories);
    const DELIVERYMETHODS = prepareForSelect(deliveryMethods);
    const PAYMENTMETHODS = prepareForSelect(paymentMethods);
    const REGIONS = prepareForSelect(regionsArray);
    const SEGMENTS = prepareForSelect(segments);
    const ROLES = prepareForSelect(discountsRoles);

    const VALUETYPES = [
        { label: 'Проценты', value: 'Проценты' },
        { label: 'Рубли', value: 'Рубли' },
    ];
    const DISCOUNTCONDITION = [
        { label: 'Первый заказ', value: 'Первый заказ' },
        { label: 'Заказ от определенной суммы', value: 'Заказ от определенной суммы' },
        {
            label: 'Заказ от определенной суммы заданного бренда',
            value: 'Заказ от определенной суммы заданного бренда',
        },
        {
            label: 'Заказ от определенной суммы заданной категории',
            value: 'Заказ от определенной суммы заданной категории',
        },
        { label: 'Количество единиц одного товара', value: 'Количество единиц одного товара' },
        { label: 'Способ доставки', value: 'Способ доставки' },
        { label: 'Способ оплаты', value: 'Способ оплаты' },
        { label: 'Регион', value: 'Регион' },
        { label: 'Пользователь', value: 'Пользователь' },
        { label: 'Порядковый номер заказа', value: 'Порядковый номер заказа' },
        {
            label: 'Суммируется с другими маркетинговыми инструментами',
            value: 'Суммируется с другими маркетинговыми инструментами',
        },
    ];
    const DISCOUNTOBJECT = [
        { label: 'Товар', value: 'Товар' },
        { label: 'Бренд товара', value: 'Бренд товара' },
        { label: 'Категорию товара', value: 'Категорию товара' },
        { label: 'Доставку', value: 'Доставку' },
        { label: 'Сумму корзины', value: 'Сумму корзины' },
        { label: 'Все товары', value: 'Все товары' },
        { label: 'Все бандлы', value: 'Все бандлы' },
        { label: 'Все бренды', value: 'Все бренды' },
        { label: 'Все категории', value: 'Все категории' },
        { label: 'Мастер-класс', value: 'Мастер-класс' },
        { label: 'Все мастер-классы', value: 'Все мастер-классы' },
        { label: 'Бандл из товаров', value: 'Бандл из товаров' },
        { label: 'Бандл из мастер-классов', value: 'Бандл из мастер-классов' },
    ];

    enum ConditionType {
        FirstOrder = 'Первый заказ',
        CertainSum = 'Заказ от определенной суммы',
        CertainSumBrand = 'Заказ от определенной суммы заданного бренда',
        CertainSumCategory = 'Заказ от определенной суммы заданной категории',
        ProductCount = 'Количество единиц одного товара',
        DeliveryMethod = 'Способ доставки',
        PaymentMethod = 'Способ оплаты',
        Region = 'Регион',
        User = 'Пользователь',
        OrderNumber = 'Порядковый номер заказа',
        StacksWithOtherDiscounts = 'Суммируется с другими маркетинговыми инструментами',
    }

    enum ObjectType {
        Product = 'Товар',
        ProductBrand = 'Бренд товара',
        ProductCategory = 'Категорию товара',
        Delivery = 'Доставку',
        CartSum = 'Сумму корзины',
        AllProducts = 'Все товары',
        AllBundles = 'Все бандлы',
        AllBrands = 'Все бренды',
        AllCategories = 'Все категории',
        MasterClass = 'Мастер-класс',
        AllMasterClasses = 'Все мастер-классы',
        ProductsBundle = 'Бандл из товаров',
        MasterClassesBundle = 'Бандл из мастер-классов',
    }

    const changeCondition = (value: string) => {
        for (const key in ConditionType) {
            if (value === (ConditionType as any)[key]) setCondition(value);
        }
    };

    const changeObject = (value: string) => {
        for (const key in ObjectType) {
            if (value === (ObjectType as any)[key]) setObject(value);
        }
    };

    //Формирование полей, раскрывающихся при определенном выборе селекта "Скидка на"
    const getDiscountObjectBlock = () => {
        switch (object) {
            case ObjectType.Product: {
                return (
                    <Layout.Item col={3}>
                        <Form.Field
                            name="discountOffers"
                            label="Офферы"
                            bottomTooltip={
                                <p css={{ color: colors?.grey600, marginTop: scale(1, true) }}>
                                    ID офферов через запятую
                                </p>
                            }
                        />
                    </Layout.Item>
                );
            }
            case ObjectType.ProductBrand: {
                return (
                    <>
                        <Layout.Item col={3}>
                            <Form.Field name="discountBrands" label="Бренды">
                                <MultiSelect options={BRANDS} />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item css={{ gridColumnStart: 2, gridColumnEnd: 5 }}>
                            <Form.Field
                                name="discountOffersException"
                                label="За исключением офферов"
                                bottomTooltip={
                                    <p
                                        css={{
                                            color: colors?.grey600,
                                            marginTop: scale(1, true),
                                        }}
                                    >
                                        ID офферов через запятую
                                    </p>
                                }
                            />
                        </Layout.Item>
                    </>
                );
            }
            case ObjectType.ProductCategory: {
                return (
                    <>
                        <Layout.Item col={3}>
                            <Form.Field name="discountCategories" label="Категории">
                                <MultiSelect options={CATEGORIES} />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item css={{ gridColumnStart: 2, gridColumnEnd: 5 }}>
                            <Form.Field name="discountBrandsException" label="За исключением брендов">
                                <MultiSelect options={BRANDS} />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item css={{ gridColumnStart: 2, gridColumnEnd: 5 }}>
                            <Form.Field
                                name="discountOffersException"
                                label="За исключением офферов"
                                bottomTooltip={
                                    <p
                                        css={{
                                            color: colors?.grey600,
                                            marginTop: scale(1, true),
                                        }}
                                    >
                                        ID офферов через запятую
                                    </p>
                                }
                            />
                        </Layout.Item>
                    </>
                );
            }
            case ObjectType.MasterClass: {
                return (
                    <Layout.Item col={3}>
                        <Form.Field
                            name="discountTicketTypes"
                            label="Типы билетов"
                            bottomTooltip={
                                <p css={{ color: colors?.grey600, marginTop: scale(1, true) }}>
                                    ID типов билетов через запятую
                                </p>
                            }
                        />
                    </Layout.Item>
                );
            }
            case ObjectType.ProductsBundle: {
                return (
                    <Layout.Item col={3}>
                        <Form.Field
                            name="discountOffers"
                            label="Офферы"
                            bottomTooltip={
                                <p css={{ color: colors?.grey600, marginTop: scale(1, true) }}>
                                    ID офферов через запятую
                                </p>
                            }
                        />
                    </Layout.Item>
                );
            }
            case ObjectType.MasterClassesBundle: {
                return (
                    <Layout.Item col={3}>
                        <Form.Field
                            name="discountMasterClasses"
                            label="Мастер-классы"
                            bottomTooltip={
                                <p css={{ color: colors?.grey600, marginTop: scale(1, true) }}>
                                    ID мастер-классов через запятую
                                </p>
                            }
                        />
                    </Layout.Item>
                );
            }
            default: {
                return null;
            }
        }
    };
    const discountObjectBlock = getDiscountObjectBlock();

    //Формирование полей, раскрывающихся при определенном выборе селекта "Условие предоставления скидки"
    const getDiscountConditionBlock = () => {
        switch (condition) {
            case ConditionType.CertainSum: {
                return (
                    <Layout.Item col={1}>
                        <Form.Field name="conditionCertainSum" label="От (руб.)" type="number" />
                    </Layout.Item>
                );
            }
            case ConditionType.CertainSumBrand: {
                return (
                    <>
                        <Layout.Item col={1}>
                            <Form.Field name="conditionCertainSum" label="От (руб.)" type="number" />
                        </Layout.Item>
                        <Layout.Item col={4}>
                            <Form.Field name="conditionBrands" label="Бренды">
                                <MultiSelect options={BRANDS} />
                            </Form.Field>
                        </Layout.Item>
                    </>
                );
            }
            case ConditionType.CertainSumCategory: {
                return (
                    <>
                        <Layout.Item col={1}>
                            <Form.Field name="conditionCertainSum" label="От (руб.)" type="number" />
                        </Layout.Item>
                        <Layout.Item col={4}>
                            <Form.Field name="conditionCategories" label="Категории">
                                <MultiSelect options={CATEGORIES} />
                            </Form.Field>
                        </Layout.Item>
                    </>
                );
            }
            case ConditionType.ProductCount: {
                return (
                    <>
                        <Layout.Item col={1}>
                            <Form.Field name="conditionOfferCount" label="Количество" type="number" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="conditionOffer" label="Оффер" type="number" />
                        </Layout.Item>
                    </>
                );
            }
            case ConditionType.DeliveryMethod: {
                return (
                    <Layout.Item col={1}>
                        <Form.Field name="conditionDeliveryMethod" label="Способ доставки">
                            <MultiSelect options={DELIVERYMETHODS} />
                        </Form.Field>
                    </Layout.Item>
                );
            }
            case ConditionType.PaymentMethod: {
                return (
                    <Layout.Item col={1}>
                        <Form.Field name="conditionPaymentMethod" label="Способ оплаты">
                            <MultiSelect options={PAYMENTMETHODS} />
                        </Form.Field>
                    </Layout.Item>
                );
            }
            case ConditionType.Region: {
                return (
                    <Layout.Item col={4}>
                        <Form.Field name="conditionRegion" label="Регионы">
                            <MultiSelect options={REGIONS} />
                        </Form.Field>
                    </Layout.Item>
                );
            }
            case ConditionType.User: {
                return (
                    <>
                        <Layout.Item col={1}>
                            <Form.Field name="conditionUsersIds" label="ID пользователей (через запятую)" />
                        </Layout.Item>
                        <Layout.Item col={4}>
                            <Layout cols={4}>
                                <Layout.Item col={1}>
                                    <Form.Field name="conditionSegments" label="Сегменты">
                                        <MultiSelect options={SEGMENTS} />
                                    </Form.Field>
                                </Layout.Item>
                            </Layout>
                        </Layout.Item>
                        <Layout.Item col={4}>
                            <Layout cols={4}>
                                <Layout.Item col={1}>
                                    <Form.Field name="conditionRoles" label="Роли">
                                        <MultiSelect options={ROLES} />
                                    </Form.Field>
                                </Layout.Item>
                            </Layout>
                        </Layout.Item>
                    </>
                );
            }
            case ConditionType.OrderNumber: {
                return (
                    <Layout.Item col={1}>
                        <Form.Field name="conditionOrderNumber" label="Порядковый номер заказа" type="number" />
                    </Layout.Item>
                );
            }
            case ConditionType.StacksWithOtherDiscounts: {
                return (
                    <Layout.Item col={2}>
                        <Form.Field name="conditionStacksWithOtherDiscounts" label="Суммируется с другими скидками">
                            {/* Добавить другие существующие скидки */}
                            <MultiSelect options={[]} />
                        </Form.Field>
                    </Layout.Item>
                );
            }
            default: {
                return null;
            }
        }
    };
    const discountConditionBlock = getDiscountConditionBlock();

    // TODO: Сделать обнуление условных полей, при смене селекта

    return (
        <PageWrapper h1="Создание скидки">
            <>
                <DatepickerStyles />
                <Block css={{ marginBottom: scale(3) }}>
                    <Form
                        initialValues={{
                            name: '',
                            discountObject: '',
                            valueType: '',
                            value: '',
                            startDate: null,
                            endDate: null,
                            status: [],
                            discountPromocodesOnly: '',
                            discountCondition: '',
                            //Условные поля "Скидка на"
                            discountOffers: '',
                            discountBrands: '',
                            discountOffersException: '',
                            discountCategories: [],
                            discountBrandsException: [],
                            discountTicketTypes: '',
                            discountMasterClasses: '',
                            //Условные поля "Условия предоставления скидки"
                            conditionCertainSum: '',
                            conditionBrands: [],
                            conditionCategories: [],
                            conditionOfferCount: '',
                            conditionOffer: '',
                            conditionDeliveryMethod: '',
                            conditionPaymentMethod: '',
                            conditionRegion: '',
                            conditionUsersIds: '',
                            conditionSegments: '',
                            conditionRoles: '',
                            conditionOrderNumber: '',
                            conditionStacksWithOtherDiscounts: '',
                        }}
                        validationSchema={Yup.object().shape({
                            name: Yup.string().required('Обязательное поле!'),
                            discountObject: Yup.object().required('Обязательное поле!'),
                            valueType: Yup.object().required('Обязательное поле!'),
                            value: Yup.number()
                                .when('valueType', {
                                    is: obj => obj?.value === 'Проценты',
                                    then: Yup.number()
                                        .min(0, 'Введите корректное число')
                                        .max(100, 'Введите корректное число'),
                                })
                                .required('Обязательное поле!'),
                            //Условная валидация при изменении поля "Скидка на"
                            discountOffers: Yup.string().when('discountObject', {
                                is: obj =>
                                    obj?.value === ObjectType.Product || obj?.value === ObjectType.ProductsBundle,
                                then: Yup.string().required('Введите значения ID офферов!'),
                            }),
                            discountBrands: Yup.string().when('discountObject', {
                                is: obj => obj?.value === ObjectType.ProductBrand,
                                then: Yup.string().nullable().required('Выберите хотя бы один бренд!'),
                            }),
                            discountCategories: Yup.string().when('discountObject', {
                                is: obj => obj?.value === ObjectType.ProductCategory,
                                then: Yup.string().nullable().required('Выберите хотя бы одну категорию!'),
                            }),
                            discountTicketTypes: Yup.string().when('discountObject', {
                                is: obj => obj?.value === ObjectType.MasterClass,
                                then: Yup.string().required('Введите ID типов билетов для мастер-классов!'),
                            }),
                            discountMasterClasses: Yup.string().when('discountObject', {
                                is: obj => obj?.value === ObjectType.MasterClassesBundle,
                                then: Yup.string().required('Введите значения ID мастер-классов!'),
                            }),
                            //Условная валидация при изменении поля "Условия предоставления скидки"
                            conditionCertainSum: Yup.string().when('discountCondition', {
                                is: obj =>
                                    obj?.value === ConditionType.CertainSum ||
                                    obj?.value === ConditionType.CertainSumBrand ||
                                    obj?.value === ConditionType.CertainSumCategory,
                                then: Yup.string().required('Обязательное поле!'),
                            }),
                            conditionBrands: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.CertainSumBrand,
                                then: Yup.string().nullable().required('Выберите хотя бы один бренд!'),
                            }),
                            conditionCategories: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.CertainSumCategory,
                                then: Yup.string().nullable().required('Выберите хотя бы одну категорию!'),
                            }),
                            conditionOfferCount: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.ProductCount,
                                then: Yup.string().required('Обязательное поле!'),
                            }),
                            conditionOffer: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.ProductCount,
                                then: Yup.string().required('Обязательное поле!'),
                            }),
                            conditionDeliveryMethod: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.DeliveryMethod,
                                then: Yup.string().nullable().required('Выберите хотя бы один способ доставки'),
                            }),
                            conditionPaymentMethod: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.PaymentMethod,
                                then: Yup.string().nullable().required('Выберите хотя бы один способ оплаты'),
                            }),
                            conditionRegion: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.Region,
                                then: Yup.string().nullable().required('Выберите хотя бы один регион'),
                            }),
                            conditionUsersIds: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.User,
                                then: Yup.string().required('Укажите ID пользователей'),
                            }),
                            conditionSegments: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.User,
                                then: Yup.string().nullable().required('Укажите сегменты'),
                            }),
                            conditionRoles: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.User,
                                then: Yup.string().nullable().required('Укажите роли'),
                            }),
                            conditionOrderNumber: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.OrderNumber,
                                then: Yup.string().required('Обязательное поле!'),
                            }),
                            conditionStacksWithOtherDiscounts: Yup.string().when('discountCondition', {
                                is: obj => obj?.value === ConditionType.StacksWithOtherDiscounts,
                                then: Yup.string().nullable().required('Выберите хотя бы одну другую скидку!'),
                            }),
                        })}
                        onSubmit={values => {
                            console.log(values);
                        }}
                    >
                        <Block.Body>
                            <Layout cols={4}>
                                <Layout.Item col={4}>
                                    <Form.Field name="name" label="Название" />
                                </Layout.Item>
                                <Layout.Item col={4}>
                                    <Layout cols={4}>
                                        <Layout.Item col={1}>
                                            <Form.Field name="discountObject" label="Скидка на">
                                                <MultiSelect
                                                    options={DISCOUNTOBJECT}
                                                    isMulti={false}
                                                    onChange={e => changeObject(e.value)}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        {discountObjectBlock && discountObjectBlock}
                                    </Layout>
                                </Layout.Item>
                                <Layout.Item col={4}>
                                    <Layout cols={4}>
                                        <Layout.Item col={1}>
                                            <Form.Field name="valueType" label="Тип значения">
                                                <MultiSelect
                                                    options={VALUETYPES}
                                                    isMulti={false}
                                                    onChange={e => setValueType(e.value)}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={1}>
                                            <Form.Field
                                                name="value"
                                                label={
                                                    valueType === 'Проценты'
                                                        ? 'Значение в процентах'
                                                        : 'Значение в рублях'
                                                }
                                                type="number"
                                                bottomTooltip={
                                                    valueType === 'Проценты' ? (
                                                        <p css={{ color: colors?.grey600, marginTop: scale(1, true) }}>
                                                            Значение от 0 до 100
                                                        </p>
                                                    ) : null
                                                }
                                            />
                                        </Layout.Item>
                                    </Layout>
                                </Layout.Item>
                                <Layout.Item col={4}>
                                    <Layout cols={4}>
                                        <Layout.Item col={1}>
                                            <Form.Field name="startDate">
                                                <Legend label="Дата старта" />
                                                <Datepicker
                                                    selectsStart
                                                    selected={startDate}
                                                    startDate={startDate}
                                                    endDate={endDate}
                                                    maxDate={endDate}
                                                    onChange={setStartDate}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                        <Layout.Item col={1}>
                                            <Form.Field name="endDate">
                                                <Legend label="Дата окончания" />
                                                <Datepicker
                                                    selectsEnd
                                                    selected={endDate}
                                                    startDate={startDate}
                                                    endDate={endDate}
                                                    minDate={startDate}
                                                    onChange={setEndDate}
                                                />
                                            </Form.Field>
                                        </Layout.Item>
                                    </Layout>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.Field name="status" label="Статус">
                                        <MultiSelect options={STATUSES} />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={4}>
                                    <Form.Field name="discountPromocodesOnly">
                                        <Checkbox value="true">Скидка действительна только по промокоду</Checkbox>
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={2}>
                                    <Form.Field name="discountCondition" label="Условия предоставления скидки">
                                        <MultiSelect
                                            options={DISCOUNTCONDITION}
                                            isMulti={false}
                                            onChange={e => changeCondition(e.value)}
                                        />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={1} align="end">
                                    <Button size="sm" type="submit">
                                        Добавить условие
                                    </Button>
                                </Layout.Item>
                            </Layout>
                            {discountConditionBlock && (
                                <Layout cols={4} css={{ marginTop: scale(3) }}>
                                    {discountConditionBlock}
                                </Layout>
                            )}
                        </Block.Body>
                        <Button size="sm" theme="primary" css={{ margin: `${scale(5)}px ${scale(3)}px` }} type="submit">
                            Cохранить
                        </Button>
                    </Form>
                </Block>
            </>
        </PageWrapper>
    );
};

export default MarketingDiscounts;
