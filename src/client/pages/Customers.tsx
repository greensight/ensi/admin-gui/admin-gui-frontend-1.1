import React, { useState, useMemo, useEffect } from 'react';
import { useFormikContext } from 'formik';
import { Button, scale, Layout } from '@greensight/gds';
import PageWrapper from '@components/PageWrapper';
import typography from '@scripts/typography';
import Table from '@components/Table';
import Block from '@components/Block';
import Form from '@standart/Form';
import { makeCostumers } from '@scripts/mock';
import Pagination from '@standart/Pagination';
import MultiSelect from '@standart/MultiSelect';
import { useLocation } from 'react-router-dom';
import Popup from '@standart/Popup';
import Password from '@standart/Password';
import DatepickerStyles from '@standart/Datepicker/presets';
import Switcher from '@standart/Switcher';
import Datepicker from '@standart/Datepicker';
import DatepickerRange from '@standart/DatepickerRange';
import { prepareForSelect } from '@scripts/helpers';
import usePrevious from '@scripts/usePrevious';
import Mask from '@standart/Mask';
import { maskPhone } from '@scripts/mask';

const statuses = prepareForSelect(['Все', 'Создан профиль', 'Новый', 'Отклонен']);

const COLUMNS = [
    {
        Header: 'ID клиента',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Дата регистрации',
        accessor: 'date',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'ФИО',
        accessor: 'name',
    },
    {
        Header: 'Телефон',
        accessor: 'phone',
    },
    {
        Header: 'Email',
        accessor: 'email',
    },
    {
        Header: 'Сегмент RFM',
        accessor: 'segment',
    },
    {
        Header: 'Дата последнего посещения',
        accessor: 'visitDate',
        getProps: () => ({ type: 'date' }),
    },
];

const FormDataField = () => {
    const {
        values: { dataSwitcher },
        setFieldValue,
    } = useFormikContext<{ dataSwitcher: boolean }>();

    const prevDataSwitcer = usePrevious(dataSwitcher);

    useEffect(() => {
        if (prevDataSwitcer !== null && prevDataSwitcer !== dataSwitcher) {
            if (dataSwitcher) setFieldValue('registrationDate', null);
            else setFieldValue('registrationRangeDate', [null, null]);
        }
    }, [prevDataSwitcer, dataSwitcher, setFieldValue]);
    return (
        <>
            <Form.Field name="dataSwitcher" css={{ paddingBottom: scale(1) }}>
                <Switcher isSmall>{dataSwitcher ? 'Период регистрации' : 'Дата регистрации'}</Switcher>
            </Form.Field>
            {!dataSwitcher && (
                <Form.Field name="registrationDate">
                    <Datepicker />
                </Form.Field>
            )}
            {dataSwitcher && (
                <Form.Field name="registrationRangeDate">
                    <DatepickerRange />
                </Form.Field>
            )}
        </>
    );
};

const Customers = () => {
    const { pathname, search } = useLocation();

    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const [isCreationUserPopup, setIsCreationUserPopup] = useState(false);

    const data = useMemo(() => makeCostumers(10), []);
    return (
        <PageWrapper h1="Клиентская база">
            <DatepickerStyles />

            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        status: '',
                        name: '',
                        phone: '',
                        gender: '',
                        registrationDate: '',
                        registrationRangeDate: [null, null],
                        segment: '',
                        dataSwitcher: false,
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Body>
                        <Layout cols={12}>
                            <Layout.Item col={3}>
                                <Form.Field name="status" label="Статус">
                                    <MultiSelect isMulti={false} options={statuses} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="phone" label="Телефон">
                                    <Mask mask={maskPhone} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="name" label="ФИО" />
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Form.Field name="gender" label="Пол">
                                    <MultiSelect
                                        isMulti={false}
                                        options={prepareForSelect(['Все', 'Мужской', 'Женский'])}
                                    />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <FormDataField />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="segment" label="Сегмент">
                                    <MultiSelect
                                        isMulti={false}
                                        options={prepareForSelect([
                                            'Все',
                                            'Не делал заказ',
                                            '1-2 заказа',
                                            'Не посещал более 3-х месяцев',
                                        ])}
                                    />
                                </Form.Field>
                            </Layout.Item>
                        </Layout>
                    </Block.Body>
                    <Block.Footer>
                        <div css={typography('bodySm')}>Найдено 135 пользователей </div>
                        <div>
                            <Form.Reset size="sm" theme="secondary" type="button">
                                Сбросить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Footer>
                </Form>
            </Block>
            <div css={{ marginBottom: scale(3) }}>
                <Button size="sm" onClick={() => setIsCreationUserPopup(true)}>
                    Создать пользователя
                </Button>
            </div>
            <Block>
                <Block.Body>
                    <Table columns={COLUMNS} data={data} />
                    <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
                </Block.Body>
            </Block>
            <Popup
                isOpen={isCreationUserPopup}
                onRequestClose={() => setIsCreationUserPopup(false)}
                title={'Создание пользователя'}
                popupCss={{ minWidth: scale(60) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        phone: '',
                        password: '',
                    }}
                >
                    <Form.Field name="phone" label="Телефон" css={{ marginBottom: scale(2) }} />
                    <Form.FastField name="password" label="Пароль" css={{ marginBottom: scale(2) }}>
                        <Password />
                    </Form.FastField>
                    <Form.FastField
                        name="passwordConfirmation"
                        label="Подтверждение пароля"
                        css={{ marginBottom: scale(2) }}
                    >
                        <Password />
                    </Form.FastField>

                    <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsCreationUserPopup(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </PageWrapper>
    );
};

export default Customers;
