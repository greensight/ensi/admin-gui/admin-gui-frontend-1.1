import React, { useMemo, useState } from 'react';
import { scale, Button } from '@greensight/gds';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Table from '@components/Table';
import Form from '@standart/Form';
import Select from '@standart/Select';
import Switcher from '@standart/Switcher';
import { prepareForSelect, Flatten } from '@scripts/helpers';
import { getRandomItem } from '@scripts/mock';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import Popup from '@standart/Popup';
import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import usePopupState from '@scripts/usePopupState';
import { ActionType } from '@scripts/enums';
import { CHANNELS, CHANNELS_FOR_SELECT } from '@scripts/data/different';

const columns = [
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: 'Активность',
        accessor: 'active',
    },
    {
        Header: 'Канал',
        accessor: 'channel',
    },
];

const tableItem = (id: number) => {
    return {
        id,
        title: getRandomItem(['Промокод', 'Доставка', 'Новый шаблон']),
        active: getRandomItem(['Да', 'Нет']),
        channel: getRandomItem(CHANNELS),
    };
};

const makeData = (len: number) => [...Array(len).keys()].map(el => tableItem(el));

const channels = CHANNELS_FOR_SELECT;

const initialState = { title: '', active: false, channel: '', action: ActionType.Close, open: false };

type State = {
    title?: string;
    active?: boolean;
    channel?: string;
    action?: ActionType;
    open?: boolean;
};

const CommunicationTypes = () => {
    const [popupState, popupDispatch] = usePopupState<State>(initialState);
    const [idDeleteOpen, setIsDeleteOpen] = useState(false);
    const data = useMemo(() => makeData(5), []);
    const [ids, setIds, selectedRows] = useSelectedRowsData<Flatten<typeof data>>(data);

    return (
        <PageWrapper h1="Типы">
            <>
                <div css={{ marginBottom: scale(2) }}>
                    <Button size="sm" Icon={PlusIcon} onClick={() => popupDispatch({ type: ActionType.Add })}>
                        Добавить тип
                    </Button>
                    {ids.length > 0 ? (
                        <Button
                            size="sm"
                            Icon={TrashIcon}
                            onClick={() => setIsDeleteOpen(true)}
                            css={{ marginLeft: scale(2) }}
                        >
                            Удалить тип{ids.length === 1 ? '' : 'ы'}
                        </Button>
                    ) : null}
                </div>
                <Block>
                    <Block.Body>
                        <Table
                            data={data}
                            columns={columns}
                            onRowSelect={setIds}
                            handleRowInfo={row => {
                                popupDispatch({
                                    type: ActionType.Edit,
                                    payload: {
                                        title: row?.title,
                                        active: row?.active.toLowerCase() === 'да',
                                        channel: row?.channel,
                                    },
                                });
                            }}
                        />
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={Boolean(popupState.open)}
                    onRequestClose={() => popupDispatch({ type: ActionType.Close })}
                    title={`${popupState.action === 'edit' ? 'Редактирование' : 'Создание'} типа`}
                    popupCss={{ minWidth: scale(50) }}
                >
                    <Form
                        initialValues={{
                            title: popupState.title,
                            active: popupState.active,
                            channel: popupState.channel,
                        }}
                        onSubmit={vals => console.log(vals)}
                    >
                        <Form.FastField name="title" label="Название" css={{ marginBottom: scale(2) }} />
                        <Form.FastField name="active" css={{ marginBottom: scale(2) }}>
                            <Switcher>Активность</Switcher>
                        </Form.FastField>
                        <Form.FastField name="channel" label="Канал" css={{ marginBottom: scale(2) }}>
                            <Select items={channels} defaultValue={popupState.channel} />
                        </Form.FastField>
                        <Form.Reset size="sm" theme="fill" css={{ marginRight: scale(2) }}>
                            {popupState.action === 'edit' ? 'Сбросить' : 'Очистить'}
                        </Form.Reset>
                        <Button type="submit" size="sm">
                            Сохранить
                        </Button>
                    </Form>
                </Popup>
                <Popup
                    isOpen={idDeleteOpen}
                    onRequestClose={() => setIsDeleteOpen(false)}
                    title="Вы уверены, что хотите удалить следующие типы?"
                    popupCss={{ minWidth: scale(50) }}
                >
                    <ul css={{ marginBottom: scale(2) }}>
                        {selectedRows.map(r => (
                            <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                                #{r.id} – {r.title}
                            </li>
                        ))}
                    </ul>
                    <Button size="sm">Удалить</Button>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default CommunicationTypes;
