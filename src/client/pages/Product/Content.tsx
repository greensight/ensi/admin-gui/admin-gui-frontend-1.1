import React, { useState } from 'react';
import * as Yup from 'yup';
import { Layout, scale, Button } from '@greensight/gds';
import typography from '@scripts/typography';
import ContentBlock from '@components/ContentBlock';
import Popup from '@standart/Popup';
import Textarea from '@standart/Textarea';
import Legend from '@standart/Legend';
import Form from '@standart/Form';
import ExportIcon from '@svg/tokens/small/export.svg';
// import urlParser from "js-video-url-parser/lib/providerd/youtube";

const loadedPictures = ['https://placehold.it/100x200', 'https://placehold.it/100x200'];

const Content = () => {
    // TODO при интеграции, возможно, стоит заменить на useReducer и хранить все в одном стейте
    const [isOpenLoad, setIsOpenLoad] = useState(false);
    const [isOpenFeatures, setIsOpenFeatures] = useState(false);
    const [isOpenDescription, setIsOpenDescription] = useState(false);
    const [isOpenDescriptionHowTo, setIsOpenDescriptionHowTo] = useState(false);
    const [isOpenVideo, setIsOpenVideo] = useState(false);

    return (
        <>
            <h2 css={{ ...typography('h3'), marginBottom: scale(3) }}>Изображения</h2>
            <Layout cols={4}>
                <Layout.Item col={1}>
                    <ContentBlock
                        title="Основное изображение"
                        type="image"
                        onEdit={() => setIsOpenLoad(true)}
                        onRemove={() => null}
                    />
                </Layout.Item>
                <Layout.Item col={1}>
                    <ContentBlock
                        title="Фотография для каталога"
                        type="image"
                        onEdit={() => setIsOpenLoad(true)}
                        onRemove={() => null}
                    />
                </Layout.Item>
                <Layout.Item col={2} css={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap' }}>
                    <ul css={{ display: 'flex', flexWrap: 'wrap' }}>
                        {loadedPictures.map((i, index) => (
                            <li key={index} css={{ marginRight: scale(2) }}>
                                <ContentBlock
                                    onEdit={() => setIsOpenLoad(true)}
                                    onRemove={() => null}
                                    img={i}
                                    type="image"
                                />
                            </li>
                        ))}
                    </ul>
                    <Button theme="fill" size="sm" onClick={() => setIsOpenLoad(true)}>
                        Добавить
                    </Button>
                </Layout.Item>
            </Layout>
            <hr css={{ margin: `${scale(3)}px 0` }} />

            <h2 css={{ ...typography('h3'), marginBottom: scale(3) }}>Особенности</h2>
            <div css={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap' }}>
                <ul css={{ display: 'flex', flexWrap: 'wrap' }}>
                    {loadedPictures.map((i, index) => (
                        <li key={index} css={{ marginRight: scale(2) }}>
                            <ContentBlock
                                onEdit={() => setIsOpenFeatures(true)}
                                onRemove={() => null}
                                img={i}
                                type="image"
                            />
                        </li>
                    ))}
                </ul>
                <Button theme="fill" size="sm" onClick={() => setIsOpenFeatures(true)}>
                    Добавить
                </Button>
            </div>
            <hr css={{ margin: `${scale(3)}px 0` }} />

            <h2 css={{ ...typography('h3'), marginBottom: scale(3) }}>Описание</h2>
            <Layout cols={4}>
                <Layout.Item col={2}>
                    <ContentBlock
                        onEdit={() => setIsOpenDescription(true)}
                        type="text"
                        text="Позаботьтесь о здоровье, красоте и сохранности волос вместе с резинками-пружинками Vikki&Lilli. Берегите себя и свои волосы от любых негативных воздействий! Vikki&Lilli - заботливые резинки для волос в форме пружинки, подходят и для взрослых, и для совсем юных. Эти резинки устроены так, что не повреждают волосы, не заламывают и не рвут их, удерживают уверенно и аккуратно длинные и короткие хвостики. Они влагостойкие, с ними можно ходить в бассейн, сауну, бегать и заниматься спортом. Наши резинки отлично держат форму, не растягиваются и не рвутся. Надежные и эластичные, их можно надевать на руку как браслет, чтобы они не терялись и были под рукой! Vikki&Lilli - поддержат Вас и Ваши волосы с особенной заботой и любовью!"
                        onRemove={() => null}
                    />
                </Layout.Item>
                <Layout.Item col={1}>
                    <ContentBlock
                        title="Видео"
                        type="video"
                        onEdit={() => setIsOpenVideo(true)}
                        onRemove={() => null}
                    />
                </Layout.Item>
                <Layout.Item col={1}>
                    <ContentBlock
                        title="Изображение"
                        type="image"
                        onEdit={() => setIsOpenLoad(true)}
                        onRemove={() => null}
                    />
                </Layout.Item>
            </Layout>
            <hr css={{ margin: `${scale(3)}px 0` }} />

            <h2 css={{ ...typography('h3'), marginBottom: scale(3) }}>How to</h2>
            <Layout cols={4}>
                <Layout.Item col={2}>
                    <ContentBlock
                        title="Текст"
                        type="list"
                        textPlaceholder="Способ применения не указан"
                        text="первое|второе|третье"
                        onEdit={() => setIsOpenDescriptionHowTo(true)}
                        onRemove={() => null}
                        css={{ marginBottom: scale(2) }}
                    />
                    <ContentBlock
                        title="Инструкция"
                        css={{ marginBottom: scale(2) }}
                        onEdit={() => setIsOpenLoad(true)}
                        onRemove={() => null}
                        type="text"
                        textPlaceholder="Ничего еще не загружено"
                    >
                        <div css={{ display: 'flex', flexWrap: 'wrap', marginTop: scale(2) }}>
                            <Button size="sm" css={{ marginRight: scale(2) }} Icon={ExportIcon}>
                                Скачать
                            </Button>
                        </div>
                    </ContentBlock>
                </Layout.Item>
                <Layout.Item col={1}>
                    <ContentBlock
                        title="Видео"
                        type="video"
                        onEdit={() => setIsOpenVideo(true)}
                        onRemove={() => null}
                    />
                </Layout.Item>
                <Layout.Item col={1}>
                    <ContentBlock
                        title="Изображение"
                        type="image"
                        onEdit={() => setIsOpenLoad(true)}
                        onRemove={() => null}
                    />
                </Layout.Item>
            </Layout>
            <Popup
                isOpen={isOpenLoad}
                onRequestClose={() => setIsOpenLoad(false)}
                title="Загрузка файла"
                popupCss={{ maxWidth: 'initial', width: scale(70) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        value: '',
                    }}
                    id="file-upload"
                >
                    <input type="file" name="file" css={{ marginBottom: scale(2) }} required />
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsOpenLoad(false)}
                            css={{ marginRight: scale(2) }}
                            type="reset"
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
            <Popup
                isOpen={isOpenFeatures}
                onRequestClose={() => setIsOpenFeatures(false)}
                title="Добавление особенностей"
                popupCss={{ maxWidth: 'initial', width: scale(70) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        title: '',
                    }}
                    id="file-upload"
                >
                    <Form.Field label="Название" name="title" css={{ marginBottom: scale(2) }} />
                    <input type="file" name="file" css={{ marginBottom: scale(2) }} required />
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsOpenFeatures(false)}
                            css={{ marginRight: scale(2) }}
                            type="reset"
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
            <Popup
                isOpen={isOpenDescription}
                onRequestClose={() => setIsOpenDescription(false)}
                title="Редактирование описания товара"
                popupCss={{ maxWidth: 'initial', width: scale(70) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        descrition: '',
                    }}
                    validationSchema={Yup.object().shape({
                        descrition: Yup.string().required('Обязательное поле'),
                    })}
                >
                    <Form.Field name="descrition" css={{ marginBottom: scale(2) }}>
                        <Legend label="Текст описания" />
                        <Textarea css={{ width: '100%' }} />
                    </Form.Field>
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsOpenDescription(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
            <Popup
                isOpen={isOpenDescriptionHowTo}
                onRequestClose={() => setIsOpenDescriptionHowTo(false)}
                title="Редактирование How To товара"
                popupCss={{ maxWidth: 'initial', width: scale(70) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                    }}
                    initialValues={{
                        descrition: '',
                    }}
                    validationSchema={Yup.object().shape({
                        descrition: Yup.string().required('Обязательное поле'),
                    })}
                >
                    <Form.Field name="descrition" css={{ marginBottom: scale(2) }}>
                        <Legend
                            label="Используйте разделитель | для описания пунктов способов применения"
                            hint="Пример: Нанести|Подождать 5 минут|Смыть"
                        />
                        <Textarea css={{ width: '100%' }} />
                    </Form.Field>
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsOpenDescriptionHowTo(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
            <Popup
                isOpen={isOpenVideo}
                onRequestClose={() => setIsOpenVideo(false)}
                title="Редактирование товара"
                popupCss={{ maxWidth: 'initial', width: scale(70) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                        // используй urlParser для получения кода видео ютуб
                    }}
                    initialValues={{
                        videoUrl: '',
                    }}
                    validationSchema={Yup.object().shape({
                        videoUrl: Yup.string().required('Обязательное поле'),
                    })}
                    id="file-upload"
                >
                    <Form.Field name="videoUrl" css={{ marginBottom: scale(2) }} label="Ссылка на видео на YouTube" />
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsOpenVideo(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </>
    );
};

export default Content;
