import React, { useState } from 'react';
import * as Yup from 'yup';
import { Layout, scale, Button } from '@greensight/gds';
import typography from '@scripts/typography';
import Block from '@components/Block';
import EditIcon from '@svg/tokens/small/edit.svg';
import Popup from '@standart/Popup';
import Form from '@standart/Form';
import YesNoSwitcher from '@standart/YesNoSwitcher';
import Checkbox from '@standart/Checkbox';

const data = [
    {
        name: 'Ширина',
        value: '60 мм',
    },
    {
        name: 'Высота',
        value: '30 мм',
    },
    {
        name: 'Глубина',
        value: '125 мм',
    },
    {
        name: 'Вес',
        value: '12 гр',
    },
    {
        name: 'Кол-во дней для возврата',
        value: '4',
    },
    {
        name: 'Особая упаковка',
        value: 'нет',
    },
    {
        name: 'Особые условия хранения',
        value: 'Хрупкое',
    },
    {
        name: 'Газ',
        value: 'нет',
    },
    {
        name: 'Легковоспламеняющееся',
        value: 'нет',
    },
    {
        name: 'В составе элемент питания',
        value: 'нет',
    },
];

const StoreAndDelivery = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [specialPackChecked, setSpecialPackChecked] = useState(false);
    const [specialStoreChecked, setSpecialStoreChecked] = useState(false);

    return (
        <>
            <Layout cols={4} css={{ marginTop: scale(2) }}>
                <Layout.Item col={1}>
                    <Block>
                        <Block.Header>
                            <p css={typography('h3')}>Характеристики</p>
                            <Button Icon={EditIcon} type="button" theme="ghost" hidden onClick={() => setIsOpen(true)}>
                                редактировать
                            </Button>
                        </Block.Header>
                        <Block.Body>
                            <table width="100%">
                                <tbody>
                                    {data.map(i => (
                                        <tr key={i.name}>
                                            <th css={{ textAlign: 'right', paddingRight: scale(2), width: '60%' }}>
                                                {i.name}
                                            </th>
                                            <td css={{ textAlign: 'left', width: '40%' }}>{i.value}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </Block.Body>
                    </Block>
                </Layout.Item>
            </Layout>
            <Popup
                isOpen={isOpen}
                onRequestClose={() => setIsOpen(false)}
                title="Хранение и доставка"
                popupCss={{ maxWidth: 'initial', width: scale(70) }}
            >
                <Form
                    onSubmit={values => console.log(values)}
                    initialValues={{
                        width: '',
                        height: '',
                        depth: '',
                        weight: '',
                        specialPack: '',
                        specialStoreConditions: '',
                        fragile: false,
                        returnDays: '',
                        flammable: false,
                        hasBattery: false,
                        gas: false,
                    }}
                    validationSchema={Yup.object().shape({
                        width: Yup.number().required('Введите значение'),
                        height: Yup.number().required('Введите значение'),
                        depth: Yup.number().required('Введите значение'),
                        weight: Yup.number().required('Введите значение'),
                        specialPack: Yup.string().required(() => {
                            if (specialPackChecked) return 'Введите значение';
                        }),
                        specialStoreConditions: Yup.string().required(() => {
                            if (specialStoreChecked) return 'Введите значение';
                        }),
                    })}
                >
                    <Layout cols={2} css={{ marginBottom: scale(4) }}>
                        <Layout.Item col={1}>
                            <Form.Field name="width" label="Ширина, мм" type="number" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="height" label="Высота, мм" type="number" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="depth" label="Глубина, мм" type="number" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="weight" label="Вес, гр" type="number" />
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <hr />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Checkbox
                                name="specialPackCheckbox"
                                value="true"
                                checked={specialPackChecked}
                                onChange={() => setSpecialPackChecked(!specialPackChecked)}
                            >
                                Особая упаковка
                            </Checkbox>
                            {specialPackChecked ? <Form.Field name="specialPack" label=" " /> : null}
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Checkbox
                                value="true"
                                name="specialStoreCheckbox"
                                checked={specialStoreChecked}
                                onChange={() => setSpecialStoreChecked(!specialStoreChecked)}
                            >
                                Особые условия хранения
                            </Checkbox>
                            {specialStoreChecked ? <Form.Field name="specialStoreConditions" label=" " /> : null}
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="fragile" label="Хрупкий товар">
                                <YesNoSwitcher />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="returnDays" label="Дней на возврат" type="number" />
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <hr />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="flammable" label="Легковоспламеняющееся">
                                <YesNoSwitcher />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="hasBattery" label="В составе есть элемент питания">
                                <YesNoSwitcher />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.Field name="gas" label="Газ">
                                <YesNoSwitcher />
                            </Form.Field>
                        </Layout.Item>
                    </Layout>
                    <div css={{ display: 'flex' }}>
                        <Form.Reset
                            size="sm"
                            theme="outline"
                            onClick={() => setIsOpen(false)}
                            css={{ marginRight: scale(2) }}
                        >
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </div>
                </Form>
            </Popup>
        </>
    );
};

export default StoreAndDelivery;
