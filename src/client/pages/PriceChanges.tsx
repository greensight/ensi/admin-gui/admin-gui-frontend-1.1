import React, { useState, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { Button, scale, useTheme, Layout } from '@greensight/gds';

import Form from '@standart/Form';
import Pagination from '@standart/Pagination';
import DatepickerStyles from '@standart/Datepicker/presets';
import Datepicker from '@standart/Datepicker';
import DatepickerRange from '@standart/DatepickerRange';
import MultiSelect from '@standart/MultiSelect';
import Legend from '@standart/Legend';
import Table from '@components/Table';
import Block from '@components/Block';

import { STATUSES } from '@scripts/data/different';
import { sellers, makePrice } from '@scripts/mock';
import typography from '@scripts/typography';
import { CELL_TYPES } from '@scripts/enums';

const statuses = STATUSES.map(i => ({ label: i, value: i }));

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: CELL_TYPES.LINKED_ID }),
    },
    {
        Header: 'Продавец',
        accessor: 'seller',
    },
    {
        Header: 'Автор',
        accessor: 'author',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: CELL_TYPES.STATUS }),
    },
    {
        Header: 'Кол-во товаров',
        accessor: 'productsCount',
    },
    {
        Header: 'Дата создания',
        accessor: 'created',
        getProps: () => ({ type: CELL_TYPES.DATE }),
    },
];

const SELLERS = sellers.map(i => ({ label: i, value: i }));

const PriceChanges = () => {
    const { colors } = useTheme();
    const [moreFilters, setMoreFilters] = useState(true);
    const [startDate, setStartDate] = useState<Date | null>(null);
    const [endDate, setEndDate] = useState<Date | null>(null);
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makePrice(10), []);

    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>Заявки на изменение цен</h1>
            <DatepickerStyles />
            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        id: '',
                        status: [],
                        seller: null,
                        createDate: null,
                        createDatePeriod: [],
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Body>
                        <Layout cols={8}>
                            <Layout.Item col={2}>
                                <Form.Field name="id" label="ID" />
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="status" label="Статус">
                                    <MultiSelect options={statuses} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={3}>
                                <Form.Field name="seller" label="Продавец">
                                    <MultiSelect options={SELLERS} isMulti={false} />
                                </Form.Field>
                            </Layout.Item>
                            {moreFilters ? (
                                <>
                                    <Layout.Item col={4}>
                                        <Form.Field name="createDate">
                                            <Legend label="Дата создания (точная)" />
                                            <Datepicker
                                                selectsStart
                                                selected={startDate}
                                                startDate={startDate}
                                                endDate={endDate}
                                                maxDate={endDate}
                                                onChange={setStartDate}
                                            />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={4}>
                                        <Form.Field name="createDatePeriod">
                                            <Legend label="Дата создания (период)" />
                                            <DatepickerRange />
                                        </Form.Field>
                                    </Layout.Item>
                                </>
                            ) : null}
                        </Layout>
                    </Block.Body>
                    <Block.Footer>
                        <div css={typography('bodySm')}>
                            Найдено 135 предложений{' '}
                            <button
                                type="button"
                                css={{ color: colors?.primary, marginLeft: scale(2) }}
                                onClick={() => setMoreFilters(!moreFilters)}
                            >
                                {moreFilters ? 'Меньше' : 'Больше'} фильтров
                            </button>{' '}
                        </div>
                        <div>
                            <Form.Reset
                                size="sm"
                                theme="secondary"
                                type="button"
                                onClick={() => {
                                    console.log('hoh');
                                }}
                            >
                                Сбросить
                            </Form.Reset>
                            <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                                Применить
                            </Button>
                        </div>
                    </Block.Footer>
                </Form>
            </Block>

            <Block>
                <Block.Body>
                    <Table columns={COLUMNS} data={data} handleRowInfo={() => console.log('click')} />
                    <Pagination url={pathname} activePage={activePage} pages={1} />
                </Block.Body>
            </Block>
        </main>
    );
};

export default PriceChanges;
