import React from 'react';
import { Button, scale } from '@greensight/gds';
import { Persist } from 'formik-persist';
import { FormikValues } from 'formik';
import FilePond from '@components/FilePond';
import FilePondStyles from '@components/FilePond/FilePondStyles';
import Form from '@standart/Form';
import Select from '@standart/Select';
import Popup from '@standart/Popup';
import MultiSelect from '@standart/MultiSelect';
import { prepareForSelect } from '@scripts/helpers';
import Textarea from '@standart/Textarea';
import { CHANNELS_FOR_SELECT } from '@scripts/data/different';

const types = [
    'Вопрос (Email)',
    'Действия (Email)',
    'Претензия (Email)',
    'Маркетинг (Внутренний коммуникационный модуль)',
    'Другое (Внутренний коммуникационный модуль)',
    'Предупреждение (Внутренний коммуникационный модуль)',
];
const typesForSelect = prepareForSelect(types);

const SendMessagePopup = ({
    onSubmit,
    isOpen,
    close,
    selectedRows,
    statuses,
}: {
    onSubmit: (vals: FormikValues) => void;
    isOpen: boolean;
    close: () => void;
    selectedRows: any[];
    statuses: any[];
}) => {
    const formName = 'sendMessage';
    return (
        <Popup
            isOpen={isOpen}
            onRequestClose={close}
            title="Отправить сообщение"
            popupCss={{ minWidth: scale(60) }}
            scrollInside
        >
            <FilePondStyles />
            <Form
                onSubmit={onSubmit}
                initialValues={{
                    seller: '',
                    channel: '',
                    subject: '',
                    status: '',
                    type: '',
                    message: '',
                }}
            >
                <p css={{ marginBottom: scale(1) }}>Сообщение будет отправлено следующим продавцам:</p>
                <ol css={{ li: { listStyle: 'decimal inside' }, marginBottom: scale(2) }}>
                    {selectedRows.map(r => (
                        <li key={r.id}>{r.organizationName[0]}</li>
                    ))}
                </ol>
                <Persist name={formName} />
                <Form.Field name="seller" label="Продавец" css={{ marginBottom: scale(2) }}>
                    <MultiSelect options={prepareForSelect(['Первый', 'Второй', 'Третий'])} />
                </Form.Field>
                <Form.Field name="channel" label="Канал" css={{ marginBottom: scale(2) }}>
                    <Select items={CHANNELS_FOR_SELECT} />
                </Form.Field>
                <Form.Field name="status" label="Статус" css={{ marginBottom: scale(2) }}>
                    <Select items={statuses} />
                </Form.Field>
                <Form.Field name="type" label="Тип" css={{ marginBottom: scale(2) }}>
                    <Select items={typesForSelect} />
                </Form.Field>
                <Form.Field name="subject" label="Тема" css={{ marginBottom: scale(2) }} />
                <Form.Field name="message" label="Сообщение" css={{ marginBottom: scale(2) }}>
                    <Textarea />
                </Form.Field>
                <FilePond />
                <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Form.Reset size="sm" theme="outline" onClick={close} css={{ marginRight: scale(2) }}>
                        Отменить
                    </Form.Reset>
                    <Button type="submit" size="sm" theme="primary">
                        Создать чат
                    </Button>
                </div>
            </Form>
        </Popup>
    );
};

export default SendMessagePopup;
