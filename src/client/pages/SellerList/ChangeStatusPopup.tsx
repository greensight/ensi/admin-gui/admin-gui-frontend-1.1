import React from 'react';
import { Button, scale } from '@greensight/gds';
import { FormikValues } from 'formik';
import Form from '@standart/Form';
import Select from '@standart/Select';
import Popup from '@standart/Popup';

const ChangeStatusPopup = ({
    onSubmit,
    isOpen,
    close,
    selectedRows,
    statuses,
}: {
    onSubmit: (vals: FormikValues) => void;
    isOpen: boolean;
    close: () => void;
    selectedRows: any[];
    statuses: any[];
}) => {
    return (
        <Popup
            isOpen={isOpen}
            onRequestClose={close}
            title={`Изменить статус продавц${selectedRows.length === 1 ? 'а' : 'ов'}`}
            popupCss={{ minWidth: scale(60) }}
        >
            <Form onSubmit={onSubmit} initialValues={{ status: '' }}>
                <ol css={{ li: { listStyle: 'decimal inside' }, marginBottom: scale(2) }}>
                    {selectedRows.map(r => (
                        <li key={r.id}>{r.organizationName[0]}</li>
                    ))}
                </ol>
                <Form.Field name="status" css={{ marginBottom: scale(2) }}>
                    <Select label="Сменить статус" items={statuses} />
                </Form.Field>
                <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                    <Button type="submit" size="sm" theme="primary">
                        Сохранить
                    </Button>
                </div>
            </Form>
        </Popup>
    );
};

export default ChangeStatusPopup;
