import React from 'react';
import { Button, scale, Layout } from '@greensight/gds';
import { FormikValues } from 'formik';
import typography from '@scripts/typography';
import Block from '@components/Block';
import Form from '@standart/Form';
import MultiSelect from '@standart/MultiSelect';
import DatepickerStyles from '@standart/Datepicker/presets';
import DatepickerRange from '@standart/DatepickerRange';
import Mask from '@standart/Mask';
import { maskPhone } from '@scripts/mask';

interface SelectItem {
    label: string;
    value: string;
}

const Filter = ({
    onSubmit,
    onReset,
    className,
    ratingForSelect,
    statusForSelect,
    managersForSelect,
    initialValues,
    emptyInitialValues,
}: {
    onSubmit: (vals: FormikValues) => void;
    onReset: (vals: FormikValues) => void;
    className?: string;
    emptyInitialValues: FormikValues;
    initialValues: FormikValues;
    ratingForSelect: SelectItem[];
    statusForSelect: SelectItem[];
    managersForSelect: SelectItem[];
}) => {
    return (
        <Block className={className}>
            <Form initialValues={initialValues} onSubmit={onSubmit} onReset={onReset}>
                <Block.Body>
                    <DatepickerStyles />
                    <Layout cols={4}>
                        <Layout.Item col={2}>
                            <Form.Field name="registrationDate" label="Дата регистрации">
                                <DatepickerRange />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.FastField name="rating" label="Рейтинг">
                                <MultiSelect options={ratingForSelect} />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.FastField name="status" label="Статус">
                                <MultiSelect options={statusForSelect} />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.FastField name="id" label="ID" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.FastField name="organizationName" label="Название организации" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.FastField name="contactName" label="ФИО" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.FastField name="email" label="Email" />
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.FastField name="phone" label="Телефон">
                                <Mask mask={maskPhone} />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={1}>
                            <Form.FastField name="manager" label="Менеджер">
                                <MultiSelect options={managersForSelect} />
                            </Form.FastField>
                        </Layout.Item>
                    </Layout>
                </Block.Body>
                <Block.Footer>
                    <div css={typography('bodySm')}>Всего 135 </div>
                    <div>
                        <Form.Reset size="sm" theme="secondary" type="button" initialValues={emptyInitialValues}>
                            Сбросить
                        </Form.Reset>
                        <Button size="sm" theme="primary" css={{ marginLeft: scale(2) }} type="submit">
                            Применить
                        </Button>
                    </div>
                </Block.Footer>
            </Form>
        </Block>
    );
};

export default Filter;
