import React from 'react';
import { Button, scale, Layout } from '@greensight/gds';
import * as Yup from 'yup';
import { Persist } from 'formik-persist';
import { FormikValues } from 'formik';
import Form from '@standart/Form';
import Select from '@standart/Select';
import Popup from '@standart/Popup';
import Checkbox from '@standart/Checkbox';
import { prepareForSelect } from '@scripts/helpers';
import Password from '@standart/Password';
import Textarea from '@standart/Textarea';
import { maskPhone } from '@scripts/mask';
import Mask from '@standart/Mask';

const CreateSellerPopup = ({
    onSubmit,
    isOpen,
    close,
}: {
    onSubmit: (vals: FormikValues) => void;
    isOpen: boolean;
    close: () => void;
}) => {
    const formName = 'createSellerForm';
    return (
        <Popup
            isOpen={isOpen}
            onRequestClose={close}
            title="Создать продавца"
            popupCss={{ minWidth: scale(75) }}
            scrollInside
        >
            <Form
                onSubmit={onSubmit}
                initialValues={{
                    orgName: '',
                    inn: '',
                    kpp: '',
                    legalAddress: '',
                    actualAddress: '',
                    bancAccontNumber: '',
                    bankName: '',
                    bankBik: '',
                    bankKorr: '',
                    bankLegalAddress: '',
                    lastName: '',
                    name: '',
                    middleName: '',
                    email: '',
                    phone: '',
                    connectionWay: '',
                    password: '',
                    repeatPassword: '',
                    storageAddress: '',
                    companyWebSite: '',
                    confirmation: false,
                    productsAndBrands: '',
                }}
                validationSchema={Yup.object({
                    orgName: Yup.string().required('Обязательное поле'),
                    inn: Yup.string().required('Обязательное поле'),
                    kpp: Yup.string().required('Обязательное поле'),
                    legalAddress: Yup.string().required('Обязательное поле'),
                    actualAddress: Yup.string().required('Обязательное поле'),
                    bancAccontNumber: Yup.string().required('Обязательное поле'),
                    bankName: Yup.string().required('Обязательное поле'),
                    bankBik: Yup.string().required('Обязательное поле'),
                    bankKorr: Yup.string().required('Обязательное поле'),
                    bankLegalAddress: Yup.string().required('Обязательное поле'),
                    lastName: Yup.string().required('Обязательное поле'),
                    name: Yup.string().required('Обязательное поле'),
                    middleName: Yup.string().required('Обязательное поле'),
                    email: Yup.string().required('Обязательное поле').email('Неверный формат email'),
                    phone: Yup.string().required('Обязательное поле'),
                    connectionWay: Yup.string().required('Обязательное поле'),
                    password: Yup.string().required('Обязательное поле'),
                    repeatPassword: Yup.string()
                        .required('Обязательное поле')
                        .oneOf([Yup.ref('password'), ''], 'Пароли должны совпадать'),
                    storageAddress: Yup.string().required('Обязательное поле'),
                    companyWebSite: Yup.string().required('Обязательное поле').url('Неверный формат ссылки'),
                    productsAndBrands: Yup.string().required('Обязательное поле'),
                })}
            >
                <Persist name={formName} />
                <Layout cols={6}>
                    <Layout.Item col={6}>
                        <Form.FastField name="orgName" label="Юридическое наименование организации" />
                    </Layout.Item>
                    <Layout.Item col={3}>
                        <Form.FastField name="inn" label="ИНН" />
                    </Layout.Item>
                    <Layout.Item col={3}>
                        <Form.FastField name="kpp" label="КПП" />
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <Form.FastField name="legalAddress" label="Юридический адрес" />
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <Form.FastField name="actualAddress" label="Фактический адрес" />
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <hr />
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <Form.FastField name="bancAccontNumber" label="Номер банковского счета" type="number" min={0} />
                    </Layout.Item>
                    <Layout.Item col={3}>
                        <Form.FastField name="bankName" label="Наименование банка" />
                    </Layout.Item>
                    <Layout.Item col={3}>
                        <Form.FastField name="bankBik" label="БИК банка" />
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <Form.FastField name="bankKorr" label="Номер корреспондентского счета" />
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <Form.FastField name="bankLegalAddress" label="Юридический адрес банка" />
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <hr />
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <Form.FastField name="lastName" label="Фамилия" />
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <Form.FastField name="name" label="Имя" />
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <Form.FastField name="middleName" label="Отчество" />
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <Form.FastField name="email" label="Email" />
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <Form.FastField name="phone" label="Телефон">
                            <Mask mask={maskPhone} />
                        </Form.FastField>
                    </Layout.Item>
                    <Layout.Item col={2}>
                        <Form.FastField name="connectionWay" label="Способ связи">
                            <Select items={prepareForSelect(['Email', 'Телефон'])} />
                        </Form.FastField>
                    </Layout.Item>
                    <Layout.Item col={3}>
                        <Form.FastField name="password" label="Парлоль">
                            <Password />
                        </Form.FastField>
                    </Layout.Item>
                    <Layout.Item col={3}>
                        <Form.FastField name="repeatPassword" label="Повтор пароля">
                            <Password />
                        </Form.FastField>
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <hr />
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <Form.FastField name="storageAddress" label="Адреса складов отгрузки">
                            <Textarea />
                        </Form.FastField>
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <Form.FastField name="companyWebSite" label="Сайт компании" type="url" />
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <Form.FastField name="confirmation">
                            <Checkbox>
                                Подтверждение о возможности работы с Платформой с использованием автоматических
                                механизмов интеграции
                            </Checkbox>
                        </Form.FastField>
                    </Layout.Item>
                    <Layout.Item col={6}>
                        <Form.FastField
                            name="productsAndBrands"
                            label="Бренды и товарные категории, которыми вы торгуете"
                        >
                            <Textarea />
                        </Form.FastField>
                    </Layout.Item>
                    <Layout.Item col={6} justify="end">
                        <Form.Reset size="sm" theme="outline" onClick={close} css={{ marginRight: scale(2) }}>
                            Отменить
                        </Form.Reset>
                        <Button type="submit" size="sm" theme="primary">
                            Сохранить
                        </Button>
                    </Layout.Item>
                </Layout>
            </Form>
        </Popup>
    );
};
export default CreateSellerPopup;
