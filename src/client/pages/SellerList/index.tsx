import React, { useState, useMemo } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { Button, scale } from '@greensight/gds';
import loadable from '@loadable/component';
import PageWrapper from '@components/PageWrapper';
import Table from '@components/Table';
import Block from '@components/Block';
import { makeRandomData } from '@scripts/mock';
import Pagination from '@standart/Pagination';
import SettingsIcon from '@svg/tokens/small/settings.svg';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import useOnceChanged from '@scripts/useOnceChanged';
import PlusIcon from '@svg/plus.svg';
import { prepareForSelect } from '@scripts/helpers';
import ChangeStatusPopup from './ChangeStatusPopup';
import Filter from './Filter';
import useURLHelper from '@scripts/useURLHelper';

const CreateSellerPopup = loadable(() => import('./CreateSellerPopup'));
const SendMessagePopup = loadable(() => import('./SendMessagePopup'));

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Дата регистранции',
        accessor: 'registrationDate',
        getProps: () => ({ type: 'date' }),
    },
    {
        Header: 'Название организации',
        accessor: 'organizationName',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'ФИО контактного лица',
        accessor: 'contact',
    },
    {
        Header: 'Email',
        accessor: 'email',
    },
    {
        Header: 'Телефон',
        accessor: 'phone',
    },
    {
        Header: 'Рейтинг',
        accessor: 'rating',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Менеджер',
        accessor: 'manager',
    },
];

const rating = ['плохо', 'средне', 'нормально', 'хорошо', 'отлично'];
const ratingForSelect = prepareForSelect(rating);

const status = ['Новая', 'В процессе KYB', 'Отклонена', 'Согласование условий'];
const statusForSelect = prepareForSelect(status);

const managers = ['Василий Петрович Иванов', 'Николай Федорович Бонч-Бруевич', 'Василий Иванович Чаушеску'];
const managersForSelect = prepareForSelect(managers);

const getTableItem = (id: number) => ({
    id: id + 1,
    registrationDate: new Date(),
    organizationName: ['ООО Ромашка', 'ООО СогазРос', 'ПАО Нефтеюг'].map(i => [i, `/seller/detail/${id}`])[id],
    contact: ['Иванов Иван Иванович', 'Белозеров Роман Петрович', 'Васильев Илья Владимирович'][id],
    email: ['ivanov@test.ru', 'belozerovrp@mail.com', 'vasiliev@list.ru'][id],
    phone: ['+79996663322', '+79259392939', '+79329439412'][id],
    rating: rating[id],
    status: status[id],
    manager: managers[id],
});

const SellerList = () => {
    /** в этой странице две в одной: список продавцов и заявки на регистрацию */
    const { type } = useParams<{ type: 'registration' | 'active' }>();

    const { pathname, search } = useLocation();
    const searchParams = new URLSearchParams(search);
    const activePage = +(searchParams.get('page') || 1);

    const [open, setOpen] = useState('none');
    const close = () => setOpen('none');
    const wasCreateOpened = useOnceChanged(open === 'create');
    const wasStatusOpened = useOnceChanged(open === 'status');
    const wasMessageOpened = useOnceChanged(open === 'message');

    const data = useMemo(() => makeRandomData(3, getTableItem), []);
    const [ids, setIds, selectedRows] = useSelectedRowsData<typeof data[0]>(data);

    const emptyInitValues = {
        registrationDate: null,
        rating: '',
        status: '',
        id: '',
        organizationName: '',
        contactName: '',
        email: '',
        phone: '',
        manager: '',
    };

    const { initialValues, URLHelper } = useURLHelper(emptyInitValues);

    return (
        <PageWrapper h1={type === 'active' ? 'Список продавцов' : 'Заявки на регистрацию'}>
            <>
                <Filter
                    onSubmit={vals => {
                        URLHelper(vals);
                    }}
                    onReset={() => console.log('reset')}
                    initialValues={initialValues}
                    emptyInitialValues={emptyInitValues}
                    css={{ marginBottom: scale(3) }}
                    ratingForSelect={ratingForSelect}
                    statusForSelect={statusForSelect}
                    managersForSelect={managersForSelect}
                />

                <Block>
                    <Block.Header>
                        <div css={{ display: 'flex' }}>
                            <Button
                                theme="primary"
                                size="sm"
                                css={{ marginRight: scale(2) }}
                                onClick={() => setOpen('create')}
                                Icon={PlusIcon}
                            >
                                Создать продавца
                            </Button>
                            {ids.length !== 0 ? (
                                <>
                                    <Button
                                        theme="primary"
                                        size="sm"
                                        css={{ marginRight: scale(2) }}
                                        onClick={() => setOpen('status')}
                                    >
                                        Изменить статус
                                    </Button>
                                    <Button
                                        theme="outline"
                                        size="sm"
                                        css={{ marginRight: scale(2) }}
                                        Icon={SettingsIcon}
                                        onClick={() => setOpen('message')}
                                    >
                                        Отправить сообщение
                                    </Button>
                                </>
                            ) : null}
                        </div>
                    </Block.Header>
                    <Block.Body>
                        <Table columns={COLUMNS} data={data} onRowSelect={setIds} needSettingsColumn={false} />
                        <Pagination url={pathname} activePage={activePage} pages={7} css={{ marginTop: scale(2) }} />
                    </Block.Body>
                </Block>

                {wasStatusOpened ? (
                    <ChangeStatusPopup
                        isOpen={open === 'status'}
                        close={close}
                        onSubmit={vals => console.log(vals)}
                        selectedRows={selectedRows}
                        statuses={statusForSelect}
                    />
                ) : null}

                {wasMessageOpened ? (
                    <SendMessagePopup
                        isOpen={open === 'message'}
                        close={close}
                        onSubmit={vals => console.log(vals)}
                        selectedRows={selectedRows}
                        statuses={statusForSelect}
                    />
                ) : null}

                {wasCreateOpened ? (
                    <CreateSellerPopup isOpen={open === 'create'} close={close} onSubmit={vals => console.log(vals)} />
                ) : null}
            </>
        </PageWrapper>
    );
};

export default SellerList;
