import React, { useState, useMemo } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import Table from '@components/Table';
import Block from '@components/Block';
import MultiSelect from '@standart/MultiSelect';
import Datepicker from '@standart/Datepicker';
import DatepickerStyles from '@standart/Datepicker/presets';
import Checkbox from '@standart/Checkbox';
import Legend from '@standart/Legend';
import Form from '@standart/Form';
import Pagination from '@standart/Pagination';
import typography from '@scripts/typography';
import { PROMOUSERS, sellers, PROMOCODESTATUSES, PROMOTYPES, PROMOPARTNERS, makePromocodes } from '@scripts/mock';

import Trash from '@svg/tokens/small/trash.svg';
import Save from '@svg/tokens/small/export.svg';

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Дата создания',
        accessor: 'createDate',
    },
    {
        Header: 'Название',
        accessor: 'name',
    },
    {
        Header: 'Код',
        accessor: 'code',
    },
    {
        Header: 'Тип',
        accessor: 'type',
    },
    {
        Header: 'Период действия',
        accessor: 'activePeriod',
    },
    {
        Header: 'Спонсор',
        accessor: 'sponsor',
    },
    {
        Header: 'Пользователь',
        accessor: 'user',
    },
    {
        Header: 'Реферальный партнер',
        accessor: 'referralPartner',
    },
    {
        Header: 'Статус',
        accessor: 'status',
    },
];

const Promocodes = () => {
    const { colors } = useTheme();
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);
    const data = useMemo(() => makePromocodes(10), []);
    const [creationDateStart, setCreationDateStart] = useState<Date | null>(null);
    const [creationDateEnd, setCreationDateEnd] = useState<Date | null>(null);
    const [activePeriodStart, setActivePeriodStart] = useState<Date | null>(null);
    const [activePeriodEnd, setActivePeriodEnd] = useState<Date | null>(null);

    const [ids, setIds] = useState<number[]>([]);

    const onRowSelect = (ids: number[]) => setIds(ids);

    const statuses = PROMOCODESTATUSES.map(i => ({ label: i, value: i }));
    const sponsors = sellers.map(i => ({ label: i, value: i }));
    const users = PROMOUSERS.map(i => ({ label: i, value: i }));
    const types = PROMOTYPES.map(i => ({ label: i, value: i }));
    const referralPartners = PROMOPARTNERS.map(i => ({ label: i, value: i }));

    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>Промокоды</h1>

            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        creationDateStart: null,
                        creationDateEnd: null,
                        activePeriodStart: null,
                        activePeriodEnd: null,
                        isIndefinite: false,
                        type: [],
                        sponsor: [],
                        status: [],
                        user: [],
                        ID: '',
                        name: '',
                        code: '',
                        referralPartner: [],
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <DatepickerStyles />
                    <Block.Body>
                        <Layout cols={6} css={{ marginBottom: scale(3) }}>
                            <Layout.Item col={2}>
                                <Layout cols={2}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="creationDateStart">
                                            <Legend label="Дата создания от" />
                                            <Datepicker
                                                selectsStart
                                                selected={creationDateStart}
                                                startDate={creationDateStart}
                                                endDate={creationDateEnd}
                                                maxDate={creationDateEnd}
                                                onChange={setCreationDateStart}
                                            />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="creationDateEnd">
                                            <Legend label="Дата создания до" />
                                            <Datepicker
                                                selectsEnd
                                                selected={creationDateEnd}
                                                startDate={creationDateStart}
                                                endDate={creationDateEnd}
                                                minDate={creationDateStart}
                                                onChange={setCreationDateEnd}
                                            />
                                        </Form.Field>
                                    </Layout.Item>
                                </Layout>
                            </Layout.Item>
                            <Layout.Item col={2}>
                                <Layout cols={2}>
                                    <Layout.Item col={1}>
                                        <Form.Field name="activePeriodStart">
                                            <Legend label="Период действия от" />
                                            <Datepicker
                                                selectsStart
                                                selected={activePeriodStart}
                                                startDate={activePeriodStart}
                                                endDate={activePeriodEnd}
                                                maxDate={activePeriodEnd}
                                                onChange={setActivePeriodStart}
                                            />
                                        </Form.Field>
                                    </Layout.Item>
                                    <Layout.Item col={1}>
                                        <Form.Field name="activePeriodEnd">
                                            <Legend label="Период действия до" />
                                            <Datepicker
                                                selectsEnd
                                                selected={activePeriodEnd}
                                                startDate={activePeriodStart}
                                                endDate={activePeriodEnd}
                                                minDate={activePeriodStart}
                                                onChange={setActivePeriodEnd}
                                            />
                                        </Form.Field>
                                    </Layout.Item>
                                </Layout>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="isIndefinite" label="Бессрочный">
                                    <Checkbox value="isIndefinite">Да</Checkbox>
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="type" label="Тип">
                                    <MultiSelect isMulti={false} options={types} />
                                </Form.Field>
                            </Layout.Item>
                        </Layout>
                        <Layout cols={4}>
                            <Layout.Item col={1}>
                                <Form.Field name="sponsor" label="Спонсор">
                                    <MultiSelect isMulti={false} options={sponsors} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="status" label="Статус">
                                    <MultiSelect isMulti={false} options={statuses} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="user" label="Пользователь">
                                    <MultiSelect isMulti={false} options={users} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="ID" label="ID" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="name" label="Название" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="code" label="Код" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="referralPartner" label="Реферальный партнер">
                                    <MultiSelect isMulti={false} options={referralPartners} />
                                </Form.Field>
                            </Layout.Item>
                            <Layout.Item col={1} align="center">
                                {/* Ссылка на раздел коммуникации с темой промокоды */}
                                <p css={{ paddingTop: scale(3) }}>
                                    Запросы:{' '}
                                    <Link
                                        to="/"
                                        css={{
                                            color: colors?.primary,
                                            ':hover': { color: colors?.primaryHover },
                                        }}
                                    >
                                        0
                                    </Link>
                                </p>
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Button size="sm" theme="primary" css={{ marginRight: scale(1) }} type="submit">
                                    Применить
                                </Button>
                                <Form.Reset
                                    size="sm"
                                    theme="secondary"
                                    type="button"
                                    onClick={() => {
                                        setCreationDateStart(null);
                                        setCreationDateEnd(null);
                                        setActivePeriodStart(null);
                                        setActivePeriodEnd(null);
                                    }}
                                >
                                    Очистить
                                </Form.Reset>
                            </Layout.Item>
                        </Layout>
                    </Block.Body>
                </Form>
            </Block>

            <Block>
                <Block.Header>
                    <div css={{ display: 'flex', flexDirection: 'column' }}>
                        <Link to={`/marketing/promocode/create`}>
                            <Button
                                theme="primary"
                                size="sm"
                                css={{ marginRight: scale(2) }}
                                onClick={() => {
                                    console.log('add');
                                }}
                            >
                                Создать промокод
                            </Button>
                        </Link>
                        {ids.length !== 0 ? (
                            <Form
                                initialValues={{
                                    status: [],
                                }}
                                onSubmit={values => {
                                    console.log(values);
                                }}
                            >
                                <div css={{ marginTop: scale(2) }}>
                                    <p css={{ marginBottom: scale(2) }}>Выбрано: {ids.length}</p>
                                    <div css={{ display: 'flex' }}>
                                        <Form.Field name="status">
                                            <MultiSelect
                                                name="status"
                                                placeholder="-"
                                                isMulti={false}
                                                options={statuses}
                                                css={{ minWidth: scale(32) }}
                                            />
                                        </Form.Field>
                                        <Button
                                            css={{
                                                width: scale(10, true),
                                                height: scale(7, true),
                                                padding: '0 !important',
                                            }}
                                            theme="secondary"
                                        >
                                            <Save
                                                css={{
                                                    width: scale(2),
                                                    height: scale(2),
                                                }}
                                            />
                                        </Button>
                                        <Button
                                            css={{
                                                width: scale(10, true),
                                                height: scale(7, true),
                                                padding: '0 !important',
                                                marginLeft: scale(2),
                                            }}
                                        >
                                            <Trash
                                                css={{
                                                    width: scale(2),
                                                    height: scale(2),
                                                }}
                                            />
                                        </Button>
                                    </div>
                                </div>
                            </Form>
                        ) : null}
                    </div>
                </Block.Header>
                <Block.Body>
                    <Table
                        columns={COLUMNS}
                        data={data}
                        handleRowInfo={() => console.log('click')}
                        onRowSelect={onRowSelect}
                    />
                    <Pagination url={pathname} activePage={activePage} pages={7} />
                </Block.Body>
            </Block>
        </main>
    );
};

export default Promocodes;
