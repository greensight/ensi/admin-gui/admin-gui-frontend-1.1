import React, { useMemo, useState } from 'react';
import { scale, Button } from '@greensight/gds';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Table from '@components/Table';
import Form from '@standart/Form';
import { Flatten } from '@scripts/helpers';
import { getRandomItem } from '@scripts/mock';
import useSelectedRowsData from '@scripts/useSelectedRowsData';
import Popup from '@standart/Popup';
import PlusIcon from '@svg/plus.svg';
import TrashIcon from '@svg/tokens/small/trash.svg';
import usePopupState from '@scripts/usePopupState';
import usePrevious from '@scripts/usePrevious';
import { ActionType } from '@scripts/enums';

const columns = [
    {
        Header: 'Текст запроса',
        accessor: 'title',
    },
];

const tableItem = (id: number) => {
    return {
        id: `100${id}`,
        title: getRandomItem(['Промокод', 'Доставка', 'Новый шаблон']),
    };
};

const makeData = (len: number) => [...Array(len).keys()].map(el => tableItem(el));

const initialState = { title: '' };

type State = {
    title?: string;
    open?: boolean;
    action?: ActionType;
};

const ContentSearchRequests = () => {
    const [popupState, popupDispatch] = usePopupState<State>(initialState);
    const [isDeleteOpen, setIsDeleteOpen] = useState(false);
    const [data, setData] = useState(makeData(5));
    const [ids, setIds, selectedRows] = useSelectedRowsData<Flatten<typeof data>>(data);
    const [enabledSaveOrder, setEnabledSaveOrder] = useState(false);

    const prevData = usePrevious(data.map(d => d.id).join(''));
    React.useEffect(() => {
        if (!enabledSaveOrder && prevData && prevData !== data.map(d => d.id).join('')) setEnabledSaveOrder(true);
    }, [data, enabledSaveOrder, prevData]);

    return (
        <PageWrapper h1="Текст запроса">
            <>
                <div css={{ marginBottom: scale(2), display: 'flex', alignItems: 'center' }}>
                    {ids.length === 0 ? (
                        <Button size="sm" Icon={PlusIcon} onClick={() => popupDispatch({ type: ActionType.Add })}>
                            Добавить запрос
                        </Button>
                    ) : (
                        <Button size="sm" Icon={TrashIcon} onClick={() => setIsDeleteOpen(true)}>
                            Удалить запрос{ids.length === 1 ? '' : 'ы'}
                        </Button>
                    )}
                    {enabledSaveOrder ? (
                        <Button
                            size="sm"
                            theme="outline"
                            css={{ marginLeft: scale(2) }}
                            onClick={() => setEnabledSaveOrder(false)}
                        >
                            Сохранить порядок
                        </Button>
                    ) : null}
                </div>
                <Block>
                    <Block.Body>
                        <Table
                            data={data}
                            columns={columns}
                            onRowSelect={setIds}
                            handleRowInfo={row => {
                                popupDispatch({
                                    type: ActionType.Edit,
                                    payload: { title: row?.title },
                                });
                            }}
                            isDragDisabled={false}
                            setData={setData}
                        >
                            <colgroup>
                                <col width="5%" />
                                <col width="45%" />
                                <col width="45%" />
                            </colgroup>
                        </Table>
                    </Block.Body>
                </Block>
                <Popup
                    isOpen={Boolean(popupState.open)}
                    onRequestClose={() => popupDispatch({ type: ActionType.Close })}
                    title={`${popupState.action === 'edit' ? 'Редактирование' : 'Создание'} типа`}
                    popupCss={{ minWidth: scale(50) }}
                >
                    <Form initialValues={{ title: popupState.title }} onSubmit={vals => console.log(vals)}>
                        <Form.FastField name="title" label="Название" css={{ marginBottom: scale(2) }} />
                        <div css={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <Form.Reset size="sm" theme="fill" css={{ marginRight: scale(2) }}>
                                {popupState.action === 'edit' ? 'Сбросить' : 'Очистить'}
                            </Form.Reset>
                            <Button type="submit" size="sm">
                                Сохранить
                            </Button>
                        </div>
                    </Form>
                </Popup>
                <Popup
                    isOpen={isDeleteOpen}
                    onRequestClose={() => setIsDeleteOpen(false)}
                    title="Вы уверены, что хотите удалить следующие типы?"
                    popupCss={{ minWidth: scale(50) }}
                >
                    <ul css={{ marginBottom: scale(2) }}>
                        {selectedRows.map(r => (
                            <li key={r.id} css={{ marginBottom: scale(1, true) }}>
                                #{r.id} – {r.title}
                            </li>
                        ))}
                    </ul>
                    <Button size="sm">Удалить</Button>
                </Popup>
            </>
        </PageWrapper>
    );
};

export default ContentSearchRequests;
