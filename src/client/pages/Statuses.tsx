import React from 'react';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Table from '@components/Table';
import headerWithTooltip from '@components/Table/HeaderWithTooltip';

const columns = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: () =>
            headerWithTooltip({
                headerText: 'Описание',
                tooltipText:
                    'Все Отправления данного Заказа были переведены в статус /// или Смысл статуса если оно не зависит от Отправлений',
            }),
        accessor: 'description',
    },
];

const makeStatusRow = (id: number, title: string, description: string) => ({
    id,
    title,
    description,
});

const data = [
    makeStatusRow(1, 'Оформлен', 'Клиент оформил Заказ на сайте. Заказ отдает свой статус всем Отправлениям'),
    makeStatusRow(
        2,
        'Ожидает проверки АОЗ',
        'Заказ, оформленный Клиентом, нуждается в проверке по процедуре фрод-мониторинга'
    ),
    makeStatusRow(3, 'Проверка АОЗ', 'Заказ находится на процедуре фрод-мониторинга'),
    makeStatusRow(4, 'Ожидает подтверждения Продавцом', 'Ожидает подтверждения Продавцом'),
    makeStatusRow(5, 'В обработке', 'На комплектации'),
    makeStatusRow(6, 'Передан на доставку', 'Передано логистическому оператору'),
    makeStatusRow(
        7,
        'В процессе доставки',
        'Передано логистическому оператору, Принято логистическим оператором, Прибыло в город назначения, Принято в пункте назначения, Выдано курьеру для доставки'
    ),
    makeStatusRow(8, 'Находится в пункте выдачи', 'Находится в пункте выдачи'),
    makeStatusRow(9, 'Доставлен', 'Все Отправления данного Заказа находятся в статусе «Доставлено»'),
    makeStatusRow(10, 'Оформлен возврат', 'Возвращено'),
    makeStatusRow(
        0,
        'Предзаказ: ожидаем поступления товара',
        'Предзаказ: ожидает товара + все конечные статусы для прочих отправлений'
    ),
];

const Statuses = () => {
    return (
        <PageWrapper h1="Статусы заказа">
            <main>
                <Block>
                    <Block.Body>
                        <Table columns={columns} data={data} needCheckboxesCol={false} />
                    </Block.Body>
                </Block>
            </main>
        </PageWrapper>
    );
};

export default Statuses;
