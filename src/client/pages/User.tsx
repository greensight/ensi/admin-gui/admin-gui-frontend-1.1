import React, { useState, useMemo } from 'react';
import * as Yup from 'yup';
import { format } from 'date-fns';
import { CSSObject } from '@emotion/core';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import PageWrapper from '@components/PageWrapper';
import { makeUsers } from '@scripts/mock';
import typography from '@scripts/typography';
import Form from '@standart/Form';
import Popup from '@standart/Popup';
import Password from '@standart/Password';
import MultiSelect from '@standart/MultiSelect';
import Select, { SelectItemProps } from '@standart/Select';
import Block from '@components/Block';
import Edit from '@svg/tokens/small/edit.svg';
import Plus from '@svg/tokens/small/plus.svg';

const COLUMNS = [
    {
        Header: '№',
        accessor: 'id',
    },
    {
        Header: 'Login',
        accessor: 'login',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Система',
        accessor: 'system',
    },
    {
        Header: 'E-mail подтверждён',
        accessor: 'emailStatus',
        getProps: () => ({ type: 'status' }),
    },
];

const systems = [
    { label: 'Админка', value: 'Админка' },
    { label: 'MAS', value: 'MAS' },
    { label: 'Витрина', value: 'Витрина' },
];

const roles = [
    { label: 'Админ', value: 'Админ' },
    { label: 'Супер-админ', value: 'Супер-админ' },
    { label: 'Витринёр', value: 'Витринёр' },
];

const user = makeUsers(1)[0];

const Users = () => {
    const { colors } = useTheme();
    const { id, system, login, emailStatus, created } = user;
    const [isEditUserOpen, setIsEditUserOpen] = useState<boolean>(false);
    const [isAddUserRoleOpen, setIsAddUserRoleOpen] = useState<boolean>(false);
    const [formError, setFormError] = useState<string | null>(null);

    const h3Styles: CSSObject = {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    };
    const blockBodyStyles: CSSObject = {
        display: 'grid',
        gridTemplateColumns: `auto ${scale(6)}px`,
    };
    const dlStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'", gridColumnStart: 'span 2' };
    const dtStyles: CSSObject = {
        padding: `${scale(1, true)}px ${scale(1)}px ${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };
    const ddStyles: CSSObject = {
        padding: `${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
    };

    return (
        <PageWrapper h1="">
            <main css={{ flexGrow: 1, flexShrink: 1 }}>
                <h1 css={{ ...typography('h1'), marginBottom: scale(2), marginTop: 0 }}>
                    Пользователь № {id}&nbsp;-&nbsp;{login[0]}
                </h1>

                <Layout cols={{ xxxl: 2, sm: 1 }}>
                    <Layout.Item cols={1}>
                        <Block>
                            <Block.Body css={blockBodyStyles}>
                                <h3 css={h3Styles}>Основная информация</h3>
                                <Button
                                    Icon={Edit}
                                    type="button"
                                    theme="ghost"
                                    hidden
                                    onClick={() => {
                                        setIsEditUserOpen(true);
                                    }}
                                >
                                    редактировать
                                </Button>
                                <dl css={{ ...dlStyles, gridTemplateColumns: 'auto auto' }}>
                                    <dt css={dtStyles}>ID</dt>
                                    <dd css={ddStyles}>{id}</dd>
                                    <dt css={dtStyles}>Логин</dt>
                                    <dd css={ddStyles}>{login[0]}</dd>
                                    <dt css={dtStyles}>Система</dt>
                                    <dd css={ddStyles}>{system}</dd>
                                    <dt css={dtStyles}>E-mail подтверждён</dt>
                                    <dd css={ddStyles}>{emailStatus}</dd>
                                    <dt css={dtStyles}>Дата регистрации</dt>
                                    <dd css={ddStyles}>{format(new Date(created), 'dd.MM.yyyy')}</dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                    <Layout.Item cols={1}>
                        <Block>
                            <Block.Body css={blockBodyStyles}>
                                <h3 css={h3Styles}>Роли пользователя </h3>
                                <Button
                                    Icon={Plus}
                                    type="button"
                                    theme="ghost"
                                    hidden
                                    onClick={() => {
                                        setIsAddUserRoleOpen(true);
                                    }}
                                >
                                    редактировать
                                </Button>
                                <dl css={{ ...dlStyles, gridTemplateColumns: 'auto auto' }}>
                                    <dt css={dtStyles}>Роль -&nbsp;Админ</dt>
                                    <dd css={ddStyles}>Активна - да</dd>
                                    <dt css={dtStyles}>Роль -&nbsp;Супер-админ</dt>
                                    <dd css={ddStyles}>Активна - да</dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                </Layout>
            </main>
            <Popup
                isOpen={isEditUserOpen}
                onRequestClose={() => {
                    setIsEditUserOpen(false);
                }}
                title="Редактирование пользователя"
                popupCss={{ minWidth: scale(75) }}
            >
                <Form
                    onSubmit={values => {
                        setFormError(null);
                        if (values.userPassword !== values.userPasswordConfirm) {
                            setFormError('Пароли не совпадают');
                            return;
                        }
                        console.log(values);
                        setIsEditUserOpen(false);
                    }}
                    initialValues={{
                        userLogin: login[0],
                        userSystem: '',
                        userPassword: '',
                        userPasswordConfirm: '',
                    }}
                    validationSchema={Yup.object().shape({
                        userLogin: Yup.string().required('Введите логин'),
                        userSystem: Yup.string().required('Выберите систему'),
                        userPassword: Yup.string().min(8, 'Минимум 8 символов').required('Введите пароль'),
                        userPasswordConfirm: Yup.string()
                            .min(8, 'Минимум 8 символов')
                            .required('Введите подтверждение пароля'),
                    })}
                >
                    <Layout cols={2}>
                        <Layout.Item col={2}>
                            <Form.Field label="Логин" name="userLogin" />
                        </Layout.Item>
                        <Layout.Item col={2}>
                            <Form.Field name="userSystem" label="Система">
                                <MultiSelect options={systems} isMulti={false} />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, sm: 2 }}>
                            <Form.FastField name="userPassword" label="Пароль">
                                <Password />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, sm: 2 }}>
                            <Form.FastField name="userPasswordConfirm" label="Повтор пароля">
                                <Password />
                            </Form.FastField>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, sm: 2 }}>
                            <Button type="submit" size="sm" theme="primary">
                                Сохранить
                            </Button>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, sm: 2 }} css={{ alignSelf: 'center' }}>
                            {formError && <Form.Error error={formError} />}
                        </Layout.Item>
                    </Layout>
                </Form>
            </Popup>
            <Popup
                isOpen={isAddUserRoleOpen}
                onRequestClose={() => {
                    setIsAddUserRoleOpen(false);
                }}
                title="Добавление роли"
                popupCss={{ minWidth: scale(75) }}
            >
                <Form
                    onSubmit={values => {
                        console.log(values);
                        setIsAddUserRoleOpen(false);
                    }}
                    initialValues={{
                        userRole: '',
                    }}
                >
                    <Layout cols={2}>
                        <Layout.Item col={2}>
                            <Form.Field name="userRole" label="Роль">
                                <MultiSelect options={roles} isMulti={false} />
                            </Form.Field>
                        </Layout.Item>
                        <Layout.Item col={{ xxxl: 1, sm: 2 }}>
                            <Button type="submit" size="sm" theme="primary">
                                Сохранить
                            </Button>
                        </Layout.Item>
                    </Layout>
                </Form>
            </Popup>
        </PageWrapper>
    );
};

export default Users;
