import React, { useState } from 'react';
import * as Yup from 'yup';
import PageWrapper from '@components/PageWrapper';
import Block from '@components/Block';
import Form from '@standart/Form';
import { scale, Layout, Button } from '@greensight/gds';
import typography from '@scripts/typography';
import Select from '@standart/Select';
import Datepicker from '@standart/Datepicker';
import DatepickerStyles from '@standart/Datepicker/presets';
import Legend from '@standart/Legend';
import { prepareForSelect } from '@scripts/helpers';

const types = prepareForSelect([
    'Отчеты по заказам',
    'Отчеты по маркетингу',
    'Отчеты по продавцам',
    'Отчеты по подарочным сертификатам',
    'Отчеты по клиентам',
]);

const Reports = () => {
    const [startDate, setStartDate] = useState<Date | null>(null);
    const [endDate, setEndDate] = useState<Date | null>(null);

    return (
        <PageWrapper h1="Сформировать отчет">
            <>
                <DatepickerStyles />
                <Block>
                    <Block.Body>
                        <Form
                            initialValues={{ type: '', startDate: null, endDate: null }}
                            validationSchema={Yup.object().shape({
                                type: Yup.string().required('Обязательное поле'),
                                startDate: Yup.date().nullable().required('Обязательное поле'),
                                endDate: Yup.date().nullable().required('Обязательное поле'),
                            })}
                            onSubmit={val => console.log(val)}
                        >
                            <Layout cols={2} css={{ maxWidth: '50%', marginBottom: scale(3) }}>
                                <Layout.Item col={2}>
                                    <Form.Field name="type" label="Тип отчета">
                                        <Select items={types} placeholder="" />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.Field name="startDate">
                                        <Legend label="Период от" />
                                        <Datepicker
                                            selectsStart
                                            selected={startDate}
                                            startDate={startDate}
                                            endDate={endDate}
                                            maxDate={endDate}
                                            onChange={setStartDate}
                                        />
                                    </Form.Field>
                                </Layout.Item>
                                <Layout.Item col={1}>
                                    <Form.Field name="endDate">
                                        <Legend label="Период до" />
                                        <Datepicker
                                            selectsEnd
                                            selected={endDate}
                                            startDate={startDate}
                                            endDate={endDate}
                                            minDate={startDate}
                                            onChange={setEndDate}
                                        />
                                    </Form.Field>
                                </Layout.Item>
                            </Layout>
                            <Form.Reset
                                size="sm"
                                theme="outline"
                                css={{ marginRight: scale(2) }}
                                onClick={() => {
                                    setStartDate(null);
                                    setEndDate(null);
                                }}
                            >
                                Сбросить
                            </Form.Reset>
                            <Button size="sm" type="submit">
                                Сформировать
                            </Button>
                        </Form>
                    </Block.Body>
                </Block>
            </>
        </PageWrapper>
    );
};

export default Reports;
