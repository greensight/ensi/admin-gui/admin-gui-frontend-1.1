import React, { useMemo } from 'react';
import { useParams } from 'react-router-dom';
import { CSSObject } from '@emotion/core';
import { scale, useTheme, Layout } from '@greensight/gds';
import Table from '@components/Table';
import Block from '@components/Block';
import Badge from '@components/Badge';
import typography from '@scripts/typography';

const columns = [
    {
        Header: 'ID',
        accessor: 'id',
    },
    {
        Header: 'Название',
        accessor: 'name',
        getProps: () => ({ type: 'link' }),
    },
    {
        Header: 'Артикул',
        accessor: 'article',
    },
    {
        Header: 'Статус согласования',
        accessor: 'status',
    },
    {
        Header: 'Комментарий по статусу согласования',
        accessor: 'comment',
    },
];

const data = [
    {
        id: 123456,
        name: ['Стайлер для волос', '/product/1'],
        article: '11328470',
        status: 'Согласовано',
        comment: '',
    },
    {
        id: 1234235,
        name: ['Стайлер для волос', '/product/1'],
        article: '11328470',
        status: 'Согласовано',
        comment: 'комментарий',
    },
];

const RequestCheck = () => {
    const { requestId } = useParams<{ requestId: string }>();
    const { colors } = useTheme();

    const dlStyles: CSSObject = { display: 'grid', gridTemplateColumns: "'1fr 1fr'" };
    const dtStyles: CSSObject = {
        padding: `${scale(1, true)}px ${scale(1)}px ${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ...typography('bodySmBold'),
        ':last-of-type': { border: 'none' },
    };
    const ddStyles: CSSObject = {
        padding: `${scale(1, true)}px 0`,
        borderBottom: `1px solid ${colors?.grey200}`,
        ':last-of-type': { border: 'none' },
    };

    const tableData = useMemo(() => data, []);
    const tableColumns = useMemo(() => columns, []);
    return (
        <>
            <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
                <Layout cols={5} css={{ marginBottom: scale(3) }}>
                    <Layout.Item col={5}>
                        <Block css={{ marginBottom: scale(3) }}>
                            <Block.Header>
                                <h1 css={{ ...typography('h2'), margin: 0 }}>
                                    Заявка #{requestId} от 2020-12-07 10:55:27
                                </h1>
                            </Block.Header>
                            <Block.Body>
                                <dl css={{ ...dlStyles, gridTemplateColumns: '150px 1fr' }}>
                                    <dt css={dtStyles}>Статус</dt>
                                    <dd css={ddStyles}>
                                        <Badge text="В процессе" />
                                    </dd>
                                    <dt css={dtStyles}>Продавец</dt>
                                    <dd css={ddStyles}>1234567891</dd>
                                    <dt css={dtStyles}>Автор</dt>
                                    <dd css={ddStyles}>Столяров Иван Иванович</dd>
                                    <dt css={dtStyles}>Количество</dt>
                                    <dd css={ddStyles}>2 ед. товара</dd>
                                </dl>
                            </Block.Body>
                        </Block>
                    </Layout.Item>
                </Layout>

                <Block>
                    <Block.Header>
                        <h3>Товары</h3>
                    </Block.Header>
                    <Block.Body>
                        <Table
                            columns={tableColumns}
                            data={tableData}
                            needCheckboxesCol={false}
                            needSettingsColumn={false}
                        />
                    </Block.Body>
                </Block>
            </main>
        </>
    );
};

export default RequestCheck;
