import React, { useState, useMemo } from 'react';
import Block from '@components/Block';
import Form from '@standart/Form';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';

const OrganizationCard = () => {
    const { colors } = useTheme();

    return (
        <main css={{ flexGrow: 1, flexShrink: 1, padding: `${scale(2)}px ${scale(3)}px` }}>
            <h1 css={{ ...typography('h1'), marginBottom: scale(2) }}>Карточка организации ENSI</h1>

            <Block css={{ marginBottom: scale(2) }}>
                <Form
                    initialValues={{
                        orgNameShort: '',
                        orgNameFull: '',
                        orgInn: '',
                        orgKpp: '',
                        orgOkpo: '',
                        orgOgrn: '',
                        orgFactAddress: '',
                        orgLegalAddress: '',
                        orgBankAcc: '',
                        orgBankBic: '',
                        orgBankName: '',
                        orgBankKorr: '',
                        orgChiefSurname: '',
                        orgChiefName: '',
                        orgChiefFathername: '',
                        orgChiefStatute: '',
                        orgLogistSurname: '',
                        orgLogistName: '',
                        orgLogistFathername: '',
                        orgLogistPhone: '',
                        orgLogistEmail: '',
                        orgCallCenter: '',
                        orgMobileMessanger: '',
                        orgEmailSellers: '',
                        orgEmailCommon: '',
                        orgEmailRequest: '',
                    }}
                    onSubmit={values => {
                        console.log(values);
                    }}
                >
                    <Block.Body>
                        <Layout cols={12}>
                            <Layout.Item col={12}>
                                <h3 css={{ ...typography('h3') }}>Сведения об организации</h3>
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Form.Field name="orgNameShort" label="Краткое наименование организации" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Form.Field name="orgNameFull" label="Полное наименование организации" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <hr />
                            </Layout.Item>
                            <Layout.Item col={6}>
                                <Form.Field name="orgInn" label="ИНН" />
                            </Layout.Item>
                            <Layout.Item col={6}>
                                <Form.Field name="orgKpp" label="КПП" />
                            </Layout.Item>
                            <Layout.Item col={6}>
                                <Form.Field name="orgOkpo" label="ОКПО" />
                            </Layout.Item>
                            <Layout.Item col={6}>
                                <Form.Field name="orgOgrn" label="ОГРН" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <hr />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Form.Field name="orgFactAddress" label="Фактический адрес" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Form.Field name="orgLegalAddress" label="Юридический адрес" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <hr />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Form.Field name="orgBankAcc" label="Номер банковского счета" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="orgBankBic" label="БИК банка" />
                            </Layout.Item>
                            <Layout.Item col={8}>
                                <Form.Field name="orgBankName" label="Наименование банка" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Form.Field name="orgBankKorr" label="Номер корреспондентского счета" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <hr />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <h3 css={{ ...typography('h3') }}>Сведения о генеральном директоре</h3>
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="orgChiefSurname" label="Фамилия" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="orgChiefName" label="Имя" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="orgChiefFathername" label="Отчество" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Form.Field name="orgChiefStatute" label="Номер документа, подтверждающий полномочия" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <hr />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <h3 css={{ ...typography('h3') }}>Сведения о менеджере по логистике</h3>
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="orgLogistSurname" label="Фамилия" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="orgLogistName" label="Имя" />
                            </Layout.Item>
                            <Layout.Item col={4}>
                                <Form.Field name="orgLogistFathername" label="Отчество" />
                            </Layout.Item>
                            <Layout.Item col={6}>
                                <Form.Field name="orgLogistPhone" label="Контактный телефон" />
                            </Layout.Item>
                            <Layout.Item col={6}>
                                <Form.Field name="orgLogistEmail" label="Контактный e-mail" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <hr />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <h3 css={{ ...typography('h3') }}>Контактная информация</h3>
                            </Layout.Item>
                            <Layout.Item col={6}>
                                <Form.Field name="orgCallCenter" label="Телефон контактного-центра" />
                            </Layout.Item>
                            <Layout.Item col={6}>
                                <Form.Field name="orgMobileMessanger" label="Мобильный телефон для мессенджеров" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Form.Field name="orgEmailSellers" label="E-mail для продавцов" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Form.Field name="orgEmailCommon" label="Общий e-mail" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Form.Field name="orgEmailRequest" label="E-mail для заявок" />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <hr />
                            </Layout.Item>
                            <Layout.Item col={12}>
                                <Button type="submit">Сохранить</Button>
                            </Layout.Item>
                        </Layout>
                    </Block.Body>
                </Form>
            </Block>
        </main>
    );
};

export default OrganizationCard;
