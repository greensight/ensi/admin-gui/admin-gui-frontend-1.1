import React, { useMemo } from 'react';
import { Button, scale, useTheme, Layout } from '@greensight/gds';
import typography from '@scripts/typography';
import Table from '@components/Table';
import Block from '@components/Block';
import PageWrapper from '@components/PageWrapper';
import Form from '@standart/Form';
import MultiSelect from '@standart/MultiSelect';
import Pagination from '@standart/Pagination';
import { DELIVERY_SERVICE_STATUSES } from '@scripts/data/different';
import { makeDeliveryServices } from '@scripts/mock';
import { useLocation } from 'react-router-dom';

const statuses = DELIVERY_SERVICE_STATUSES.map(i => ({ label: i, value: i }));

const COLUMNS = [
    {
        Header: 'ID',
        accessor: 'id',
        getProps: () => ({ type: 'linkedID' }),
    },
    {
        Header: 'Название',
        accessor: 'title',
    },
    {
        Header: 'Статус',
        accessor: 'status',
        getProps: () => ({ type: 'status' }),
    },
    {
        Header: 'Приоритет',
        accessor: 'priority',
    },
];

const DeliveryServices = () => {
    const { colors } = useTheme();
    const data = useMemo(() => makeDeliveryServices(10), []);
    const { pathname, search } = useLocation();
    const activePage = +(new URLSearchParams(search).get('page') || 1);

    return (
        <PageWrapper h1="Логистические операторы">
            <Block css={{ marginBottom: scale(3) }}>
                <Form
                    initialValues={{
                        id: '',
                        name: '',
                        status: [],
                    }}
                    onSubmit={values => undefined}
                >
                    <Block.Body>
                        <Layout cols={3}>
                            <Layout.Item col={1}>
                                <Form.Field name="id" label="ID" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="name" label="Название" />
                            </Layout.Item>
                            <Layout.Item col={1}>
                                <Form.Field name="status" label="Статус">
                                    <MultiSelect options={statuses} />
                                </Form.Field>
                            </Layout.Item>
                        </Layout>
                    </Block.Body>
                    <Block.Footer>
                        <div>
                            <Button size="sm" theme="primary" type="submit">
                                Применить
                            </Button>
                            <Form.Reset size="sm" css={{ marginLeft: scale(2) }} theme="secondary" type="button">
                                Сбросить
                            </Form.Reset>
                        </div>
                    </Block.Footer>
                </Form>
            </Block>
            <Block>
                <Block.Body>
                    <Table columns={COLUMNS} data={data} handleRowInfo={() => undefined} />
                    <Pagination url={pathname} activePage={activePage} pages={7} />
                </Block.Body>
            </Block>
        </PageWrapper>
    );
};

export default DeliveryServices;
