#!groovy
// -*- coding: utf-8; mode: Groovy; -*-

// ===========================================================================//
// Параметры, которые надо переопределить при создании пайплайна в другом окружении

// Репозитории
def gitlabLoginPasswordCredentials = 'gitlab-credentials'

// Параметры сборки
def gitlabTokenCredentials = 'gitlab-token'

def registryName = 'dockerhub.greensight.ru'
def imageName = 'ensi-tech/admin-front'

// ===========================================================================//


properties([
    gitLabConnection('public-gitlab'),
    buildDiscarder(logRotator (artifactDaysToKeepStr: '', artifactNumToKeepStr: '1', daysToKeepStr: '', numToKeepStr: '5')),
    disableConcurrentBuilds(),
])


def fullImageName = "${registryName}/${imageName}"
def dockerNameWithTag = "${imageName}:${env.BRANCH_NAME}"

node('docker-agent'){
    gitlabCommitStatus {
        stage('Checkout') {
            cleanWs()
            checkout scm
        }

        stage ('Check helm') {
            docker.image("dockerhub.greensight.ru/ensi-tech/php-base-image:8.0-task-70144-2021june4-2-ci").inside('--entrypoint=""') {
                sh """
                    helm lint .helm
                    helm template .helm > /tmp/k8s.yaml
                    kubeval /tmp/k8s.yaml
                    kube-score score /tmp/k8s.yaml -o ci \
                        --ignore-test=container-resources \
                        --ignore-test=container-image-tag \
                        --ignore-test=container-image-pull-policy \
                        --ignore-test=container-security-context \
                        --ignore-test=pod-networkpolicy \
                        --ignore-test=pod-probes
                    rm /tmp/k8s.yaml
                """
            }
        }

        stage ('Check app') {
            docker.image("dockerhub.greensight.ru/ensi-tech/node-base-image:12-2021apr23-1").inside('--entrypoint=""') {
                sh """
                    yarn install
                    yarn eslint
                    yarn tsc
                """
            }
        }
    }
}
